package com.auxolabs.capture.positional;

import com.auxolabs.capture.positional.constants.PositionalRuleConstants;
import com.auxolabs.capture.positional.models.BooleanOpResultModel;
import com.auxolabs.capture.positional.models.RuleOpModel;
import com.auxolabs.capture.positional.models.TokenModel;
import com.auxolabs.capture.positional.operations.BaseFunction;
import com.auxolabs.capture.positional.operations.BooleanBaseOperation;
import com.auxolabs.capture.positional.parsers.RuleParserUtil;
import com.concordfax.capture.core.models.extraction.positional.trigger.PositionalExtractorTriggerConfiguration;
import org.junit.Test;
import org.yaml.snakeyaml.Yaml;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class PositionalRuleTester {

    @Test
    public void testBasicRule() {
        Yaml yaml = new Yaml();
        PositionalExtractorTriggerConfiguration positionalCollectionModel = yaml.loadAs(this.getClass().getClassLoader().getResourceAsStream("com/auxolabs/capture/positional/test_positional_rules.yaml"), PositionalExtractorTriggerConfiguration.class);
        List<RuleOpModel> ruleOpModels = new ArrayList<>();
        HashMap<String, BaseFunction> functions = new HashMap<>();
        positionalCollectionModel.getFunctions().forEach(functionModel -> {
            RuleParserUtil.parseAndAddFunction(functions, functionModel.getFunction());
        });
        positionalCollectionModel.getRules().forEach(ruleModel -> {
            RuleOpModel ruleOpModel = new RuleOpModel();
            ruleOpModel.setRuleModel(ruleModel);
            ruleOpModel.setOp(RuleParserUtil.parseAndGetRule(functions, ruleModel.getRule()));
            ruleOpModels.add(ruleOpModel);
        });

        TokenModel tagPatientNameToken = new TokenModel();
        tagPatientNameToken.put(PositionalRuleConstants.Properties.X, 600.0);
        tagPatientNameToken.put(PositionalRuleConstants.Properties.Y, 500.0);
        tagPatientNameToken.put(PositionalRuleConstants.Properties.W, 100.0);
        tagPatientNameToken.put(PositionalRuleConstants.Properties.H, 30.0);
        tagPatientNameToken.put(PositionalRuleConstants.Properties.ID, 1234);
        tagPatientNameToken.put(PositionalRuleConstants.Properties.TEXT, "Patient Name: ");
        HashSet<TokenModel> tokenModels1 = new HashSet<>();tokenModels1.add(tagPatientNameToken);
        TokenModel patientNameToken = new TokenModel();
        patientNameToken.put(PositionalRuleConstants.Properties.X, 500.0);
        patientNameToken.put(PositionalRuleConstants.Properties.Y, 515.0);
        patientNameToken.put(PositionalRuleConstants.Properties.W, 50.0);
        patientNameToken.put(PositionalRuleConstants.Properties.H, 30.0);
        patientNameToken.put(PositionalRuleConstants.Properties.ID, 2345);
        patientNameToken.put(PositionalRuleConstants.Properties.TEXT, "John Smith");
        HashSet<TokenModel> tokenModels2 = new HashSet<>();
        tokenModels1.add(patientNameToken);
        tokenModels2.add(patientNameToken);
        HashMap<String, HashSet<TokenModel>> tokens = new HashMap<>();
        tokens.put("tag_name",tokenModels1);
        tokens.put("person_name",tokenModels2);
        HashSet<BooleanOpResultModel> booleanOpResultModels = null;
        for (int i = 0; i < ruleOpModels.size(); i++) {
            if(ruleOpModels.get(i).getRuleModel().getRuleName().equals("patient name identifier - roi")){
                BooleanBaseOperation bbop = (BooleanBaseOperation) ruleOpModels.get(i).getOp();
                booleanOpResultModels =  bbop.compute(tokens);
                break;
            }
        }
        assert booleanOpResultModels!=null && booleanOpResultModels.size() == 1;
    }
}
