package com.auxolabs.capture.positional.operations;

import java.util.HashMap;

public interface BaseOperation {
    void updateParams(HashMap<String, String> newParams);
    BaseOperation clone();
}
