package com.auxolabs.capture.positional.utils.lexer.indexer.index;

import com.auxolabs.capture.positional.utils.lexer.indexer.types.StateType;
import com.auxolabs.capture.positional.utils.lexer.indexer.types.TagModel;
import com.auxolabs.capture.positional.utils.lexer.indexer.types.TokenSet;
import com.auxolabs.capture.positional.utils.lexer.utils.Tokenizer;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableInt;
import scala.Tuple2;

import java.io.*;
import java.util.*;

public class StateMachine {

    private final String tailSequences[] = {"s"};
    //"ed", "er", "ing", "est", "'s", "n", "s"};

    private State root;
    private long trieNumberOfWords;
    private Stack<Object> relationStack;
    private Tokenizer tokenizer;

    public StateMachine() {
        root = new State();
        root.setStateType(StateType.NON_LEAF);
        trieNumberOfWords = 0;
        tokenizer = new Tokenizer();
    }

    public static void main(String[] args) {
        StateMachine sm = new StateMachine();
        TagModel tagModel = new TagModel("Test");
        sm.insertIndex("burdened amount", tagModel, false);
        sm.insertIndex("burdened", new TagModel("Test2"), false);
        sm.insertIndex("amount", new TagModel("Test1"), false);

        List<Annotation1> annotations = sm.searchWord(" get all the budget with burdened amount between 500 and 1000", 2, true);
        System.out.println(annotations);
        System.out.println("Completed");
    }

    public boolean insertIndex(String indexEntry, TagModel tag, boolean isBrand) {
        if (indexEntry == null || indexEntry.length() == 0) {
            return false;
        }

        indexEntry = indexEntry.toLowerCase();
        State node = root;
        String token;
        boolean isLastToken;
        int tokenLength;
        TokenSet tokenSet = tokenizer.tokenize(indexEntry);
        String tokens[] = tokenizer.getAddToLexerEffectiveTokens(tokenSet.getTokens());

        for (int tokenListHandle = 0; tokenListHandle < tokens.length; tokenListHandle++) {
            token = tokens[tokenListHandle];
            trieNumberOfWords++;
            tokenLength = token.length();
            isLastToken = (tokenListHandle == tokens.length - 1);

            //Loop over each token; if last letter of the token and last word of phrase then LEAF_TERMINATING
            //else if last letter of token and not last word of phrase then LEAF_CONTINUED
            //else NON_LEAF
            for (short handle = 0; handle < tokenLength; handle++) {
                node = node.addTransitionPathFromState(token.charAt(handle), (handle == tokenLength - 1 ?
                        (isLastToken ? StateType.LEAF_TERMINATING : StateType.LEAF_CONTINUED)
                        : StateType.NON_LEAF));
            }
            if (!isLastToken) {
                node = node.addTransitionPathFromState(' ', StateType.NON_LEAF);
            }
        }
        addTagToNode(node, tag, isBrand);
        return true;
    }

    public boolean deleteWord(String indexEntry, TagModel tag) {
        if (indexEntry == null || indexEntry.length() == 0) {
            return false;
        }
        State node = root;
        boolean isLastToken;
        int tokenLength;
        indexEntry = indexEntry.toLowerCase();
        TokenSet tokenSet = tokenizer.tokenize(indexEntry);
        String tokens[] = tokenizer.getAddToLexerEffectiveTokens(tokenSet.getTokens());

        for (int tokenListHandle = 0; tokenListHandle < tokens.length; tokenListHandle++) {
            String token = tokens[tokenListHandle];
            tokenLength = token.length();
            isLastToken = (tokenListHandle == tokens.length - 1);
            for (short handle = 0; handle < tokenLength; handle++) {
                node = node.followTransitionPathFromState(token.charAt(handle));
                if (node == null) {
                    return false;
                }
            }
            if (!isLastToken) {
                node = node.followTransitionPathFromState(' ');
                if (node == null) {
                    return false;
                }
            }
        }

        Tags tags = node.getTags();
        if (tags != null) {
            if (!tags.contains(tag)) {
                return false;
            }
            tags.remove(tag);
            if (tags.size() < 1) {
                node.setTags(null);
            } else if (tags.size() >= 1) {
                node.setTags(tags);
            }
        } else {
            return false;
        }
        StateType state = node.getStateType();
        if (state.equals(StateType.LEAF_TERMINATING)) {
            if (tags == null || tags.size() <= 0) {
                TreeMap<Character, State> transitionStates = node.getTransitionStates();
                if (transitionStates == null || transitionStates.keySet().size() <= 0) {
                    node.setStateType(StateType.DELETED);
                } else {
                    node.setStateType(StateType.NON_LEAF);
                }
            }
        } else if (state.equals(StateType.AMBIGUOUS)) {
            if (tags == null || tags.size() <= 0) {
                node.setStateType(StateType.LEAF_CONTINUED);
            }
        } else {
            return false;
        }
        return true;
    }

    public ArrayList<Annotation1> searchWord(String text, int tolerance, boolean longestMatch) {
        MutableInt lastEmittedTokenOffset = new MutableInt(0);
        ArrayList<Annotation1> annotations = new ArrayList<Annotation1>();
        ArrayList<Annotation1> ambiguousAnnotations = new ArrayList<>();

        if (text == null) {
            return annotations;
        }

        StringBuffer matchBuffer = new StringBuffer();
        Queue<String> tokenBuffer = new LinkedList<String>();
        TokenSet tokenSet = tokenizer.tokenize(text);
        String tokens[] = tokenSet.getTokens();
        String originalTokens[] = tokenSet.getOriginalTokens();
        List<Tuple2<Integer, Integer>> tokenOffsets = tokenSet.getTokenOffsets();
        List<String> sentenceTokens = Arrays.asList(tokens);

        State prevNode = root, currentNode = null;
        String token;
        String casedToken;
        String originalToken;
        int tokenLength;
        boolean shouldBreak;
        Character currentChar;

        for (int tokenListHandle = 0; tokenListHandle < tokens.length; tokenListHandle++) {
            shouldBreak = false;
            token = tokenizer.getLexerAcceptedString(tokens[tokenListHandle]);
            originalToken = originalTokens[tokenListHandle];
            tokenLength = token.length();
            if (tokenLength == 0) {
                if (tokenBuffer.size() > 0)
                    tokenBuffer.add(originalToken);
                continue;
            }

            casedToken = token;
            token = token.toLowerCase();
            short handle;
            for (handle = 0; handle < tokenLength && !shouldBreak; handle++) {
                currentChar = token.charAt(handle);
                currentNode = prevNode.followTransitionPathFromState(currentChar);

                if (currentNode == null || currentNode.getStateType().equals(StateType.DELETED)) { //unable to find a path in the tree and no path through tailsequences
                    if (tokenLength - handle > tolerance || !checkIfTokenEndsWithTailSequence(token, handle)) {
                        //no index path and we have more characters to match that tolerance limit
                        //reset handle to the initial token that caused this sequence of matches;
                        // the outer loop will then change the handle to point to the next token
                        tokenListHandle -= (tokenBuffer.size());
                        clearMatchAndTokenBuffer(matchBuffer, tokenBuffer);
                        annotations.addAll(ambiguousAnnotations);
                        ambiguousAnnotations.clear();
                        prevNode = root;
                        //Move to the outer loop as we have found a case that cannot be tolerated
                        break;
                    }
                    //Within tolerance limit
                    switch (prevNode.getStateType()) {
                        //Ambiguous node => emit and then look ahead for a larger sequence
                        case AMBIGUOUS:
                            addToBufferAndEmitToken(tokenBuffer, originalToken, prevNode, annotations, tokenListHandle,
                                    sentenceTokens, tokenOffsets, ambiguousAnnotations, longestMatch, true, lastEmittedTokenOffset);
                            prevNode = prevNode.followTransitionPathFromState(' ');
                            break;
                        //Terminating node => emit,
                        //reset back to token succeeding the token that initiated the sequence of matches
                        //exit inner loop
                        case LEAF_TERMINATING:
                            addToBufferAndEmitToken(tokenBuffer, originalToken, prevNode, annotations, tokenListHandle,
                                    sentenceTokens, tokenOffsets, ambiguousAnnotations, longestMatch, false, lastEmittedTokenOffset);
                            prevNode = root;
                            tokenListHandle -= (tokenBuffer.size() - 1);
                            clearMatchAndTokenBuffer(matchBuffer, tokenBuffer);
                            shouldBreak = true;
                            break;
                        //continue => add to buffer, look ahead for larger sequence
                        //exit inner loop
                        case LEAF_CONTINUED:
                            addToBuffer(tokenBuffer, originalToken, prevNode);
                            prevNode = prevNode.followTransitionPathFromState(' ');
                            shouldBreak = true;
                            break;
                        //non leaf => check if one of the tail sequences lead us to a result
                        case NON_LEAF:
                            State nearNode;
                            for (String tailSequence : tailSequences) {
                                nearNode = getAlternateTenseNode(tailSequence, prevNode);
                                if (nearNode == null) {
                                    nearNode = getAlternateTenseNode(
                                            matchBuffer.substring(matchBuffer.length() - 1) + tailSequence, prevNode);
                                    if (nearNode != null) {
                                        shouldBreak = true;
                                        switch (nearNode.getStateType()) {
                                            case AMBIGUOUS:
                                                addToBufferAndEmitToken(tokenBuffer, originalToken, nearNode, annotations,
                                                        tokenListHandle, sentenceTokens, tokenOffsets,
                                                        ambiguousAnnotations, longestMatch, true, lastEmittedTokenOffset);
                                                prevNode = prevNode.followTransitionPathFromState(' ');
                                                break;
                                            case LEAF_TERMINATING:
                                                addToBufferAndEmitToken(tokenBuffer, originalToken, nearNode, annotations,
                                                        tokenListHandle, sentenceTokens, tokenOffsets,
                                                        ambiguousAnnotations, longestMatch, false, lastEmittedTokenOffset);
                                                prevNode = root;
                                                tokenListHandle -= (tokenBuffer.size() - 1);
                                                clearMatchAndTokenBuffer(matchBuffer, tokenBuffer);
                                                break;
                                            case LEAF_CONTINUED:
                                                addToBuffer(tokenBuffer, originalToken, nearNode);
                                                prevNode = prevNode.followTransitionPathFromState(' ');
                                                break;
                                        }

                                    }
                                } else {
                                    shouldBreak = true;
                                    switch (nearNode.getStateType()) {
                                        case AMBIGUOUS:
                                            addToBufferAndEmitToken(tokenBuffer, originalToken, nearNode, annotations,
                                                    tokenListHandle, sentenceTokens, tokenOffsets,
                                                    ambiguousAnnotations, longestMatch, true, lastEmittedTokenOffset);
                                            prevNode = prevNode.followTransitionPathFromState(' ');
                                            break;
                                        case LEAF_TERMINATING:
                                            addToBufferAndEmitToken(tokenBuffer, originalToken, nearNode, annotations,
                                                    tokenListHandle, sentenceTokens, tokenOffsets,
                                                    ambiguousAnnotations, longestMatch, false, lastEmittedTokenOffset);
                                            prevNode = root;
                                            tokenListHandle -= (tokenBuffer.size() - 1);
                                            clearMatchAndTokenBuffer(matchBuffer, tokenBuffer);
                                            break;
                                        case LEAF_CONTINUED:
                                            addToBuffer(tokenBuffer, originalToken, nearNode);
                                            prevNode = prevNode.followTransitionPathFromState(' ');
                                            break;
                                    }
                                }
                                //a tail sequence led to a result
                                if (shouldBreak) {
                                    tokenListHandle -= (tokenBuffer.size() - 1);
                                    annotations.addAll(ambiguousAnnotations);
                                    ambiguousAnnotations.clear();
                                    clearMatchAndTokenBuffer(matchBuffer, tokenBuffer);
                                    prevNode = root;
                                    break;
                                }
                            }
                            //a tail sequence did not lead to a result
                            if (!shouldBreak) {
                                if (tokenBuffer.size() > 0) {
                                    tokenListHandle -= (tokenBuffer.size() - 1);
                                }
                                annotations.addAll(ambiguousAnnotations);
                                ambiguousAnnotations.clear();
                                clearMatchAndTokenBuffer(matchBuffer, tokenBuffer);
                                shouldBreak = true;
                                prevNode = root;
                            }
                            break;

                    }
                } else { //index path not empty
                    matchBuffer.append(token.charAt(handle)); //keep adding to matchBuffer
                    prevNode = currentNode;
                }
            } //End of per character loop

            if (currentNode != null) { //index path not empty but matchBuffer is burned out
                prevNode = root;
                if (tokenLength - handle > tolerance) {
                    tokenListHandle -= (tokenBuffer.size());
                    annotations.addAll(ambiguousAnnotations);
                    ambiguousAnnotations.clear();
                    clearMatchAndTokenBuffer(matchBuffer, tokenBuffer);
                    break;
                }
                switch (currentNode.getStateType()) {
                    case NON_LEAF:
                        for (String tailSequence : tailSequences) {
                            State alternateTenseNode = getAlternateTenseNode(tailSequence, currentNode);
                            if (alternateTenseNode != null) {
                                shouldBreak = true;
                                switch (alternateTenseNode.getStateType()) {
                                    case LEAF_CONTINUED:
                                        addToBuffer(tokenBuffer, originalToken, alternateTenseNode);
                                        prevNode = alternateTenseNode.followTransitionPathFromState(' ');
                                        break;
                                    case LEAF_TERMINATING:
                                        addToBufferAndEmitToken(tokenBuffer, originalToken, alternateTenseNode, annotations,
                                                tokenListHandle, sentenceTokens, tokenOffsets,
                                                ambiguousAnnotations, longestMatch, false, lastEmittedTokenOffset);
                                        tokenListHandle -= (tokenBuffer.size() - 1);
                                        clearMatchAndTokenBuffer(matchBuffer, tokenBuffer);
                                        break;
                                    case AMBIGUOUS:
                                        addToBufferAndEmitToken(tokenBuffer, originalToken, alternateTenseNode, annotations,
                                                tokenListHandle, sentenceTokens, tokenOffsets,
                                                ambiguousAnnotations, longestMatch, true, lastEmittedTokenOffset);
                                        break;
                                }
                                break;

                            } else {
                                alternateTenseNode = getAlternateTenseNode(
                                        matchBuffer.substring(matchBuffer.length() - 1) + tailSequence, currentNode);
                                if (alternateTenseNode != null) {
                                    shouldBreak = true;
                                    switch (alternateTenseNode.getStateType()) {
                                        case LEAF_CONTINUED:
                                            addToBuffer(tokenBuffer, originalToken, alternateTenseNode);
                                            prevNode = alternateTenseNode.followTransitionPathFromState(' ');
                                            break;
                                        case LEAF_TERMINATING:
                                            addToBufferAndEmitToken(tokenBuffer, originalToken, alternateTenseNode,
                                                    annotations, tokenListHandle, sentenceTokens, tokenOffsets,
                                                    ambiguousAnnotations, longestMatch, false, lastEmittedTokenOffset);
                                            tokenListHandle -= (tokenBuffer.size() - 1);
                                            clearMatchAndTokenBuffer(matchBuffer, tokenBuffer);
                                            break;
                                        case AMBIGUOUS:
                                            addToBufferAndEmitToken(tokenBuffer, originalToken, alternateTenseNode,
                                                    annotations, tokenListHandle, sentenceTokens, tokenOffsets,
                                                    ambiguousAnnotations, longestMatch, true, lastEmittedTokenOffset);
                                            prevNode = currentNode.followTransitionPathFromState(' ');
                                            break;
                                    }
                                }
                            }
                            if (shouldBreak) {
                                tokenListHandle -= (tokenBuffer.size() - 1);
                                annotations.addAll(ambiguousAnnotations);
                                ambiguousAnnotations.clear();
                                clearMatchAndTokenBuffer(matchBuffer, tokenBuffer);
                                shouldBreak = true;
                                prevNode = root;
                                break;
                            }
                        }
                        if (!shouldBreak) {
                            if (tokenBuffer.size() > 0) {
                                tokenListHandle -= tokenBuffer.size();
                            }
                            annotations.addAll(ambiguousAnnotations);
                            ambiguousAnnotations.clear();
                            clearMatchAndTokenBuffer(matchBuffer, tokenBuffer);
                            prevNode = root;
                        }
                        break;
                    case LEAF_TERMINATING:
                        addToBufferAndEmitToken(tokenBuffer, originalToken, currentNode, annotations,
                                tokenListHandle, sentenceTokens, tokenOffsets,
                                ambiguousAnnotations, longestMatch, false, lastEmittedTokenOffset);
                        tokenListHandle -= (tokenBuffer.size() - 1);
                        clearMatchAndTokenBuffer(matchBuffer, tokenBuffer);
                        break;
                    case LEAF_CONTINUED:
                        addToBuffer(tokenBuffer, originalToken, currentNode);
                        prevNode = currentNode.followTransitionPathFromState(' ');
                        break;
                    case AMBIGUOUS:
                        addToBufferAndEmitToken(tokenBuffer, originalToken, currentNode, annotations,
                                tokenListHandle, sentenceTokens, tokenOffsets,
                                ambiguousAnnotations, longestMatch, true, lastEmittedTokenOffset);
                        prevNode = currentNode.followTransitionPathFromState(' ');
                        break;
                }


            }

            if ((tokenListHandle == tokens.length - 1) && (tokenBuffer.size() > 1)) {
                tokenListHandle -= (tokenBuffer.size() - 1);
                annotations.addAll(ambiguousAnnotations);
                ambiguousAnnotations.clear();
                clearMatchAndTokenBuffer(matchBuffer, tokenBuffer);
                prevNode = root;
            }

        }//End of per token loop

        annotations.addAll(ambiguousAnnotations);
        Collections.sort(annotations);
        return annotations;
    }

    private Annotation1 addToBufferAndEmitToken(Queue<String> tokenBuffer, String token, State node,
                                                ArrayList<Annotation1> annotations, int handle,
                                                List<String> sentenceTokens, List<Tuple2<Integer, Integer>> tokenOffsets,
                                                ArrayList<Annotation1> ambiguousAnnotations, boolean longestMatch,
                                                boolean ambiguous, MutableInt lastEmittedTokenOffset) {
        addToBuffer(tokenBuffer, token, node);
        return emit(tokenBuffer, node, annotations, ambiguousAnnotations, handle, longestMatch, sentenceTokens,
                tokenOffsets, ambiguous, lastEmittedTokenOffset);
    }

    private void addToBuffer(Queue<String> tokenBuffer, String token, State node) {
        tokenBuffer.add(token);
    }

    private void clearMatchAndTokenBuffer(StringBuffer matchBuffer, Queue<String> tokenBuffer) {
        tokenBuffer.clear();
        matchBuffer.setLength(0);
    }

    private boolean checkIfTokenEndsWithTailSequence(String token, short handle) {
        String tailToken = token.substring(handle);
        for (String tailSequence : tailSequences) {
            if (token.equals(tailSequence)) {
                return false;
            }
            if (tailToken.equals(tailSequence)) {
                return true;
            }
        }
        return false;
    }

    private State getAlternateTenseNode(String tailSequence, State currentNode) {
        State nextNode = currentNode;
        for (Character ch : tailSequence.toCharArray()) {
            nextNode = currentNode.followTransitionPathFromState(ch);
            if (nextNode == null) {
                return null;
            }
            currentNode = nextNode;
        }
        if (currentNode.getStateType() == StateType.NON_LEAF)
            return null;
        return currentNode;
    }

    private Annotation1 emitNumber(String text, ArrayList<Annotation1> annotations, int startOffset, int endOffset,
                                   int number, List<String> sentenceTokens, int localOffset) {
        TagModel tagModel = new TagModel("Number");
        Map<String, String> features = new HashMap<>();
        features.put("Number", String.valueOf(number));
        tagModel.setFeatures(features);
        Tags tags = new Tags();
        tags.add(tagModel);

        Annotation1 annotation = new Annotation1(text, tags, localOffset, sentenceTokens,
                sentenceTokens, startOffset, endOffset);
        annotations.add(annotation);
        return annotation;
    }

    private Annotation1 emit(Queue<String> tokenBuffer, State node, ArrayList<Annotation1> annotations,
                             ArrayList<Annotation1> ambiguousAnnotations, int handle, boolean longestMatch,
                             List<String> sentenceTokens, List<Tuple2<Integer, Integer>> tokenOffsets,
                             boolean ambiguous, MutableInt lastEmittedTokenOffset) {
        String yyText = StringUtils.join(tokenBuffer, " ");
        List<String> tokens = new ArrayList<String>(tokenBuffer);
        int firstTokenOffset = handle - tokenBuffer.size() + 1;
        Tuple2<Integer, Integer> firstTokeIndices = tokenOffsets.get(firstTokenOffset);
        int lastTokenOffset = firstTokenOffset + tokens.size() - 1;
        Tuple2<Integer, Integer> lastTokeIndices = tokenOffsets.get(lastTokenOffset);

        Tags tags = extractTagNamesFromNodeTags(node.getTags());
        int tokenEndOffset = firstTokenOffset + tokens.size() - 1;
        Annotation1 annotation = new Annotation1(new String(yyText), tags,
                firstTokenOffset, tokens, new ArrayList<String>(sentenceTokens), firstTokeIndices._1(),
                lastTokeIndices._2());

        if (longestMatch) {
            ambiguousAnnotations.clear();
            if (lastTokeIndices._2() > lastEmittedTokenOffset.getValue()) {
                lastEmittedTokenOffset.setValue(lastTokeIndices._2());
                if (ambiguous) {
                    ambiguousAnnotations.add(annotation);
                } else {
                    annotations.add(annotation);
                }
            }
        } else {
            annotations.add(annotation);
        }

        return annotation;
    }

    private boolean removeAmbiguousAnnotaionTags(Tags tagModels, List<Annotation1> ambiguousAnnotations) {
        List<Annotation1> updatedAnnotations = new ArrayList<>();
        for (TagModel tag : tagModels) {
            ambiguousAnnotations.addAll(updatedAnnotations);
            updatedAnnotations.clear();
            Iterator<Annotation1> annotationIterator = ambiguousAnnotations.iterator();
            while (annotationIterator.hasNext()) {
                Annotation1 ambiguousAnnotation = annotationIterator.next();
                Tags ambiguousTags = ambiguousAnnotation.getTags();
                boolean tagUpdated = false;
                Iterator<TagModel> tagIterator = ambiguousTags.iterator();
                while (tagIterator.hasNext()) {
                    TagModel ambiguousTag = tagIterator.next();
                    if (tag.equals(ambiguousTag)) {
                        tagIterator.remove();
                        tagUpdated = true;
                    }
                }
                if (tagUpdated) {
                    annotationIterator.remove();
                    Tags updatedTags = ambiguousAnnotation.getTags();
                    if (!updatedTags.isEmpty()) {
                        updatedAnnotations.add(ambiguousAnnotation);
                    }
                }
            }
        }
        ambiguousAnnotations.addAll(updatedAnnotations);
        Collections.sort(ambiguousAnnotations);
        return true;
    }

    private Tags extractTagNamesFromNodeTags(Tags nodeTags) {
        Tags annotationTags = new Tags();
        annotationTags.setBrand(nodeTags.isBrand());
        annotationTags.addAll(nodeTags);
        return annotationTags;
    }

    private void addTagToNode(State node, TagModel tag, boolean isBrand) {
        if (node.getTags() == null) {
            Tags tags = new Tags(node);
            node.setTags(tags);
            tags.setIsBrand(isBrand);
        }
        node.getTags().add(tag);
    }

    public long size() {
        return trieNumberOfWords;
    }

    public boolean deserialize(String fileName) {
        FileInputStream fileIn;
        try {
            fileIn = new FileInputStream(fileName);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            try {
                root = (State) in.readObject();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            relationStack = new Stack<Object>();
            fetchRelationsFromStack(in);
            fileIn.close();
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            root = null;
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            root = null;
            return false;
        }
    }

    public boolean deserialize(InputStream file) {
        try {
            ObjectInputStream in = new ObjectInputStream(file);
            try {
                root = (State) in.readObject();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            relationStack = new Stack<Object>();
            fetchRelationsFromStack(in);
            return true;
        } catch (IOException ex) {
            ex.printStackTrace();
            root = null;
            return false;
        }
    }

    public boolean serialize(String fileName) {
        FileOutputStream fileOut;
        try {
            fileOut = new FileOutputStream(fileName);
            ObjectOutputStream output = new ObjectOutputStream(fileOut);
            relationStack = new Stack<Object>();
            output.writeObject(root);
            writeRelationsToStack(root.getTransitionStates());
            writeStackToStream(output);
            relationStack = null;
            fileOut.close();
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

    }

    private void writeRelationsToStack(TreeMap<Character, State> nodes) {
        if (nodes == null)
            return;
        for (Map.Entry<Character, State> entry : nodes.entrySet()) {
            State node = entry.getValue();
            relationStack.push(node.getTags());
            writeRelationsToStack(node.getTransitionStates());
        }
    }

    private void writeStackToStream(ObjectOutputStream output) {
        while (!relationStack.empty()) {
            try {
                output.writeObject(relationStack.pop());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void fetchRelationsFromStack(ObjectInputStream input) {
        try {
            while (input.available() > 0) {
                Tags tags = (Tags) input.readObject();
                relationStack.push(tags);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}