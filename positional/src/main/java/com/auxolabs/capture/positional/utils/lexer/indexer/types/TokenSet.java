package com.auxolabs.capture.positional.utils.lexer.indexer.types;

import scala.Tuple2;

import java.util.List;

public class TokenSet {
    private String[] tokens;
    private String[] originalTokens; // without lemmatization
    private List<Tuple2<Integer, Integer>> tokenOffsets;

    public TokenSet(String[] tokens, String[] originalTokens, List<Tuple2<Integer, Integer>> tokenOffsets) {
        this.tokens = tokens;
        this.originalTokens = originalTokens;
        this.tokenOffsets = tokenOffsets;
    }

    public String[] getTokens() {
        return tokens;
    }

    public void setTokens(String[] tokens) {
        this.tokens = tokens;
    }

    public String[] getOriginalTokens() {
        return originalTokens;
    }

    public void setOriginalTokens(String[] originalTokens) {
        this.originalTokens = originalTokens;
    }

    public List<Tuple2<Integer, Integer>> getTokenOffsets() {
        return tokenOffsets;
    }

    public void setTokenOffsets(List<Tuple2<Integer, Integer>> tokenOffsets) {
        this.tokenOffsets = tokenOffsets;
    }
}
