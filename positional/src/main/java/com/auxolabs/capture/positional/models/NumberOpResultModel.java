package com.auxolabs.capture.positional.models;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.HashMap;

@Data
public class NumberOpResultModel extends OpResultModel {
    private HashMap<String, TokenModel> tokens;
    private Object result;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        NumberOpResultModel that = (NumberOpResultModel) o;

        return this.hashCode() == that.hashCode();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(tokens)
                .append(result)
                .toHashCode();
    }
}
