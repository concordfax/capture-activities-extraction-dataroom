package com.auxolabs.capture.positional.utils.lexer.indexer.index;


import com.auxolabs.capture.positional.utils.lexer.indexer.types.TagModel;

import java.io.Serializable;
import java.util.TreeSet;

public class Tags extends TreeSet<TagModel>
        implements Serializable {

    private State parent;
    private boolean isBrand;

    public Tags(State parent) {
        this.parent = parent;
    }

    public Tags() {

    }

    public State getParent() {
        return parent;
    }

    public void setParent(State parent) {
        this.parent = parent;
    }

    public void setIsBrand(boolean isBrand) {
        this.isBrand = isBrand;
    }

    public boolean isBrand() {
        return isBrand;
    }

    public void setBrand(boolean isBrand) {
        this.isBrand = isBrand;
    }
}
