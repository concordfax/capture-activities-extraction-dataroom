package com.auxolabs.capture.positional.exceptions;

public class PositionalProfileNotFoundException extends PositionalRuleException {

    public PositionalProfileNotFoundException(String profileId) {
        super(String.format("positional rule with id '%s' not found", profileId));
    }
}
