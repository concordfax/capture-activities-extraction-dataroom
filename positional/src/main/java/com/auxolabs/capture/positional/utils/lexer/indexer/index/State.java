package com.auxolabs.capture.positional.utils.lexer.indexer.index;


import com.auxolabs.capture.positional.utils.lexer.indexer.types.StateType;

import java.io.Serializable;
import java.util.TreeMap;


class State implements Serializable {

    private static final long serialVersionUID = 8106383452390141867L;

    private TreeMap<Character, State> transitionStates;

    private StateType stateType;

    private Tags tags;


    public State() {
        initialize();
    }

    private void initialize() {
        transitionStates = null;
        tags = null;

    }

    public State addTransitionPathFromState(Character ch, StateType stateType) {
        State nextState = null;
        if (transitionStates == null) {
            transitionStates = new TreeMap<Character, State>();
        } else {
            nextState = transitionStates.get(ch);
        }
        if (nextState == null) {
            nextState = new State();
            transitionStates.put(ch, nextState);
            nextState.setStateType(stateType);
        } else if (nextState.getStateType() == StateType.DELETED) {
            nextState.setStateType(stateType);
        }
        //if there is conflict in nodeType, then assign ambiguous
        else if ((nextState.getStateType() == StateType.LEAF_TERMINATING &&
                stateType == StateType.LEAF_CONTINUED) || (nextState.getStateType() == StateType.LEAF_CONTINUED &&
                stateType == StateType.LEAF_TERMINATING)) {
            nextState.setStateType(StateType.AMBIGUOUS);

        } else if (stateType != StateType.NON_LEAF && nextState.getStateType() != StateType.AMBIGUOUS) {
            nextState.setStateType(stateType);
        }
        return nextState;
    }

    public State followTransitionPathFromState(Character ch) {
        if (transitionStates == null) {
            return null;
        } else {
            return transitionStates.get(ch);
        }
    }

    public TreeMap<Character, State> getTransitionStates() {
        return transitionStates;
    }

    public void setTransitionStates(TreeMap<Character, State> transitionStates) {
        this.transitionStates = transitionStates;
    }

    public StateType getStateType() {
        return stateType;
    }

    public void setStateType(StateType stateType) {
        this.stateType = stateType;
    }

    public Tags getTags() {
        return tags;
    }

    public void setTags(Tags tags) {
        this.tags = tags;
    }
}