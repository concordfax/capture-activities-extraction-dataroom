package com.auxolabs.capture.positional.utils.lexer.indexer.types;

/**
 * Represents a Node's type
 */
public enum StateType {
    NON_LEAF,               //Not a leaf node
    LEAF_TERMINATING,       //Leaf node and not con
    LEAF_CONTINUED,         //Leaf node which terminates a word part of a group of pages
    AMBIGUOUS,               //CONTINUED AND TERMINATING
    DELETED,                //Deleted Node
    ;
}
