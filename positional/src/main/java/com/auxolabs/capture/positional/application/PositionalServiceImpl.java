package com.auxolabs.capture.positional.application;

import com.auxolabs.capture.positional.PositionalService;
import com.auxolabs.capture.positional.exceptions.MissingTokenException;
import com.auxolabs.capture.positional.exceptions.PositionalProfileNotFoundException;
import com.auxolabs.capture.positional.exceptions.PositionalRuleException;
import com.auxolabs.capture.positional.models.RuleOpModel;
import com.auxolabs.capture.positional.models.TokenModel;
import com.auxolabs.capture.positional.operations.BaseFunction;
import com.auxolabs.capture.positional.operations.BooleanBaseOperation;
import com.auxolabs.capture.positional.parsers.RuleParserUtil;
import com.concordfax.capture.core.models.extraction.positional.trigger.PositionalExtractorTriggerConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.NotImplementedException;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

@Slf4j
public class PositionalServiceImpl implements PositionalService{

    @Inject
    public PositionalServiceImpl() {
    }

    @Override
    public List<RuleOpModel> process(PositionalExtractorTriggerConfiguration triggerConfiguration, HashMap<String, HashSet<TokenModel>> tokens) {

        if(null == triggerConfiguration)
            throw new PositionalProfileNotFoundException("NA");

        List<RuleOpModel> ruleOpModels = new ArrayList<>();
        HashMap<String, BaseFunction> functions = new HashMap<>();
        triggerConfiguration.getFunctions().forEach(functionModel -> {
            RuleParserUtil.parseAndAddFunction(functions, functionModel.getFunction());
        });
        triggerConfiguration.getRules().forEach(ruleModel -> {
            RuleOpModel ruleOpModel = new RuleOpModel();
            ruleOpModel.setRuleModel(ruleModel);
            ruleOpModel.setOp(RuleParserUtil.parseAndGetRule(functions, ruleModel.getRule()));
            ruleOpModels.add(ruleOpModel);
        });
        ruleOpModels.forEach(ruleModel -> {
            if(ruleModel.getOp() instanceof BooleanBaseOperation){
                BooleanBaseOperation bbop = (BooleanBaseOperation) ruleModel.getOp();
                ruleModel.setResults(new HashSet<>());
                try {
                    ruleModel.getResults().addAll(bbop.compute(tokens));
                }catch (MissingTokenException mte){
                    // ignore; this should be caught and managed while rules are added
                } catch (PositionalRuleException pre){
                    log.error(pre.getMessage(), pre);
                }finally {
                    ruleModel.setOp(null);
                }
            }else {
                throw new NotImplementedException();
            }
        });
        return ruleOpModels;
    }
}
