package com.auxolabs.capture.positional.utils.lexer.indexer.index;


import com.auxolabs.capture.positional.utils.lexer.indexer.types.TagModel;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FilterMap {

    private static final int MAX_COUNT_VAL = 7;
    private static final int MIN_COUNT_VAL = 1;
    private HashMap<String, List<Filter>> tagFilters;

    public FilterMap() {
        tagFilters = new HashMap<String, List<Filter>>();
    }

    public void insertOrUpdateMapEntry(TagModel tag, ArrayList<Filter> filters) {
        if (tagFilters.containsKey(tag.getType())) {
            tagFilters.get(tag.getType()).addAll(filters);
        } else {
            tagFilters.put(tag.getType(), filters);
        }
    }

    public List<Filter> getFilters(TagModel tag) {
        return tagFilters.get(tag.getType());
    }

    public boolean validateAnnotation(Annotation1 annotation, List<Filter> filters) {

        if (filters == null) {
            return true;
        }

        boolean shouldEmit = true;
        for (Filter filter : filters) {
            switch (filter) {
                case MAX_COUNT:
                    shouldEmit &= (annotation.getCount() <= MAX_COUNT_VAL);
                    break;
                case MIN_COUNT:
                    shouldEmit &= (annotation.getCount() >= MIN_COUNT_VAL);
                    break;
                case FIRST_LETTER_CAPS:
                    if (!Character.isUpperCase(annotation.getText().charAt(0))) {
                        shouldEmit &= false;
                    }
                    break;
                case NOT_FREQUENT_WORD:
                    if (annotation.getTags().contains("Common_Word")) {
                        shouldEmit &= false;
                    }
                    break;
                case NOT_STOP_WORD:
                    if (annotation.getTags().contains("Stop_Word")) {
                        shouldEmit &= false;
                    }
                    break;
                case BRAND_ONLY_FILTER:
                    if (!annotation.getTags().isBrand()) {
                        shouldEmit &= false;
                    }
                    break;
                case NON_START_WORD:
                    if (annotation.getLocalOffset() == 0) {
                        shouldEmit &= false;
                    }
                    break;
                case ALL_LETTERS_CAPS:
                    if (!StringUtils.isAllUpperCase(annotation.getText())) {
                        shouldEmit &= false;
                    }
                    break;
                case ALL_TOKEN_STARTS_WITH_CAPS:
                    for (String token : annotation.getTokens()) {
                        if (!Character.isUpperCase(token.charAt(0))) {
                            shouldEmit &= false;
                            break;
                        }
                    }
                    break;
                case PREV_WORD_STARTS_WITH_NOT_CAPITAL:
                    int localOffset = annotation.getLocalOffset();
                    if (localOffset >= 1) {
                        String prevToken = annotation.getSentenceTokens().get(localOffset - 1);
                        if (prevToken.length() > 0 && Character.isUpperCase(prevToken.charAt(0))) {
                            shouldEmit &= false;
                        }
                    }

                    break;
                case NEXT_WORD_STARTS_WITH_NOT_CAPITAL:
                    localOffset = annotation.getLocalOffset();
                    if (annotation.getSentenceTokens().size() > localOffset + annotation.getTokens().size()) {
                        String nextToken = annotation.getSentenceTokens().get(localOffset + annotation.getTokens().size());
                        if (nextToken.length() > 0 && Character.isUpperCase(nextToken.charAt(0))) {
                            shouldEmit &= false;
                        }
                    }
                    break;
                default:
                    break;
            }
            if (!shouldEmit) {
                return false;
            }
        }

        return shouldEmit;
    }

}
