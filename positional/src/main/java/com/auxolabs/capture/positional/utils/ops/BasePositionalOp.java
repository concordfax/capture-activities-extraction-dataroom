package com.auxolabs.capture.positional.utils.ops;

import com.auxolabs.capture.positional.models.PositionalFieldDimensionsModel;

public interface BasePositionalOp {
    void perform(PositionalFieldDimensionsModel d);
}
