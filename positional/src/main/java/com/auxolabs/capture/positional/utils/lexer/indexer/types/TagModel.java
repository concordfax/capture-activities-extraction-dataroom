package com.auxolabs.capture.positional.utils.lexer.indexer.types;

import java.util.Map;
import java.util.Set;

public class TagModel implements Comparable<TagModel> {

    private String type;
    private Map<String, String> features;

    public TagModel(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map<String, String> getFeatures() {
        return features;
    }

    public void setFeatures(Map<String, String> features) {
        this.features = features;
    }

    public int compareTo(TagModel other) {
        return this.type.compareTo(other.type);
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof TagModel)) {
            return false;
        }

        TagModel otherTag = (TagModel) other;
        if (this.type.equals(otherTag.type)) {
//            return compareFeatures(this, otherTag);
            return true;
        } else
            return false;
    }

    private boolean compareFeatures(TagModel tag1, TagModel tag2) {
        Map<String, String> features1 = tag1.getFeatures();
        Map<String, String> features2 = tag2.getFeatures();

        if (features1 == null) {
            return features2 == null ? true : false;
        } else if (features2 == null) {
            return false;
        }

        Set<String> keys1 = features1.keySet();
        Set<String> keys2 = features2.keySet();
        if (!keys1.equals(keys2)) {
            return false;
        } else {
            for (String key : keys1) {
                if (!features1.get(key).equals(features2.get(key))) {
                    return false;
                }
            }
        }
        return true;
    }
}
