package com.auxolabs.capture.positional.operations;

import com.auxolabs.capture.positional.models.BooleanOpResultModel;
import com.auxolabs.capture.positional.models.NumberOpResultModel;
import com.auxolabs.capture.positional.models.TokenModel;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.function.BiFunction;

@Slf4j
public class LogicalOperation implements BooleanBaseOperation {

    private NumberBaseOperation bo1;
    private NumberBaseOperation bo2;
    private BiFunction<Object, Object, Boolean> func;

    public LogicalOperation(NumberBaseOperation bo1, NumberBaseOperation bo2, String operator) {
        this.bo1 = bo1;
        this.bo2 = bo2;
        this.func = getFunc(operator);
    }

    public LogicalOperation(NumberBaseOperation bo1, NumberBaseOperation bo2, BiFunction<Object, Object, Boolean> func) {
        this.bo1 = bo1;
        this.bo2 = bo2;
        this.func = func;
    }

    private BiFunction<Object, Object, Boolean> getFunc(String operator) {
        switch (operator) {
            case ">":
                return (d1, d2) -> {
                    if(d1 instanceof Double && d2 instanceof Double) {
                        Double t1 = (Double) d1;
                        Double t2 = (Double) d2;
                        return t1 > t2;
                    }else if (d1 instanceof Integer && d2 instanceof Integer){
                        Integer t1 = (Integer) d1;
                        Integer t2 = (Integer) d2;
                        return t1 > t2;
                    }else {
                        return false;
                    }
                };
            case "<":
                return (d1, d2) -> {
                    if(d1 instanceof Double && d2 instanceof Double) {
                        Double t1 = (Double) d1;
                        Double t2 = (Double) d2;
                        return t1 < t2;
                    }else if (d1 instanceof Integer && d2 instanceof Integer){
                        Integer t1 = (Integer) d1;
                        Integer t2 = (Integer) d2;
                        return t1 < t2;
                    }else {
                        return false;
                    }
                };
            case "<=":
                return (d1, d2) -> {
                    if(d1 instanceof Double && d2 instanceof Double) {
                        Double t1 = (Double) d1;
                        Double t2 = (Double) d2;
                        return t1 <= t2;
                    }else if (d1 instanceof Integer && d2 instanceof Integer){
                        Integer t1 = (Integer) d1;
                        Integer t2 = (Integer) d2;
                        return t1 <= t2;
                    }else {
                        return false;
                    }
                };
            case ">=":
                return (d1, d2) -> {
                    if(d1 instanceof Double && d2 instanceof Double) {
                        Double t1 = (Double) d1;
                        Double t2 = (Double) d2;
                        return t1 >= t2;
                    }else if (d1 instanceof Integer && d2 instanceof Integer){
                        Integer t1 = (Integer) d1;
                        Integer t2 = (Integer) d2;
                        return t1 >= t2;
                    }else {
                        return false;
                    }
                };
            case "!=":
                return (d1, d2) -> {
                    if(d1 instanceof Double && d2 instanceof Double) {
                        Double t1 = (Double) d1;
                        Double t2 = (Double) d2;
                        return !t1.equals(t2);
                    }else if (d1 instanceof Integer && d2 instanceof Integer){
                        Integer t1 = (Integer) d1;
                        Integer t2 = (Integer) d2;
                        return !t1.equals(t2);
                    }else {
                        return false;
                    }
                };
        }
        return null;
    }

    @Override
    public HashSet<BooleanOpResultModel> compute(HashMap<String, HashSet<TokenModel>> tokens) {
        HashSet<BooleanOpResultModel> answers = new HashSet<>();

        HashSet<NumberOpResultModel> results1 = bo1.compute(tokens);
        HashSet<NumberOpResultModel> results2 = bo2.compute(tokens);

        for (NumberOpResultModel numberOpResultModel1 : results1) {
            for (NumberOpResultModel numberOpResultModel2 : results2) {
                // find the keys in both the options
                Set<String> set1 = new HashSet<>(numberOpResultModel1.getTokens().keySet());
                Set<String> set2 = new HashSet<>(numberOpResultModel2.getTokens().keySet());

                // if element present in both sets, it should be the same element
                set1.retainAll(set2);
                Set<String> intersection = set1;
                boolean isAllCommonMatching = true;
                for (String s : intersection) {
                    if(numberOpResultModel1.getTokens().get(s).hashCode() != numberOpResultModel2.getTokens().get(s).hashCode()){
                        isAllCommonMatching = false;
                        break;
                    }
                }

                if(!isAllCommonMatching)
                    continue;

                Boolean answer =  func.apply(numberOpResultModel1.getResult(), numberOpResultModel2.getResult());
                if(answer) {
                    BooleanOpResultModel numberOpResultModel = new BooleanOpResultModel();
                    numberOpResultModel.setResult(true);
                    numberOpResultModel.setTokens(new HashMap<>());
                    numberOpResultModel.getTokens().putAll(numberOpResultModel1.getTokens());
                    numberOpResultModel.getTokens().putAll(numberOpResultModel2.getTokens());
                    answers.add(numberOpResultModel);
                }else {
//                    log.debug();
                }
            }
        }
        return answers;
    }

    @Override
    public void updateParams(HashMap<String, String> newParams) {
        this.bo1.updateParams(newParams);
        this.bo2.updateParams(newParams);
    }

    @Override
    public BaseOperation clone() {
        return new LogicalOperation((NumberBaseOperation) bo1.clone(), (NumberBaseOperation) bo2.clone(), func);
    }
}
