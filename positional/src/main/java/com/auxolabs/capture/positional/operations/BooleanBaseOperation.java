package com.auxolabs.capture.positional.operations;

import com.auxolabs.capture.positional.models.BooleanOpResultModel;
import com.auxolabs.capture.positional.models.TokenModel;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public interface BooleanBaseOperation extends BaseOperation{
    HashSet<BooleanOpResultModel> compute(HashMap<String, HashSet<TokenModel>> tokens);
}
