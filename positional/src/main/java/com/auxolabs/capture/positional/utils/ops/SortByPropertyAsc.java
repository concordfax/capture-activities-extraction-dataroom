package com.auxolabs.capture.positional.utils.ops;

import com.auxolabs.capture.positional.models.PositionalFieldDimensionsModel;
import com.auxolabs.capture.positional.models.TokenModel;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;

@Slf4j
public class SortByPropertyAsc implements BasePositionalOp {
    @Override
    public void perform(PositionalFieldDimensionsModel d) {
        d.getFieldResultsModels().forEach(positionalFieldDimensionsModel -> {
            Double val = 0.0;
            for (TokenModel tokenModel : positionalFieldDimensionsModel.getTokenModels()) {
                try {
                    Object v1 = tokenModel.getOrDefault(d.getProp().name(), null);
                    if (v1 instanceof Double) {
                        val += (Double) v1;
                    }
                }catch (Exception e){
                    // ignore
                }
            }
            if(positionalFieldDimensionsModel.getTokenModels().get(0).containsKey(d.getProp().name())) {
                positionalFieldDimensionsModel.put(d.getProp(), val);
            }
        });
        d.getFieldResultsModels().sort((o1, o2) -> {
            try {
                Object v1 = o1.getOrDefault(d.getProp(), null);
                Object v2 = o2.getOrDefault(d.getProp(), null);
                if(v1 instanceof Double && v2 instanceof Double){
                    return (int)((Double)v1 - (Double) v2);
                }
            }catch (Exception e){
                // ignore
            }
            return 0;
        });
    }
}
