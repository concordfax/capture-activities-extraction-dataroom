package com.auxolabs.capture.positional.operations;

import com.auxolabs.capture.positional.models.NumberOpResultModel;
import com.auxolabs.capture.positional.models.TokenModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.function.BiFunction;

public class PowOperation implements NumberBaseOperation {

    private NumberBaseOperation bo;
    private Double power;
    private BiFunction<Double, Double, Double> func;

    public PowOperation(NumberBaseOperation bo, Double power) {
        this.bo = bo;
        this.power = power;
        this.func = Math::pow;
    }

    @Override
    public HashSet<NumberOpResultModel> compute(HashMap<String, HashSet<TokenModel>> tokens) {
        HashSet<NumberOpResultModel> answers = new HashSet<>();

        HashSet<NumberOpResultModel> results = bo.compute(tokens);
        for (NumberOpResultModel result : results) {
            NumberOpResultModel numberOpResultModel = new NumberOpResultModel();
            numberOpResultModel.setResult(this.func.apply((Double) result.getResult(), this.power));
            numberOpResultModel.setTokens(new HashMap<>(result.getTokens()));
            answers.add(numberOpResultModel);
        }

        return answers;
    }

    @Override
    public void updateParams(HashMap<String, String> newParams) {
        this.bo.updateParams(newParams);
    }

    @Override
    public BaseOperation clone() {
        return new PowOperation((NumberBaseOperation) bo.clone(), power);
    }
}

