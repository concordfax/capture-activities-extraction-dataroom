package com.auxolabs.capture.positional.models;

import lombok.Data;

import java.util.List;

@Data
public class OntologyModel {
    private List<LexiconModel> lexicons;
}
