package com.auxolabs.capture.positional.models;

import com.concordfax.capture.core.models.extraction.positional.output.PositionalPropertyTypes;
import lombok.Data;

import java.util.HashMap;
import java.util.List;

@Data
public class PositionalFieldResultsModel extends HashMap<PositionalPropertyTypes, Object> {
    private OpResultModel opResultModel;
    private List<TokenModel> tokenModels;
}
