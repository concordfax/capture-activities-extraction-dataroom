package com.auxolabs.capture.positional.utils.lexer.indexer;


import com.auxolabs.capture.positional.utils.lexer.indexer.index.Annotation1;
import com.auxolabs.capture.positional.utils.lexer.indexer.index.Filter;
import com.auxolabs.capture.positional.utils.lexer.indexer.index.FilterMap;
import com.auxolabs.capture.positional.utils.lexer.indexer.index.StateMachine;
import com.auxolabs.capture.positional.utils.lexer.indexer.types.TagModel;
import com.auxolabs.capture.positional.utils.lexer.utils.Utilities;

import java.io.*;
import java.util.*;


public class Lexer {
    private static final Object lock = new Object();
    private static Lexer lexer = null;
    private StateMachine stateMachine;
    private FilterMap filterMap;

    private Lexer() throws IOException {
        stateMachine = new StateMachine();
        filterMap = new FilterMap();
    }

    public static Lexer getInstance() throws IOException {
        synchronized (lock) {
            if (lexer == null) {
                lexer = new Lexer();
            }
        }
        return lexer;
    }

    public void addToIndex(String text, String tagType) {
        addToIndex(text, tagType, null);
    }

    public void addToIndex(String text, String tagType, Map<String, String> features) {
        TagModel tag = new TagModel(tagType);
        if (features != null) {
            tag.setFeatures(features);
        }
        synchronized (lock) {
            stateMachine.insertIndex(text, tag, false);
        }
    }

    public ArrayList<Annotation1> extractTags(String text, boolean longestMatch, boolean annotateNumbers) {
        return stateMachine.searchWord(text, 2, longestMatch);
    }

    private boolean indexFilesInputs(List<String> fileNames, boolean isBrand, boolean isCaseSensitive) {
        if (fileNames == null) {
            return false;
        }

        String line;
        try {

            for (String fileName : fileNames) {
                File indexFile = new File(fileName);
                BufferedReader br =
                        new BufferedReader(new FileReader(indexFile));
                while ((line = br.readLine()) != null) {
                    line = line.replace('-', ' ');
                    line = line.replaceAll(",", "");
                    line = line.toLowerCase();
                    line.trim();
                    synchronized (lock) {
                        stateMachine.insertIndex(line, new TagModel(indexFile.getName()), isBrand);
                    }
                }
                br.close();
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean indexStream(boolean isBrand, InputStream inputStream) {
        if (inputStream == null) {
            return false;
        }

        String line;
        Scanner scanner = new Scanner(inputStream);

        while (scanner.hasNext()) {
            line = scanner.nextLine();
            String elements[] = line.split("\\|");
            if (elements.length == 2) {
                String tag = elements[0];
                if (tag.contains("#")) {
                    String tagProps[] = tag.split("#");
                    if (tagProps.length > 1) {
                        tag = tagProps[0];
                        filterMap.insertOrUpdateMapEntry(new TagModel(tag), getFiltersFromCSV(tagProps[1]));
                    }
                }
                String values[] = elements[1].split(";");
                for (String value : values) {
                    value = value.replace('-', ' ');
                    value = value.replaceAll(",", "");
                    value.trim();
                    synchronized (lock) {
                        stateMachine.insertIndex(value, new TagModel(tag), isBrand);
                    }
                }
            }
        }
        scanner.close();
        return true;
    }

    private boolean indexDictionaryFiles(boolean isBrand, File... fileNames) {
        if (fileNames == null) {
            return false;
        }
        try {
            for (File indexFile : fileNames) {
                indexStream(isBrand, new FileInputStream(indexFile));
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private ArrayList<Filter> getFiltersFromCSV(String tagProps) {
        ArrayList<Filter> filters = new ArrayList<Filter>();
        for (String filterName : tagProps.split(",")) {
            filters.add(Filter.valueOf(filterName.toUpperCase()));
        }
        return filters;
    }


    private ArrayList<Annotation1> filterAnnotations(ArrayList<Annotation1> annotations) {
        ArrayList<Annotation1> filteredAnnotations = new ArrayList<Annotation1>();
        for (Annotation1 annotation : annotations) {
            Iterator<TagModel> tagIterator = annotation.getTags().iterator();
            while (tagIterator.hasNext()) {
                TagModel tag = tagIterator.next();
                if (!filterMap.validateAnnotation(annotation, filterMap.getFilters(tag))) {
                    tagIterator.remove();
                }
            }
            if (annotation.getTags() != null && annotation.getTags().size() > 0) {
                annotation.setTags(Utilities.cleanTags(annotation.getTags()));
                filteredAnnotations.add(annotation);
            }
        }
        return filteredAnnotations;
    }

    private ArrayList<Annotation1> filterAnnotations(List<Annotation1> annotations, List<Filter> filters) {
        ArrayList<Annotation1> filteredAnnotations = new ArrayList<Annotation1>();
        for (Annotation1 annotation : annotations) {
            Iterator<TagModel> tagIterator = annotation.getTags().iterator();
            while (tagIterator.hasNext()) {
                tagIterator.next();
                if (!filterMap.validateAnnotation(annotation, filters)) {
                    tagIterator.remove();
                }
            }
            if (annotation.getTags() != null && annotation.getTags().size() > 0) {
                filteredAnnotations.add(annotation);
            }
        }
        return filteredAnnotations;
    }

    public boolean removeFromIndex(String text, String tagType) {
        synchronized (lock) {
            return stateMachine.deleteWord(text, new TagModel(tagType));
        }
    }

    public boolean removeFromIndex(String text, String tagType, Map<String, String> features) {
        TagModel tag = new TagModel(tagType);
        if (features != null) {
            tag.setFeatures(features);
        }
        synchronized (lock) {
            return stateMachine.deleteWord(text, tag);
        }
    }
}
