package com.auxolabs.capture.positional.exceptions;

public class PositionalRuleException extends RuntimeException {
    public PositionalRuleException(String message) {
        super(message);
    }
}
