package com.auxolabs.capture.positional;

import com.auxolabs.capture.positional.models.RuleOpModel;
import com.auxolabs.capture.positional.models.TokenModel;
import com.concordfax.capture.core.models.extraction.positional.trigger.PositionalExtractorTriggerConfiguration;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public interface PositionalService {
    List<RuleOpModel> process(PositionalExtractorTriggerConfiguration extractorTriggerConfiguration, HashMap<String, HashSet<TokenModel>> tokens);
}
