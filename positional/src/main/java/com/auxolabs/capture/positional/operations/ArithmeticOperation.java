package com.auxolabs.capture.positional.operations;

import com.auxolabs.capture.positional.constants.PositionalRuleConstants;
import com.auxolabs.capture.positional.models.NumberOpResultModel;
import com.auxolabs.capture.positional.models.TokenModel;

import java.util.*;
import java.util.function.BiFunction;

public class ArithmeticOperation implements NumberBaseOperation {

    private NumberBaseOperation bo1;
    private NumberBaseOperation bo2;
    private BiFunction<Double, Double, Double> func;

    public ArithmeticOperation(NumberBaseOperation bo1, NumberBaseOperation bo2, String operator) {
        this.bo1 = bo1;
        this.bo2 = bo2;
        this.func = getFunc(operator);
    }

    public ArithmeticOperation(NumberBaseOperation bo1, NumberBaseOperation bo2, BiFunction<Double, Double, Double> func) {
        this.bo1 = bo1;
        this.bo2 = bo2;
        this.func = func;
    }

    private BiFunction<Double, Double, Double> getFunc(String operator) {
        switch (operator) {
            case PositionalRuleConstants.ArithmeticOperators.PLUS:
                return (d1, d2) -> d1 + d2;
            case PositionalRuleConstants.ArithmeticOperators.MINUS:
                return (d1, d2) -> d1 - d2;
            case PositionalRuleConstants.ArithmeticOperators.MUL:
                return (d1, d2) -> d1 * d2;
            case PositionalRuleConstants.ArithmeticOperators.DIV:
                return (d1, d2) -> d1 / d2;
        }
        return null;
    }

    @Override
    public HashSet<NumberOpResultModel> compute(HashMap<String, HashSet<TokenModel>> tokens) {
        HashSet<NumberOpResultModel> answers = new HashSet<>();

        HashSet<NumberOpResultModel> results1 = bo1.compute(tokens);
        HashSet<NumberOpResultModel> results2 = bo2.compute(tokens);

        for (NumberOpResultModel numberOpResultModel1 : results1) {
            for (NumberOpResultModel numberOpResultModel2 : results2) {
                // find the keys in both the options
                Set<String> set1 = new HashSet<>(numberOpResultModel1.getTokens().keySet());
                Set<String> set2 = new HashSet<>(numberOpResultModel2.getTokens().keySet());

                // if element present in both sets, it should be the same element
                set1.retainAll(set2);
                Set<String> intersection = set1;
                boolean isAllCommonMatching = true;
                for (String s : intersection) {
                    if(numberOpResultModel1.getTokens().get(s).hashCode() != numberOpResultModel2.getTokens().get(s).hashCode()){
                        isAllCommonMatching = false;
                        break;
                    }
                }

                if(!isAllCommonMatching)
                    continue;

                Double answer =  func.apply((Double) numberOpResultModel1.getResult(), (Double) numberOpResultModel2.getResult());
                NumberOpResultModel numberOpResultModel = new NumberOpResultModel();
                numberOpResultModel.setResult(answer);
                numberOpResultModel.setTokens(new HashMap<>());
                numberOpResultModel.getTokens().putAll(numberOpResultModel1.getTokens());
                numberOpResultModel.getTokens().putAll(numberOpResultModel2.getTokens());
                answers.add(numberOpResultModel);
            }
        }
        return answers;
    }

    @Override
    public void updateParams(HashMap<String, String> newParams) {
        this.bo1.updateParams(newParams);
        this.bo2.updateParams(newParams);
    }

    @Override
    public BaseOperation clone() {
        return new ArithmeticOperation((NumberBaseOperation) bo1.clone(), (NumberBaseOperation) bo2.clone(), func);
    }
}
