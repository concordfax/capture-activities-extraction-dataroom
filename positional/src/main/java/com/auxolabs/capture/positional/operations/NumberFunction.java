package com.auxolabs.capture.positional.operations;

import java.util.HashMap;

public class NumberFunction implements BaseFunction {

    private NumberBaseOperation numberBaseOperation;

    public NumberFunction(NumberBaseOperation numberBaseOperation) {
        this.numberBaseOperation = numberBaseOperation;

    }

    @Override
    public NumberBaseOperation apply(HashMap<String, String> newParams) {
        NumberBaseOperation newObj = (NumberBaseOperation) numberBaseOperation.clone();
        newObj.updateParams(newParams);
        return newObj;
    }
}
