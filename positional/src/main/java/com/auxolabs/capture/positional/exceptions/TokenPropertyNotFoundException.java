package com.auxolabs.capture.positional.exceptions;

public class TokenPropertyNotFoundException extends PositionalRuleException {

    public TokenPropertyNotFoundException(String property) {
        super(String.format("token property '%s' not found.", property));
    }
}
