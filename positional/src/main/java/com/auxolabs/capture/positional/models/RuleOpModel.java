package com.auxolabs.capture.positional.models;

import com.auxolabs.capture.positional.operations.BaseOperation;
import com.concordfax.capture.core.models.extraction.positional.trigger.RuleModel;
import lombok.Data;

import java.util.HashSet;

@Data
public class RuleOpModel {
    private RuleModel ruleModel;
    private BaseOperation op;
    private HashSet<OpResultModel> results;
}
