package com.auxolabs.capture.positional.operations;

import java.util.HashMap;
import java.util.function.Function;

public interface BaseFunction extends Function<HashMap<String, String>, BaseOperation> {
}
