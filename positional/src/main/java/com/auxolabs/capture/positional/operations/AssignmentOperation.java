package com.auxolabs.capture.positional.operations;

import com.auxolabs.capture.positional.constants.PositionalRuleConstants;
import com.auxolabs.capture.positional.exceptions.MissingTokenException;
import com.auxolabs.capture.positional.models.NumberOpResultModel;
import com.auxolabs.capture.positional.models.TokenModel;
import com.auxolabs.capture.positional.exceptions.PositionalRuleException;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class AssignmentOperation implements NumberBaseOperation {
    private Double number;
    private String variable;

    public AssignmentOperation(String variable) {
        this.variable = variable;
    }

    public AssignmentOperation(Double number) {
        this.number = number;
    }

    @Override
    public HashSet<NumberOpResultModel> compute(HashMap<String, HashSet<TokenModel>> tokens) {
        HashSet<NumberOpResultModel> numberResults = new HashSet<>();

        if (number != null) {
            NumberOpResultModel numberOpResultModel = new NumberOpResultModel();
            numberOpResultModel.setResult(number);
            numberOpResultModel.setTokens(new HashMap<>());
            numberResults.add(numberOpResultModel);
            return numberResults;
        }else if(variable!=null){
            String[] vars = variable.split(PositionalRuleConstants.VARIABLE_PROPERTY_SEPARATOR);
            if (vars.length != 2)
                throw new PositionalRuleException("Unsatisfied variable " + variable);
            if (!tokens.containsKey(vars[0])) {
                tokens.put(vars[0], new HashSet<>());
                //throw new MissingTokenException(vars[0]);
            }
            HashSet<TokenModel> temp = new HashSet<>(tokens.get(vars[0]));

            temp.removeIf(tokenModel -> !(tokenModel.containsKey(vars[1])));


            for (TokenModel tokenModel : temp) {
                NumberOpResultModel numberOpResultModel = new NumberOpResultModel();
                numberOpResultModel.setResult(tokenModel.get(vars[1]));
                numberOpResultModel.setTokens(new HashMap<>());
                numberOpResultModel.getTokens().put(vars[0], tokenModel);
                numberResults.add(numberOpResultModel);
            }
        }

        return numberResults;
    }

    @Override
    public void updateParams(HashMap<String, String> newParams) {
        if(newParams!=null && variable!=null){
            String variableWithoutProperty = this.variable.substring(0,this.variable.lastIndexOf("."));
            if(newParams.containsKey(variableWithoutProperty)){
                this.variable = StringUtils.replace(this.variable, variableWithoutProperty,newParams.get(variableWithoutProperty));
            }
        }
    }

    @Override
    public BaseOperation clone() {
        return number!=null ? new AssignmentOperation(number) : new AssignmentOperation(variable);
    }
}
