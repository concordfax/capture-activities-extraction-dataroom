package com.auxolabs.capture.positional.exceptions;

public class MissingTokenException extends PositionalRuleException {
    public MissingTokenException(String tokenName) {
        super( String.format("missing token '%s'",tokenName));
    }
}
