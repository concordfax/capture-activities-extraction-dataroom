package com.auxolabs.capture.positional.exceptions;

public class FunctionNotFoundException extends PositionalRuleException {
    public FunctionNotFoundException(String funcName) {
        super(String.format("function with name %s not found.", funcName));
    }
}
