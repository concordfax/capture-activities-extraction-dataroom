package com.auxolabs.capture.positional.init;

import com.auxolabs.capture.positional.utils.ops.PositionalOpsFactory;

public class PositionalInitializer {

    public PositionalInitializer() {
        PositionalOpsFactory.register();
    }
}
