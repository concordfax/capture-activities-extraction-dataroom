package com.auxolabs.capture.positional.operations;

import com.auxolabs.capture.positional.models.BooleanOpResultModel;
import com.auxolabs.capture.positional.models.NumberOpResultModel;
import com.auxolabs.capture.positional.models.TokenModel;

import java.util.HashMap;
import java.util.HashSet;

public class NotOperation implements BooleanBaseOperation {

    private NumberBaseOperation bo1;
    private NumberBaseOperation bo2;

    public NotOperation(NumberBaseOperation bo1, NumberBaseOperation bo2){
        this.bo1 = bo1;
        this.bo2 = bo2;
    }

    @Override
    public HashSet<BooleanOpResultModel> compute(HashMap<String, HashSet<TokenModel>> tokens) {
        HashSet<BooleanOpResultModel> answers = new HashSet<>();

        HashSet<NumberOpResultModel> results1 = bo1.compute(tokens);
        HashSet<NumberOpResultModel> results2 = bo2.compute(tokens);

        results1.forEach(numberOpResultModel1 -> {
            boolean isPresent = false;
            for (NumberOpResultModel numberOpResultModel2 : results2) {
                if(numberOpResultModel1.getResult().equals(numberOpResultModel2.getResult())){
                    isPresent = true;
                    break;
                }
            }
            if(!isPresent) {
                BooleanOpResultModel booleanOpResultModel = new BooleanOpResultModel();
                booleanOpResultModel.setResult(true);
                booleanOpResultModel.setTokens(new HashMap<>());
                booleanOpResultModel.getTokens().putAll(numberOpResultModel1.getTokens());
                booleanOpResultModel.getTokens().putAll(new HashMap<>());
                answers.add(booleanOpResultModel);
            }
        });

        return answers;
    }

    @Override
    public void updateParams(HashMap<String, String> newParams) {
        this.bo1.updateParams(newParams);
        this.bo2.updateParams(newParams);
    }

    @Override
    public BaseOperation clone() {
        return new NotOperation((NumberBaseOperation) bo1.clone(), (NumberBaseOperation) bo2.clone());
    }
}
