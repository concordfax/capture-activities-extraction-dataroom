package com.auxolabs.capture.positional.utils.lexer.utils;

import com.auxolabs.capture.positional.utils.lexer.indexer.types.TokenSet;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Tokenizer {
    private static final List<Character> TOKEN_SEPERATORS = Arrays.asList('/', '_', '-', '"', ',', ';', ':', '(', ')', '!', '{', '}', '[', ']', '<', '>', '|', '?');
    private static final List<Character> ACCEPTED_SPECIAL_CHARS = Arrays.asList('-', '+', '&', ' ', '.');
    private static final List<Character> ACCEPTED_SENTENCE_END = Arrays.asList('.'); // pages which can occur at diff semantic positions

    public TokenSet tokenize(String text) {
        text = text.trim();
        List<Tuple2<Integer, Integer>> tokenOffsets = new ArrayList<>();
        boolean startToken = false;
        List<String> tokens = new ArrayList<>();
        List<String> originalTokens = new ArrayList<>();
        String token = "";
        int startIndex = 0;
        int endIndex = 0;
        int length = text.length();
        boolean isLastCharacterSpl = false;
        if (length > 0 && ACCEPTED_SENTENCE_END.contains(text.charAt(length - 1))) {
            length--;
            isLastCharacterSpl = true;
        }
        for (int offset = 0; offset < length; offset++) {
            char ch = text.charAt(offset);
            if (isTokenSeperator(ch)) {
                if (startToken & token.length() > 0) {
                    originalTokens.add(token);
                    tokens.add(token);
                    tokenOffsets.add(new Tuple2<>(startIndex, endIndex));
                    token = new String("");
                    startToken = false;
                }
                if (!Character.isWhitespace(ch)) {
                    tokens.add("" + ch);
                    originalTokens.add("" + ch);
                    tokenOffsets.add(new Tuple2<>(offset, offset));
                }
            } else {
                if (!startToken) {
                    if (isAcceptedChar(ch)) {
                        startToken = true;
                        startIndex = offset;
                        endIndex = offset;
                        token = token + ch;
                    }
                } else {
                    if (isAcceptedChar(ch)) {
                        token = token + ch;
                    }
                    endIndex = endIndex + 1;
                }
            }
        }
        if (token.length() > 0) {
            originalTokens.add(token);
            tokens.add(token);
            tokenOffsets.add(new Tuple2<>(startIndex, endIndex));
        }
        if (isLastCharacterSpl) {
            char ch = text.charAt(length);
            originalTokens.add("" + ch);
            tokens.add("" + ch);
            tokenOffsets.add(new Tuple2<>(length, length));
        }
        return new TokenSet(tokens.toArray(new String[tokens.size()]), originalTokens.toArray(new String[originalTokens.size()]), tokenOffsets);
    }

    private boolean isAcceptedChar(char ch) {
        if (Character.isLetterOrDigit(ch) || ACCEPTED_SPECIAL_CHARS.contains(ch)) {
            return true;
        }
        return false;
    }

    private boolean isTokenSeperator(char ch) {
        if (Character.isWhitespace(ch) || TOKEN_SEPERATORS.contains(ch)) {
            return true;
        }
        return false;
    }

    public String getLexerAcceptedString(String token) {
        return token.replaceAll("[^a-zA-Z0-9]", " ").trim(); //all other char removal for now
    }

    public String[] getAddToLexerEffectiveTokens(String[] tokens) {
        List<String> effectiveTokens = new ArrayList<>();
        for (String token : tokens) {
            String tempToken = getLexerAcceptedString(token);
            if (tempToken.length() > 0) {
                effectiveTokens.add(tempToken);
            }
        }
        return effectiveTokens.toArray(new String[effectiveTokens.size()]);
    }
}
