package com.auxolabs.capture.positional.exceptions;

public class MissingPropertyException extends PositionalRuleException {
    public MissingPropertyException(String varName, String property) {
        super(String.format(varName));
    }
}
