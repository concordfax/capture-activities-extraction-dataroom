package com.auxolabs.capture.positional.utils.lexer.utils;


import com.auxolabs.capture.positional.utils.lexer.indexer.index.Tags;
import com.auxolabs.capture.positional.utils.lexer.indexer.types.TagModel;
import com.auxolabs.capture.positional.utils.lexer.indexer.types.TokenSet;

import java.io.File;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Utilities {

    public static final String INTER_SENTENCE = "/|_|-|\t|\"|\\,|\\;|\\:|\\(|\\)|\\s+|\\!";


    public static ArrayList<String> getSentencesFromText(final String text) {
        ArrayList<String> sentences = new ArrayList<>();
        BreakIterator iterator = BreakIterator.getSentenceInstance(Locale.US);
        iterator.setText(text);
        int start = iterator.first();
        for (int end = iterator.next();
             end != BreakIterator.DONE;
             start = end, end = iterator.next()) {
            sentences.add(text.substring(start, end));
        }
        return sentences;
    }

    public static Tags cleanTags(Tags nodeTags) {
        Tags annotationTags = new Tags();
        int beginOfTagMarker;
        for (TagModel tag : nodeTags) {
            String tagType = tag.getType();
            beginOfTagMarker = tagType.indexOf("$");
            String cleanedTagType = (beginOfTagMarker == -1 ? tagType : tagType.substring(0, tagType.indexOf("$")));
            if (!cleanedTagType.equals(tagType)) {
                TagModel cleanedTag = new TagModel(cleanedTagType);
                cleanedTag.setFeatures(tag.getFeatures());
                annotationTags.add(cleanedTag);
            } else {
                annotationTags.add(tag);
            }
        }
        annotationTags.setBrand(nodeTags.isBrand());
        return annotationTags;
    }

    public static String cleanTag(String tag) {
        int beginOfTagMarker;
        beginOfTagMarker = tag.indexOf("$");
        return beginOfTagMarker == -1 ? tag : tag.substring(0, tag.indexOf("$"));
    }

//    public static Lexer initialize(LotharConfig config) {
//        Lexer lexer = Lexer.getInstance();
//        lexer.indexDictionaryFiles(true, getFilesFromPaths(config.getBrandIndexFiles()));
//        lexer.indexDictionaryFiles(false, getFilesFromPaths(config.getSupportIndexFiles()));
//        return lexer;
//    }

    private static File[] getFilesFromPaths(List<String> filePaths) {
        File[] files = new File[filePaths.size()];
        for (int handle = 0; handle < filePaths.size(); handle++) {
            files[handle] = new File(filePaths.get(handle));
        }
        return files;
    }

    public static List<Integer> getTokensPositionsFromSentence(String sentence) {
        ArrayList<Integer> tokenPositions = new ArrayList<>();
//        PTBTokenizer<CoreLabel> ptbt = new PTBTokenizer<>(new StringReader(sentence),
//                new CoreLabelTokenFactory(), "");
//        while (ptbt.hasNext()) {
//            CoreLabel label = ptbt.next();
//            tokenPositions.add(label.beginPosition());
//        }

        Tokenizer tokenizer = new Tokenizer();
        TokenSet tokenSet = tokenizer.tokenize(sentence.trim());
        tokenSet.getTokenOffsets().forEach(integerIntegerTuple2 -> {
            tokenPositions.add(integerIntegerTuple2._1());
        });

        return tokenPositions;
    }


    public static ArrayList<Date> identifyDatesFromString(String sentence) {
        ArrayList<Date> allDates = new ArrayList<>();

//        Parser parser = new Parser();
//        List<DateGroup> groups = parser.parse(sentence);
//        for (DateGroup group : groups) {
//            allDates.addAll(group.getDates());
//        }

        return allDates;
    }
}

