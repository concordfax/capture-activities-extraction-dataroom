package com.auxolabs.capture.positional.models;

import lombok.Data;

@Data
public abstract class OpResultModel {
    @Override
    public abstract int hashCode();
}
