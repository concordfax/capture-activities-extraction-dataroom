package com.auxolabs.capture.positional.models;

import lombok.Getter;

public enum LexiconSpecialPropTypes {
    MAX_WORD_COUNT(Integer.class),
    MIN_WORD_COUNT(Integer.class),
    ;

    @Getter
    private Class klass;

    LexiconSpecialPropTypes(Class klass) {
        this.klass = klass;
    }
}
