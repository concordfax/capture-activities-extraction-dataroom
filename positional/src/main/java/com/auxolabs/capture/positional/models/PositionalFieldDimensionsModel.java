package com.auxolabs.capture.positional.models;

import com.concordfax.capture.core.models.extraction.positional.output.PositionalPropertyTypes;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class PositionalFieldDimensionsModel {
    private PositionalPropertyTypes prop;
    private List<PositionalFieldResultsModel> fieldResultsModels;

    public PositionalFieldDimensionsModel() {
        this.fieldResultsModels = new ArrayList<>();
    }
}
