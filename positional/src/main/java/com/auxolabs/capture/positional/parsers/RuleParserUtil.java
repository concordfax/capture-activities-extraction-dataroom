package com.auxolabs.capture.positional.parsers;

import com.auxolabs.capture.positional.exceptions.ThrowingErrorListener;
import com.auxolabs.capture.positional.operations.BaseFunction;
import com.auxolabs.capture.positional.operations.BaseOperation;
import lombok.extern.slf4j.Slf4j;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

import java.util.HashMap;

@Slf4j
public class RuleParserUtil {

    /**
     * @param functions list of functions that are used in the rule as a map of function name to the actual function
     * @param sRule the rule as a string which is to be parsed and returned as an operation
     * @return the rule that is passed is returned as RelationalOperation object that can be executed to the get the results
     */
    public static BaseOperation parseAndGetRule(HashMap<String, BaseFunction> functions, String sRule){
        try {
            PositionalRuleLexer prLexer = new PositionalRuleLexer(new ANTLRInputStream(sRule));
            prLexer.removeErrorListeners();
            prLexer.addErrorListener(ThrowingErrorListener.INSTANCE);
            PositionalRuleParser prParser = new PositionalRuleParser(new CommonTokenStream(prLexer));
            prParser.removeErrorListeners();
            prParser.addErrorListener(ThrowingErrorListener.INSTANCE);
            prParser.functions = functions;
            PositionalRuleParser.Relational_operationContext ec = prParser.relational_operation();
            return ec.op;
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    public static void parseAndAddFunction(HashMap<String, BaseFunction> functions, String sRule){
        try {
            PositionalRuleLexer prLexer = new PositionalRuleLexer(new ANTLRInputStream(sRule));
            prLexer.removeErrorListeners();
            prLexer.addErrorListener(ThrowingErrorListener.INSTANCE);
            PositionalRuleParser prParser = new PositionalRuleParser(new CommonTokenStream(prLexer));
            prParser.removeErrorListeners();
            prParser.addErrorListener(ThrowingErrorListener.INSTANCE);
            prParser.functions = functions;
            PositionalRuleParser.Function_defContext ec = prParser.function_def();
            functions.put(ec.funcName, ec.func);
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }


    public static Boolean checkIfRuleParsedSuccessfully(HashMap<String, BaseFunction> functions, String sRule){
        try {
            PositionalRuleLexer prLexer = new PositionalRuleLexer(new ANTLRInputStream(sRule));
            prLexer.removeErrorListeners();
            prLexer.addErrorListener(ThrowingErrorListener.INSTANCE);
            PositionalRuleParser prParser = new PositionalRuleParser(new CommonTokenStream(prLexer));
            prParser.removeErrorListeners();
            prParser.addErrorListener(ThrowingErrorListener.INSTANCE);
            prParser.functions = functions;
            PositionalRuleParser.Relational_operationContext ec = prParser.relational_operation();
            // if no errors till now and no tokens left to parse
            return prLexer.getAllTokens().size() == 0;
        }catch (Exception e){
            return false;
        }
    }

    public static void main(String[] args) {

    }
}
