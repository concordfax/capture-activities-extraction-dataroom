package com.auxolabs.capture.positional.operations;

import com.auxolabs.capture.positional.models.NumberOpResultModel;
import com.auxolabs.capture.positional.models.TokenModel;

import java.util.*;
import java.util.function.Function;

public class AbsOperation implements NumberBaseOperation {

    private NumberBaseOperation bo;
    private Function<Double, Double> func;

    public AbsOperation(NumberBaseOperation bo) {
        this.bo = bo;
        this.func = Math::abs;
    }

    @Override
    public HashSet<NumberOpResultModel> compute(HashMap<String, HashSet<TokenModel>> tokens) {
        HashSet<NumberOpResultModel> answers = new HashSet<>();

        HashSet<NumberOpResultModel> results = bo.compute(tokens);
        for (NumberOpResultModel result : results) {
            NumberOpResultModel numberOpResultModel = new NumberOpResultModel();
            numberOpResultModel.setResult(this.func.apply((Double) result.getResult()));
            numberOpResultModel.setTokens(new HashMap<>(result.getTokens()));
            answers.add(numberOpResultModel);
        }

        return answers;
    }

    @Override
    public void updateParams(HashMap<String, String> newParams) {
        this.bo.updateParams(newParams);
    }

    @Override
    public BaseOperation clone() {
        return new AbsOperation((NumberBaseOperation) bo.clone());
    }
}

