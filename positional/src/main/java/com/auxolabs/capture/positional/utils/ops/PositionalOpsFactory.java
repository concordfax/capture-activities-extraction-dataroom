package com.auxolabs.capture.positional.utils.ops;

import com.auxolabs.capture.positional.exceptions.PositionalRuleException;
import com.concordfax.capture.core.models.extraction.positional.output.PositionalOpTypes;

import java.util.HashMap;

public class PositionalOpsFactory {

    private static HashMap<PositionalOpTypes, BasePositionalOp> opObjects = new HashMap<>();

    public static void register(){
        create(PositionalOpTypes.SortByPropertyAsc);
    }

    private static void create(PositionalOpTypes positionalOpTypes){
        switch (positionalOpTypes){
            case SortByPropertyAsc:
                opObjects.put(positionalOpTypes, new SortByPropertyAsc());
        }
    }

    public static BasePositionalOp get(PositionalOpTypes type){
        if(!opObjects.containsKey(type))
            throw new PositionalRuleException(String.format("op with name %s not found", type));
        return opObjects.getOrDefault(type, null);
    }
}
