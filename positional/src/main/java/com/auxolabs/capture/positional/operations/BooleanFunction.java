package com.auxolabs.capture.positional.operations;

import java.util.HashMap;

public class BooleanFunction implements BaseFunction {

    private BooleanBaseOperation booleanBaseOperation;

    public BooleanFunction(BooleanBaseOperation booleanBaseOperation) {
        this.booleanBaseOperation = booleanBaseOperation;

    }

    @Override
    public BooleanBaseOperation apply(HashMap<String, String> newParams) {
        BooleanBaseOperation newObj = (BooleanBaseOperation) booleanBaseOperation.clone();
        newObj.updateParams(newParams);
        return newObj;
    }
}
