package com.auxolabs.capture.positional.utils;

import com.auxolabs.capture.positional.exceptions.FunctionNotFoundException;
import com.auxolabs.capture.positional.operations.BaseFunction;
import com.auxolabs.capture.positional.operations.BaseOperation;

import java.util.HashMap;
import java.util.List;

public class FunctionCaller {

    public static BaseOperation callFunction(HashMap<String, BaseFunction> functions, String funcName, List<String> variables) {
        HashMap<String, String> params = new HashMap<>();
        for (int i = 0; i < variables.size(); i++) {
            params.put(String.format("op.%d", i), variables.get(i));
        }
        if(!functions.containsKey(funcName)){
            throw new FunctionNotFoundException(funcName);
        }

        return functions.get(funcName).apply(params);
    }
}
