package com.auxolabs.capture.positional.models;

import com.auxolabs.capture.positional.constants.PositionalRuleConstants;
import com.auxolabs.capture.positional.exceptions.TokenPropertyNotFoundException;
import lombok.Data;

import java.util.HashMap;

@Data
public class TokenModel extends HashMap<String, Object>{
    private String tokenName;

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        TokenModel that = (TokenModel) o;
        return this.hashCode()==that.hashCode();
    }

    @Override
    public int hashCode() {
        if(this.containsKey(PositionalRuleConstants.Properties.ID)){
            return (Integer) this.get(PositionalRuleConstants.Properties.ID);
        }else {
            throw new TokenPropertyNotFoundException(PositionalRuleConstants.Properties.ID);
        }
    }
}
