package com.auxolabs.capture.positional.operations;

import com.auxolabs.capture.positional.models.NumberOpResultModel;
import com.auxolabs.capture.positional.models.TokenModel;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public interface NumberBaseOperation extends BaseOperation{
    HashSet<NumberOpResultModel> compute(HashMap<String, HashSet<TokenModel>> tokens);
}
