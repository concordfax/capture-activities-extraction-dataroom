package com.auxolabs.capture.positional.operations;

import com.auxolabs.capture.positional.constants.PositionalRuleConstants;
import com.auxolabs.capture.positional.models.BooleanOpResultModel;
import com.auxolabs.capture.positional.models.TokenModel;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.function.BiFunction;

@Slf4j
public class RelationalOperation implements BooleanBaseOperation {

    private BooleanBaseOperation bo1;
    private BooleanBaseOperation bo2;
    private BiFunction<Boolean, Boolean, Boolean> func;
    private String operator;

    public RelationalOperation(BooleanBaseOperation bo1, BooleanBaseOperation bo2, String operator) {
        this.bo1 = bo1;
        this.bo2 = bo2;
        this.func = getFunc(operator);
        this.operator = operator;
    }

    private BiFunction<Boolean, Boolean, Boolean> getFunc(String operator) {
        switch (operator) {
            case PositionalRuleConstants.RelationalOperators.AND:
                return (d1, d2) -> d1 && d2;
            case PositionalRuleConstants.RelationalOperators.OR:
                return (d1, d2) -> d1 || d2;
        }
        return null;
    }

    @Override
    public HashSet<BooleanOpResultModel> compute(HashMap<String, HashSet<TokenModel>> tokens) {
        HashSet<BooleanOpResultModel> answers = new HashSet<>();

        HashSet<BooleanOpResultModel> results1 = bo1.compute(tokens);
        HashSet<BooleanOpResultModel> results2 = bo2.compute(tokens);

        if(results1.isEmpty() || results2.isEmpty()){
            HashSet<BooleanOpResultModel> nonEmptyResults = results1.isEmpty() ? results2 : results1;
            for (BooleanOpResultModel resultBooleanOp : nonEmptyResults) {
                Boolean answer =  func.apply(false, resultBooleanOp.getResult());
                if(answer) {
                    answers.add(resultBooleanOp);
                }
            }
        }else if(operator.equals(PositionalRuleConstants.RelationalOperators.OR)){
            // we dont care about the answers from either side being false
            answers.addAll(results1);
            answers.addAll(results2);
        }else {
            for (BooleanOpResultModel numberOpResultModel1 : results1) {
                for (BooleanOpResultModel numberOpResultModel2 : results2) {
                    // find the keys in both the options
                    Set<String> set1 = new HashSet<>(numberOpResultModel1.getTokens().keySet());
                    Set<String> set2 = new HashSet<>(numberOpResultModel2.getTokens().keySet());

                    // if element present in both sets, it should be the same element
                    set1.retainAll(set2);
                    Set<String> intersection = set1;
                    boolean isAllCommonMatching = true;
                    for (String s : intersection) {
                        if (numberOpResultModel1.getTokens().get(s).hashCode() != numberOpResultModel2.getTokens().get(s).hashCode()) {
                            isAllCommonMatching = false;
                            break;
                        }
                    }

                    if (!isAllCommonMatching)
                        continue;

                    Boolean answer = func.apply(numberOpResultModel1.getResult(), numberOpResultModel2.getResult());
                    if (answer) {
                        BooleanOpResultModel numberOpResultModel = new BooleanOpResultModel();
                        numberOpResultModel.setResult(true);
                        numberOpResultModel.setTokens(new HashMap<>());
                        numberOpResultModel.getTokens().putAll(numberOpResultModel1.getTokens());
                        numberOpResultModel.getTokens().putAll(numberOpResultModel2.getTokens());
                        answers.add(numberOpResultModel);
                    } else {
//                    log.debug();
                    }
                }
            }
        }
        return answers;
    }

    @Override
    public void updateParams(HashMap<String, String> newParams) {
        this.bo1.updateParams(newParams);
        this.bo2.updateParams(newParams);
    }

    @Override
    public BaseOperation clone() {
        return new RelationalOperation((BooleanBaseOperation)bo1.clone(), (BooleanBaseOperation) bo2.clone(), operator);
    }
}
