package com.auxolabs.capture.positional.utils.lexer.indexer.index;

import java.util.List;

public class Annotation1 implements Comparable<Annotation1> {

    private String text;
    private Tags tags;
    private int localOffset;
    private int count;
    private List<String> tokens;
    private List<String> sentenceTokens;
    private int startIndex;
    private int endIndex;

    private boolean isNegation = false;

    public Annotation1(String text, Tags tags, int localOffset, List<String> tokens, List<String> sentenceTokens,
                       int startIndex, int endIndex) {
        this.text = text;
        this.tags = tags;
        this.localOffset = localOffset;
        this.tokens = tokens;
        this.sentenceTokens = sentenceTokens;
        this.startIndex = startIndex;
        this.endIndex = endIndex;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Tags getTags() {
        return tags;
    }

    public void setTags(Tags tags) {
        this.tags = tags;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getLocalOffset() {
        return localOffset;
    }

    public void setLocalOffset(int localOffset) {
        this.localOffset = localOffset;
    }

    public List<String> getTokens() {
        return tokens;
    }

    public void setTokens(List<String> tokens) {
        this.tokens = tokens;
    }

    public List<String> getSentenceTokens() {
        return sentenceTokens;
    }

    public void setSentenceTokens(List<String> sentenceTokens) {
        this.sentenceTokens = sentenceTokens;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public void setEndIndex(int endIndex) {
        this.endIndex = endIndex;
    }

    public boolean isNegation() {
        return isNegation;
    }

    public void setNegation(boolean negation) {
        isNegation = negation;
    }

    @Override
    public int hashCode() {
        return text.hashCode() * 10 + localOffset;
    }

    @Override
    public boolean equals(Object obj) {
        Annotation1 toCompareAnnotation = (Annotation1) obj;
        return toCompareAnnotation.text.equals(this.text) && toCompareAnnotation.localOffset == this.localOffset;
    }

    @Override
    public int compareTo(Annotation1 o) {
        return Integer.compare(this.startIndex, o.startIndex);
    }

    @Override
    public String toString() {
        return text + "-(" + startIndex + ", " + endIndex + ")";
    }
}
