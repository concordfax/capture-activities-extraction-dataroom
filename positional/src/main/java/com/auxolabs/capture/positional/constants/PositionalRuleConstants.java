package com.auxolabs.capture.positional.constants;

public class PositionalRuleConstants {
    public static final String VARIABLE_PROPERTY_SEPARATOR = "\\.";

    public static class RelationalOperators{
        public static final String OR = "or";
        public static final String AND = "and";
    }

    public static class ArithmeticOperators{
        public static final String PLUS = "+";
        public static final String MINUS = "-";
        public static final String MUL = "*";
        public static final String DIV = "/";
    }

    public static class Properties{
        public static final String X = "x";
        public static final String Y = "y";
        public static final String W = "w";
        public static final String H = "h";
        public static final String TEXT = "text";
        public static final String SPE_ANO = "speAno";
        public static final String ID = "id";
        public static final String ANNOTATION = "annotation";
    }

    public static class PageProps{
        public static final String PAGE_WIDTH = "pageWidth";
        public static final String PAGE_HEIGHT = "pageHeight";
        public static final String PAGE_PROPS = "page_props";
        public static final Integer PAGE_PROPS_ID = 00000;
    }

    public static class SpecialPerms{
        public static final int MAX_WORDS_IN_TAG_PATIENT_NAME = 5;
        public static final int MAX_WORDS_IN_TAG_PATIENT_DOB = 4;
        public static final int MAX_WORDS_IN_TAG_PATIENT_MRN = 3;
    }
}
