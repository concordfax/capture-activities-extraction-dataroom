package com.auxolabs.capture.positional.models;

import lombok.Data;

import java.util.HashMap;
import java.util.List;

@Data
public class LexiconModel {
    private String name;
    private HashMap<LexiconSpecialPropTypes, Object> props;
    private List<String> phrases;
    private List<String> regex;

}
