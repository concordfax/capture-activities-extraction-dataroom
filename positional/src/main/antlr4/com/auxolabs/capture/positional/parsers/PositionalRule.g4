grammar PositionalRule;


// TODO: 08/06/18 add parsers to use a rule in another rule; logic should be different from functions; as they are not reusable
@header{
    import java.util.HashMap;
    import java.util.function.Function;
    import com.auxolabs.capture.positional.operations.*;
    import com.auxolabs.capture.positional.utils.*;
}

@members{
    HashMap<String, BaseFunction> functions = null;
}

function_call returns [BaseOperation op]
    :  first_func_name=FUNCTION_NAME BRACE_OPEN m_vars=multi_variable BRACE_CLOSE { $op=FunctionCaller.callFunction(functions, $first_func_name.text, $m_vars.vars); }
    ;

// TODO: 04/06/18 remove multi-var implementation to var (comma var)*
multi_variable returns [List<String> vars]
    : first_var=VARIABLE { $vars = new ArrayList(); $vars.add($first_var.text); }
    | second_var=VARIABLE COMMA third_var=VARIABLE { $vars = new ArrayList(); $vars.add($second_var.text); $vars.add($third_var.text);}
    | fourth_var=VARIABLE COMMA fifth_var=VARIABLE COMMA sixth_var=VARIABLE { $vars = new ArrayList(); $vars.add($fourth_var.text); $vars.add($fifth_var.text); $vars.add($sixth_var.text);}
    ;

function_def returns [String funcName, BaseFunction func]
    : first=boolean_function_def { $funcName=$first.funcName; $func=$first.func; }
    | second=number_function_def { $funcName=$second.funcName; $func=$second.func; }
    ;

boolean_function_def returns [String funcName, BooleanFunction func]
    : FUNCTION_DEF first_func_name=FUNCTION_NAME CURLY_BRACE_OPEN (FUNCTION_RETURN)? first_op=relational_operation CURLY_BRACE_CLOSE { $funcName=$first_func_name.text; $func=new BooleanFunction($first_op.op); }
    | FUNCTION_DEF second_func_name=FUNCTION_NAME CURLY_BRACE_OPEN (FUNCTION_RETURN)? second_op=logical_operation CURLY_BRACE_CLOSE { $funcName=$second_func_name.text; $func=new BooleanFunction($second_op.op); }
    ;

number_function_def returns [String funcName, NumberFunction func]
    : FUNCTION_DEF first_func_name=FUNCTION_NAME CURLY_BRACE_OPEN (FUNCTION_RETURN)? first_op=number_operation CURLY_BRACE_CLOSE { $funcName=$first_func_name.text; $func=new NumberFunction($first_op.op); }
    ;

relational_operation returns [BooleanBaseOperation op]
    : BRACE_OPEN self=relational_operation BRACE_CLOSE {$op = $self.op; }
    | eigth=relational_operation REALTIONAL_OPERATOR ninth=relational_operation {$op = new RelationalOperation($eigth.op, $ninth.op, $REALTIONAL_OPERATOR.getText()); }
    | first=relational_operation REALTIONAL_OPERATOR second=logical_operation {$op = new RelationalOperation($second.op, $first.op, $REALTIONAL_OPERATOR.getText()); }
    | third=logical_operation REALTIONAL_OPERATOR fourth=relational_operation {$op = new RelationalOperation($fourth.op, $third.op, $REALTIONAL_OPERATOR.getText()); }
    | fifth=logical_operation REALTIONAL_OPERATOR sixth=logical_operation {$op = new RelationalOperation($sixth.op, $fifth.op, $REALTIONAL_OPERATOR.getText()); }
    | seventh=logical_operation { $op = (BooleanBaseOperation) $seventh.op; }
    | tenth=function_call { $op = (BooleanBaseOperation) $tenth.op; }
    | eleventh=not_operation {  $op = (BooleanBaseOperation) $eleventh.op; }
    ;

not_operation returns [BooleanBaseOperation op]
    : NOT_OPERATION BRACE_OPEN first=number_operation COMMA second=number_operation BRACE_CLOSE {$op = new NotOperation($first.op, $second.op); }
    ;

logical_operation returns [BooleanBaseOperation op]
    : first=number_operation LOGICAL_OPERATOR second=number_operation  {$op = new LogicalOperation($first.op, $second.op, $LOGICAL_OPERATOR.getText()); }
    ;

abs_operation returns [NumberBaseOperation op]
    : ABS_OPERATION BRACE_OPEN first=number_operation BRACE_CLOSE {$op = new AbsOperation($first.op); }
    ;

pow_operation returns [NumberBaseOperation op]
    : BRACE_OPEN first=number_operation BRACE_CLOSE POW second=NUMBER {$op = new PowOperation($first.op, Double.parseDouble($second.getText()));}
    ;

number_operation returns [NumberBaseOperation op]
    : BRACE_OPEN self=number_operation BRACE_CLOSE { $op= $self.op; }
    | first=pow_operation { $op= $first.op; }
    | second=abs_operation { $op= $second.op; }
    | third=arithmetic_operation { $op= $third.op; }
    | fourth=assignment_operation { $op= $fourth.op; }
    | fifth=function_call { $op = (NumberBaseOperation) $fifth.op; }
    ;

arithmetic_operation returns [NumberBaseOperation op]
    : BRACE_OPEN sixth=number_operation BRACE_CLOSE ARTHIMETIC_OPERATOR BRACE_OPEN seventh=number_operation BRACE_CLOSE {$op = new ArithmeticOperation($sixth.op, $seventh.op, $ARTHIMETIC_OPERATOR.getText()); }
    | BRACE_OPEN self=number_operation BRACE_CLOSE ARTHIMETIC_OPERATOR first=assignment_operation {$op = new ArithmeticOperation($self.op, $first.op, $ARTHIMETIC_OPERATOR.getText()); }
    | second=assignment_operation ARTHIMETIC_OPERATOR BRACE_OPEN third=number_operation BRACE_CLOSE {$op = new ArithmeticOperation($second.op, $third.op, $ARTHIMETIC_OPERATOR.getText()); }
    | fourth=assignment_operation ARTHIMETIC_OPERATOR fifth=assignment_operation {$op = new ArithmeticOperation($fourth.op, $fifth.op, $ARTHIMETIC_OPERATOR.getText()); }
    ;

assignment_operation returns [NumberBaseOperation op]
    : NUMBER {$op = new AssignmentOperation(Double.parseDouble($NUMBER.getText())); }
    | VARIABLE {$op = new AssignmentOperation($VARIABLE.getText()); }
    ;

FUNCTION_DEF : 'function';
COMMA: ',';
REALTIONAL_OPERATOR : ('and' | 'or' );
LOGICAL_OPERATOR: ('>' | '<' | '>=' | '<=' | '!=' ) ;
ARTHIMETIC_OPERATOR : '+' | '-' | '/' | '*';
POW: '^' ;
ABS_OPERATION : 'abs';
NOT_OPERATION: 'not';
NUMBER : [0-9]+ ;
SQUARE_BRACE_OPEN : '[' ;
SQUARE_BRACE_CLOSE : ']' ;
CURLY_BRACE_OPEN : '{' ;
CURLY_BRACE_CLOSE : '}' ;
BRACE_OPEN : '(' ;
BRACE_CLOSE : ')' ;
FUNCTION_RETURN: 'ret' ;
FUNCTION_NAME: 'func_'[a-zA-Z]+;
VARIABLE : ([\\_a-z]+('.'[a-z]+)? | 'op.'[0-9]+('.'[a-z]+)?);
WS
   : [ \r\n\t] + -> skip
   ;