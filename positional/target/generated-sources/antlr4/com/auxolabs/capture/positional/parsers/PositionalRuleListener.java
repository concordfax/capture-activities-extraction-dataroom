// Generated from com/auxolabs/capture/positional/parsers/PositionalRule.g4 by ANTLR 4.5
package com.auxolabs.capture.positional.parsers;

    import java.util.HashMap;
    import java.util.function.Function;
    import com.auxolabs.capture.positional.operations.*;
    import com.auxolabs.capture.positional.utils.*;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link PositionalRuleParser}.
 */
public interface PositionalRuleListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link PositionalRuleParser#function_call}.
	 * @param ctx the parse tree
	 */
	void enterFunction_call(PositionalRuleParser.Function_callContext ctx);
	/**
	 * Exit a parse tree produced by {@link PositionalRuleParser#function_call}.
	 * @param ctx the parse tree
	 */
	void exitFunction_call(PositionalRuleParser.Function_callContext ctx);
	/**
	 * Enter a parse tree produced by {@link PositionalRuleParser#multi_variable}.
	 * @param ctx the parse tree
	 */
	void enterMulti_variable(PositionalRuleParser.Multi_variableContext ctx);
	/**
	 * Exit a parse tree produced by {@link PositionalRuleParser#multi_variable}.
	 * @param ctx the parse tree
	 */
	void exitMulti_variable(PositionalRuleParser.Multi_variableContext ctx);
	/**
	 * Enter a parse tree produced by {@link PositionalRuleParser#function_def}.
	 * @param ctx the parse tree
	 */
	void enterFunction_def(PositionalRuleParser.Function_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link PositionalRuleParser#function_def}.
	 * @param ctx the parse tree
	 */
	void exitFunction_def(PositionalRuleParser.Function_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link PositionalRuleParser#boolean_function_def}.
	 * @param ctx the parse tree
	 */
	void enterBoolean_function_def(PositionalRuleParser.Boolean_function_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link PositionalRuleParser#boolean_function_def}.
	 * @param ctx the parse tree
	 */
	void exitBoolean_function_def(PositionalRuleParser.Boolean_function_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link PositionalRuleParser#number_function_def}.
	 * @param ctx the parse tree
	 */
	void enterNumber_function_def(PositionalRuleParser.Number_function_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link PositionalRuleParser#number_function_def}.
	 * @param ctx the parse tree
	 */
	void exitNumber_function_def(PositionalRuleParser.Number_function_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link PositionalRuleParser#relational_operation}.
	 * @param ctx the parse tree
	 */
	void enterRelational_operation(PositionalRuleParser.Relational_operationContext ctx);
	/**
	 * Exit a parse tree produced by {@link PositionalRuleParser#relational_operation}.
	 * @param ctx the parse tree
	 */
	void exitRelational_operation(PositionalRuleParser.Relational_operationContext ctx);
	/**
	 * Enter a parse tree produced by {@link PositionalRuleParser#not_operation}.
	 * @param ctx the parse tree
	 */
	void enterNot_operation(PositionalRuleParser.Not_operationContext ctx);
	/**
	 * Exit a parse tree produced by {@link PositionalRuleParser#not_operation}.
	 * @param ctx the parse tree
	 */
	void exitNot_operation(PositionalRuleParser.Not_operationContext ctx);
	/**
	 * Enter a parse tree produced by {@link PositionalRuleParser#logical_operation}.
	 * @param ctx the parse tree
	 */
	void enterLogical_operation(PositionalRuleParser.Logical_operationContext ctx);
	/**
	 * Exit a parse tree produced by {@link PositionalRuleParser#logical_operation}.
	 * @param ctx the parse tree
	 */
	void exitLogical_operation(PositionalRuleParser.Logical_operationContext ctx);
	/**
	 * Enter a parse tree produced by {@link PositionalRuleParser#abs_operation}.
	 * @param ctx the parse tree
	 */
	void enterAbs_operation(PositionalRuleParser.Abs_operationContext ctx);
	/**
	 * Exit a parse tree produced by {@link PositionalRuleParser#abs_operation}.
	 * @param ctx the parse tree
	 */
	void exitAbs_operation(PositionalRuleParser.Abs_operationContext ctx);
	/**
	 * Enter a parse tree produced by {@link PositionalRuleParser#pow_operation}.
	 * @param ctx the parse tree
	 */
	void enterPow_operation(PositionalRuleParser.Pow_operationContext ctx);
	/**
	 * Exit a parse tree produced by {@link PositionalRuleParser#pow_operation}.
	 * @param ctx the parse tree
	 */
	void exitPow_operation(PositionalRuleParser.Pow_operationContext ctx);
	/**
	 * Enter a parse tree produced by {@link PositionalRuleParser#number_operation}.
	 * @param ctx the parse tree
	 */
	void enterNumber_operation(PositionalRuleParser.Number_operationContext ctx);
	/**
	 * Exit a parse tree produced by {@link PositionalRuleParser#number_operation}.
	 * @param ctx the parse tree
	 */
	void exitNumber_operation(PositionalRuleParser.Number_operationContext ctx);
	/**
	 * Enter a parse tree produced by {@link PositionalRuleParser#arithmetic_operation}.
	 * @param ctx the parse tree
	 */
	void enterArithmetic_operation(PositionalRuleParser.Arithmetic_operationContext ctx);
	/**
	 * Exit a parse tree produced by {@link PositionalRuleParser#arithmetic_operation}.
	 * @param ctx the parse tree
	 */
	void exitArithmetic_operation(PositionalRuleParser.Arithmetic_operationContext ctx);
	/**
	 * Enter a parse tree produced by {@link PositionalRuleParser#assignment_operation}.
	 * @param ctx the parse tree
	 */
	void enterAssignment_operation(PositionalRuleParser.Assignment_operationContext ctx);
	/**
	 * Exit a parse tree produced by {@link PositionalRuleParser#assignment_operation}.
	 * @param ctx the parse tree
	 */
	void exitAssignment_operation(PositionalRuleParser.Assignment_operationContext ctx);
}