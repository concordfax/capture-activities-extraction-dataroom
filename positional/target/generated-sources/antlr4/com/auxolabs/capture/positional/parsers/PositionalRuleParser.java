// Generated from com/auxolabs/capture/positional/parsers/PositionalRule.g4 by ANTLR 4.5
package com.auxolabs.capture.positional.parsers;

    import java.util.HashMap;
    import java.util.function.Function;
    import com.auxolabs.capture.positional.operations.*;
    import com.auxolabs.capture.positional.utils.*;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class PositionalRuleParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		FUNCTION_DEF=1, COMMA=2, REALTIONAL_OPERATOR=3, LOGICAL_OPERATOR=4, ARTHIMETIC_OPERATOR=5, 
		POW=6, ABS_OPERATION=7, NOT_OPERATION=8, NUMBER=9, SQUARE_BRACE_OPEN=10, 
		SQUARE_BRACE_CLOSE=11, CURLY_BRACE_OPEN=12, CURLY_BRACE_CLOSE=13, BRACE_OPEN=14, 
		BRACE_CLOSE=15, FUNCTION_RETURN=16, FUNCTION_NAME=17, VARIABLE=18, WS=19;
	public static final int
		RULE_function_call = 0, RULE_multi_variable = 1, RULE_function_def = 2, 
		RULE_boolean_function_def = 3, RULE_number_function_def = 4, RULE_relational_operation = 5, 
		RULE_not_operation = 6, RULE_logical_operation = 7, RULE_abs_operation = 8, 
		RULE_pow_operation = 9, RULE_number_operation = 10, RULE_arithmetic_operation = 11, 
		RULE_assignment_operation = 12;
	public static final String[] ruleNames = {
		"function_call", "multi_variable", "function_def", "boolean_function_def", 
		"number_function_def", "relational_operation", "not_operation", "logical_operation", 
		"abs_operation", "pow_operation", "number_operation", "arithmetic_operation", 
		"assignment_operation"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'function'", "','", null, null, null, "'^'", "'abs'", "'not'", 
		null, "'['", "']'", "'{'", "'}'", "'('", "')'", "'ret'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "FUNCTION_DEF", "COMMA", "REALTIONAL_OPERATOR", "LOGICAL_OPERATOR", 
		"ARTHIMETIC_OPERATOR", "POW", "ABS_OPERATION", "NOT_OPERATION", "NUMBER", 
		"SQUARE_BRACE_OPEN", "SQUARE_BRACE_CLOSE", "CURLY_BRACE_OPEN", "CURLY_BRACE_CLOSE", 
		"BRACE_OPEN", "BRACE_CLOSE", "FUNCTION_RETURN", "FUNCTION_NAME", "VARIABLE", 
		"WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "PositionalRule.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }


	    HashMap<String, BaseFunction> functions = null;

	public PositionalRuleParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class Function_callContext extends ParserRuleContext {
		public BaseOperation op;
		public Token first_func_name;
		public Multi_variableContext m_vars;
		public TerminalNode BRACE_OPEN() { return getToken(PositionalRuleParser.BRACE_OPEN, 0); }
		public TerminalNode BRACE_CLOSE() { return getToken(PositionalRuleParser.BRACE_CLOSE, 0); }
		public TerminalNode FUNCTION_NAME() { return getToken(PositionalRuleParser.FUNCTION_NAME, 0); }
		public Multi_variableContext multi_variable() {
			return getRuleContext(Multi_variableContext.class,0);
		}
		public Function_callContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_call; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PositionalRuleListener ) ((PositionalRuleListener)listener).enterFunction_call(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PositionalRuleListener ) ((PositionalRuleListener)listener).exitFunction_call(this);
		}
	}

	public final Function_callContext function_call() throws RecognitionException {
		Function_callContext _localctx = new Function_callContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_function_call);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(26);
			((Function_callContext)_localctx).first_func_name = match(FUNCTION_NAME);
			setState(27);
			match(BRACE_OPEN);
			setState(28);
			((Function_callContext)_localctx).m_vars = multi_variable();
			setState(29);
			match(BRACE_CLOSE);
			 ((Function_callContext)_localctx).op = FunctionCaller.callFunction(functions, (((Function_callContext)_localctx).first_func_name!=null?((Function_callContext)_localctx).first_func_name.getText():null), ((Function_callContext)_localctx).m_vars.vars); 
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Multi_variableContext extends ParserRuleContext {
		public List<String> vars;
		public Token first_var;
		public Token second_var;
		public Token third_var;
		public Token fourth_var;
		public Token fifth_var;
		public Token sixth_var;
		public List<TerminalNode> VARIABLE() { return getTokens(PositionalRuleParser.VARIABLE); }
		public TerminalNode VARIABLE(int i) {
			return getToken(PositionalRuleParser.VARIABLE, i);
		}
		public List<TerminalNode> COMMA() { return getTokens(PositionalRuleParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(PositionalRuleParser.COMMA, i);
		}
		public Multi_variableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multi_variable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PositionalRuleListener ) ((PositionalRuleListener)listener).enterMulti_variable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PositionalRuleListener ) ((PositionalRuleListener)listener).exitMulti_variable(this);
		}
	}

	public final Multi_variableContext multi_variable() throws RecognitionException {
		Multi_variableContext _localctx = new Multi_variableContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_multi_variable);
		try {
			setState(44);
			switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(32);
				((Multi_variableContext)_localctx).first_var = match(VARIABLE);
				 ((Multi_variableContext)_localctx).vars =  new ArrayList(); _localctx.vars.add((((Multi_variableContext)_localctx).first_var!=null?((Multi_variableContext)_localctx).first_var.getText():null)); 
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(34);
				((Multi_variableContext)_localctx).second_var = match(VARIABLE);
				setState(35);
				match(COMMA);
				setState(36);
				((Multi_variableContext)_localctx).third_var = match(VARIABLE);
				 ((Multi_variableContext)_localctx).vars =  new ArrayList(); _localctx.vars.add((((Multi_variableContext)_localctx).second_var!=null?((Multi_variableContext)_localctx).second_var.getText():null)); _localctx.vars.add((((Multi_variableContext)_localctx).third_var!=null?((Multi_variableContext)_localctx).third_var.getText():null));
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(38);
				((Multi_variableContext)_localctx).fourth_var = match(VARIABLE);
				setState(39);
				match(COMMA);
				setState(40);
				((Multi_variableContext)_localctx).fifth_var = match(VARIABLE);
				setState(41);
				match(COMMA);
				setState(42);
				((Multi_variableContext)_localctx).sixth_var = match(VARIABLE);
				 ((Multi_variableContext)_localctx).vars =  new ArrayList(); _localctx.vars.add((((Multi_variableContext)_localctx).fourth_var!=null?((Multi_variableContext)_localctx).fourth_var.getText():null)); _localctx.vars.add((((Multi_variableContext)_localctx).fifth_var!=null?((Multi_variableContext)_localctx).fifth_var.getText():null)); _localctx.vars.add((((Multi_variableContext)_localctx).sixth_var!=null?((Multi_variableContext)_localctx).sixth_var.getText():null));
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_defContext extends ParserRuleContext {
		public String funcName;
		public BaseFunction func;
		public Boolean_function_defContext first;
		public Number_function_defContext second;
		public Boolean_function_defContext boolean_function_def() {
			return getRuleContext(Boolean_function_defContext.class,0);
		}
		public Number_function_defContext number_function_def() {
			return getRuleContext(Number_function_defContext.class,0);
		}
		public Function_defContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_def; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PositionalRuleListener ) ((PositionalRuleListener)listener).enterFunction_def(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PositionalRuleListener ) ((PositionalRuleListener)listener).exitFunction_def(this);
		}
	}

	public final Function_defContext function_def() throws RecognitionException {
		Function_defContext _localctx = new Function_defContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_function_def);
		try {
			setState(52);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(46);
				((Function_defContext)_localctx).first = boolean_function_def();
				 ((Function_defContext)_localctx).funcName = ((Function_defContext)_localctx).first.funcName; ((Function_defContext)_localctx).func = ((Function_defContext)_localctx).first.func; 
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(49);
				((Function_defContext)_localctx).second = number_function_def();
				 ((Function_defContext)_localctx).funcName = ((Function_defContext)_localctx).second.funcName; ((Function_defContext)_localctx).func = ((Function_defContext)_localctx).second.func; 
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Boolean_function_defContext extends ParserRuleContext {
		public String funcName;
		public BooleanFunction func;
		public Token first_func_name;
		public Relational_operationContext first_op;
		public Token second_func_name;
		public Logical_operationContext second_op;
		public TerminalNode FUNCTION_DEF() { return getToken(PositionalRuleParser.FUNCTION_DEF, 0); }
		public TerminalNode CURLY_BRACE_OPEN() { return getToken(PositionalRuleParser.CURLY_BRACE_OPEN, 0); }
		public TerminalNode CURLY_BRACE_CLOSE() { return getToken(PositionalRuleParser.CURLY_BRACE_CLOSE, 0); }
		public TerminalNode FUNCTION_NAME() { return getToken(PositionalRuleParser.FUNCTION_NAME, 0); }
		public Relational_operationContext relational_operation() {
			return getRuleContext(Relational_operationContext.class,0);
		}
		public TerminalNode FUNCTION_RETURN() { return getToken(PositionalRuleParser.FUNCTION_RETURN, 0); }
		public Logical_operationContext logical_operation() {
			return getRuleContext(Logical_operationContext.class,0);
		}
		public Boolean_function_defContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_boolean_function_def; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PositionalRuleListener ) ((PositionalRuleListener)listener).enterBoolean_function_def(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PositionalRuleListener ) ((PositionalRuleListener)listener).exitBoolean_function_def(this);
		}
	}

	public final Boolean_function_defContext boolean_function_def() throws RecognitionException {
		Boolean_function_defContext _localctx = new Boolean_function_defContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_boolean_function_def);
		int _la;
		try {
			setState(74);
			switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(54);
				match(FUNCTION_DEF);
				setState(55);
				((Boolean_function_defContext)_localctx).first_func_name = match(FUNCTION_NAME);
				setState(56);
				match(CURLY_BRACE_OPEN);
				setState(58);
				_la = _input.LA(1);
				if (_la==FUNCTION_RETURN) {
					{
					setState(57);
					match(FUNCTION_RETURN);
					}
				}

				setState(60);
				((Boolean_function_defContext)_localctx).first_op = relational_operation(0);
				setState(61);
				match(CURLY_BRACE_CLOSE);
				 ((Boolean_function_defContext)_localctx).funcName = (((Boolean_function_defContext)_localctx).first_func_name!=null?((Boolean_function_defContext)_localctx).first_func_name.getText():null); ((Boolean_function_defContext)_localctx).func = new BooleanFunction(((Boolean_function_defContext)_localctx).first_op.op); 
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(64);
				match(FUNCTION_DEF);
				setState(65);
				((Boolean_function_defContext)_localctx).second_func_name = match(FUNCTION_NAME);
				setState(66);
				match(CURLY_BRACE_OPEN);
				setState(68);
				_la = _input.LA(1);
				if (_la==FUNCTION_RETURN) {
					{
					setState(67);
					match(FUNCTION_RETURN);
					}
				}

				setState(70);
				((Boolean_function_defContext)_localctx).second_op = logical_operation();
				setState(71);
				match(CURLY_BRACE_CLOSE);
				 ((Boolean_function_defContext)_localctx).funcName = (((Boolean_function_defContext)_localctx).second_func_name!=null?((Boolean_function_defContext)_localctx).second_func_name.getText():null); ((Boolean_function_defContext)_localctx).func = new BooleanFunction(((Boolean_function_defContext)_localctx).second_op.op); 
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Number_function_defContext extends ParserRuleContext {
		public String funcName;
		public NumberFunction func;
		public Token first_func_name;
		public Number_operationContext first_op;
		public TerminalNode FUNCTION_DEF() { return getToken(PositionalRuleParser.FUNCTION_DEF, 0); }
		public TerminalNode CURLY_BRACE_OPEN() { return getToken(PositionalRuleParser.CURLY_BRACE_OPEN, 0); }
		public TerminalNode CURLY_BRACE_CLOSE() { return getToken(PositionalRuleParser.CURLY_BRACE_CLOSE, 0); }
		public TerminalNode FUNCTION_NAME() { return getToken(PositionalRuleParser.FUNCTION_NAME, 0); }
		public Number_operationContext number_operation() {
			return getRuleContext(Number_operationContext.class,0);
		}
		public TerminalNode FUNCTION_RETURN() { return getToken(PositionalRuleParser.FUNCTION_RETURN, 0); }
		public Number_function_defContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_number_function_def; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PositionalRuleListener ) ((PositionalRuleListener)listener).enterNumber_function_def(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PositionalRuleListener ) ((PositionalRuleListener)listener).exitNumber_function_def(this);
		}
	}

	public final Number_function_defContext number_function_def() throws RecognitionException {
		Number_function_defContext _localctx = new Number_function_defContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_number_function_def);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(76);
			match(FUNCTION_DEF);
			setState(77);
			((Number_function_defContext)_localctx).first_func_name = match(FUNCTION_NAME);
			setState(78);
			match(CURLY_BRACE_OPEN);
			setState(80);
			_la = _input.LA(1);
			if (_la==FUNCTION_RETURN) {
				{
				setState(79);
				match(FUNCTION_RETURN);
				}
			}

			setState(82);
			((Number_function_defContext)_localctx).first_op = number_operation();
			setState(83);
			match(CURLY_BRACE_CLOSE);
			 ((Number_function_defContext)_localctx).funcName = (((Number_function_defContext)_localctx).first_func_name!=null?((Number_function_defContext)_localctx).first_func_name.getText():null); ((Number_function_defContext)_localctx).func = new NumberFunction(((Number_function_defContext)_localctx).first_op.op); 
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Relational_operationContext extends ParserRuleContext {
		public BooleanBaseOperation op;
		public Relational_operationContext eigth;
		public Relational_operationContext first;
		public Logical_operationContext third;
		public Token REALTIONAL_OPERATOR;
		public Relational_operationContext fourth;
		public Relational_operationContext self;
		public Logical_operationContext fifth;
		public Logical_operationContext sixth;
		public Logical_operationContext seventh;
		public Function_callContext tenth;
		public Not_operationContext eleventh;
		public Relational_operationContext ninth;
		public Logical_operationContext second;
		public TerminalNode REALTIONAL_OPERATOR() { return getToken(PositionalRuleParser.REALTIONAL_OPERATOR, 0); }
		public List<Logical_operationContext> logical_operation() {
			return getRuleContexts(Logical_operationContext.class);
		}
		public Logical_operationContext logical_operation(int i) {
			return getRuleContext(Logical_operationContext.class,i);
		}
		public List<Relational_operationContext> relational_operation() {
			return getRuleContexts(Relational_operationContext.class);
		}
		public Relational_operationContext relational_operation(int i) {
			return getRuleContext(Relational_operationContext.class,i);
		}
		public TerminalNode BRACE_OPEN() { return getToken(PositionalRuleParser.BRACE_OPEN, 0); }
		public TerminalNode BRACE_CLOSE() { return getToken(PositionalRuleParser.BRACE_CLOSE, 0); }
		public Function_callContext function_call() {
			return getRuleContext(Function_callContext.class,0);
		}
		public Not_operationContext not_operation() {
			return getRuleContext(Not_operationContext.class,0);
		}
		public Relational_operationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_relational_operation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PositionalRuleListener ) ((PositionalRuleListener)listener).enterRelational_operation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PositionalRuleListener ) ((PositionalRuleListener)listener).exitRelational_operation(this);
		}
	}

	public final Relational_operationContext relational_operation() throws RecognitionException {
		return relational_operation(0);
	}

	private Relational_operationContext relational_operation(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Relational_operationContext _localctx = new Relational_operationContext(_ctx, _parentState);
		Relational_operationContext _prevctx = _localctx;
		int _startState = 10;
		enterRecursionRule(_localctx, 10, RULE_relational_operation, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(111);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				{
				setState(87);
				((Relational_operationContext)_localctx).third = logical_operation();
				setState(88);
				((Relational_operationContext)_localctx).REALTIONAL_OPERATOR = match(REALTIONAL_OPERATOR);
				setState(89);
				((Relational_operationContext)_localctx).fourth = relational_operation(5);
				((Relational_operationContext)_localctx).op =  new RelationalOperation(((Relational_operationContext)_localctx).fourth.op, ((Relational_operationContext)_localctx).third.op, ((Relational_operationContext)_localctx).REALTIONAL_OPERATOR.getText()); 
				}
				break;
			case 2:
				{
				setState(92);
				match(BRACE_OPEN);
				setState(93);
				((Relational_operationContext)_localctx).self = relational_operation(0);
				setState(94);
				match(BRACE_CLOSE);
				((Relational_operationContext)_localctx).op =  ((Relational_operationContext)_localctx).self.op; 
				}
				break;
			case 3:
				{
				setState(97);
				((Relational_operationContext)_localctx).fifth = logical_operation();
				setState(98);
				((Relational_operationContext)_localctx).REALTIONAL_OPERATOR = match(REALTIONAL_OPERATOR);
				setState(99);
				((Relational_operationContext)_localctx).sixth = logical_operation();
				((Relational_operationContext)_localctx).op =  new RelationalOperation(((Relational_operationContext)_localctx).sixth.op, ((Relational_operationContext)_localctx).fifth.op, ((Relational_operationContext)_localctx).REALTIONAL_OPERATOR.getText()); 
				}
				break;
			case 4:
				{
				setState(102);
				((Relational_operationContext)_localctx).seventh = logical_operation();
				 ((Relational_operationContext)_localctx).op =  (BooleanBaseOperation) ((Relational_operationContext)_localctx).seventh.op; 
				}
				break;
			case 5:
				{
				setState(105);
				((Relational_operationContext)_localctx).tenth = function_call();
				 ((Relational_operationContext)_localctx).op =  (BooleanBaseOperation) ((Relational_operationContext)_localctx).tenth.op; 
				}
				break;
			case 6:
				{
				setState(108);
				((Relational_operationContext)_localctx).eleventh = not_operation();
				  ((Relational_operationContext)_localctx).op =  (BooleanBaseOperation) ((Relational_operationContext)_localctx).eleventh.op; 
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(125);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(123);
					switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
					case 1:
						{
						_localctx = new Relational_operationContext(_parentctx, _parentState);
						_localctx.eigth = _prevctx;
						_localctx.eigth = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_relational_operation);
						setState(113);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(114);
						((Relational_operationContext)_localctx).REALTIONAL_OPERATOR = match(REALTIONAL_OPERATOR);
						setState(115);
						((Relational_operationContext)_localctx).ninth = relational_operation(8);
						((Relational_operationContext)_localctx).op =  new RelationalOperation(((Relational_operationContext)_localctx).eigth.op, ((Relational_operationContext)_localctx).ninth.op, ((Relational_operationContext)_localctx).REALTIONAL_OPERATOR.getText()); 
						}
						break;
					case 2:
						{
						_localctx = new Relational_operationContext(_parentctx, _parentState);
						_localctx.first = _prevctx;
						_localctx.first = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_relational_operation);
						setState(118);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(119);
						((Relational_operationContext)_localctx).REALTIONAL_OPERATOR = match(REALTIONAL_OPERATOR);
						setState(120);
						((Relational_operationContext)_localctx).second = logical_operation();
						((Relational_operationContext)_localctx).op =  new RelationalOperation(((Relational_operationContext)_localctx).second.op, ((Relational_operationContext)_localctx).first.op, ((Relational_operationContext)_localctx).REALTIONAL_OPERATOR.getText()); 
						}
						break;
					}
					} 
				}
				setState(127);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Not_operationContext extends ParserRuleContext {
		public BooleanBaseOperation op;
		public Number_operationContext first;
		public Number_operationContext second;
		public TerminalNode NOT_OPERATION() { return getToken(PositionalRuleParser.NOT_OPERATION, 0); }
		public TerminalNode BRACE_OPEN() { return getToken(PositionalRuleParser.BRACE_OPEN, 0); }
		public TerminalNode COMMA() { return getToken(PositionalRuleParser.COMMA, 0); }
		public TerminalNode BRACE_CLOSE() { return getToken(PositionalRuleParser.BRACE_CLOSE, 0); }
		public List<Number_operationContext> number_operation() {
			return getRuleContexts(Number_operationContext.class);
		}
		public Number_operationContext number_operation(int i) {
			return getRuleContext(Number_operationContext.class,i);
		}
		public Not_operationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_not_operation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PositionalRuleListener ) ((PositionalRuleListener)listener).enterNot_operation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PositionalRuleListener ) ((PositionalRuleListener)listener).exitNot_operation(this);
		}
	}

	public final Not_operationContext not_operation() throws RecognitionException {
		Not_operationContext _localctx = new Not_operationContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_not_operation);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(128);
			match(NOT_OPERATION);
			setState(129);
			match(BRACE_OPEN);
			setState(130);
			((Not_operationContext)_localctx).first = number_operation();
			setState(131);
			match(COMMA);
			setState(132);
			((Not_operationContext)_localctx).second = number_operation();
			setState(133);
			match(BRACE_CLOSE);
			((Not_operationContext)_localctx).op =  new NotOperation(((Not_operationContext)_localctx).first.op, ((Not_operationContext)_localctx).second.op); 
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Logical_operationContext extends ParserRuleContext {
		public BooleanBaseOperation op;
		public Number_operationContext first;
		public Token LOGICAL_OPERATOR;
		public Number_operationContext second;
		public TerminalNode LOGICAL_OPERATOR() { return getToken(PositionalRuleParser.LOGICAL_OPERATOR, 0); }
		public List<Number_operationContext> number_operation() {
			return getRuleContexts(Number_operationContext.class);
		}
		public Number_operationContext number_operation(int i) {
			return getRuleContext(Number_operationContext.class,i);
		}
		public Logical_operationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logical_operation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PositionalRuleListener ) ((PositionalRuleListener)listener).enterLogical_operation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PositionalRuleListener ) ((PositionalRuleListener)listener).exitLogical_operation(this);
		}
	}

	public final Logical_operationContext logical_operation() throws RecognitionException {
		Logical_operationContext _localctx = new Logical_operationContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_logical_operation);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(136);
			((Logical_operationContext)_localctx).first = number_operation();
			setState(137);
			((Logical_operationContext)_localctx).LOGICAL_OPERATOR = match(LOGICAL_OPERATOR);
			setState(138);
			((Logical_operationContext)_localctx).second = number_operation();
			((Logical_operationContext)_localctx).op =  new LogicalOperation(((Logical_operationContext)_localctx).first.op, ((Logical_operationContext)_localctx).second.op, ((Logical_operationContext)_localctx).LOGICAL_OPERATOR.getText()); 
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Abs_operationContext extends ParserRuleContext {
		public NumberBaseOperation op;
		public Number_operationContext first;
		public TerminalNode ABS_OPERATION() { return getToken(PositionalRuleParser.ABS_OPERATION, 0); }
		public TerminalNode BRACE_OPEN() { return getToken(PositionalRuleParser.BRACE_OPEN, 0); }
		public TerminalNode BRACE_CLOSE() { return getToken(PositionalRuleParser.BRACE_CLOSE, 0); }
		public Number_operationContext number_operation() {
			return getRuleContext(Number_operationContext.class,0);
		}
		public Abs_operationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_abs_operation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PositionalRuleListener ) ((PositionalRuleListener)listener).enterAbs_operation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PositionalRuleListener ) ((PositionalRuleListener)listener).exitAbs_operation(this);
		}
	}

	public final Abs_operationContext abs_operation() throws RecognitionException {
		Abs_operationContext _localctx = new Abs_operationContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_abs_operation);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(141);
			match(ABS_OPERATION);
			setState(142);
			match(BRACE_OPEN);
			setState(143);
			((Abs_operationContext)_localctx).first = number_operation();
			setState(144);
			match(BRACE_CLOSE);
			((Abs_operationContext)_localctx).op =  new AbsOperation(((Abs_operationContext)_localctx).first.op); 
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Pow_operationContext extends ParserRuleContext {
		public NumberBaseOperation op;
		public Number_operationContext first;
		public Token second;
		public TerminalNode BRACE_OPEN() { return getToken(PositionalRuleParser.BRACE_OPEN, 0); }
		public TerminalNode BRACE_CLOSE() { return getToken(PositionalRuleParser.BRACE_CLOSE, 0); }
		public TerminalNode POW() { return getToken(PositionalRuleParser.POW, 0); }
		public Number_operationContext number_operation() {
			return getRuleContext(Number_operationContext.class,0);
		}
		public TerminalNode NUMBER() { return getToken(PositionalRuleParser.NUMBER, 0); }
		public Pow_operationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pow_operation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PositionalRuleListener ) ((PositionalRuleListener)listener).enterPow_operation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PositionalRuleListener ) ((PositionalRuleListener)listener).exitPow_operation(this);
		}
	}

	public final Pow_operationContext pow_operation() throws RecognitionException {
		Pow_operationContext _localctx = new Pow_operationContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_pow_operation);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(147);
			match(BRACE_OPEN);
			setState(148);
			((Pow_operationContext)_localctx).first = number_operation();
			setState(149);
			match(BRACE_CLOSE);
			setState(150);
			match(POW);
			setState(151);
			((Pow_operationContext)_localctx).second = match(NUMBER);
			((Pow_operationContext)_localctx).op =  new PowOperation(((Pow_operationContext)_localctx).first.op, Double.parseDouble(((Pow_operationContext)_localctx).second.getText()));
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Number_operationContext extends ParserRuleContext {
		public NumberBaseOperation op;
		public Number_operationContext self;
		public Pow_operationContext first;
		public Abs_operationContext second;
		public Arithmetic_operationContext third;
		public Assignment_operationContext fourth;
		public Function_callContext fifth;
		public TerminalNode BRACE_OPEN() { return getToken(PositionalRuleParser.BRACE_OPEN, 0); }
		public TerminalNode BRACE_CLOSE() { return getToken(PositionalRuleParser.BRACE_CLOSE, 0); }
		public Number_operationContext number_operation() {
			return getRuleContext(Number_operationContext.class,0);
		}
		public Pow_operationContext pow_operation() {
			return getRuleContext(Pow_operationContext.class,0);
		}
		public Abs_operationContext abs_operation() {
			return getRuleContext(Abs_operationContext.class,0);
		}
		public Arithmetic_operationContext arithmetic_operation() {
			return getRuleContext(Arithmetic_operationContext.class,0);
		}
		public Assignment_operationContext assignment_operation() {
			return getRuleContext(Assignment_operationContext.class,0);
		}
		public Function_callContext function_call() {
			return getRuleContext(Function_callContext.class,0);
		}
		public Number_operationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_number_operation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PositionalRuleListener ) ((PositionalRuleListener)listener).enterNumber_operation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PositionalRuleListener ) ((PositionalRuleListener)listener).exitNumber_operation(this);
		}
	}

	public final Number_operationContext number_operation() throws RecognitionException {
		Number_operationContext _localctx = new Number_operationContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_number_operation);
		try {
			setState(174);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(154);
				match(BRACE_OPEN);
				setState(155);
				((Number_operationContext)_localctx).self = number_operation();
				setState(156);
				match(BRACE_CLOSE);
				 ((Number_operationContext)_localctx).op =  ((Number_operationContext)_localctx).self.op; 
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(159);
				((Number_operationContext)_localctx).first = pow_operation();
				 ((Number_operationContext)_localctx).op =  ((Number_operationContext)_localctx).first.op; 
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(162);
				((Number_operationContext)_localctx).second = abs_operation();
				 ((Number_operationContext)_localctx).op =  ((Number_operationContext)_localctx).second.op; 
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(165);
				((Number_operationContext)_localctx).third = arithmetic_operation();
				 ((Number_operationContext)_localctx).op =  ((Number_operationContext)_localctx).third.op; 
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(168);
				((Number_operationContext)_localctx).fourth = assignment_operation();
				 ((Number_operationContext)_localctx).op =  ((Number_operationContext)_localctx).fourth.op; 
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(171);
				((Number_operationContext)_localctx).fifth = function_call();
				 ((Number_operationContext)_localctx).op =  (NumberBaseOperation) ((Number_operationContext)_localctx).fifth.op; 
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Arithmetic_operationContext extends ParserRuleContext {
		public NumberBaseOperation op;
		public Number_operationContext sixth;
		public Token ARTHIMETIC_OPERATOR;
		public Number_operationContext seventh;
		public Number_operationContext self;
		public Assignment_operationContext first;
		public Assignment_operationContext second;
		public Number_operationContext third;
		public Assignment_operationContext fourth;
		public Assignment_operationContext fifth;
		public List<TerminalNode> BRACE_OPEN() { return getTokens(PositionalRuleParser.BRACE_OPEN); }
		public TerminalNode BRACE_OPEN(int i) {
			return getToken(PositionalRuleParser.BRACE_OPEN, i);
		}
		public List<TerminalNode> BRACE_CLOSE() { return getTokens(PositionalRuleParser.BRACE_CLOSE); }
		public TerminalNode BRACE_CLOSE(int i) {
			return getToken(PositionalRuleParser.BRACE_CLOSE, i);
		}
		public TerminalNode ARTHIMETIC_OPERATOR() { return getToken(PositionalRuleParser.ARTHIMETIC_OPERATOR, 0); }
		public List<Number_operationContext> number_operation() {
			return getRuleContexts(Number_operationContext.class);
		}
		public Number_operationContext number_operation(int i) {
			return getRuleContext(Number_operationContext.class,i);
		}
		public List<Assignment_operationContext> assignment_operation() {
			return getRuleContexts(Assignment_operationContext.class);
		}
		public Assignment_operationContext assignment_operation(int i) {
			return getRuleContext(Assignment_operationContext.class,i);
		}
		public Arithmetic_operationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arithmetic_operation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PositionalRuleListener ) ((PositionalRuleListener)listener).enterArithmetic_operation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PositionalRuleListener ) ((PositionalRuleListener)listener).exitArithmetic_operation(this);
		}
	}

	public final Arithmetic_operationContext arithmetic_operation() throws RecognitionException {
		Arithmetic_operationContext _localctx = new Arithmetic_operationContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_arithmetic_operation);
		try {
			setState(204);
			switch ( getInterpreter().adaptivePredict(_input,10,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(176);
				match(BRACE_OPEN);
				setState(177);
				((Arithmetic_operationContext)_localctx).sixth = number_operation();
				setState(178);
				match(BRACE_CLOSE);
				setState(179);
				((Arithmetic_operationContext)_localctx).ARTHIMETIC_OPERATOR = match(ARTHIMETIC_OPERATOR);
				setState(180);
				match(BRACE_OPEN);
				setState(181);
				((Arithmetic_operationContext)_localctx).seventh = number_operation();
				setState(182);
				match(BRACE_CLOSE);
				((Arithmetic_operationContext)_localctx).op =  new ArithmeticOperation(((Arithmetic_operationContext)_localctx).sixth.op, ((Arithmetic_operationContext)_localctx).seventh.op, ((Arithmetic_operationContext)_localctx).ARTHIMETIC_OPERATOR.getText()); 
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(185);
				match(BRACE_OPEN);
				setState(186);
				((Arithmetic_operationContext)_localctx).self = number_operation();
				setState(187);
				match(BRACE_CLOSE);
				setState(188);
				((Arithmetic_operationContext)_localctx).ARTHIMETIC_OPERATOR = match(ARTHIMETIC_OPERATOR);
				setState(189);
				((Arithmetic_operationContext)_localctx).first = assignment_operation();
				((Arithmetic_operationContext)_localctx).op =  new ArithmeticOperation(((Arithmetic_operationContext)_localctx).self.op, ((Arithmetic_operationContext)_localctx).first.op, ((Arithmetic_operationContext)_localctx).ARTHIMETIC_OPERATOR.getText()); 
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(192);
				((Arithmetic_operationContext)_localctx).second = assignment_operation();
				setState(193);
				((Arithmetic_operationContext)_localctx).ARTHIMETIC_OPERATOR = match(ARTHIMETIC_OPERATOR);
				setState(194);
				match(BRACE_OPEN);
				setState(195);
				((Arithmetic_operationContext)_localctx).third = number_operation();
				setState(196);
				match(BRACE_CLOSE);
				((Arithmetic_operationContext)_localctx).op =  new ArithmeticOperation(((Arithmetic_operationContext)_localctx).second.op, ((Arithmetic_operationContext)_localctx).third.op, ((Arithmetic_operationContext)_localctx).ARTHIMETIC_OPERATOR.getText()); 
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(199);
				((Arithmetic_operationContext)_localctx).fourth = assignment_operation();
				setState(200);
				((Arithmetic_operationContext)_localctx).ARTHIMETIC_OPERATOR = match(ARTHIMETIC_OPERATOR);
				setState(201);
				((Arithmetic_operationContext)_localctx).fifth = assignment_operation();
				((Arithmetic_operationContext)_localctx).op =  new ArithmeticOperation(((Arithmetic_operationContext)_localctx).fourth.op, ((Arithmetic_operationContext)_localctx).fifth.op, ((Arithmetic_operationContext)_localctx).ARTHIMETIC_OPERATOR.getText()); 
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Assignment_operationContext extends ParserRuleContext {
		public NumberBaseOperation op;
		public Token NUMBER;
		public Token VARIABLE;
		public TerminalNode NUMBER() { return getToken(PositionalRuleParser.NUMBER, 0); }
		public TerminalNode VARIABLE() { return getToken(PositionalRuleParser.VARIABLE, 0); }
		public Assignment_operationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignment_operation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PositionalRuleListener ) ((PositionalRuleListener)listener).enterAssignment_operation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PositionalRuleListener ) ((PositionalRuleListener)listener).exitAssignment_operation(this);
		}
	}

	public final Assignment_operationContext assignment_operation() throws RecognitionException {
		Assignment_operationContext _localctx = new Assignment_operationContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_assignment_operation);
		try {
			setState(210);
			switch (_input.LA(1)) {
			case NUMBER:
				enterOuterAlt(_localctx, 1);
				{
				setState(206);
				((Assignment_operationContext)_localctx).NUMBER = match(NUMBER);
				((Assignment_operationContext)_localctx).op =  new AssignmentOperation(Double.parseDouble(((Assignment_operationContext)_localctx).NUMBER.getText())); 
				}
				break;
			case VARIABLE:
				enterOuterAlt(_localctx, 2);
				{
				setState(208);
				((Assignment_operationContext)_localctx).VARIABLE = match(VARIABLE);
				((Assignment_operationContext)_localctx).op =  new AssignmentOperation(((Assignment_operationContext)_localctx).VARIABLE.getText()); 
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 5:
			return relational_operation_sempred((Relational_operationContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean relational_operation_sempred(Relational_operationContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 7);
		case 1:
			return precpred(_ctx, 6);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\25\u00d7\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3/\n\3\3\4\3\4\3\4\3\4\3\4\3\4\5"+
		"\4\67\n\4\3\5\3\5\3\5\3\5\5\5=\n\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\5\5"+
		"G\n\5\3\5\3\5\3\5\3\5\5\5M\n\5\3\6\3\6\3\6\3\6\5\6S\n\6\3\6\3\6\3\6\3"+
		"\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7"+
		"\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7r\n\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7"+
		"\3\7\3\7\3\7\7\7~\n\7\f\7\16\7\u0081\13\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b"+
		"\3\b\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3"+
		"\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f"+
		"\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u00b1\n\f\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r"+
		"\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3"+
		"\r\3\r\3\r\5\r\u00cf\n\r\3\16\3\16\3\16\3\16\5\16\u00d5\n\16\3\16\2\3"+
		"\f\17\2\4\6\b\n\f\16\20\22\24\26\30\32\2\2\u00e0\2\34\3\2\2\2\4.\3\2\2"+
		"\2\6\66\3\2\2\2\bL\3\2\2\2\nN\3\2\2\2\fq\3\2\2\2\16\u0082\3\2\2\2\20\u008a"+
		"\3\2\2\2\22\u008f\3\2\2\2\24\u0095\3\2\2\2\26\u00b0\3\2\2\2\30\u00ce\3"+
		"\2\2\2\32\u00d4\3\2\2\2\34\35\7\23\2\2\35\36\7\20\2\2\36\37\5\4\3\2\37"+
		" \7\21\2\2 !\b\2\1\2!\3\3\2\2\2\"#\7\24\2\2#/\b\3\1\2$%\7\24\2\2%&\7\4"+
		"\2\2&\'\7\24\2\2\'/\b\3\1\2()\7\24\2\2)*\7\4\2\2*+\7\24\2\2+,\7\4\2\2"+
		",-\7\24\2\2-/\b\3\1\2.\"\3\2\2\2.$\3\2\2\2.(\3\2\2\2/\5\3\2\2\2\60\61"+
		"\5\b\5\2\61\62\b\4\1\2\62\67\3\2\2\2\63\64\5\n\6\2\64\65\b\4\1\2\65\67"+
		"\3\2\2\2\66\60\3\2\2\2\66\63\3\2\2\2\67\7\3\2\2\289\7\3\2\29:\7\23\2\2"+
		":<\7\16\2\2;=\7\22\2\2<;\3\2\2\2<=\3\2\2\2=>\3\2\2\2>?\5\f\7\2?@\7\17"+
		"\2\2@A\b\5\1\2AM\3\2\2\2BC\7\3\2\2CD\7\23\2\2DF\7\16\2\2EG\7\22\2\2FE"+
		"\3\2\2\2FG\3\2\2\2GH\3\2\2\2HI\5\20\t\2IJ\7\17\2\2JK\b\5\1\2KM\3\2\2\2"+
		"L8\3\2\2\2LB\3\2\2\2M\t\3\2\2\2NO\7\3\2\2OP\7\23\2\2PR\7\16\2\2QS\7\22"+
		"\2\2RQ\3\2\2\2RS\3\2\2\2ST\3\2\2\2TU\5\26\f\2UV\7\17\2\2VW\b\6\1\2W\13"+
		"\3\2\2\2XY\b\7\1\2YZ\5\20\t\2Z[\7\5\2\2[\\\5\f\7\7\\]\b\7\1\2]r\3\2\2"+
		"\2^_\7\20\2\2_`\5\f\7\2`a\7\21\2\2ab\b\7\1\2br\3\2\2\2cd\5\20\t\2de\7"+
		"\5\2\2ef\5\20\t\2fg\b\7\1\2gr\3\2\2\2hi\5\20\t\2ij\b\7\1\2jr\3\2\2\2k"+
		"l\5\2\2\2lm\b\7\1\2mr\3\2\2\2no\5\16\b\2op\b\7\1\2pr\3\2\2\2qX\3\2\2\2"+
		"q^\3\2\2\2qc\3\2\2\2qh\3\2\2\2qk\3\2\2\2qn\3\2\2\2r\177\3\2\2\2st\f\t"+
		"\2\2tu\7\5\2\2uv\5\f\7\nvw\b\7\1\2w~\3\2\2\2xy\f\b\2\2yz\7\5\2\2z{\5\20"+
		"\t\2{|\b\7\1\2|~\3\2\2\2}s\3\2\2\2}x\3\2\2\2~\u0081\3\2\2\2\177}\3\2\2"+
		"\2\177\u0080\3\2\2\2\u0080\r\3\2\2\2\u0081\177\3\2\2\2\u0082\u0083\7\n"+
		"\2\2\u0083\u0084\7\20\2\2\u0084\u0085\5\26\f\2\u0085\u0086\7\4\2\2\u0086"+
		"\u0087\5\26\f\2\u0087\u0088\7\21\2\2\u0088\u0089\b\b\1\2\u0089\17\3\2"+
		"\2\2\u008a\u008b\5\26\f\2\u008b\u008c\7\6\2\2\u008c\u008d\5\26\f\2\u008d"+
		"\u008e\b\t\1\2\u008e\21\3\2\2\2\u008f\u0090\7\t\2\2\u0090\u0091\7\20\2"+
		"\2\u0091\u0092\5\26\f\2\u0092\u0093\7\21\2\2\u0093\u0094\b\n\1\2\u0094"+
		"\23\3\2\2\2\u0095\u0096\7\20\2\2\u0096\u0097\5\26\f\2\u0097\u0098\7\21"+
		"\2\2\u0098\u0099\7\b\2\2\u0099\u009a\7\13\2\2\u009a\u009b\b\13\1\2\u009b"+
		"\25\3\2\2\2\u009c\u009d\7\20\2\2\u009d\u009e\5\26\f\2\u009e\u009f\7\21"+
		"\2\2\u009f\u00a0\b\f\1\2\u00a0\u00b1\3\2\2\2\u00a1\u00a2\5\24\13\2\u00a2"+
		"\u00a3\b\f\1\2\u00a3\u00b1\3\2\2\2\u00a4\u00a5\5\22\n\2\u00a5\u00a6\b"+
		"\f\1\2\u00a6\u00b1\3\2\2\2\u00a7\u00a8\5\30\r\2\u00a8\u00a9\b\f\1\2\u00a9"+
		"\u00b1\3\2\2\2\u00aa\u00ab\5\32\16\2\u00ab\u00ac\b\f\1\2\u00ac\u00b1\3"+
		"\2\2\2\u00ad\u00ae\5\2\2\2\u00ae\u00af\b\f\1\2\u00af\u00b1\3\2\2\2\u00b0"+
		"\u009c\3\2\2\2\u00b0\u00a1\3\2\2\2\u00b0\u00a4\3\2\2\2\u00b0\u00a7\3\2"+
		"\2\2\u00b0\u00aa\3\2\2\2\u00b0\u00ad\3\2\2\2\u00b1\27\3\2\2\2\u00b2\u00b3"+
		"\7\20\2\2\u00b3\u00b4\5\26\f\2\u00b4\u00b5\7\21\2\2\u00b5\u00b6\7\7\2"+
		"\2\u00b6\u00b7\7\20\2\2\u00b7\u00b8\5\26\f\2\u00b8\u00b9\7\21\2\2\u00b9"+
		"\u00ba\b\r\1\2\u00ba\u00cf\3\2\2\2\u00bb\u00bc\7\20\2\2\u00bc\u00bd\5"+
		"\26\f\2\u00bd\u00be\7\21\2\2\u00be\u00bf\7\7\2\2\u00bf\u00c0\5\32\16\2"+
		"\u00c0\u00c1\b\r\1\2\u00c1\u00cf\3\2\2\2\u00c2\u00c3\5\32\16\2\u00c3\u00c4"+
		"\7\7\2\2\u00c4\u00c5\7\20\2\2\u00c5\u00c6\5\26\f\2\u00c6\u00c7\7\21\2"+
		"\2\u00c7\u00c8\b\r\1\2\u00c8\u00cf\3\2\2\2\u00c9\u00ca\5\32\16\2\u00ca"+
		"\u00cb\7\7\2\2\u00cb\u00cc\5\32\16\2\u00cc\u00cd\b\r\1\2\u00cd\u00cf\3"+
		"\2\2\2\u00ce\u00b2\3\2\2\2\u00ce\u00bb\3\2\2\2\u00ce\u00c2\3\2\2\2\u00ce"+
		"\u00c9\3\2\2\2\u00cf\31\3\2\2\2\u00d0\u00d1\7\13\2\2\u00d1\u00d5\b\16"+
		"\1\2\u00d2\u00d3\7\24\2\2\u00d3\u00d5\b\16\1\2\u00d4\u00d0\3\2\2\2\u00d4"+
		"\u00d2\3\2\2\2\u00d5\33\3\2\2\2\16.\66<FLRq}\177\u00b0\u00ce\u00d4";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}