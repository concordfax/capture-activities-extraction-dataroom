<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <groupId>com.concordfax.capture.activities</groupId>
        <artifactId>extraction</artifactId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>app</artifactId>
    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                    <annotationProcessorPaths>
                        <path>
                            <groupId>com.google.dagger</groupId>
                            <artifactId>dagger-compiler</artifactId>
                            <version>2.11</version>
                        </path>
                    </annotationProcessorPaths>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-shade-plugin</artifactId>
                <version>2.3</version>
                <configuration>
                    <createDependencyReducedPom>true</createDependencyReducedPom>
                    <filters>
                        <filter>
                            <artifact>*:*</artifact>
                            <excludes>
                                <exclude>META-INF/*.SF</exclude>
                                <exclude>META-INF/*.DSA</exclude>
                                <exclude>META-INF/*.RSA</exclude>
                            </excludes>
                        </filter>
                    </filters>
                </configuration>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>shade</goal>
                        </goals>
                        <configuration>
                            <transformers>
                                <transformer
                                        implementation="org.apache.maven.plugins.shade.resource.ServicesResourceTransformer"/>
                                <transformer
                                        implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                                    <mainClass>com.auxolabs.cvision.CaptureApplication</mainClass>
                                </transformer>
                            </transformers>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <dropwizard.version>1.1.0</dropwizard.version>
        <opencv.version>3.2.0-1.3</opencv.version>
        <leptonica.version>1.73-1.2</leptonica.version>
        <tesseract.version>3.04.01-1.2</tesseract.version>
        <stanford.version>3.9.2</stanford.version>
        <mongojack.version>2.8.2</mongojack.version>
        <dagger.version>2.11</dagger.version>
    </properties>

    <repositories>
        <repository>
            <id>com.springsource.repository.bundles.external</id>
            <name>SpringSource Enterprise Bundle Repository - External Bundle Releases</name>
            <url>http://repository.springsource.com/maven/bundles/external</url>
        </repository>
    </repositories>

    <dependencies>
        <dependency>
            <groupId>io.dropwizard</groupId>
            <artifactId>dropwizard-core</artifactId>
            <version>${dropwizard.version}</version>
        </dependency>
        <dependency>
            <groupId>io.dropwizard</groupId>
            <artifactId>dropwizard-jersey</artifactId>
            <version>${dropwizard.version}</version>
        </dependency>
        <dependency>
            <groupId>org.glassfish.hk2</groupId>
            <artifactId>hk2-api</artifactId>
            <version>2.5.0-b36</version>
        </dependency>
        <dependency>
            <groupId>com.smoketurner</groupId>
            <artifactId>dropwizard-swagger</artifactId>
            <version>1.1.0-1</version>
        </dependency>

        <dependency>
            <groupId>com.google.dagger</groupId>
            <artifactId>dagger</artifactId>
            <version>${dagger.version}</version>
        </dependency>

        <dependency>
            <groupId>com.google.dagger</groupId>
            <artifactId>dagger-compiler</artifactId>
            <version>${dagger.version}</version>
            <scope>provided</scope>
        </dependency>



        <dependency>
            <groupId>org.bytedeco.javacpp-presets</groupId>
            <artifactId>leptonica</artifactId>
            <version>${leptonica.version}</version>
        </dependency>


        <dependency>
            <groupId>org.bytedeco.javacpp-presets</groupId>
            <artifactId>opencv-platform</artifactId>
            <version>${opencv.version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.tika</groupId>
            <artifactId>tika-core</artifactId>
            <version>1.16</version>
        </dependency>

        <!-- Stanford -->
        <dependency>
            <groupId>edu.stanford.nlp</groupId>
            <artifactId>stanford-corenlp</artifactId>
            <version>${stanford.version}</version>
        </dependency>


        <dependency>
            <groupId>com.joestelmach</groupId>
            <artifactId>natty</artifactId>
            <version>0.11</version>
        </dependency>


        <dependency>
            <groupId>org.apache.sanselan</groupId>
            <artifactId>sanselan</artifactId>
            <version>0.97-incubator</version>
        </dependency>

        <dependency>
            <groupId>javax.media.jai</groupId>
            <artifactId>com.springsource.javax.media.jai.codec</artifactId>
            <version>1.1.3</version>
        </dependency>
        <dependency>
            <groupId>com.twelvemonkeys.imageio</groupId>
            <artifactId>imageio-tiff</artifactId>
            <version>3.4.1</version>
        </dependency>

        <dependency>
            <groupId>com.concordfax.capture.activities</groupId>
            <artifactId>mandrake</artifactId>
            <version>1.0-SNAPSHOT</version>
        </dependency>

        <dependency>
            <groupId>com.concordfax.capture.activities</groupId>
            <artifactId>positional</artifactId>
            <version>1.0-SNAPSHOT</version>
        </dependency>

        <dependency>
            <groupId>com.concordfax.capture.activities</groupId>
            <artifactId>annotator</artifactId>
            <version>1.0-SNAPSHOT</version>
        </dependency>
        <dependency>
            <groupId>com.concordfax.capture.activities</groupId>
            <artifactId>dpengine</artifactId>
            <version>1.0-SNAPSHOT</version>
        </dependency>
        <dependency>
            <groupId>info.debatty</groupId>
            <artifactId>java-string-similarity</artifactId>
            <version>RELEASE</version>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.10</version>
            <scope>provided</scope>
        </dependency>
    </dependencies>


</project>