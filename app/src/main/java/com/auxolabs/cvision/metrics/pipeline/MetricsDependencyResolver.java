package com.auxolabs.cvision.metrics.pipeline;

import com.concordfax.capture.core.utils.dependency.DependencyResolverApiResponse;
import com.concordfax.capture.core.utils.dependency.IDependencyResolverClient;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.List;

public class MetricsDependencyResolver implements IDependencyResolverClient {

    private Gson gson;
    private Type typeRef;

    @Inject
    public MetricsDependencyResolver() {
        this.typeRef = new TypeToken<List<DependencyResolverApiResponse>>() {
        }.getType();
        this.gson = new Gson();
    }


    @Override
    public List<DependencyResolverApiResponse> getActivityDependencies(String dependencyResolverApi, String documentId,
                                                                       String activityId, String partitionKey) {
        try (InputStream is = this.getClass().getClassLoader().getResourceAsStream("com/auxolabs/cvision/pipeline/metrics/dependency-resolver.api-response.json")) {
            return gson.fromJson(new InputStreamReader(is), typeRef);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
