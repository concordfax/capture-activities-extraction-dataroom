package com.auxolabs.cvision.metrics;

import com.concordfax.capture.core.web.helpers.HealthWatchable;
import com.google.gson.internal.LinkedTreeMap;
import org.apache.http.HttpStatus;

import javax.inject.Inject;
import java.util.Map;

public class DBHealthWatchable implements HealthWatchable {

    private LinkedTreeMap metrics;

    @Inject
    public DBHealthWatchable() {
        this.metrics = new LinkedTreeMap();
    }

    @Override
    public String name() {
        return "db";
    }

    @Override
    public Map result() {
        return metrics;
    }

    @Override
    public int httpStatus() {
        return HttpStatus.SC_OK;
    }

    @Override
    public boolean isHealthy() {
        return true;
    }
}
