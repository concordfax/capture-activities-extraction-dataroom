package com.auxolabs.cvision.metrics.pipeline;

import com.auxolabs.capture.annotator.AnnotatorService;
import com.auxolabs.capture.annotator.models.Annotation;
import com.auxolabs.capture.annotator.models.response.SentenceModel;
import com.concordfax.capture.core.models.extraction.annotators.ExtractionAnnotatorConfiguration;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MetricsAnnotationsService implements AnnotatorService {

    private Gson gson;
    private Type typeRef;

    @Inject
    public MetricsAnnotationsService() {
        this.typeRef = new TypeToken<List<Annotation>>() {
        }.getType();
        this.gson = new Gson();
    }

    @Override
    public Map<String, List<SentenceModel>> getAnnotatorToAnnotations(List<String> textList, ExtractionAnnotatorConfiguration annotatorConfiguration) {

        Map<String, List<Annotation>> annotatorToAnnotationsMap = new LinkedTreeMap<>();
        try (InputStream is = this.getClass().getClassLoader().getResourceAsStream("com/auxolabs/cvision/pipeline/metrics/ner.annotation.json")) {
            annotatorToAnnotationsMap.put("ner", gson.fromJson(new InputStreamReader(is), typeRef));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        try (InputStream is = this.getClass().getClassLoader().getResourceAsStream("com/auxolabs/cvision/pipeline/metrics/lexer.annotation.json")) {
            annotatorToAnnotationsMap.put("lexer", gson.fromJson(new InputStreamReader(is), typeRef));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        try (InputStream is = this.getClass().getClassLoader().getResourceAsStream("com/auxolabs/cvision/pipeline/metrics/datetime.annotation.json")) {
            annotatorToAnnotationsMap.put("datetime", gson.fromJson(new InputStreamReader(is), typeRef));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        try (InputStream is = this.getClass().getClassLoader().getResourceAsStream("com/auxolabs/cvision/pipeline/metrics/pos.annotation.json")) {
            annotatorToAnnotationsMap.put("pos", gson.fromJson(new InputStreamReader(is), typeRef));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        Map<String, List<SentenceModel>> annotatorToSentenceMap = new LinkedTreeMap<>();

        annotatorToAnnotationsMap.forEach((s, annotations) -> {
            List<SentenceModel> sentenceModels = new ArrayList<>();
            textList.forEach(string -> {
                SentenceModel sentenceModel = new SentenceModel();
                sentenceModel.setQueryString(string);
                sentenceModel.setAnnotations(annotatorToAnnotationsMap.get(s));
                sentenceModels.add(sentenceModel);
            });
            annotatorToSentenceMap.put(s, sentenceModels);
        });

        return annotatorToSentenceMap;
    }
}
