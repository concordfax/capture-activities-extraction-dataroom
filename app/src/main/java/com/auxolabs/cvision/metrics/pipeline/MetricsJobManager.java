package com.auxolabs.cvision.metrics.pipeline;

import com.concordfax.capture.core.jobmanager.JobManager;
import com.concordfax.capture.core.jobmanager.JobManagerConfig;
import com.concordfax.capture.core.pipeline.Pipeline;

import javax.inject.Inject;
import javax.inject.Named;

import static com.auxolabs.cvision.constants.CaptureAppConstants.Pipeline.METRICS_PIPELINE;

public class MetricsJobManager extends JobManager {

    @Inject
    public MetricsJobManager(JobManagerConfig jobManagerConfig, @Named(METRICS_PIPELINE) Pipeline healthCheckPipeline) {
        super(jobManagerConfig, healthCheckPipeline);
    }
}
