package com.auxolabs.cvision.metrics.pipeline;

import com.concordfax.capture.core.jobmanager.CaptureJob;
import com.concordfax.capture.core.web.helpers.HealthWatchable;
import com.google.gson.internal.LinkedTreeMap;
import lombok.Setter;
import org.apache.http.HttpStatus;

import javax.inject.Inject;
import java.util.Map;


@Setter
public class PipelineHealthWatchable implements HealthWatchable {

    private static final String METRIC_NAME = "pipeline";
    private static final String DOCUMENT_ID = "5cc6a366bc831709f08a0a1b";
    private static final String ACTIVITY_ID = "a6bbbeb2-f485-46e0-85ae-7a4fbc295088";

    private LinkedTreeMap metrics;
    private int httpStatus;

    private String failedStep;

    private MetricsJobManager jobManager;

    @Inject
    public PipelineHealthWatchable(MetricsJobManager metricsJobManager) {
        this.jobManager = metricsJobManager;
        this.metrics = new LinkedTreeMap();
        this.metrics.put("stepsCount", 8);
        this.metrics.put("pageCount", 3);
    }

    @Override
    public String name() {
        return METRIC_NAME;
    }

    @Override
    public Map result() {
        return this.metrics;
    }

    @Override
    public int httpStatus() {
        return httpStatus;
    }

    @Override
    public boolean isHealthy() {
        CaptureJob captureJob = new CaptureJob();
        captureJob.setDocumentId(DOCUMENT_ID);
        captureJob.setActivityId(ACTIVITY_ID);
        try {
            this.jobManager.createNewJob(captureJob);
            if (this.jobManager.getCompletedJobsCount().get() % 10 == 0)
                this.jobManager.reset();
            httpStatus = HttpStatus.SC_OK;
            return true;
        } catch (Exception e) {
            StringBuilder errorBuilder = new StringBuilder();
            if (e instanceof RuntimeException) {
                for (Object ex = e; ((Throwable) ex).getCause() != null && ex.hashCode() != ((Throwable) ex).getCause().hashCode(); ex = ((Throwable) ex).getCause()) {
                    addError(errorBuilder, e.getCause());
                }
            }

            addError(errorBuilder, e);
            System.err.println(errorBuilder.toString());
            httpStatus = HttpStatus.SC_INTERNAL_SERVER_ERROR;
            return false;
        }
    }

    private void addError(StringBuilder errorBuilder, Throwable e) {
        if (errorBuilder.length() > 0) {
            errorBuilder.append("\n");
        }

        errorBuilder.append(e.getMessage());
        errorBuilder.append("\n");

        for (int i = 0; i < e.getStackTrace().length; ++i) {
            errorBuilder.append(e.getStackTrace()[i].toString());
            if (i != e.getStackTrace().length - 1) {
                errorBuilder.append("\n");
            }
        }

    }
}
