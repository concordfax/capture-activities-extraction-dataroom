package com.auxolabs.cvision.metrics.pipeline;

import com.auxolabs.cvision.db.IDBService;
import com.concordfax.capture.core.models.document.DocumentCollectionModel;
import com.concordfax.capture.core.models.document.activity.ActivityResult;
import com.concordfax.capture.core.models.environment.EnvironmentCollectionModel;
import com.concordfax.capture.core.models.extraction.ExtractionProfileCollection;
import com.concordfax.capture.core.models.process.ProcessCollectionModel;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.inject.Inject;
import java.io.InputStream;

public class MetricsDB implements IDBService {

    private ObjectMapper objectMapper;

    @Inject
    public MetricsDB() {
        this.objectMapper = new ObjectMapper();
    }


    @Override
    public DocumentCollectionModel cccGetDocumentById(String documentId, String partitionKey) {
        try (InputStream is = this.getClass().getClassLoader().getResourceAsStream("com/auxolabs/cvision/pipeline/metrics/document.json")) {
            DocumentCollectionModel docObj = objectMapper.readValue(is, DocumentCollectionModel.class);
            return docObj;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ProcessCollectionModel cccGetProcessById(String processDefId,Double version) {
        try (InputStream is = this.getClass().getClassLoader().getResourceAsStream("com/auxolabs/cvision/pipeline/metrics/process.json")) {
            ProcessCollectionModel processObj = objectMapper.readValue(is, ProcessCollectionModel.class);
            return processObj;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public EnvironmentCollectionModel cccGetEnvironmentById(String envId) {
        try (InputStream is = this.getClass().getClassLoader().getResourceAsStream("com/auxolabs/cvision/pipeline/metrics/environment.json")) {
            EnvironmentCollectionModel envObj = objectMapper.readValue(is, EnvironmentCollectionModel.class);
            return envObj;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Boolean updateActivityResult(String documentId, Integer activityIdx, String partitionKey, ActivityResult resultModel) {
        return true;
    }

    @Override
    public ExtractionProfileCollection getExtractionProfile(String extractionProfileId) {
        try (InputStream is = this.getClass().getClassLoader().getResourceAsStream("com/auxolabs/cvision/pipeline/metrics/extraction.json")) {
            ExtractionProfileCollection extractionProfile = objectMapper.readValue(is, ExtractionProfileCollection.class);
            return extractionProfile;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
