package com.auxolabs.cvision.metrics.pipeline;

import com.auxolabs.cvision.pipeline.ExtractionPipelineMessage;
import com.concordfax.capture.core.jobmanager.CaptureJob;
import com.concordfax.capture.core.logging.LogMaker;
import com.concordfax.capture.core.pipeline.AbstractJob;
import com.concordfax.capture.core.pipeline.Pipeline;
import com.concordfax.capture.core.pipeline.PipelineStep;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class HealthCheckPipeline implements Pipeline {

    private List<PipelineStep> pipelineSteps;

    @Inject
    public HealthCheckPipeline(PipelineStep... pipelineSteps) {
        this.pipelineSteps = Arrays.asList(pipelineSteps);
    }

    @Override
    public void start(Integer integer) {

    }

    @Override
    public List<PipelineStep> steps() {
        return pipelineSteps;
    }

    @Override
    public AbstractJob submit(AbstractJob abstractJob) {
        assert abstractJob instanceof CaptureJob;
        CaptureJob captureJob = (CaptureJob) abstractJob;
        ExtractionPipelineMessage message = new ExtractionPipelineMessage();
        message.setActivityId(captureJob.getExtractionProfileId());
        message.setDocumentId(captureJob.getDocumentId());
        message.setJobRef(captureJob);
        message.setLogMaker(new LogMaker(captureJob.getDocumentId(), captureJob.getExtractionProfileId(), UUID.randomUUID().toString()));
        this.run(message);
        return abstractJob;
    }

    @Override
    public void stop() {

    }

}
