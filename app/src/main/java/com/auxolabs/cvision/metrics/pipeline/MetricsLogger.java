package com.auxolabs.cvision.metrics.pipeline;

import com.concordfax.capture.core.logging.ILogger;
import com.concordfax.capture.core.logging.LogLevels;

import javax.inject.Inject;
import java.util.Map;

public class MetricsLogger implements ILogger {

    @Inject
    public MetricsLogger() {
    }

    @Override
    public void trace(String s, LogLevels logLevels, Map<String, String> map) {
        // ignore
    }

    @Override
    public void error(Exception e, Map<String, String> map, Map<String, Double> map1) {
        // ignore
    }

    @Override
    public void event(String s, Map<String, String> map, Map<String, Double> map1) {
        // ignore
    }
}
