package com.auxolabs.cvision.metrics.pipeline;

import com.concordfax.capture.core.storage.blobstore.IBlobService;
import org.apache.commons.lang.NotImplementedException;

import javax.inject.Inject;
import java.io.InputStream;
import java.util.Map;

public class MetricsBlobService implements IBlobService {

    @Inject
    public MetricsBlobService() {
    }

    @Override
    public String putFile(InputStream inputStream, long l, String s, Map<String, String> map) throws Exception {
        throw new NotImplementedException();
    }

    @Override
    public InputStream getFile(String s, Map<String, String> map) throws Exception {
        return this.getClass().getClassLoader().getResourceAsStream(s);
    }
}
