package com.auxolabs.cvision.metrics.pipeline;

import com.concordfax.capture.core.utils.mlmodel.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.NotImplementedException;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;

public class MetricsModelServicesClient implements ModelClientService {

    private ObjectMapper objectMapper;
    private TypeReference<ResponseModel<PredictResponseModel>> typeRef;

    @Inject
    public MetricsModelServicesClient() {
        this.typeRef = new TypeReference<ResponseModel<PredictResponseModel>>() {
        };
        this.objectMapper = new ObjectMapper();
    }

    @Override
    public ResponseModel<PredictResponseModel> predict(String baseUri, PredictRequestModel request) throws IOException {
        try (InputStream is = this.getClass().getClassLoader().getResourceAsStream("com/auxolabs/cvision/pipeline/metrics/doc-class-result.json")) {
            return objectMapper.readValue(is, typeRef);
        }
    }

    @Override
    public ResponseModel<TrainResponseModel> startTraining(String s, TrainRequestModel trainRequestModel) throws IOException {
        throw new NotImplementedException();
    }

    @Override
    public ResponseModel<TrainStatusResponseModel> getTrainingStatus(String s) throws IOException {
        throw new NotImplementedException();
    }
}
