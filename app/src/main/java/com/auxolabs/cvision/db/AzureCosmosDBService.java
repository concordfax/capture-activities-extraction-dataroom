package com.auxolabs.cvision.db;

import com.concordfax.capture.core.db.dao.*;
import com.concordfax.capture.core.models.account.AccountProfileIdModel;
import com.concordfax.capture.core.models.document.DocumentActivityResultUpdateModel;
import com.concordfax.capture.core.models.document.DocumentCollectionModel;
import com.concordfax.capture.core.models.document.activity.ActivityResult;
import com.concordfax.capture.core.models.environment.EnvironmentCollectionModel;
import com.concordfax.capture.core.models.extraction.ExtractionProfileCollection;
import com.concordfax.capture.core.models.process.ProcessCollectionModel;

import javax.inject.Inject;

public class AzureCosmosDBService implements IDBService {

    private DocumentDAO documentDAO;
    private ProcessDAO processDAO;
    private EnvironmentDAO environmentDAO;
    private MLModelDAO mlModelDAO;
    private ExtractionDAO extractionDAO;
    private ProfileIDDAO profileIDDAO;

    @Inject
    public AzureCosmosDBService(DocumentDAO documentDAO, ProcessDAO processDAO, EnvironmentDAO environmentDAO,
                                MLModelDAO mlModelDAO, ExtractionDAO extractionDAO,ProfileIDDAO profileIDDAO) {
        this.documentDAO = documentDAO;
        this.processDAO = processDAO;
        this.environmentDAO = environmentDAO;
        this.mlModelDAO = mlModelDAO;
        this.extractionDAO = extractionDAO;
        this.profileIDDAO = profileIDDAO;
    }

    @Override
    public AccountProfileIdModel cccGetProfileIdForAccountId(String accountID,String envId){
        try{
            AccountProfileIdModel apidModel = profileIDDAO.getExtractionProfileId(accountID,envId);
            return apidModel;
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public DocumentCollectionModel cccGetDocumentById(String documentId, String partitionKey) {
        try {
            DocumentCollectionModel docObj = documentDAO.getDocument(documentId, partitionKey);
            return docObj;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ProcessCollectionModel cccGetProcessById(String processDefId,Double version) {
        try {
            ProcessCollectionModel processObj = processDAO.getProcessById(processDefId,version);
            return processObj;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public EnvironmentCollectionModel cccGetEnvironmentById(String envId) {
        try {
            EnvironmentCollectionModel envObj = environmentDAO.getEnvironment(envId);
            return envObj;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Boolean updateActivityResult(String documentId, Integer activityIdx, String partitionKey, ActivityResult resultModel) {
        try {
            DocumentActivityResultUpdateModel documentActivityResultUpdateModel = new DocumentActivityResultUpdateModel();
            documentActivityResultUpdateModel.setActivityIndex(activityIdx);
            documentActivityResultUpdateModel.setDocumentId(documentId);
            documentActivityResultUpdateModel.setResult(resultModel);
            documentActivityResultUpdateModel.setPartitionKey(partitionKey);
            Boolean updateResultObj = documentDAO.setActivityResult(documentActivityResultUpdateModel);
            return updateResultObj;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ExtractionProfileCollection getExtractionProfile(String extractionProfileId) {
        try {
            ExtractionProfileCollection positionalProfile = extractionDAO.getExtractionProfile(extractionProfileId);
            return positionalProfile;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ExtractionProfileCollection putExtractionProfile(ExtractionProfileCollection extractionProfile) {
        try {
            ExtractionProfileCollection newPositionalProfile = extractionDAO.putExtractionProfile(extractionProfile);
            return newPositionalProfile;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
