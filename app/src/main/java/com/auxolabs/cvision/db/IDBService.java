package com.auxolabs.cvision.db;

import com.concordfax.capture.core.models.account.AccountProfileIdModel;
import com.concordfax.capture.core.models.document.DocumentCollectionModel;
import com.concordfax.capture.core.models.document.activity.ActivityResult;
import com.concordfax.capture.core.models.environment.EnvironmentCollectionModel;
import com.concordfax.capture.core.models.extraction.ExtractionProfileCollection;
import com.concordfax.capture.core.models.process.ProcessCollectionModel;
import org.apache.commons.lang.NotImplementedException;

public interface IDBService {

    default AccountProfileIdModel cccGetProfileIdForAccountId(String accountId,String envId){throw new NotImplementedException();}

    // updated CCC interfaces before integration
    default DocumentCollectionModel cccGetDocumentById(String documentId, String partitionKey) {
        throw new NotImplementedException();
    }

    default ProcessCollectionModel cccGetProcessById(String processDefId,Double version) {
        throw new NotImplementedException();
    }

    default EnvironmentCollectionModel cccGetEnvironmentById(String envId) {
        throw new NotImplementedException();
    }

    default Boolean updateActivityResult(String documentId, Integer activityIdx, String partitionKey, ActivityResult resultModel) {
        throw new NotImplementedException();
    }

    default ExtractionProfileCollection getExtractionProfile(String extractionProfileId) {
        throw new NotImplementedException();
    }

    default ExtractionProfileCollection putExtractionProfile(ExtractionProfileCollection extractionProfile) {
        throw new NotImplementedException();
    }

    default ExtractionProfileCollection deleteExtractionProfileIfExists(ExtractionProfileCollection extractionProfile) {
        throw new NotImplementedException();
    }

}
