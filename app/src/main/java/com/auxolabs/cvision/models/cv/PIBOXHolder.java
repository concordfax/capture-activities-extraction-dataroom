package com.auxolabs.cvision.models.cv;

import com.auxolabs.capture.annotator.models.Annotation;
import com.auxolabs.capture.annotator.models.TokenPosition;
import lombok.Getter;
import lombok.Setter;
import org.bytedeco.javacpp.lept;

import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

public class PIBOXHolder implements Comparable<PIBOXHolder> {
    private String text;

    private lept.PIX pix;
    private lept.BOX box;

    private Rectangle2D.Double rect;

    private TokenPosition tokenPosition;

    private ArrayList<ArrayList<PBText>> lines = new ArrayList<>();

    @Getter
    @Setter
    private boolean isVisited;

    @Getter
    @Setter
    private boolean sameBlockText;

    @Getter
    @Setter
    private List<PBText2> words;

    @Getter
    @Setter
    private String tempOrgText;


    private String tempInfo;
    private List<Annotation> annotationResults = new ArrayList<>();


    public PIBOXHolder() {

    }

    public PIBOXHolder(lept.PIX pix) {
        this.pix = pix;
        this.rect = new Rectangle2D.Double(pix.w(), pix.h(), pix.w(), pix.h());
    }

    public PIBOXHolder(lept.PIX pix, lept.BOX box) {
        this.pix = pix;
        this.box = box;
        this.rect = new Rectangle2D.Double(box.x(), box.y(), box.w(), box.h());
    }

    public static PIBOXHolder CreateEmpty(lept.BOX box) {
        PIBOXHolder piboxHolder = new PIBOXHolder();
        piboxHolder.rect = new Rectangle2D.Double(box.x(), box.y(), box.w(), box.h());
        return piboxHolder;
    }

    public lept.PIX getPix() {
        return pix;
    }

    public void setPix(lept.PIX pix) {
        this.pix = pix;
        this.rect = new Rectangle2D.Double(pix.w(), pix.h(), pix.w(), pix.h());
    }

    public lept.BOX getBox() {
        return box;
    }

    public void setBox(lept.BOX box) {
        this.box = box;
    }

    public Double x() {
        return rect.getX();
    }

    public Double y() {
        return rect.getY();
    }

    public Double w() {
        return rect.getWidth();
    }

    public Double h() {
        return rect.getHeight();
    }

    public void x(Double x) {
        rect.x = x;
    }

    public void y(Double y) {
        rect.y = y;
    }

    public void w(Double w) {
        rect.width = w;
    }

    public void h(Double h) {
        rect.height = h;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Rectangle2D getRect() {
        return rect;
    }

    public void setRect(Double x, Double y, Double w, Double h) {
        this.rect = new Rectangle2D.Double(x, y, w, h);
    }

    public void setRect(Integer x, Integer y, Integer w, Integer h) {
        this.rect = new Rectangle2D.Double(x, y, w, h);
    }

    public ArrayList<ArrayList<PBText>> getLines() {
        return lines;
    }

    public void setLines(ArrayList<ArrayList<PBText>> lines) {
        this.lines = lines;
    }

    public TokenPosition getTokenPosition() {
        return tokenPosition;
    }

    public void setTokenPosition(TokenPosition tokenPosition) {
        this.tokenPosition = tokenPosition;
    }

    public boolean notEquals(PIBOXHolder piboxHolder) {
        return !equals(piboxHolder);
    }

    @Override
    public boolean equals(Object obj) {
        try {
            PIBOXHolder o = (PIBOXHolder) obj;
            if (this.x().equals(o.x()) && this.y().equals(o.y())) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public int compareTo(PIBOXHolder o) {
        if (this.x().equals(o.x()) && this.y().equals(o.y())) {
            return 0;
        } else if (this.x().compareTo(o.x()) < 0 && this.y().compareTo(o.y()) < 0) {
            return -1;
        } else {
            return 1;
        }
    }

    public String getTempInfo() {
        return tempInfo;
    }

    public void setTempInfo(String tempInfo) {
        this.tempInfo = tempInfo;
    }

    public List<Annotation> getAnnotationResults(){
        return annotationResults;
    }


    @Override
    public String toString() {
        if (lines != null && !lines.isEmpty() && lines.get(0) != null && !lines.get(0).isEmpty() && lines.get(0).get(0) != null)
            return "text -> " + lines.get(0).get(0).getText();
        return text;
    }
}
