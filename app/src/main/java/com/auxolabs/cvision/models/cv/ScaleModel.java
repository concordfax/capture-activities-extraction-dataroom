package com.auxolabs.cvision.models.cv;

import lombok.Data;

@Data
public class ScaleModel {
    private Float scalingFactor = 1.0f;
    private boolean isScaled = false;
}
