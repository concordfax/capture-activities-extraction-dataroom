package com.auxolabs.cvision.models.cv;

import lombok.Data;

@Data
public class PBText2 {
    private Integer x;
    private Integer y;
    private Integer w;
    private Integer h;
    private String text;
    private String fontFamily;
    private Double fontSize;
    private boolean isBold;
    private boolean isItalic;
    private boolean isUnderline;
    private Double confidence;
    private Integer paraNumber;
    private Integer lineNumber;
    private Integer l;
    private Integer t;
    private Integer r;
    private Integer b;
    private boolean isWordFirst;

    private boolean isVisited;

    public PBText2() {
    }

    public PBText2(String text, String fontFamily, Integer fontSize, boolean isBold, boolean isItalic, boolean isUnderline) {
        this.text = text;
        this.fontFamily = fontFamily;
        this.fontSize = fontSize * 1.0;
        this.isBold = isBold;
        this.isItalic = isItalic;
        this.isUnderline = isUnderline;
    }

    @Override
    public String toString() {
        return String.format("{%s -> (%d, %d, %d, %d)}", text, x, y, w, h);
    }
}
