package com.auxolabs.cvision.models.ocr;

import com.auxolabs.capture.annotator.models.Annotation;
import com.auxolabs.cvision.streamlining.models.ImageSegment;
import com.concordfax.capture.core.models.ocr.abbyy.Block;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class SegBlock {
    private int blockId;
    private Block block;
    private List<Para> paragraphList;
    private boolean isBold;
    private String value;
    private int l;
    private int t;
    private int r;
    private int b;
    private HashMap<String,List<Annotation>> blockAnnotations;
    private List<ImageSegment> mappedSegments = new ArrayList<>();
    @Override
    public String toString() {
        return "SegBlock{" +
                "value='" + value + '\'' +
                '}';
    }
}
