package com.auxolabs.cvision.models.ocr;

import com.concordfax.capture.core.models.ocr.abbyy.Formatting;
import lombok.Data;

import java.util.List;

@Data
public class Segment {
    private int l;
    private int t;
    private int r;
    private int b;
    private String value;
    private Formatting formatting;
}
