package com.auxolabs.cvision.models.ocr;

import com.auxolabs.cvision.streamlining.models.ImageSegment;
import com.concordfax.capture.core.models.ocr.abbyy.Paragraph;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Para {
    private int paraId;

    private int blockId;

    private int l;
    private int t;
    private int r;
    private int b;
    private String value;
    private boolean isBold;
    private Paragraph paragraphRef;
    private List<Lines> lines;
    private List<ImageSegment> mappedSegments = new ArrayList<>();
}
