package com.auxolabs.cvision.models.ocr;

import lombok.Data;

@Data
public class Point {
    private int l;
    private int t;
    private int r;
    private int b;

    public Point() {
    }

    public Point(int l, int t, int r, int b) {
        this.l = l;
        this.t = t;
        this.r = r;
        this.b = b;
    }

    public void reset() {
        this.l = -1;
        this.t = -1;
        this.r = -1;
        this.b = -1;
    }
}
