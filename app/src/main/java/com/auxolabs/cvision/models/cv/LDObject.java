package com.auxolabs.cvision.models.cv;

import lombok.Data;

@Data
public class LDObject {
    private PIBOXHolder linesInImage;
    private PIBOXHolder imgWithoutLines;
}
