package com.auxolabs.cvision.models.ocr;

import com.auxolabs.cvision.streamlining.models.ImageSegment;
import lombok.Data;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
public class Lines {
    private int lineID;

    private int paraId;
    private int blockId;

    private String value;
    private int l;
    private int b;
    private int r;
    private int t;
    private int baseLine;
    private boolean isBold;
    private List<Segment> segments;
    private List<ImageSegment> mappedSegments = new ArrayList<>();

    private boolean inValidate;

    public int x() {
        return l;
    }

    public int y() {
        return t;
    }

    public Double w() {
        return (r-l)*1.0;
    }

    public Double h() {
        return (b-t)*1.0;
    }

    private String _id = UUID.randomUUID().toString();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Lines lines = (Lines) o;

        return new EqualsBuilder()
                .append(_id, lines._id)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(_id)
                .toHashCode();
    }
}
