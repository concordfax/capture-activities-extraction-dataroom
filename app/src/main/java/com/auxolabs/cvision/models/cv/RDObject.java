package com.auxolabs.cvision.models.cv;

import lombok.Data;

@Data
public class RDObject {
    private PIBOXHolder textInImage;
    private PIBOXHolder imagesInImage;
}
