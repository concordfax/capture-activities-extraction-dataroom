package com.auxolabs.cvision.models.nlp;

import com.auxolabs.cvision.models.cv.PBText;
import com.auxolabs.cvision.models.cv.PBText2;
import com.auxolabs.cvision.models.cv.PIBOXHolder;
import com.auxolabs.cvision.models.ocr.SegBlock;
import com.auxolabs.cvision.streamlining.models.ImageSegment;
import com.auxolabs.cvision.models.cv.ScaleModel;

import com.concordfax.capture.core.models.document.result.extraction.ExtractionField;
import com.concordfax.capture.core.models.extraction.DocumentTypeMapping;
import com.concordfax.capture.core.models.extraction.output.ExtractorType;
import com.concordfax.capture.core.models.ocr.abbyy.*;
import com.google.common.util.concurrent.AtomicDouble;
import lombok.Data;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

@Data
public class PageModel {

    private Integer pageNumber;
    private Integer pageWidth;
    private Integer pageHeight;

    private List<SegBlock> blockList;
    private List<ImageSegment> imageSegments;
    private List<PBText2> words;

    private DocumentTypeMapping documentTypeMapping;

    private HashMap<ExtractorType, HashMap<String, ExtractionField>> extractorToRuleField = new HashMap<>();

    private ScaleModel scaleModel;

    public PageModel(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public void setPageProps(Integer pageWidth, Integer pageHeight){
        this.pageWidth = pageWidth;
        this.pageHeight = pageHeight;
    }

    public void setPageNumber(int pageNumber)  {
        this.pageNumber = pageNumber;
    }

    private Point getPointForFormatting(Formatting formatting) {
        Point formattingPoint = new Point(-1, -1, -1, -1);
        if (formatting.getCharParams() != null) {
            formatting.getCharParams().forEach(charParams -> {
                setLocationBasedOnChar(formattingPoint, charParams);
            });
        }
        return formattingPoint;
    }

    private PIBOXHolder createPIBOXHolderForFormatting(Formatting formatting) {
        StringBuilder textInLine = new StringBuilder("");
        formatting.getCharParams().forEach(textInLine::append);
        PIBOXHolder piboxHolder = new PIBOXHolder();
        Point formattingPoint = getPointForFormatting(formatting);
        piboxHolder.setRect(formattingPoint.getL(), formattingPoint.getT(),
                formattingPoint.getR() - formattingPoint.getL(),
                formattingPoint.getB() - formattingPoint.getT());
        PBText pbText = new PBText(textInLine.toString(), formatting.getFontFamily() != null ?
                formatting.getFontFamily() : "Arial",
                Double.parseDouble(formatting.getFontSize() != null ? formatting.getFontSize() : "10"),
                false, false, false);
        pbText.setWordFirst(formatting.getCharParams().get(0).getWordFirst() != null);
        pbText.setConfidence(80.0f);
        piboxHolder.setLines(new ArrayList<>());
        piboxHolder.getLines().add(new ArrayList<>());
        piboxHolder.getLines().get(0).add(pbText);
        return piboxHolder;
    }

    private String scale(String val){
        Integer intVal = Integer.parseInt(val);
        return String.valueOf(scaleInt(intVal));
    }

    private Integer scaleInt(Integer val){
        Double scaled = (double) (val * scaleModel.getScalingFactor());
        return scaled.intValue();
    }

    private void applyScalingString(Object object){
        try {
            Method setL = object.getClass().getMethod("setL", String.class);
            Method setT = object.getClass().getMethod("setT", String.class);
            Method setR = object.getClass().getMethod("setR", String.class);
            Method setB = object.getClass().getMethod("setB", String.class);
            Method getL = object.getClass().getMethod("getL");
            Method getT = object.getClass().getMethod("getT");
            Method getR = object.getClass().getMethod("getR");
            Method getB = object.getClass().getMethod("getB");
            setL.invoke(object, scale((String) getL.invoke(object)));
            setT.invoke(object, scale((String) getT.invoke(object)));
            setR.invoke(object, scale((String) getR.invoke(object)));
            setB.invoke(object, scale((String) getB.invoke(object)));
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    private void applyScalingInt(Object object){
        try {
            Method setL = object.getClass().getMethod("setL", Integer.class);
            Method setT = object.getClass().getMethod("setT", Integer.class);
            Method setR = object.getClass().getMethod("setR", Integer.class);
            Method setB = object.getClass().getMethod("setB", Integer.class);
            Method getL = object.getClass().getMethod("getL");
            Method getT = object.getClass().getMethod("getT");
            Method getR = object.getClass().getMethod("getR");
            Method getB = object.getClass().getMethod("getB");
            setL.invoke(object, scaleInt((Integer) getL.invoke(object)));
            setT.invoke(object, scaleInt((Integer) getT.invoke(object)));
            setR.invoke(object, scaleInt((Integer) getR.invoke(object)));
            setB.invoke(object, scaleInt((Integer) getB.invoke(object)));
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    public void extractOCROutput(Page page, int pageNumber) {
        this.pageNumber = pageNumber;
//        this.piBoxes = new ArrayList<>();
        this.words = new ArrayList<>();
        page.setPageHeight(scale(page.getPageHeight()));
        page.setPageWidth(scale(page.getPageWidth()));
        if (page.getBlocks() != null) {
            page.getBlocks().forEach(block -> {
//                applyScalingString(block);
                if (block.getBlockType().equals(AbbyyConstants.BlockType.TEXT)) {
                    if (block.getText() != null) {
                        block.getText().forEach(this::extractDataFromTextModel);
                    }
                } else if (block.getBlockType().equals(AbbyyConstants.BlockType.TABLE)) {
                    if (block.getRow() != null) {
                        block.getRow().forEach(row -> {
                            row.getCell().forEach(cell -> {
                                if (cell.getText() != null) {
                                    cell.getText().forEach(this::extractDataFromTextModel);
                                }
                            });
                        });
                    }
                }
            });
        }
    }

    private void extractDataFromTextModel(Text text) {
        if (text.getPara() != null) {
            text.getPara().forEach(paragraph -> {
                if (paragraph.getLine() != null) {
                    paragraph.getLine().forEach(line -> {
//                        applyScalingInt(line);
                        if (line.getFormatting() != null) {
                            line.getFormatting().forEach(formatting -> {
                                if (formatting != null && formatting.getCharParams() != null) {
//                                    this.piBoxes.add(createPIBOXHolderForFormatting(formatting));
                                    this.words.addAll(getWordsFromLine(formatting));
                                }
                            });
                        }

                    });
                }
            });
        }
    }

    private List<PBText2> getWordsFromLine(Formatting formatting) {
        List<PBText2> words = new ArrayList<>();
        StringBuilder currWordText = new StringBuilder("");
        Point wordPoint = new Point(-1, -1, -1, -1);
        AtomicInteger count = new AtomicInteger(0);
        AtomicDouble conf = new AtomicDouble(0.0);
        AtomicBoolean isWordFirst = new AtomicBoolean(false);
        if (formatting.getCharParams() != null) {
            formatting.getCharParams().forEach(charParams -> {
                if ((charParams.getWordFirst() != null && charParams.getWordFirst().equals("1"))
                        || charParams.getValue().equals(" ")) {
                    if (currWordText.length() > 0) {
                        words.add(createSegmentBased(wordPoint, currWordText.toString(), formatting,
                                conf.get() / count.get(), isWordFirst.get()));
                        currWordText.delete(0, currWordText.length());
                        conf.set(0.0);
                        wordPoint.reset();
                    }
                    if (!charParams.getValue().equals(" ")) {
                        if (currWordText.length() == 0 && charParams.getWordFirst() != null)
                            isWordFirst.set(true);
                        currWordText.append(charParams.getValue());
                        conf.addAndGet(Double.parseDouble(charParams.getCharConfidence() == null ? "100.0"
                                : charParams.getCharConfidence()));
                        count.getAndIncrement();
                        setLocationBasedOnChar(wordPoint, charParams);
                    }
                } else {
                    if (currWordText.length() == 0 && charParams.getWordFirst() != null)
                        isWordFirst.set(true);
                    currWordText.append(charParams.getValue());
                    conf.addAndGet(Double.parseDouble(charParams.getCharConfidence() == null ? "100.0"
                            : charParams.getCharConfidence()));
                    count.getAndIncrement();
                    setLocationBasedOnChar(wordPoint, charParams);
                }
            });
            if (currWordText.length() > 0) {
                words.add(createSegmentBased(wordPoint, currWordText.toString(), formatting,
                        conf.get() / count.get(), isWordFirst.get()));
                currWordText.delete(0, currWordText.length());
                wordPoint.reset();
            }
        }
        return words;
    }

    private PBText2 createSegmentBased(Point wordPoint, String text, Formatting formatting, Double confidence, Boolean isWordFirst) {
        PBText2 segmentModel = new PBText2();
        segmentModel.setText(text);
        segmentModel.setX(wordPoint.getL());
        segmentModel.setY(wordPoint.getT());
        segmentModel.setW(wordPoint.getR() - wordPoint.getL());
        segmentModel.setH(wordPoint.getB() - wordPoint.getT());
        segmentModel.setFontSize(Double.parseDouble(formatting.getFontSize()));
        segmentModel.setFontFamily(formatting.getFontFamily());
        segmentModel.setConfidence(confidence);
        segmentModel.setWordFirst(isWordFirst);
        return segmentModel;
    }

    private void setLocationBasedOnChar(Point wordPoint, CharParams charParams) {
        if (wordPoint.getL() == -1 || wordPoint.getL() > charParams.getL()) {
            wordPoint.setL((int) (charParams.getL()));
        }
        if (wordPoint.getT() == -1 || wordPoint.getT() > charParams.getT()) {
            wordPoint.setT((int) (charParams.getT()));
        }
        if (wordPoint.getR() == -1 || wordPoint.getR() < charParams.getR()) {
            wordPoint.setR((int) (charParams.getR()));
        }
        if (wordPoint.getB() == -1 || wordPoint.getB() < charParams.getB()) {
            wordPoint.setB((int) (charParams.getB()));
        }
    }

    public void cleanup() {
    }
}
