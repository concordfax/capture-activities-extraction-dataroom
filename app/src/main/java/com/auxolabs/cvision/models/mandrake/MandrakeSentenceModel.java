package com.auxolabs.cvision.models.mandrake;

import com.auxolabs.capture.annotator.models.Annotation;
import com.kaiser.models.ParserValue;
import lombok.Data;

import java.util.HashMap;
import java.util.List;

/**
 * Each sentence from the text, with triggered keyPhrases
 */
@Data

public class MandrakeSentenceModel {
    private int sentenceId;
    private String sentence;
    private List<Annotation> annotations;
    private ParserValue sentencePV;
    private HashMap<String, List<Annotation>> annotationsMap;

    public MandrakeSentenceModel(int sentenceId, String sentence, List<Annotation> annotations, ParserValue sentencePV) {
        this.sentenceId = sentenceId;
        this.sentence = sentence;
        this.annotations = annotations;
        this.sentencePV = sentencePV;
    }

    public MandrakeSentenceModel(){}
}
