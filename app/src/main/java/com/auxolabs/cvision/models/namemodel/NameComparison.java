package com.auxolabs.cvision.models.namemodel;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NameComparison {
    private String rulesPatientName;
    private String mlPatientName;
    private String mlNonPatientName;
}
