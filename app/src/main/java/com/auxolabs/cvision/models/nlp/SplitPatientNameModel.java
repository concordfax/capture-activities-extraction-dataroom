package com.auxolabs.cvision.models.nlp;

import lombok.Data;

@Data
public class SplitPatientNameModel {
    private String firstName;
    private String middleName;
    private String secondName;
}
