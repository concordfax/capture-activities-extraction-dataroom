package com.auxolabs.cvision.models.cv;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PBText {
    private String text;
    private String fontFamily;
    private Double fontSize;
    private boolean isBold;
    private boolean isItalic;
    private boolean isUnderline;
    private float confidence;
    private boolean isWordFirst;

    public PBText() {
    }

    public PBText(String text, String fontFamily, Double fontSize, boolean isBold, boolean isItalic, boolean isUnderline) {
        this.text = text;
        this.fontFamily = fontFamily;
        this.fontSize = fontSize;
        this.isBold = isBold;
        this.isItalic = isItalic;
        this.isUnderline = isUnderline;
    }
}
