package com.auxolabs.cvision.models.cv;

import com.auxolabs.cvision.models.ocr.Lines;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * This is used to hold line candidates which could possibly be merged into a single line
 */
@Data
public class SameLineGroup {
    private Integer y;
    private Integer h;
    private List<Lines> lines = new ArrayList<>();
    private String text;

    // refactor y based on newly added box
    public void addBox(Lines newBox) {
        lines.add(newBox);
        y = 0;
        lines.forEach(box -> {
            y += box.y();
        });
        y /= lines.size();
        h = 0;
        lines.forEach(box -> {
            h += box.h().intValue();
        });
        h /= lines.size();
    }
}
