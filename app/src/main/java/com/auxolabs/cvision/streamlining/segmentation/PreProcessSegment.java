package com.auxolabs.cvision.streamlining.segmentation;

import com.auxolabs.cvision.models.ocr.Lines;
import com.auxolabs.cvision.models.ocr.Para;
import com.auxolabs.cvision.models.ocr.SegBlock;
import com.auxolabs.cvision.streamlining.models.ImageSegment;
import com.auxolabs.cvision.streamlining.utils.TextPreProcessingUtility;

import java.util.List;

public class PreProcessSegment {

    public void preProcessSegment(List<SegBlock> segBlockList){
        // TODO: 30/6/19 the preprocessing should be mapped into the character level of each segment
        segBlockList.forEach(segBlock -> {
            segBlock.getParagraphList().forEach(para -> {
                para.getLines().forEach(line->{
                    line.getMappedSegments().forEach(imageSegment -> {
                        imageSegment.setText(TextPreProcessingUtility.preprocess(imageSegment.getText()));
                    });
                });
            });
        });

        //modifying value of lines according to the preprocessed text in lines
        segBlockList.forEach(segBlock -> {
            segBlock.getParagraphList().forEach(para -> {
                para.getLines().forEach(line -> {
                    StringBuilder lineValue = new StringBuilder();
                    for(ImageSegment mappedSegment:line.getMappedSegments()){
                        if(lineValue.toString().length()==0 || lineValue.toString().endsWith(" ") ){
                            lineValue.append(mappedSegment.getText());
                        }
                        else{
                            lineValue.append(mappedSegment.getText());
                        }

                    }
                    line.setValue(lineValue.toString());
                });
            });
        });

        //modifying value of para according to the new text in lines
        segBlockList.forEach(segBlock -> {
            segBlock.getParagraphList().forEach(para -> {
                StringBuilder paraValue = new StringBuilder();
                for(Lines line:para.getLines()){
                    if(paraValue.toString().length()==0 || paraValue.toString().endsWith(" ") ){
                        paraValue.append(line.getValue());
                    }
                    else{
                        paraValue.append(line.getValue());
                    }

                }
                para.setValue(paraValue.toString());
            });
        });

        //modifying block value according to the new text in para
        segBlockList.forEach(segBlock -> {
            StringBuilder blockValue = new StringBuilder();
            for(Para para : segBlock.getParagraphList()){
                if(blockValue.toString().length()==0 || blockValue.toString().endsWith(" ") ){
                    blockValue.append(para.getValue());
                }
                else{
                    blockValue.append(para.getValue());
                }
            }
            segBlock.setValue(blockValue.toString());
        });

    }




}
