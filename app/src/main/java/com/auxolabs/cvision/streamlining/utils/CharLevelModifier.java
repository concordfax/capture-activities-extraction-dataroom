package com.auxolabs.cvision.streamlining.utils;

import com.auxolabs.cvision.streamlining.models.ImageSegment;
import com.concordfax.capture.core.models.ocr.abbyy.CharParams;
import com.concordfax.capture.core.models.ocr.abbyy.Formatting;

import java.util.List;

public class CharLevelModifier {

    public void addNewCharParamsForSpaceAsLastChar(ImageSegment imageSegment){
        Formatting formatting = imageSegment.getFormatting();
        List<CharParams> listCharParams = formatting.getCharParams();
        Integer widthOfSpaceCharacter = 3;
        Integer l = listCharParams.get(listCharParams.size()-1).getR();
        Integer t = listCharParams.get(listCharParams.size()-1).getT();
        Integer r = listCharParams.get(listCharParams.size()-1).getR() + widthOfSpaceCharacter;
        Integer b = listCharParams.get(listCharParams.size()-1).getB();

        CharParams newSpaceCharParam = new CharParams();
        newSpaceCharParam.setValue(" ");
        newSpaceCharParam.setL(l);
        newSpaceCharParam.setB(b);
        newSpaceCharParam.setT(t);
        newSpaceCharParam.setR(r);

        // TODO: 2019-04-18 add other properties as needed

        formatting.getCharParams().add(newSpaceCharParam);
        imageSegment.setText(imageSegment.getText()+" ");
    }

    public void addNewCharParamsForSpaceAsFirstChar(ImageSegment imageSegment){
        Formatting formatting = imageSegment.getFormatting();
        List<CharParams> listCharParams = formatting.getCharParams();
        Integer widthOfSpaceCharacter = 3;
        Integer l = listCharParams.get(0).getR();
        Integer t = listCharParams.get(0).getT();
        Integer r = listCharParams.get(0).getR() + widthOfSpaceCharacter;
        Integer b = listCharParams.get(0).getB();

        CharParams newSpaceCharParam = new CharParams();
        newSpaceCharParam.setValue(" ");
        newSpaceCharParam.setL(l);
        newSpaceCharParam.setB(b);
        newSpaceCharParam.setT(t);
        newSpaceCharParam.setR(r);

        // TODO: 2019-04-18 add other properties as needed

        formatting.getCharParams().add(0,newSpaceCharParam);
        imageSegment.setText(" "+imageSegment.getText());
    }
}
