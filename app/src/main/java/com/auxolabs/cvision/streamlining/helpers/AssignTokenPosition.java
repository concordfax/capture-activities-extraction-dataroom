package com.auxolabs.cvision.streamlining.helpers;

import com.auxolabs.capture.annotator.AnnotationsProcessor;
import com.auxolabs.capture.annotator.models.TokenPosition;
import com.auxolabs.capture.annotator.models.response.SentenceModel;
import com.auxolabs.cvision.models.ocr.Lines;
import com.auxolabs.cvision.models.ocr.Para;
import com.auxolabs.cvision.models.ocr.SegBlock;
import com.auxolabs.cvision.streamlining.models.ImageSegment;
import com.concordfax.capture.core.models.extraction.annotators.ExtractionAnnotatorConfiguration;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AssignTokenPosition {

    public void assignTokenPositionToBlockAndSegments(List<SegBlock> segBlocks){
        segBlocks.forEach(segBlock -> {
            generateTextAndAddTokenPostionInfo(segBlock.getParagraphList());

        });
    }

    private void generateTextAndAddTokenPostionInfo(List<Para> paras) {
        StringBuilder formattedText = new StringBuilder("");
        int offSet =0;
        for(Para para:paras){
            for(Lines line:para.getLines()){
                for(ImageSegment subSegment:line.getMappedSegments()) {
                    StringBuilder textInBox = new StringBuilder(subSegment.getText());
                    if (textInBox.length() != 0) {
                        // filter out unnecessary noise coming due to possible pen marks on paper, for dates
                        {
                            Pattern pattern = Pattern.compile("([0-9]{2,4}(/)[0-9]{2}(/)[0-9]{2})|([0-9]{2}(/)[0-9]{2}(/)[0-9]{2,4})");
                            Matcher matcher = pattern.matcher(textInBox.toString());
                            int currentEnd = 0;
                            StringBuilder currFormattedText = new StringBuilder("");
                            while (matcher.find()) {
                                String matchedText = textInBox.substring(matcher.start(), matcher.end());
                                if (matcher.end() < textInBox.length()) {
                                    String nextChar = textInBox.substring(matcher.end(), matcher.end() + 1);
                                    if (nextChar.matches("[a-zA-Z]")) {
                                        currFormattedText.append(textInBox.substring(currentEnd, matcher.start()));
                                        currFormattedText.append(matchedText);
                                        currFormattedText.append(" ");
                                        currentEnd = matcher.end();
                                    }
                                }
                            }
                            if (currentEnd < textInBox.length()) {
                                currFormattedText.append(textInBox.substring(currentEnd, textInBox.length()));
                            }
                            subSegment.setText(textInBox.toString());
                            subSegment.setTokenPosition(new TokenPosition(offSet, formattedText.length() + currFormattedText.length()-1));
                            formattedText.append(currFormattedText);
                            offSet = formattedText.length();
                        }
                    }

                }

            }
        }



    }
    //add token position in each of the subsegment
    private void assignTokenPositionForSubsegments(List<ImageSegment> subsegments){
        StringBuilder textInSubSeg = new StringBuilder("");
        for(int i=0;i<subsegments.size();i++){
            StringBuilder text = new StringBuilder();
            text.append( subsegments.get(i).getText());
            subsegments.get(i).setTokenPosition(new TokenPosition(textInSubSeg.length(),textInSubSeg.length()+text.length()-1));
            textInSubSeg.append(text);
        }
    }
}
