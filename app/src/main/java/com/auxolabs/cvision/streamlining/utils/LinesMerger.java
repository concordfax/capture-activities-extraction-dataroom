package com.auxolabs.cvision.streamlining.utils;

import com.auxolabs.cvision.models.ocr.SegBlock;
import com.auxolabs.cvision.models.ocr.Lines;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

import static java.lang.Math.abs;

public class LinesMerger {

    private static final Double THRESHOLD = 15.0;
    private static final String COLON = ":";

    private boolean checkIfEndsWithContinuationSequence(Lines lines) {
        return lines.getValue().endsWith(COLON);
    }

    private boolean isPossibleContinuationSequence(Lines lines) {
        return !lines.getValue().endsWith(COLON) &&
                !(lines.getSegments() !=null && !lines.getSegments().isEmpty() && lines.getSegments().get(0).getValue().contains(COLON)); // check if first segment of line contains colon
    }

    private boolean isBreakersBetweenLines(Lines line1, Lines line2, List<SegBlock> blocks) {
        Lines topLine = line1.getT() < line2.getT() ? line1 : line2;
        Lines bottomLine = line1.getB() < line2.getB() ? line2 : line1;
        for (SegBlock block : blocks) {
            if (block.getBlock().getBlockType().equals("Separator")) {
                // check if block intersects with intersection of the lines
                if ((line1.getR() <= block.getL() && line2.getL() >= block.getR())
                        && (
                        (topLine.getT() > block.getT() && bottomLine.getB() < block.getB())
                                || (topLine.getT() < block.getB() && bottomLine.getB() > block.getB())
                                || (bottomLine.getB() > block.getT() && topLine.getT() < block.getT())
                )) {
                    return true;
                }
            }
        }
        return false;
    }

    public void mergeLinesOnTextProperties(List<SegBlock> segBlockList) {
        // get all lines
        List<Lines> lines = new ArrayList<>();
        segBlockList.forEach(segBlock -> {
            segBlock.getParagraphList().forEach(para -> {
                lines.addAll(para.getLines());
            });
        });

        // sort lines by Y
        lines.sort(Comparator.comparingInt(Lines::y));

        // try merging lines
        HashSet<Lines> invalidated = new HashSet<>();
        for (int i = 0; i < lines.size() - 1; i++) {
            Lines currentLine = lines.get(i);
            // if current line block ends with continuation sequence then find line block towards right and see if they can be merged
            if (checkIfEndsWithContinuationSequence(currentLine)) {
                Lines lineTowardsRight = getLineTowardsRight(currentLine, lines, invalidated);
                if (lineTowardsRight != null && isPossibleContinuationSequence(lineTowardsRight)
                        && !isBreakersBetweenLines(currentLine, lineTowardsRight, segBlockList)) {
                    invalidated.add(lineTowardsRight);
                    combineLines(currentLine, lineTowardsRight);
                }
            }
        }

        // remove invalidated from the blocks and paragraphs
        segBlockList.forEach(segBlock -> {
            segBlock.getParagraphList().forEach(para -> {
                para.getLines().removeIf(invalidated::contains);
            });
        });
        segBlockList.forEach(segBlock -> {
            segBlock.getParagraphList().removeIf(para -> para.getLines().isEmpty());
        });
        segBlockList.removeIf(segBlock -> segBlock.getParagraphList().isEmpty());
    }

    private Lines getLineTowardsRight(Lines curLine, List<Lines> lines, HashSet<Lines> searchedLines) {
        List<Lines> bestPossibleLine = new ArrayList<>();
        for (Lines line : lines) {
            if (!curLine.equals(line) || !searchedLines.contains(line)) {
                if (isLineTowardsRight(curLine, line)) {
                    bestPossibleLine.add(line);
                }
            }
        }
        bestPossibleLine.sort(Comparator.comparingInt(Lines::getL));
        return bestPossibleLine.size() > 0 ? bestPossibleLine.get(0) : null;
    }

    private boolean isLineTowardsRight(Lines line1, Lines line2) {
        return Math.abs(line1.getT() - line2.getT()) <= THRESHOLD &&
                Math.abs(line1.getB() - line2.getB()) <= THRESHOLD &&
                line1.getL() < line2.getL() &&
                abs(line1.getBaseLine() - line2.getBaseLine()) <= line2.getB() - line2.getT();
    }

    private void combineLines(Lines currentLine, Lines nearLine) {
        currentLine.setValue(currentLine.getValue() + " " + nearLine.getValue());
        currentLine.setT(nearLine.getT() < currentLine.getT() ? nearLine.getT() : currentLine.getT());
        currentLine.setL(nearLine.getL() < currentLine.getL() ? nearLine.getL() : currentLine.getL());
        currentLine.setB(nearLine.getB() < currentLine.getB() ? currentLine.getB() : nearLine.getB());
        currentLine.setR(nearLine.getR() < currentLine.getR() ? currentLine.getR() : nearLine.getR());
        currentLine.setBaseLine(nearLine.getBaseLine() < currentLine.getBaseLine() ? nearLine.getBaseLine() : currentLine.getBaseLine());
        currentLine.getMappedSegments().addAll(nearLine.getMappedSegments());
    }

    private void removeTheLineFromBlock(SegBlock block, Lines line) {
        block.getParagraphList().forEach(para -> {
//            if (para.getLines().contains(line))
            para.getLines().remove(line);
        });
    }
}
