package com.auxolabs.cvision.streamlining.models;

import com.auxolabs.capture.annotator.models.Annotation;
import com.auxolabs.capture.annotator.models.TokenPosition;
import com.auxolabs.capture.annotator.models.response.SentenceModel;
import com.auxolabs.cvision.models.cv.PBText;
import com.auxolabs.cvision.models.cv.PIBOXHolder;
import com.auxolabs.cvision.models.ocr.Segment;
import com.concordfax.capture.core.models.ocr.abbyy.CharParams;
import com.concordfax.capture.core.models.ocr.abbyy.Formatting;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bytedeco.javacpp.lept;

import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ImageSegment {

    private String value;

    @Getter
    @Setter
    private int id;

    private Formatting formatting;
    @Getter
    @Setter
    private List<Formatting> listOfFormatting = new ArrayList<>();

    private Rectangle2D.Double rect;

    @Getter
    @Setter
    private TokenPosition tokenPosition;

    @Getter
    @Setter
    private List<ImageSegment> subSegments;

    @Getter
    @Setter
    private boolean isVisited;

    @Getter
    @Setter
    private Map<String,SentenceModel> annotatorNameToAnnotations;

    @Setter
    @Getter
    private int blockId;

    @Setter
    @Getter
    private int paraId;

    @Setter
    @Getter
    private int lineId;

    @Getter
    @Setter
    private boolean flagOfSplitedBasedOnColumn;

    @Getter
    @Setter
    private boolean flagOfCapitalMerged;

    public ImageSegment(){}

    public Double x() {
        return rect.getX();
    }

    public Double y() {
        return rect.getY();
    }

    public Double w() {
        return rect.getWidth();
    }

    public Double h() {
        return rect.getHeight();
    }

    public void x(Double x) {
        rect.x = x;
    }

    public void y(Double y) {
        rect.y = y;
    }

    public void w(Double w) {
        rect.width = w;
    }

    public void h(Double h) {
        rect.height = h;
    }

    public String getText() {
        return this.value;
    }

    public void setText(String text){this.value = text; }

    public void setFormatting(Formatting formatting ) {
        this.formatting = formatting;
    }

    public Formatting getFormatting(){
        return this.formatting;
    }

    public Rectangle2D getRect() {
        return rect;
    }

    public void setRect(Double x, Double y, Double w, Double h) {
        this.rect = new Rectangle2D.Double(x, y, w, h);
    }

    public void setRect(Integer x, Integer y, Integer w, Integer h) {
        this.rect = new Rectangle2D.Double(x, y, w, h);
    }

    public static ImageSegment CreateEmpty(lept.BOX box) {
        ImageSegment imageSegment = new ImageSegment();
        imageSegment.rect = new Rectangle2D.Double(box.x(), box.y(), box.w(), box.h());
        return imageSegment;
    }

    public TokenPosition getTokenPosition() {
        return tokenPosition;
    }

    public void setTokenPosition(TokenPosition tokenPosition) {
        this.tokenPosition = tokenPosition;
    }

}
