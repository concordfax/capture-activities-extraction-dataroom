package com.auxolabs.cvision.streamlining.constants;

public class PositionalConstants {
    public static final String SSN_REGEX = "(?i)[0-9|X]{3}\\s{0,1}-\\s{0,1}[0-9|X]{2}\\s{0,1}-\\s{0,1}-{0,1}[0-9|X]{4}";
    public static final String MRN_POST_PROCESSING_REGEX = "(?i)[^-a-z0-9_#()\\/]";
    public static final String SSN_POST_PROCESSING_REGEX = "(?i)[^-0-9X#*]";
    public static final String NAME_POST_PROCESSING_REGEX = "(?i)[^-a-z0-9,.\\s]";
}
