package com.auxolabs.cvision.streamlining.utils;

import com.auxolabs.cvision.streamlining.models.ImageSegment;
import com.concordfax.capture.core.models.ocr.abbyy.CharParams;
import com.concordfax.capture.core.models.ocr.abbyy.Formatting;

import java.util.*;

public class NLPBasedSegmentation {
    private static int MERGING_THRESHOLD = 25;
    private static int THRESHOLD = 10;

    /**
     * This function iterates over all the segments obtained from OCR and tries to form larger segments using segments that have all capital letters
     * @param segmentsFromOcr Segments from OCR
     */
    public void createSegmentsByMergingWordsWithAllCapitalLetters(List<ImageSegment> segmentsFromOcr) {
        for (int i = 0; i < segmentsFromOcr.size() - 1; i++) {
            // identify segment with all CAPS
            if (!segmentsFromOcr.get(i).isVisited()
                    && isUpperCase(segmentsFromOcr.get(i).getText().trim())
                    && segmentsFromOcr.get(i).getText().trim().length() > 1) {
                ImageSegment selectedAllCapsOcrSegment = segmentsFromOcr.get(i);
                selectedAllCapsOcrSegment.setFlagOfCapitalMerged(true);
                // iterate over all the remaining segments to identify the other segment in that segment
                // TODO: 2019-04-18 optimize code to perform only nearby search
                for (int j = i + 1; j < segmentsFromOcr.size(); j++) {
                    ImageSegment possibleCandidate = segmentsFromOcr.get(j);
                    //checking for nearby segments having only capital letters to merge and also merging non alphabetic words
                    if (!possibleCandidate.isVisited() &&((isUpperCase(possibleCandidate.getText().trim()) && checkIfNearbySegment(possibleCandidate, selectedAllCapsOcrSegment))
                            || (isNotAlpha(possibleCandidate.getText()) && checkIfNearbySegment(possibleCandidate, selectedAllCapsOcrSegment))
                            //to solve cases like DARYL L NELSON; if we not include white space while merging it will result like DARYLLNELSON; to avoid this we are merging nearby white space with all caps segment
                            || (isWhiteSpace(possibleCandidate.getText()) && checkIfNearbySegment(possibleCandidate,selectedAllCapsOcrSegment)))

                            ) {
                        //creating a new segment with merging all the sub segments
                        mergeSegments(selectedAllCapsOcrSegment, possibleCandidate);
                        possibleCandidate.setVisited(true);

                    }
                }
            }
        }
        segmentsFromOcr.removeIf(ImageSegment::isVisited);
    }

    //to check the string contains only uppercase characters in it
    private boolean isUpperCase(String s) {
        boolean ok = true;
        for (char c : s.toCharArray()) {
            if (Character.isLetter(c) && !Character.isUpperCase(c)) {
                ok = false;
                break;
            }
        }
        // check if has atleast one char
        if (!s.matches(".*[a-zA-Z0-9].*"))
            return false;
        return ok;
    }

    //to check the string is a white space
    private boolean isWhiteSpace(String s){
        boolean ok = true;
        for(char c : s.toCharArray()){
            if(!Character.isWhitespace(c)){
               ok = false;
               break;
            }
        }
        return ok;
    }

    private boolean isNotAlpha(String s) {
        boolean ok = false;
        String specialCharacters = "[" + ", -/@#!*$%^&.'_+={}()"+ "]+";
        if(s.matches(specialCharacters)){
            ok = true;
        }
        return ok;
    }

    private boolean checkIfNearbySegment(ImageSegment possibleCandidate, ImageSegment segment) {
        return possibleCandidate.x() > segment.x()
                && Math.abs(segment.x() + segment.w() - possibleCandidate.x()) < MERGING_THRESHOLD
                && Math.abs(possibleCandidate.y() - segment.y()) <= THRESHOLD
                && (possibleCandidate.h() - segment.h() <= THRESHOLD || segment.h() > possibleCandidate.h());
    }

    private void mergeSegments(ImageSegment segment1, ImageSegment segment2) {
        if (segment1.x() > segment2.x())
            segment1.x(segment2.x() * 1.0);
        if (segment1.y() > segment2.y())
            segment1.y(segment2.y() * 1.0);
        if (segment1.x() + segment1.w() < segment2.w() + segment2.x())
            segment1.w(((segment2.x() + segment2.w()) - segment1.x()) * 1.0);
        if (segment1.h() < segment2.h())
            segment1.h(segment2.h() * 1.0);
        segment1.setText(segment1.getText() + segment2.getText());
        segment1.getFormatting().getCharParams().addAll(segment2.getFormatting().getCharParams());

    }

    /**
     * Iterate over all the segments and check if they contain a segment splitter and split the segment at that
     * @param segments segments
     * @return
     */
    public void splitSegmentsBasedOnSegmentSplitters(List<ImageSegment> segments) {
        List<ImageSegment> newSegments = new ArrayList<>();
        List<Integer> positionsForSegments = new ArrayList<>();
        for (int i = 0; i < segments.size(); i++) {
            ImageSegment segment = segments.get(i);
            Integer prevSplit = 0;
            String text = segment.getText();

            // iterate over all the characters in the text and split if splitting condition met
            for (int charIdx = 0; charIdx < text.length() - 1; charIdx++) {
                if (checkIfSplitConditionMet(text, charIdx)) {
                    ImageSegment splitSegment = createNewSegmentFromSplitPositions(segment, text, prevSplit, charIdx);
                    newSegments.add(splitSegment);
                    positionsForSegments.add(i);
                    prevSplit = charIdx + 1;
                }
            }
            if(prevSplit > 0){
                segment.setVisited(true);
                ImageSegment splitSegment = createNewSegmentFromSplitPositions(segment, text, prevSplit, text.length()-1);
                newSegments.add(splitSegment);
                positionsForSegments.add(i);
            }
        }

        // add all newly created segments at appropriate positions, before removing the visited segments as that may change the positions
        for (int i = 0; i < newSegments.size(); i++) {
            segments.add(positionsForSegments.get(i)+i, newSegments.get(i));
        }

        // remove all visited segments
        segments.removeIf(ImageSegment::isVisited);
    }


    private boolean checkIfSplitConditionMet(String text, Integer charIdx){
        return text.charAt(charIdx) == ':'
                && ( charIdx == 0  || !Character.isDigit(text.charAt(charIdx - 1) ))
                && !Character.isDigit(text.charAt(charIdx + 1));
    }

    /**
     * Create new segment from the start and end positions of the original segments
     *
     * @param subSegment
     * @param text
     * @param start
     * @param end
     * @return
     */
    private ImageSegment createNewSegmentFromSplitPositions(ImageSegment subSegment, String text, int start, int end) {
        ImageSegment imageSegment = new ImageSegment();
        try {
            imageSegment.setText(text.substring(start, end + 1));

            List<CharParams> charParamsInSegment = new ArrayList<>(subSegment.getFormatting().getCharParams().subList(start, end + 1));
            Integer x = subSegment.getFormatting().getCharParams().get(start).getL();
            Integer y = subSegment.getFormatting().getCharParams().get(start).getT();
            Integer w = subSegment.getFormatting().getCharParams().get(end).getL() -
                    subSegment.getFormatting().getCharParams().get(start).getL();
            Integer h = Collections.max(charParamsInSegment, Comparator.comparingInt(CharParams::getB)).getB() -
                    Collections.min(charParamsInSegment, Comparator.comparingInt(CharParams::getT)).getT();
            imageSegment.setRect(x, y, w, h);
            imageSegment.setFormatting(new Formatting());
            imageSegment.getFormatting().setCharParams(charParamsInSegment);
            imageSegment.setBlockId(subSegment.getBlockId());
            imageSegment.setParaId(subSegment.getParaId());
            imageSegment.setLineId(subSegment.getLineId());
            imageSegment.setFlagOfSplitedBasedOnColumn(true);
            return imageSegment;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}

