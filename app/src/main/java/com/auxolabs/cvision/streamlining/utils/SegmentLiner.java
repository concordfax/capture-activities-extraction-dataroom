package com.auxolabs.cvision.streamlining.utils;

import com.auxolabs.cvision.models.cv.SameLineGroup;
import com.auxolabs.cvision.models.cv.PIBOXHolder;
import com.auxolabs.cvision.models.ocr.Lines;
import com.auxolabs.cvision.models.ocr.SegBlock;

import java.util.ArrayList;
import java.util.List;

public class SegmentLiner {

    private static final int TOLERANCE = 15;

    /**
     * Find and merge line candidates which can be merged to form a single large line
     * @param blocks blocks in the document
     */
    public void findAndUpdateLinesThatCouldBeSplitAcrossParagraphs(List<SegBlock> blocks) {
        List<SameLineGroup> groups = new ArrayList<>();
        blocks.forEach(segBlock -> {
            segBlock.getParagraphList().forEach(para -> {
                para.getLines().forEach(box -> {
                    boolean flag = false;
                    for (SameLineGroup group : groups) {
                if(checkIfBoxPartOfGroup(box, group)){
//                        if (Math.abs(box.y() - group.getY()) <= TOLERANCE) {
                            group.addBox(box);
                            flag = true;
                            break;
                        }
                    }
                    // the line is not part of any group
                    if (!flag) {
                        SameLineGroup newGroup = new SameLineGroup();
                        newGroup.addBox(box);
                        groups.add(newGroup);
                    }
                });
            });
        });

        // sort boxes in group by x axis
        groups.forEach(sameLineGroup -> {
            sameLineGroup.getLines().sort((o1, o2) -> (int) (o1.x() - o2.x()));
        });

        blocks.forEach(segBlock -> {
            segBlock.getParagraphList().forEach(para -> {
                for (SameLineGroup group : groups) {

                    // since group contains list of line, we add the lines in the group to
                    if( para.getParaId() == group.getLines().get(0).getParaId()){

                        // remove all lines in paragraph as all lines are added to one of the groups
                        para.getLines().clear();

                        // add all lines from group to paragraph
                        para.getLines().addAll(group.getLines());
                    }
                }
            });
        });
    }

    // TODO: 22/01/18 migrate to this logic
    private boolean checkIfBoxPartOfGroup(Lines box, SameLineGroup group) {
        if ((group.getY() <= box.y() && group.getH() >= box.h()) // box inside group
                || (box.y() - group.getY() <= TOLERANCE && box.y() + box.h() <= group.getY() + group.getH()) // box towards top of group
                || (group.getY() - box.y() <= TOLERANCE) && group.getY() + group.getH() <= box.y() + box.h()) // box towards bottom of group
            return true;
        return false;
    }
}
