package com.auxolabs.cvision.streamlining.utils;

import com.auxolabs.cvision.models.ocr.Lines;
import com.auxolabs.cvision.models.ocr.Para;
import com.auxolabs.cvision.streamlining.models.ImageSegment;
import com.concordfax.capture.core.models.ocr.abbyy.CharParams;
import com.concordfax.capture.core.models.ocr.abbyy.Formatting;

import java.util.Comparator;
import java.util.List;

public class SortingUtility {

    private CharLevelModifier charLevelModifier;

    public SortingUtility(){
        this.charLevelModifier = new CharLevelModifier();
    }

    //to solve 05/31/1994 6534322 Dob case ie, to sort them and to make 6534322 05/31/1994 Dob
    public void sortSegmentsComparedAndMerged(ImageSegment mergedSegments){
        if(mergedSegments.getSubSegments()!=null && !mergedSegments.isFlagOfSplitedBasedOnColumn()){
            mergedSegments.getSubSegments().sort(Comparator.comparingDouble(imageSegment -> imageSegment.y()));
            // identify lines
            Double prevY = mergedSegments.getSubSegments().get(0).y();
            Integer curRowStart = 0;
            for (int i = 1; i < mergedSegments.getSubSegments().size(); i++) {
                // check if same line
                if(Math.abs(prevY - mergedSegments.getSubSegments().get(i).y()) > 5){
                    // not same line, then take segments from curRowStart to i-1 and sort them
                    List<ImageSegment> subSegments = mergedSegments.getSubSegments().subList(curRowStart, i);
                    subSegments.sort(Comparator.comparingDouble(ImageSegment::x));
                    // after sorting set the segments back and update the curRowStart on the way
                    for (ImageSegment subSegment : subSegments) {
                        mergedSegments.getSubSegments().set(curRowStart++, subSegment);
                    }
                    // setting prevY to next element
                    prevY = mergedSegments.getSubSegments().get(i).y();
                }
            }
            for(int i =1 ;i<mergedSegments.getSubSegments().size();i++){
                if(!(mergedSegments.getSubSegments().get(i-1).getText().endsWith(" ")
                        || mergedSegments.getSubSegments().get(i).getText().startsWith(" ")
                        || (Character.isDigit(mergedSegments.getSubSegments().get(i-1).getText().charAt(mergedSegments.getSubSegments().get(i-1).getText().length()-1))
                        && Character.isDigit(mergedSegments.getSubSegments().get(i).getText().charAt(0))))){
                    mergedSegments.getSubSegments().get(i-1).setText(mergedSegments.getSubSegments().get(i-1).getText()+" ");
                }
            }
            StringBuilder text = new StringBuilder();

            for(ImageSegment subsegment: mergedSegments.getSubSegments()){
                text.append(subsegment.getText());
            }
            mergedSegments.setText(text.toString());
        }
    }
    public void sortLines(List<Lines> linesList){
        if(linesList.isEmpty())
            return;
        // sort based on Y to identify rows
        linesList.sort(Comparator.comparingInt(Lines::y));
        // identify lines
        Integer prevY = linesList.get(0).y();
        Integer curRowStart = 0;
        for (int i = 1; i < linesList.size(); i++) {
            // check if same line
            if(Math.abs(prevY - linesList.get(i).y()) >=10 ){
                // not same line, then take segments from curRowStart to i-1 and sort them
                // after sorting set the segments back and update the curRowStart on the way
                sortBasedOnXLines(linesList, curRowStart, i);

                // point next row start to current element
                curRowStart = i;

                // setting prevY to next element
                prevY = linesList.get(i).y();
            }
        }

        // sort the last row
        if(curRowStart < linesList.size()){
            // take all remaining segments
            sortBasedOnXLines(linesList, curRowStart, linesList.size());
        }

    }

    public void sortParagraphs(List<Para> paragraphList){
        if(paragraphList.isEmpty())
            return;
        // sort based on Y to identify rows
        paragraphList.sort(Comparator.comparingInt(Para::getT));
        // identify lines
        Integer prevY = paragraphList.get(0).getT();
        Integer curRowStart = 0;
        for (int i = 1; i < paragraphList.size(); i++) {
            // check if same line
            if(Math.abs(prevY - paragraphList.get(i).getT()) >= 10){
                // not same line, then take segments from curRowStart to i-1 and sort them
                // after sorting set the segments back and update the curRowStart on the way
                sortBasedOnXPara(paragraphList, curRowStart, i);

                // point next row start to current element
                curRowStart = i;

                // setting prevY to next element
                prevY = paragraphList.get(i).getT();
            }
        }

        // sort the last row
        if(curRowStart < paragraphList.size()){
            // take all remaining segments
            sortBasedOnXPara(paragraphList, curRowStart, paragraphList.size());
        }

    }

    //after adding unvisited segments from abby to segmentcomparedandmerged the order may vary and to again sort that back into increasing order of x
    public void sortMappedSegments(List<ImageSegment> mappedSegments){
        if(mappedSegments.isEmpty())
            return;
        // sort based on Y to identify rows
        mappedSegments.sort(Comparator.comparingDouble(ImageSegment::y));
        // identify lines
        Double prevY = mappedSegments.get(0).y();
        Integer curRowStart = 0;
        for (int i = 1; i < mappedSegments.size(); i++) {
            // check if same line
            if(Math.abs(prevY - mappedSegments.get(i).y()) > 10){
                // not same line, then take segments from curRowStart to i-1 and sort them
                // after sorting set the segments back and update the curRowStart on the way
                sortBasedOnXMappedSegments(mappedSegments, curRowStart, i);

                // point next row start to current element
                curRowStart = i;

                // setting prevY to next element
                prevY = mappedSegments.get(i).y();
            }
        }

        // sort the last row
        if(curRowStart < mappedSegments.size()){
            // take all remaining segments
            sortBasedOnXMappedSegments(mappedSegments, curRowStart, mappedSegments.size());
        }

        // add a space b/w segments if consecutive segments do not contain space b/w them
        for (int i = 1; i < mappedSegments.size(); i++) {
            if(!(mappedSegments.get(i-1).getText().endsWith(" ")
                    || mappedSegments.get(i).getText().startsWith(" ")
                    || (Character.isDigit(mappedSegments.get(i-1).getText().charAt(mappedSegments.get(i-1).getText().length()-1))
                    && Character.isDigit(mappedSegments.get(i).getText().charAt(0))))){
                charLevelModifier.addNewCharParamsForSpaceAsLastChar(mappedSegments.get(i-1));
            }
        }
    }

    /**
     * Sort the sublist of elements in the list of segments and preserving the position of the sublist relative to the list
     * @param segments the entire of list of segments
     * @param start denotes the start of the sublist
     * @param end denotes the end of the sublist
     */
    private void sortBasedOnXMappedSegments(List<ImageSegment> segments, Integer start, Integer end){

        List<ImageSegment> subSegments = segments.subList(start, end);

        subSegments.sort(Comparator.comparingDouble(ImageSegment::x));
        // after sorting set the segments back and update the curRowStart on the way
        for (int i = 0; i < subSegments.size(); i++) {
            ImageSegment subSegment = subSegments.get(i);
            segments.set(start+i, subSegment);
        }
    }

    private void sortBasedOnXLines(List<Lines> segments, Integer start, Integer end){

        List<Lines> subSegments = segments.subList(start, end);

        subSegments.sort(Comparator.comparingDouble(Lines::x));
        // after sorting set the segments back and update the curRowStart on the way
        for (int i = 0; i < subSegments.size(); i++) {
            Lines subSegment = subSegments.get(i);
            segments.set(start+i, subSegment);
        }
    }

    private void sortBasedOnXPara(List<Para> paragraphs, Integer start, Integer end){

        List<Para> subSegments = paragraphs.subList(start, end);

        subSegments.sort(Comparator.comparingDouble(Para::getL));
        // after sorting set the segments back and update the curRowStart on the way
        for (int i = 0; i < subSegments.size(); i++) {
            Para subSegment = subSegments.get(i);
            paragraphs.set(start+i, subSegment);
        }
    }




}
