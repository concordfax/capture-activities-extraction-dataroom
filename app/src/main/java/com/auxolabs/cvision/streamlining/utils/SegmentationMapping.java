package com.auxolabs.cvision.streamlining.utils;

import com.auxolabs.cvision.streamlining.constants.SegmentationConstants;
import com.auxolabs.cvision.streamlining.models.ImageSegment;
import com.concordfax.capture.core.models.ocr.abbyy.Formatting;
import java.io.*;
import java.util.*;

public class SegmentationMapping {

    private SortingUtility sortingUtility = new SortingUtility();
    private int THRESHOLD = 10;

    public void compareSegmentsAndMerge(List<ImageSegment> segmentsFromOCR , List<ImageSegment> segmentsFromPositional) throws IOException {
        NLPBasedSegmentation nlpBasedSegmentation = new NLPBasedSegmentation();
        // merging based on CAPS
        nlpBasedSegmentation.createSegmentsByMergingWordsWithAllCapitalLetters(segmentsFromOCR);

        // merging with image based segmentation
        // given a segment obtained from image based segmentation, we try to identify all the segments
        // obtained from image based segmentation that can be accommodated in it.
        ImageSegment previousOCRSegment = null;
        for (ImageSegment wrappingImageSegment : segmentsFromPositional) {
            for (ImageSegment ocrSegment : segmentsFromOCR) {
                if (!ocrSegment.isVisited()){
                    // check if ocr segment can be included in a image based segment
                    if (wrappingImageSegment.getRect().contains(ocrSegment.getRect()) ||
                            checkIfLeftCornerSegment(ocrSegment, wrappingImageSegment) || checkIfMiddleSegment(ocrSegment, wrappingImageSegment)
                            || checkIfRightCornerSegment(ocrSegment, wrappingImageSegment)
                            || checkIfBottomSegment(previousOCRSegment, ocrSegment, wrappingImageSegment)) {

                        if(wrappingImageSegment.getSubSegments()==null)
                            wrappingImageSegment.setSubSegments(new ArrayList<>());

                        wrappingImageSegment.getSubSegments().add(ocrSegment);
                        ocrSegment.setVisited(true);
                    }
                    previousOCRSegment = ocrSegment;
                }
            }
        }

        // remove all ocr segments that have been included in a larger image segment, that have been marked with the visited tag
        segmentsFromOCR.removeIf(ImageSegment::isVisited);

        // find all segments that have subsegments
        for (ImageSegment imageSegment : segmentsFromPositional) {
            if(imageSegment.getSubSegments()!=null && !imageSegment.getSubSegments().isEmpty()){
                mergeSubSegmentsToFormLargerSegment(imageSegment);
                segmentsFromOCR.add(imageSegment);
            }
        }

        List<ImageSegment> segmentsMerged = new ArrayList<>();
        segmentsFromOCR.addAll(segmentsMerged);
        segmentsFromOCR.removeIf(ImageSegment::isVisited);

        //splitting segments based on column ":"
        nlpBasedSegmentation.splitSegmentsBasedOnSegmentSplitters(segmentsFromOCR);
    }


    // only handle left corner cases
    private boolean checkIfLeftCornerSegment(ImageSegment abbySegment, ImageSegment wrappingImageSegment) {
        try {
            return Math.abs(abbySegment.x() - wrappingImageSegment.x()) <= THRESHOLD
                    && Math.abs(abbySegment.y() - wrappingImageSegment.y()) <= THRESHOLD
                    && (abbySegment.h() - wrappingImageSegment.h() <= THRESHOLD || wrappingImageSegment.h() > abbySegment.h())
                    && (abbySegment.w() - wrappingImageSegment.w() <= THRESHOLD || wrappingImageSegment.w() > abbySegment.w());
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    // only handle right corner cases
    private boolean checkIfRightCornerSegment(ImageSegment abbySegment, ImageSegment wrappingImageSegment) {
        try {
            return abbySegment.x() > wrappingImageSegment.x()
                    && (Math.abs((abbySegment.x()) - (wrappingImageSegment.x() + wrappingImageSegment.w())) <= THRESHOLD
                    || Math.abs((abbySegment.x() + abbySegment.w()) - (wrappingImageSegment.x() + wrappingImageSegment.w())) <= THRESHOLD)
                    && Math.abs(abbySegment.y() - wrappingImageSegment.y()) <= THRESHOLD
                    && (abbySegment.h() - wrappingImageSegment.h() <= THRESHOLD || wrappingImageSegment.h() > abbySegment.h());
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    // only handle corner cases
    private boolean checkIfMiddleSegment(ImageSegment abbySegment, ImageSegment wrappingImageSegment) {
        try {
            return abbySegment.x() >= wrappingImageSegment.x()
                    && Math.abs(abbySegment.y() - wrappingImageSegment.y()) <= THRESHOLD
                    && (Math.abs(abbySegment.h() - wrappingImageSegment.h()) <= THRESHOLD || wrappingImageSegment.h() > abbySegment.h())
                    && ((abbySegment.w() + abbySegment.x()) <= (wrappingImageSegment.x() + wrappingImageSegment.w()));
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    // only handle patient name below cases
    private boolean checkIfBottomSegment(ImageSegment previousAbbySegment, ImageSegment currentAbbySegment,
                                         ImageSegment wrappingImageSegment ) {

        boolean isMergableBlock = false;
        try {
            if(null == previousAbbySegment) {
                isMergableBlock = false;
            } else {

                /***
                 * Check if the following conditions are satisfied
                 * 1. Difference in "x" coordinate between previous Abby segment and Image segment block is less than or equal to the 70
                 * 2. Difference in "y" coordinate between previous Abby segment and Image segment block is less than or equal to the THRESHOLD(10)
                 * 3. Check if previousAbbySegment is in UPPERCASE and ends with COMMA(a continuation sequence)
                 * 4. Check if currentAbbySegment is in UPPERCASE
                 * 5. Difference in "x" coordinate between between previous Abby segment and current Abby segment is less than or equal to the THRESHOLD(10)
                 * 6. Difference in "y" coordinate between previous Abby segment and current Abby segment is less than or equal to 50
                 * 7. Check if there is atleast any previous sub-segments to the current segment
                 */
                if ((null != wrappingImageSegment.getSubSegments())
                        && (Math.abs((previousAbbySegment.w() + previousAbbySegment.x()) - (wrappingImageSegment.x() + wrappingImageSegment.w())) <= 70)
                        && ((previousAbbySegment.y() - wrappingImageSegment.getRect().getY()) <= THRESHOLD)
                        && isUpperCase(previousAbbySegment.getText().trim())
                        && previousAbbySegment.getText().endsWith(SegmentationConstants.CONTINUATION_SEQUENCE)
                        && isUpperCase(currentAbbySegment.getText().trim())
                        && ((Math.abs(previousAbbySegment.x() - currentAbbySegment.x()) <= THRESHOLD)
                        && (Math.abs(previousAbbySegment.y() - currentAbbySegment.y())) <= 50)) {
                    isMergableBlock = true;
                }
            }
            return isMergableBlock;
        } catch (NullPointerException npe) {
            return isMergableBlock;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    private String valueOfTheSegment(List<ImageSegment> segmentsInside){
        StringBuilder text = new StringBuilder();
        for(ImageSegment subsegment: segmentsInside){
            text.append(subsegment.getText());
        }
        return text.toString();
    }

    // TODO: 2019-04-18 formatting should only be created from objects having similar formatting properties
    private Formatting createFormattingObjectByMergingFormatting(List<ImageSegment> segmentsInside){
        Formatting formatting = segmentsInside.get(0).getFormatting();
        for(int i=1; i<segmentsInside.size(); i++){
            formatting.getCharParams().addAll(segmentsInside.get(i).getFormatting().getCharParams());
        }
        return formatting;
    }

    /**
     * Creates a larger segment which can be used to hold multiple seb-segments, initialized with the wrappingImageSegment
     */
    private void mergeSubSegmentsToFormLargerSegment(ImageSegment wrappingImageSegment){
        sortingUtility.sortMappedSegments(wrappingImageSegment.getSubSegments());
        wrappingImageSegment.setText(valueOfTheSegment(wrappingImageSegment.getSubSegments()));
        Double x = wrappingImageSegment.getSubSegments().get(0).x();
        Double w = ((wrappingImageSegment.getSubSegments().get(wrappingImageSegment.getSubSegments().size()-1).x())+
                (wrappingImageSegment.getSubSegments().get(wrappingImageSegment.getSubSegments().size()-1).w()))-(wrappingImageSegment.getSubSegments().get(0).x());
        Double h = wrappingImageSegment.h();
        Double y = wrappingImageSegment.y();
        wrappingImageSegment.setRect(x,y,w,h);
        wrappingImageSegment.setFormatting(createFormattingObjectByMergingFormatting(wrappingImageSegment.getSubSegments()));
        // TODO: 2019-04-17 block id will correspond to that of the first element
        wrappingImageSegment.setBlockId(wrappingImageSegment.getSubSegments().get(0).getBlockId());
        wrappingImageSegment.setParaId(wrappingImageSegment.getSubSegments().get(0).getParaId());
        wrappingImageSegment.setLineId(wrappingImageSegment.getSubSegments().get(0).getLineId());
        wrappingImageSegment.getSubSegments().clear();
    }

    private boolean isUpperCase(String s) {
        boolean ok = true;
        for (char c : s.toCharArray()) {
            if (Character.isLetter(c) && !Character.isUpperCase(c)) {
                ok = false;
                break;
            }
        }
        // check if has atleast one char
        if (!s.matches(".*[a-zA-Z0-9].*"))
            return false;
        return ok;
    }

}
