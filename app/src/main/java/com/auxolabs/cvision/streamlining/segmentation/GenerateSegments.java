package com.auxolabs.cvision.streamlining.segmentation;

import com.auxolabs.cvision.models.ocr.*;
import com.auxolabs.cvision.streamlining.models.ImageSegment;
import com.concordfax.capture.core.models.ocr.abbyy.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GenerateSegments {
    private int blockId = 0;
    private int wordId=0;
    private int paraId = 0;
    private int lineId=0;

    public  void getSegment(Page page,List<ImageSegment> ocrSegments,List<SegBlock> segBlockList) {
        if (page.getBlocks() != null) {
            page.getBlocks().forEach(block -> {
                switch (block.getBlockType()) {
                    case AbbyyConstants.BlockType.TEXT:
                        if (block.getText() != null) {
                            for (Text text : block.getText()) {
                                try {
                                    extractDataFromTextModel(text, block,ocrSegments,segBlockList);
                                } catch (Exception e) {
                                    throw new RuntimeException(e);
                                }
                            }
                        }
                        break;
                    case AbbyyConstants.BlockType.TABLE:
                        if (block.getRow() != null) {
                            block.getRow().forEach(row -> {
                                row.getCell().forEach(cell -> {
                                    if (cell.getText() != null) {
                                        for (Text text : cell.getText()) {
                                            try {
                                                extractDataFromTextModel(text, block,ocrSegments,segBlockList);
                                            } catch (Exception e) {
                                                throw new RuntimeException(e);
                                            }
                                        }
                                    }
                                });
                            });
                        }
                        break;
                    case AbbyyConstants.BlockType.SEPARATOR:
                        segBlockList.add(combineParasAndGetBlock(new ArrayList<>(), block));
                        break;
                }
            });
        }
    }

    private  void extractDataFromTextModel(Text text, Block block,List<ImageSegment> ocrSegments,List<SegBlock> segBlockList) throws IOException {
        List<Para> paraList = new ArrayList<>();
        if (text.getPara() != null) {
            for (Paragraph paragraph : text.getPara()) {
                List<Lines> lineList = new ArrayList<>();
                if (paragraph.getLine() != null) {
                    for (Line line : paragraph.getLine()) {
                        List<Segment> segmentList = new ArrayList<>();
                        if (line.getFormatting() != null) {
                            line.getFormatting().forEach(formatting -> {
                                if (formatting != null && formatting.getCharParams() != null) {
                                    Segment segment = combineCharParamsAndGetSegment(formatting);
                                    segmentList.add(segment);

                                }
                            });
                            //creating segments from formatting in the form of ImageSegment
                            for (Segment segment : segmentList) {
                                ImageSegment imageSegment = new ImageSegment();
                                imageSegment.setBlockId(blockId);
                                imageSegment.setParaId(paraId);
                                imageSegment.setLineId(lineId);
                                imageSegment.setText(segment.getValue());
                                imageSegment.setFormatting(segment.getFormatting());
                                imageSegment.setRect(segment.getL(), segment.getT(), (segment.getR() - segment.getL()), (segment.getB() - segment.getT()));
                                imageSegment.setId(wordId);
                                wordId++;
                                ocrSegments.add(imageSegment);
                            }
                            Lines linesFromOcr = combineSegmentsAndGetLines(line, segmentList);
                            lineList.add(linesFromOcr);
                        }
                    }
                }
                if (lineList.size() > 0) {
                    Para paragraphsFromOcr = combineLinesAndGetPara(lineList, paragraph);
                    paraList.add(paragraphsFromOcr);
                }
            }
        }

        segBlockList.add(combineParasAndGetBlock(paraList, block));
    }

    private Segment combineCharParamsAndGetSegment(Formatting formatting){
        Segment segment = new Segment();
        StringBuilder segmentBuilder = new StringBuilder();
        List<CharParams> charParams = formatting.getCharParams();
        for (int i = 0; i < charParams.size(); i++) {
            CharParams charParam = charParams.get(i);
            if (charParam.getValue().isEmpty()) {
                segmentBuilder.append(" ");
            }
            //to add space after special character appeared in a word ex: before: Raji,Forrest afterthis function it will be Raji, Forrest Before: MR#123456 after: MR# 123456 ; beneficial for Mandrake.
            if (charParam.getWordFirst() != null && charParam.getWordFirst().equals("1")){
                if (i==0 && segmentBuilder.length()>0 && !Character.isLetterOrDigit(segmentBuilder.toString().substring(segmentBuilder.length()-1).charAt(0)) ||
                        i-1>=0 && charParams.get(i-1) !=null && !Character.isLetterOrDigit(charParams.get(i-1).getValue().charAt(0)) && !Character.isWhitespace(charParams.get(i-1).getValue().charAt(0)) ){
                    segmentBuilder.append(" ");
                    charParams.add(i,getCharParamsForSpace(charParams.get(i-1)));
                    i++;


                }
            }
            segmentBuilder.append(charParam.getValue());
        }
        String segmentValue = segmentBuilder.toString();
        segmentValue = trimMultipleSpaces(segmentValue);
        segment.setValue(segmentValue);
        segment.setFormatting(formatting);
            Point segmentCoordinate = new Point(-1,-1,-1,-1);
            for (int i = 0; i < formatting.getCharParams().size(); i++){
                getCoordinatesOfParaOrSegmentOrBlock(segmentCoordinate,formatting.getCharParams().get(i));
            setCoordinatesOfParaOrSegmentOrBlock(segment,segmentCoordinate);
        }

        return segment;
    }

    private CharParams getCharParamsForSpace(CharParams prevCharParam){
        CharParams charParamForSpace = new CharParams();
        charParamForSpace.setValue(" ");
        charParamForSpace.setL(prevCharParam.getL()+5);
        charParamForSpace.setT(prevCharParam.getT());
        charParamForSpace.setR(prevCharParam.getR()+5);
        charParamForSpace.setB(prevCharParam.getB());
        return charParamForSpace;
    }

    private  Lines combineSegmentsAndGetLines(Line line, List<Segment> segmentList) {
        Lines lines = new Lines();
        StringBuilder lineBuilder = new StringBuilder();
        for (Segment segment : segmentList) {
            lineBuilder.append(segment.getValue());
        }
        String lineValue = lineBuilder.toString();
        lineValue = trimMultipleSpaces(lineValue);
        lines.setValue(lineValue);
        lines.setLineID(lineId);
        lines.setSegments(segmentList);
        lines.setB(line.getB());
        lines.setL(line.getL());
        lines.setR(line.getR());
        lines.setT(line.getT());
        lines.setBaseLine(line.getBaseline());
        lineId++;
        lines.setBlockId(blockId);
        lines.setParaId(paraId);
        return lines;
    }

    private static String trimMultipleSpaces(String phrase) {
        return phrase.replaceAll(" +", " ");
    }

    private  void getCoordinatesOfParaOrSegmentOrBlock(Point wordPoint, Object charOrLineOrPara) {
        if (charOrLineOrPara instanceof CharParams) {
            CharParams charParams = (CharParams) charOrLineOrPara;
            if (!(charParams.getL() == 0 || charParams.getB() == 0 || charParams.getR() == 0 || charParams.getT() == 0)) {
                if (wordPoint.getL() == -1 || wordPoint.getL() > charParams.getL()) {
                    wordPoint.setL((charParams.getL()));
                }
                if (wordPoint.getT() == -1 || wordPoint.getT() > charParams.getT()) {
                    wordPoint.setT((charParams.getT()));
                }
                if (wordPoint.getR() == -1 || wordPoint.getR() < charParams.getR()) {
                    wordPoint.setR((charParams.getR()));
                }
                if (wordPoint.getB() == -1 || wordPoint.getB() < charParams.getB()) {
                    wordPoint.setB((charParams.getB()));
                }
            }
        } else if (charOrLineOrPara instanceof Lines) {
            Lines line = (Lines) charOrLineOrPara;
            if (!(line.getL() == 0 || line.getB() == 0 || line.getR() == 0 || line.getT() == 0)) {
                if (wordPoint.getL() == -1 || wordPoint.getL() > line.getL()) {
                    wordPoint.setL((line.getL()));
                }
                if (wordPoint.getT() == -1 || wordPoint.getT() > line.getT()) {
                    wordPoint.setT((line.getT()));
                }
                if (wordPoint.getR() == -1 || wordPoint.getR() < line.getR()) {
                    wordPoint.setR((line.getR()));
                }
                if (wordPoint.getB() == -1 || wordPoint.getB() < line.getB()) {
                    wordPoint.setB((line.getB()));
                }
            }
        } else if (charOrLineOrPara instanceof Para) {
            Para para = (Para) charOrLineOrPara;
            if (!(para.getL() == 0 || para.getB() == 0 || para.getR() == 0 || para.getT() == 0)) {
                if (wordPoint.getL() == -1 || wordPoint.getL() > para.getL()) {
                    wordPoint.setL((para.getL()));
                }
                if (wordPoint.getT() == -1 || wordPoint.getT() > para.getT()) {
                    wordPoint.setT((para.getT()));
                }
                if (wordPoint.getR() == -1 || wordPoint.getR() < para.getR()) {
                    wordPoint.setR((para.getR()));
                }
                if (wordPoint.getB() == -1 || wordPoint.getB() < para.getB()) {
                    wordPoint.setB((para.getB()));
                }
            }
        }
    }

    private  void setCoordinatesOfParaOrSegmentOrBlock(Object coordinates, Point point) {
        if (coordinates instanceof Segment) {
            Segment segment = (Segment) coordinates;
            segment.setT(point.getT());
            segment.setL(point.getL());
            segment.setR(point.getR());
            segment.setB(point.getB());
        } else if (coordinates instanceof Para) {
            Para para = (Para) coordinates;
            para.setT(point.getT());
            para.setL(point.getL());
            para.setR(point.getR());
            para.setB(point.getB());
        } else if (coordinates instanceof SegBlock) {
            SegBlock segBlock = (SegBlock) coordinates;
            segBlock.setT(point.getT());
            segBlock.setL(point.getL());
            segBlock.setR(point.getR());
            segBlock.setB(point.getB());
        }
    }

    private  Para combineLinesAndGetPara(List<Lines> linesList, Paragraph paragraphRef) {
        Para paras = new Para();
        paras.setParaId(paraId);
        paras.setLines(linesList);
        StringBuilder paraBuilder = new StringBuilder();
        for (Lines lines : linesList) {
            paraBuilder.append(lines.getValue()).append(" ");
        }
        String paragraph = paraBuilder.toString();
        paragraph = trimMultipleSpaces(paragraph);
        paras.setValue(paragraph);
        paras.setParagraphRef(paragraphRef);
        Point paragraphCoordinate = new Point(-1, -1, -1, -1);
        for (Lines lineInParagraph : linesList) {
            getCoordinatesOfParaOrSegmentOrBlock(paragraphCoordinate, lineInParagraph);
        }
        setCoordinatesOfParaOrSegmentOrBlock(paras, paragraphCoordinate);
        paras.setBlockId(blockId);
        paraId++;
        return paras;
    }

    private  SegBlock combineParasAndGetBlock(List<Para> paragraphsInBlock, Block block) {
        SegBlock segBlock = new SegBlock();
        StringBuilder sb = new StringBuilder();
        for (Para paragraph : paragraphsInBlock) {
            sb.append(paragraph.getValue());
        }
        segBlock.setBlockId(blockId++);
        segBlock.setParagraphList(paragraphsInBlock);
        segBlock.setB(Integer.valueOf(block.getB()));
        segBlock.setL(Integer.valueOf(block.getL()));
        segBlock.setR(Integer.valueOf(block.getR()));
        segBlock.setT(Integer.valueOf(block.getT()));
        segBlock.setBlock(block);
        return segBlock;
    }
}
