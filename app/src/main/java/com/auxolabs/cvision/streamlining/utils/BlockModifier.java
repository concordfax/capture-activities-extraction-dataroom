package com.auxolabs.cvision.streamlining.utils;

import com.auxolabs.cvision.models.ocr.SegBlock;

import java.util.HashSet;
import java.util.List;

public class BlockModifier {

    public void intersectIfOverlapping(List<SegBlock> segBlockList){
        HashSet<SegBlock> invalidated = new HashSet<>();
        for (int i = 0; i < segBlockList.size(); i++) {
            for (int j = i+1; j < segBlockList.size(); j++) {
                SegBlock segBlock1 = segBlockList.get(i);
                SegBlock segBlock2 = segBlockList.get(j);
                Integer areaI = isOverlappingBlocks(segBlock1, segBlock2);
                Integer area1 = area(segBlock1);
                Integer area2 = area(segBlock2);
                if(areaI>0) {
                    Double percentage1 = 100.0 * areaI / area1;
                    Double percentage2 = 100.0 * areaI / area2;
                    if (percentage1 > percentage2 && percentage1 >= 50.0) {
                        merge(segBlock2, segBlock1);
                        invalidated.add(segBlock1);
                    }
                    if (percentage2 > percentage1 && percentage2 >= 50.0) {
                        merge(segBlock1, segBlock2);
                        invalidated.add(segBlock2);
                    }
                }
            }
        }
        segBlockList.removeIf(invalidated::contains);
    }

    private Integer isOverlappingBlocks(SegBlock block1, SegBlock block2){
        return Math.max(0, Math.min(block1.getR(), block2.getR()) -
                Math.max(block1.getL(), block2.getL())) *
                Math.max(0, Math.min(block1.getB(), block2.getB()) -
                        Math.max(block1.getT(), block2.getT()));
    }

    private void merge(SegBlock block1, SegBlock block2){
        block1.setValue(block1.getValue()+" "+block2.getValue());
        block1.setL(Math.min(block1.getL(), block2.getL()));
        block1.setT(Math.min(block1.getT(), block2.getT()));
        block1.setR(Math.max(block1.getR(), block2.getR()));
        block1.setB(Math.max(block1.getB(), block2.getB()));
        block1.getParagraphList().addAll(block2.getParagraphList());
    }

    private Integer area(SegBlock segBlock){
        return ( segBlock.getR()- segBlock.getL() ) * (segBlock.getB() - segBlock.getT() );
    }
}
