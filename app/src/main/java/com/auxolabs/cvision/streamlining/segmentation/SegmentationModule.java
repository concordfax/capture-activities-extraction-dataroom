package com.auxolabs.cvision.streamlining.segmentation;

import com.auxolabs.cvision.models.ocr.*;
import com.auxolabs.cvision.streamlining.utils.*;
import com.auxolabs.cvision.streamlining.models.ImageSegment;
import com.concordfax.capture.core.models.ocr.abbyy.*;

import java.io.*;
import java.util.*;

import static org.bytedeco.javacpp.lept.returnErrorFloat;
import static scala.collection.immutable.Nil.indexOf;
import static scala.collection.immutable.Nil.thisCollection;

public class SegmentationModule {

    private SegmentationMapping segmentationMapping;
    private List<SegBlock> segBlockList;
    /**
     * Segments obtained from the OCR process; We use the "formatting" object within the Abbyy OCR output
     */
    private List<ImageSegment> ocrSegments;
    /**
     * Segments obtained as a result of image based segmentation
     */
    // TODO: 28/6/19 change all ids from global to passing variables
    private List<ImageSegment> imageSegments;
    private SortingUtility sortingUtility;
    private LinesMerger linesMerger;
    private GenerateSegments generateSegments;
    private CharLevelModifier charLevelModifier;
    private BlockModifier blockModifier;
    private PreProcessSegment preProcessSegment;

    public SegmentationModule(List<ImageSegment> imageSegments){
        this.imageSegments = imageSegments;
        this.ocrSegments = new ArrayList<>();
        this.segBlockList = new ArrayList<>();
        this.sortingUtility = new SortingUtility();
        this.segmentationMapping = new SegmentationMapping();
        this.linesMerger = new LinesMerger();
        this.generateSegments = new GenerateSegments();
        this.charLevelModifier = new CharLevelModifier();
        this.blockModifier = new BlockModifier();
        this.preProcessSegment = new PreProcessSegment();


    }

    /**
     * Hierarchy:
     *  Block -> Para(*) -> Line(*) -> Segment
     *  Para and Line are yet to be integrated
     * @param page
     * @return
     * @throws IOException
     */
    public  List<SegBlock> getBlocks(Page page) throws IOException {
        // converts OCR output to word level segments and creates a list of blocks as well;
        // Blocks can hold a number of segments, Blocks are populated into the segBlockList field
        // world level segments are added to ocrSegments field
        generateSegments.getSegment(page,ocrSegments,segBlockList);
        // The segments obtained from image based segmentation is mapped to the segments obtained from OCR
        // the image based segments are available in <code>private List<ImageSegment> imageSegments</code>
        // the final segments after merging are stored in <code>List<ImageSegment> segmentsComparedAndMerged</code>
        segmentationMapping.compareSegmentsAndMerge(ocrSegments, imageSegments);

        // collect all segments with same block id and add them as a block
        // TODO: 2019-04-17 make this process more efficient
        segBlockList.forEach(segBlock->{
            ocrSegments.forEach(segment -> {
                if (segBlock.getBlockId() == segment.getBlockId()) {
                    segBlock.getMappedSegments().add(segment);

                }
            });
        });
        segBlockList.forEach(segBlock -> {
            segBlock.getParagraphList().forEach(para->{
                segBlock.getMappedSegments().forEach(segment->{
                    if(para.getParaId()==segment.getParaId()){
                        para.getMappedSegments().add(segment);
                    }
                });
            });
        });
        segBlockList.forEach(segBlock -> {
            segBlock.getParagraphList().forEach(para -> {
                para.getLines().forEach(lines->{
                    para.getMappedSegments().forEach(segment->{
                        if(lines.getLineID() == segment.getLineId()){
                            lines.getMappedSegments().add(segment);
                        }

                    });
                    sortingUtility.sortMappedSegments(lines.getMappedSegments());
                });
            });
        });

        segBlockList.forEach(segBlock -> {
            segBlock.getParagraphList().forEach(para->{
                if(!para.getLines().isEmpty()){
                    para.getLines().removeIf(lines -> lines.getMappedSegments().isEmpty());

                    for(int i=0;i<para.getLines().size();i++){
                        StringBuilder lineValue = new StringBuilder();
                        for(ImageSegment mappedSeg:para.getLines().get(i).getMappedSegments()){
                            lineValue.append(mappedSeg.getText());
                        }
                        para.getLines().get(i).setValue(lineValue.toString());
                        para.getLines().get(i).setL((int) para.getLines().get(i).getMappedSegments().get(0).getRect().getX());
                        para.getLines().get(i).setT((int) para.getLines().get(i).getMappedSegments().get(0).getRect().getY());
                        para.getLines().get(i).setR((int) (para.getLines().get(i).getMappedSegments().get(para.getLines().get(i).getMappedSegments().size() - 1).getRect().getX() + para.getLines().get(i).getMappedSegments().get(para.getLines().get(i).getMappedSegments().size() - 1).getRect().getWidth()));
                        para.getLines().get(i).setB((int) (para.getLines().get(i).getMappedSegments().get(para.getLines().get(i).getMappedSegments().size() - 1).getRect().getY() + para.getLines().get(i).getMappedSegments().get(para.getLines().get(i).getMappedSegments().size() - 1).getRect().getHeight()));


                    }
                }
            });
        });

        linesMerger.mergeLinesOnTextProperties(segBlockList);
        modifyPargraphAndBlocksAfterLineMerge();
        sortBlocks();
        blockModifier.intersectIfOverlapping(segBlockList);

        // remove empty blocks if any
        segBlockList.removeIf(segBlock -> segBlock.getParagraphList().isEmpty());

        preProcessSegment.preProcessSegment(segBlockList);

        //creating good paragraphs
        segBlockList.forEach(blocks->{
            if(!blocks.getParagraphList().isEmpty()) {
                blocks.getParagraphList().forEach(para -> {
                    sortingUtility.sortLines(para.getLines());
                    StringBuilder paraBuilder = new StringBuilder();
                    Point paragraphCoordinate = new Point(-1, -1, -1, -1);
                    for (int i = 0; i < para.getLines().size(); i++) {
                        if (paraBuilder.toString().length() == 0 || paraBuilder.toString().endsWith(" ")) {
                            paraBuilder.append(para.getLines().get(i).getValue());
                        } else {
                            paraBuilder.append(" ").append(para.getLines().get(i).getValue());
                            para.getLines().get(i).setValue(" " + para.getLines().get(i).getValue());
                            charLevelModifier.addNewCharParamsForSpaceAsFirstChar(para.getLines().get(i).getMappedSegments().get(0));
                        }

                        getCoordinatesOfParaOrSegmentOrBlock(paragraphCoordinate, para.getLines().get(i));
                    }
                    setCoordinatesOfParaOrSegmentOrBlock(para, paragraphCoordinate);
                    String paragraph = paraBuilder.toString();
                    para.setValue(paragraph);
                });
            }
        });

        //creating blocks
        segBlockList.forEach(blocks->{
            sortingUtility.sortParagraphs(blocks.getParagraphList());
            StringBuilder blockBuilder = new StringBuilder();
            Point blockCoordinate = new Point(-1,-1,-1,-1);
            for (int i=0;i<blocks.getParagraphList().size();i++) {
                if(blockBuilder.toString().length()==0 || blockBuilder.toString().endsWith(" ") ){
                    blockBuilder.append(blocks.getParagraphList().get(i).getValue());
                }
                else{
                    blockBuilder.append(" ");
                    blockBuilder.append(blocks.getParagraphList().get(i).getValue());
                    blocks.getParagraphList().get(i).setValue(" "+blocks.getParagraphList().get(i).getValue());
                    charLevelModifier.addNewCharParamsForSpaceAsFirstChar(blocks.getParagraphList().get(i).getLines().get(0).getMappedSegments().get(0));
                }
                getCoordinatesOfParaOrSegmentOrBlock(blockCoordinate,blocks.getParagraphList().get(i));
            }

            setCoordinatesOfParaOrSegmentOrBlock(blocks,blockCoordinate);
            String blockValue = blockBuilder.toString();
            blocks.setValue(blockValue);


        });

        segBlockList.forEach(segBlock -> {
            segBlock.getParagraphList().forEach(para->{
                para.getMappedSegments().clear();
            });
            segBlock.getMappedSegments().clear();
        });
        return segBlockList;
    }

    private void modifyPargraphAndBlocksAfterLineMerge(){
        for (int i = 0; i < segBlockList.size(); i++) {
            SegBlock segBlock = segBlockList.get(i);
            List<Para> paragraphList1 = segBlock.getParagraphList();
            for (int j = 0; j < paragraphList1.size(); j++) {
                Para para = paragraphList1.get(j);
                if (para.getLines().size() > 0) {
                    // TODO: 18/10/18 pass correct paragraph ref
                    Para tempPara = combineLinesAndGetPara(para.getLines(), null);
                    paragraphList1.set(j, tempPara);
                }
                else {
                    paragraphList1.remove(j);
                    j--;
                }
            }
            SegBlock tempSegBlock = combineParasAndGetBlock(segBlock, paragraphList1);
            segBlockList.set(i, tempSegBlock);
        }
    }

    private Para combineLinesAndGetPara(List<Lines> linesList, Paragraph paragraphRef){
        Para paras = new Para();
        paras.setLines(linesList);
        StringBuilder paraBuilder = new StringBuilder();
        for (Lines lines : linesList){
            paraBuilder.append(lines.getValue()).append(" ");
        }
        String paragraph = paraBuilder.toString();
        paragraph = trimMultipleSpaces(paragraph);
        paras.setValue(paragraph);
        paras.setParagraphRef(paragraphRef);
        Point paragraphCoordinate = new Point(-1,-1,-1,-1);
        for (Lines lineInParagraph : linesList) {
            getCoordinatesOfParaOrSegmentOrBlock(paragraphCoordinate, lineInParagraph);
        }
        setCoordinatesOfParaOrSegmentOrBlock(paras,paragraphCoordinate);
        return paras;
    }

    private SegBlock combineParasAndGetBlock(SegBlock segBlock, List<Para> paragraphsInBlock){
        StringBuilder sb = new StringBuilder();
        for (Para paragraph : paragraphsInBlock){
            sb.append(paragraph.getValue()).append(" ");
        }
        segBlock.setParagraphList(paragraphsInBlock);
        segBlock.setValue(sb.toString());
        Point blockCoordinate = new Point(-1,-1,-1,-1);
        for (Para paragraph : paragraphsInBlock) {
            getCoordinatesOfParaOrSegmentOrBlock(blockCoordinate, paragraph);
        }
        setCoordinatesOfParaOrSegmentOrBlock(segBlock,blockCoordinate);
        return segBlock;
    }

    private String trimMultipleSpaces(String phrase){
        return phrase.replaceAll(" +", " ");
    }

    private void sortBlocks(){
        segBlockList.sort(Comparator.comparingInt(SegBlock::getT));


    }

    private StringBuilder valueOfTheSegment(List<ImageSegment> segmentsInside){
        StringBuilder text = new StringBuilder(segmentsInside.get(0).getText());
        for(int j=1;j<segmentsInside.size();j++){
            //to add space infront of every segment in the blocks while generating value for block
            if(!Character.isWhitespace(text.toString().charAt(text.length()-1))){
                text.append(" ");
            }
            text.append(segmentsInside.get(j).getText());
        }
        return text;
    }

    private  void setCoordinatesOfParaOrSegmentOrBlock(Object coordinates, Point point) {
        if (coordinates instanceof Segment) {
            Segment segment = (Segment) coordinates;
            segment.setT(point.getT());
            segment.setL(point.getL());
            segment.setR(point.getR());
            segment.setB(point.getB());
        } else if (coordinates instanceof Para) {
            Para para = (Para) coordinates;
            para.setT(point.getT());
            para.setL(point.getL());
            para.setR(point.getR());
            para.setB(point.getB());
        } else if (coordinates instanceof SegBlock) {
            SegBlock segBlock = (SegBlock) coordinates;
            segBlock.setT(point.getT());
            segBlock.setL(point.getL());
            segBlock.setR(point.getR());
            segBlock.setB(point.getB());
        }
    }

    private  void getCoordinatesOfParaOrSegmentOrBlock(Point wordPoint, Object charOrLineOrPara) {
        if (charOrLineOrPara instanceof CharParams) {
            CharParams charParams = (CharParams) charOrLineOrPara;
            if (!(charParams.getL() == 0 || charParams.getB() == 0 || charParams.getR() == 0 || charParams.getT() == 0)) {
                if (wordPoint.getL() == -1 || wordPoint.getL() > charParams.getL()) {
                    wordPoint.setL((charParams.getL()));
                }
                if (wordPoint.getT() == -1 || wordPoint.getT() > charParams.getT()) {
                    wordPoint.setT((charParams.getT()));
                }
                if (wordPoint.getR() == -1 || wordPoint.getR() < charParams.getR()) {
                    wordPoint.setR((charParams.getR()));
                }
                if (wordPoint.getB() == -1 || wordPoint.getB() < charParams.getB()) {
                    wordPoint.setB((charParams.getB()));
                }
            }
        } else if (charOrLineOrPara instanceof Lines) {
            Lines line = (Lines) charOrLineOrPara;
            if (!(line.getL() == 0 || line.getB() == 0 || line.getR() == 0 || line.getT() == 0)) {
                if (wordPoint.getL() == -1 || wordPoint.getL() > line.getL()) {
                    wordPoint.setL((line.getL()));
                }
                if (wordPoint.getT() == -1 || wordPoint.getT() > line.getT()) {
                    wordPoint.setT((line.getT()));
                }
                if (wordPoint.getR() == -1 || wordPoint.getR() < line.getR()) {
                    wordPoint.setR((line.getR()));
                }
                if (wordPoint.getB() == -1 || wordPoint.getB() < line.getB()) {
                    wordPoint.setB((line.getB()));
                }
            }
        } else if (charOrLineOrPara instanceof Para) {
            Para para = (Para) charOrLineOrPara;
            if (!(para.getL() == 0 || para.getB() == 0 || para.getR() == 0 || para.getT() == 0)) {
                if (wordPoint.getL() == -1 || wordPoint.getL() > para.getL()) {
                    wordPoint.setL((para.getL()));
                }
                if (wordPoint.getT() == -1 || wordPoint.getT() > para.getT()) {
                    wordPoint.setT((para.getT()));
                }
                if (wordPoint.getR() == -1 || wordPoint.getR() < para.getR()) {
                    wordPoint.setR((para.getR()));
                }
                if (wordPoint.getB() == -1 || wordPoint.getB() < para.getB()) {
                    wordPoint.setB((para.getB()));
                }
            }
        }
    }

}

