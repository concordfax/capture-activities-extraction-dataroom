package com.auxolabs.cvision.streamlining.utils;

import com.auxolabs.cvision.models.ocr.SegBlock;
import com.auxolabs.cvision.streamlining.models.ImageSegment;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextPreProcessingUtility {


    public static String preprocess(String text) {
        text = preprocessPossibleSSN(text);
//        text = preprocessPossibleDOB(text);
//        text = preprocessPossibleDates(text);
        text = preprocessErroneousTexts(text);
        text = text.replaceAll("[^\\\\a-zA-Z0-9/.\\s-#@,']", " ");
        return text.replaceAll("(\\s)+", " ");
    }

    private static String preprocessErroneousTexts(String preprocessErroneousText) {
        String textRegex = "Enc.(\\s)+Date";
        preprocessErroneousText = processRegex(preprocessErroneousText, textRegex, textRegex, "Enc Date");
        return preprocessErroneousText;

    }

    // TODO: 2019-06-07 "*" and "#" in the ssnRegex doesn't seem to work for "***-22-4444"
    private static String preprocessPossibleSSN(String possibleSSNText) {
        String ssnRegex = "(\\b([\\dXx*#]{3}\\s*)([\\.\\-\\,])(\\s*[\\dXx*#]{2}\\s*)([\\.\\-\\,])(\\s*\\d{4})\\b)|[\\s:]([*]{9})\\b";
        possibleSSNText = processRegex(possibleSSNText, ssnRegex, "(\\s)*[\\-,\\\\.](\\s)*", "-");
        possibleSSNText = processRegex(possibleSSNText, ssnRegex, "[:]", ": ");
        possibleSSNText = processRegex(possibleSSNText, ssnRegex, "[*]", "X");
        return processRegex(possibleSSNText, ssnRegex, "[#]", "X");
    }

    private static String preprocessPossibleDOB(String possibleDOBText) {
        String dobRegex = "\\b(\\d{1,2}\\s*)([\\.\\-\\,/])(\\s*\\d{1,2}\\s*)([\\.\\-\\,/])(\\s*\\d{4})\\b";
        return processRegex(possibleDOBText, dobRegex, "(\\s)*[\\-,\\/\\.](\\s)*", "/");
    }

    private static String preprocessPossibleDates(String possibleDatesText) {
        String dobRegex = "\\b(((\\d{1,2}\\s*)([\\.\\,/]))?(\\s*\\d{1,2}\\s*)([\\.\\,/]))?(\\s*\\d{4})-";
        return processRegex(possibleDatesText, dobRegex, "(\\s)*[\\-](\\s)*", " - ");
    }

    private static String processRegex(String s, String regex, String originalPattern, String replaceText) {
        StringBuilder formattedText = new StringBuilder("");
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(s);
        int currentEnd = 0;
        while (matcher.find()) {
            String matchedText = s.substring(matcher.start(), matcher.end());
            matchedText = matchedText.replaceAll(originalPattern, replaceText);
            matchedText = matchedText.toUpperCase();
            if (matcher.end() <= s.length()) {
                formattedText.append(s.substring(currentEnd, matcher.start()));
                formattedText.append(matchedText);
                formattedText.append(" ");
                currentEnd = matcher.end();
            }
        }
        if (currentEnd < s.length()) {
            formattedText.append(s.substring(currentEnd, s.length()));
        }
        return formattedText.toString();
    }
}
