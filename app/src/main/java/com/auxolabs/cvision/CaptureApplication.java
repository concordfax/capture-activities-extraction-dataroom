package com.auxolabs.cvision;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.util.ContextInitializer;
import com.auxolabs.cvision.di.DaggerICapture;
import com.auxolabs.cvision.di.HealthcheckResourceDropwizard;
import com.auxolabs.cvision.di.ICapture;
import com.auxolabs.cvision.web.config.CacheControlFilter;
import com.auxolabs.cvision.web.config.CaptureConfig;
import com.auxolabs.cvision.web.init.CaptureInitializer;
import com.auxolabs.cvision.web.resources.ExtendedHealthCheckResource;
import com.auxolabs.cvision.web.resources.ExtendedJobManagerResource;
import com.concordfax.capture.core.web.resources.JobManagerResourceDropwizard;
import edu.stanford.nlp.util.logging.RedwoodConfiguration;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.slf4j.LoggerFactory;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;

@Slf4j
public class CaptureApplication extends Application<CaptureConfig> {

    private static final String CACHE_FILTER = "cacheFilter";

    @Getter
    private static CaptureConfig captureConfig;

    @Getter
    private static ICapture iCapture;

    public static void main(String[] args) throws Exception {
        System.getProperties().setProperty("log4j.logger.com.joestelmach.natty", "OFF");
        RedwoodConfiguration.current().clear().apply();
        new CaptureApplication().run(args);
    }

    @Override
    public void initialize(Bootstrap<CaptureConfig> bootstrap) {
        bootstrap.addBundle(new SwaggerBundle<CaptureConfig>() {
            @Override
            protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(CaptureConfig configuration) {
                return configuration.getSwaggerBundleConfiguration();
            }
        });
    }

    private void startServices() {
        CaptureInitializer captureInitializer = CaptureApplication.iCapture.ocrInit();
    }

    @Override
    public void run(CaptureConfig configuration, Environment environment) throws Exception {

        captureConfig = configuration;

        CaptureApplication.iCapture = DaggerICapture.create();
        ExtendedJobManagerResource jobManagerResourceDropwizard = iCapture.jobManagerResource();
        ExtendedHealthCheckResource healthCheckResourceDropwizard = iCapture.healthCheckResource();

        startServices();
        LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
        context.reset();
        ContextInitializer initializer = new ContextInitializer(context);
        initializer.autoConfig();

        // Enable CORS headers
        final FilterRegistration.Dynamic cors =
                environment.servlets().addFilter("CORS", CrossOriginFilter.class);
        cors.setInitParameter("allowedOrigins", "*");
        cors.setInitParameter("allowedHeaders", "X-Requested-With,Content-Type,Accept,Origin, X-Auth-User");
        cors.setInitParameter("allowedMethods", "GET,PUT,POST,DELETE,OPTIONS,HEAD");
        // Add URL mapping
        cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
        environment.jersey().register(healthCheckResourceDropwizard);
        environment.jersey().register(jobManagerResourceDropwizard);

        environment.servlets().addFilter(CACHE_FILTER, new CacheControlFilter())
                .addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");

    }
}