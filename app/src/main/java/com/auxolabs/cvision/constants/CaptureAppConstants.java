package com.auxolabs.cvision.constants;

public class CaptureAppConstants {

    public static final String PERSON_NAME = "Person_Name";

    public static class ANNOTATIONS {
        public static final String FIRST_NAME = "patient_first_name";
        public static final String MIDDLE_NAME = "patient_middle_initial";
        public static final String LAST_NAME = "patient_last_name";
        public static final String DOB = "DOB";
    }

    public static class InitConstants {
        public static final String LEXICON_BASE_FOLDER = "nlp/lexicon";
        public static final String NAMES_FOLDER = "nlp/names";
    }

    public static class AzureDependencies {
        public static class Cosmos {
            public static final String CONTAINER = "container";
            public static final String CONNECTION_STRING = "connString";
        }
    }

    public static class TAG_ANNOTATIONS {
        public static final String TAG_NAME = "TAG_NAME";
    }

    public static class Field{
        public static class Normalizer{
            public static final String NAME = "Name";
            public static final String DATE = "Date";
            public static final String SSN = "SSN";
            public static final String MRN = "MRN";
        }
    }

    public static class Pipeline{
        public static final String METRICS_PIPELINE = "health-check-pipeline";
    }
}
