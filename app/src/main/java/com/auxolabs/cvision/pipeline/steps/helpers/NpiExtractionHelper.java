package com.auxolabs.cvision.pipeline.steps.helpers;

import com.auxolabs.cvision.models.nlp.PageModel;
import com.concordfax.capture.core.models.document.result.extraction.ExtractionField;
import com.concordfax.capture.core.models.document.result.extraction.SimpleExtractionField;

import java.text.BreakIterator;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NpiExtractionHelper {
    private static final String PATTERN = "^1[\\d]{9}";

    public HashMap<String, ExtractionField> npiFinder (PageModel pageModel) {
        HashMap<String, ExtractionField> returnValue = new HashMap<>();
        StringBuilder wholePageText= new StringBuilder();
        pageModel.getBlockList().forEach(block->{
            wholePageText.append(block.getValue()+ System.lineSeparator());
        });
        String sentence = wholePageText.toString().toLowerCase();
        List<String> wordsList = stringToList(sentence);
        List<String> npiValuesList = tenDigitFinder(wordsList);
        List<String> npiList = npiChecker(npiValuesList);
        if (npiList.size() > 0) {
            HashMap<String, List<String>> npiMap = npiTagValueMap(npiList,pageModel,sentence);
            List<String> expectedNpi = expectedNpi(npiMap);
            if(expectedNpi != null) {
                String npi = expectedNpi.get(0);
                returnValue.put("NPIExtractorRule",new SimpleExtractionField("npi",0.8f,npi,"npi"));
            }
        }
        return returnValue;
    }

    private static List<String> stringToList(String sentence) {
        List<String> words = new ArrayList<String>();
        BreakIterator breakIterator = BreakIterator.getWordInstance();
        breakIterator.setText(sentence);
        int lastIndex = breakIterator.first();
        while (BreakIterator.DONE != lastIndex) {
            int firstIndex = lastIndex;
            lastIndex = breakIterator.next();
            if (lastIndex != BreakIterator.DONE && Character.isLetterOrDigit(sentence.charAt(firstIndex))) {
                words.add(sentence.substring(firstIndex, lastIndex));
            }
        }

        return words;
    }

    private static List<String> tenDigitFinder(List<String> values) {
        List<String> tenDigitList = new ArrayList<String>();

        Pattern pattern = Pattern.compile(PATTERN);
        for (String value : values) {
            Matcher matcher = pattern.matcher(value);
            if (matcher.matches() == true) {
                tenDigitList.add(value);
            }
        }
        return tenDigitList;
    }

    private static List<String> npiChecker(List<String> npiValue) {

        List<String> validNpi = new ArrayList<String>();
        String npi = "";

        for (String value : npiValue) {
            npi = value;
            int total = 24;
            List<Integer> squarredList = new ArrayList<Integer>();
            List<Integer> oddAlternatesList = new ArrayList<Integer>();
            List<Integer> evenAlternatesList = new ArrayList<Integer>();
            int checkSum = 0;
            int digit = 0;
            Integer numLength = npi.length();
            String firstNine = npi.substring(0, numLength - 1);
            String lastNumber = npi.substring(numLength - 1);

            for (int i = 0; i < firstNine.length(); i++) {
                if (i % 2 == 0) {
                    oddAlternatesList.add(Integer.parseInt(String.valueOf(firstNine.charAt(i))));
                } else {
                    evenAlternatesList.add(Integer.parseInt(String.valueOf(firstNine.charAt(i))));
                }
            }
            for (int j = 0; j < oddAlternatesList.size(); j++) {
                Integer oddIndividual = oddAlternatesList.get(j);
                int squarred = oddIndividual * 2;
                int lastValue = squarred;
                if (String.valueOf(lastValue).length() > 1) {
                    lastValue = lastValue % 10;
                    lastValue += 1;
                    squarredList.add(lastValue);
                } else {
                    squarredList.add(lastValue);
                }
                total += lastValue;
            }
            for (int k = 0; k < evenAlternatesList.size(); k++) {
                Integer evenIndividual = evenAlternatesList.get(k);
                total += evenIndividual;
            }

            if ((total % 10 == 0) && (String.valueOf(total).length() > 1)) {
                checkSum = total % 10;
            } else if (String.valueOf(total).length() > 1) {
                digit = total % 10;
                for (int l = 0; l < 10; l++) {
                    if ((digit + l) % 10 == 0) {
                        checkSum = l;
                        break;
                    }
                }
            }

            if (!lastNumber.equals(String.valueOf(checkSum))) {
            } else {
                validNpi.add(npi);
            }
        }

        return validNpi;
    }

    private static ArrayList<String> getNpiTags(PageModel pageModel){
        ArrayList<String> npiTagsDuplicated = new ArrayList<>();
        pageModel.getBlockList().forEach(list->{
            list.getBlockAnnotations().forEach((key,value)->{
                if(key.equals("lexer")){
                    value.forEach(v->{
                        if(v.getAnswer().equals("NPI_TAGS")){
                            npiTagsDuplicated.add(v.getWord().toLowerCase());
                        }
                    });
                }
            });
        });

        Set<String> npiTagSet = new LinkedHashSet<>(npiTagsDuplicated);
        int n = npiTagSet.size();
        ArrayList<String> npiTags = new ArrayList(n);
        for (String x : npiTagSet)
            npiTags.add(x);
        return npiTags;
    }

    private static HashMap<String, List<String>> npiTagValueMap(List<String> validNpiList,PageModel pageModel,String sentence) {

        ArrayList<String> tagList = getNpiTags(pageModel);
        String sentenceLower = sentence.toLowerCase();
        HashMap<String,List<String>> returnVal = new HashMap<>();
        for (String value : validNpiList) {
            int npiIndex = sentenceLower.indexOf(value);
            Map<Integer, String> map = getMapOfLocationToTag(sentence, tagList);
            Integer minAbs = 0;
            Integer counter =  1;
            Integer minKey = 0;
            for (Integer key: map.keySet()) {
                int diff;
                diff = (npiIndex - key);
                int absDiff = Math.abs(diff);
                if (absDiff < 50) {
                    if(counter == 1){
                        minAbs = absDiff;
                        minKey = key;
                    }else if(absDiff < minAbs){
                        minAbs = absDiff;
                        minKey = key;
                    }
                    counter ++;
                }
            }
            String tagName = map.get(minKey);
            if(tagName == null)
                tagName = "NoTag";
            if(returnVal.containsKey(tagName))  {
                List<String> relatedNPIs = returnVal.get(tagName);
                relatedNPIs.add(value);
                returnVal.put(tagName, relatedNPIs);
            }else{
                List<String> relatedNPIs = new ArrayList<String>();
                relatedNPIs.add(value);
                returnVal.put(tagName, relatedNPIs);
            }

        }
        return returnVal;
    }

    private static Map<Integer, String> getMapOfLocationToTag(String sentence, ArrayList<String> tagList){
        Map<Integer, String> indexToString = new TreeMap<Integer, String>();
        Map<Integer, String> returnVal = new TreeMap<Integer, String>();
        for(String tag : tagList) {
            String regex = "\\b" + tag + "\\b";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(sentence.toLowerCase());
            while (matcher.find() == true) {
                indexToString.put(matcher.start(), tag);
            }
        }
        Integer lastKey = -1;
        Integer mapSize = indexToString.size();
        Integer counter = 0;
        for(Integer key : indexToString.keySet()){
            if(lastKey == -1){
                lastKey = key;
                returnVal.put(key, indexToString.get(key));
                continue;
            }else if(counter < mapSize){
                Integer valDiff = indexToString.get(lastKey).length() - indexToString.get(key).length();
                Integer keyDiff = key - lastKey;
                if(valDiff == keyDiff){
                    returnVal.put(lastKey, indexToString.get(lastKey));
                }else{
                    returnVal.put(key, indexToString.get(key));
                }
                lastKey = key;
            }
            counter ++;
        }
        return returnVal;
    }

    private List<String> expectedNpi (HashMap<String, List<String>> returnVal) {

        if (returnVal.containsKey("servicing npi")) {
            return returnVal.get("servicing npi");
        }
        else if (returnVal.containsKey("servicing provider npi")) {
            return returnVal.get("requesting provider npi");
        }
        else if (returnVal.containsKey("servicing provider")) {
            return returnVal.get("requesting provider");
        }
        else if (returnVal.containsKey("provider id")) {
            return returnVal.get("provider id");
        }
        else if (returnVal.containsKey("provider no")) {
            return returnVal.get("provider no");
        }
        else if (returnVal.containsKey("provider npi")) {
            return returnVal.get("provider npi");
        }
        else if (returnVal.containsKey("npi")) {
            return returnVal.get("npi");
        }
        else if (returnVal.containsKey("doctor npi")) {
            return returnVal.get("doctor npi");
        }
        else if (returnVal.containsKey("physician npi")) {
            return returnVal.get("physician npi");
        }
        else if (returnVal.containsKey("hospital npi")) {
            return returnVal.get("hospital npi");
        }
        else {
            return returnVal.get("NoTag");
        }
    }

}

