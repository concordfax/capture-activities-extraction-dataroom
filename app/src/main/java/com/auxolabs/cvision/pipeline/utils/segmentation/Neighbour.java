package com.auxolabs.cvision.pipeline.utils.segmentation;

import lombok.Data;

@Data
public class Neighbour<T> {
    private Double angle;
    private Double distance;
    private T obj;

    public Neighbour(Double angle, Double distance, T obj) {
        this.angle = angle;
        this.distance = distance;
        this.obj = obj;
    }
}
