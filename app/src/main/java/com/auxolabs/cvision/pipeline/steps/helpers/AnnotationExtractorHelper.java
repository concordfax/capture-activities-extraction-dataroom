package com.auxolabs.cvision.pipeline.steps.helpers;

import com.auxolabs.capture.annotator.models.response.SentenceModel;
import com.auxolabs.cvision.ml.MLExtractorService;
import com.auxolabs.cvision.ml.constants.MLExtractorConstants;
import com.auxolabs.cvision.ml.models.request.MLIDRequestModel;
import com.auxolabs.cvision.ml.models.response.MLIDResponseModel;
import com.auxolabs.cvision.models.nlp.PageModel;
import com.auxolabs.cvision.models.ocr.Lines;
import com.auxolabs.cvision.models.ocr.Para;
import com.auxolabs.cvision.models.ocr.SegBlock;
import com.auxolabs.cvision.streamlining.models.ImageSegment;
import com.concordfax.capture.core.models.document.result.extraction.ExtractionField;
import com.concordfax.capture.core.models.document.result.extraction.SimpleExtractionField;
import com.concordfax.capture.core.models.extraction.ml.MLExtractorConfiguration;
import com.concordfax.capture.core.models.extraction.ml.trigger.MLExtractorTriggerConfiguration;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class AnnotationExtractorHelper {
    private MLExtractorService mlExtractorService;

    @Inject
    public AnnotationExtractorHelper(MLExtractorService mlExtractorService) {
        this.mlExtractorService = mlExtractorService;
    }

    public HashMap<String, ExtractionField> processPage(MLExtractorConfiguration extractorConfiguration,PageModel pageModel) {
        HashMap<String, ExtractionField> finalout = new HashMap<>();
        for(MLExtractorTriggerConfiguration triggerConfiguration: extractorConfiguration.getTrigger()){
            if(triggerConfiguration.getTriggerName().equals(MLExtractorConstants.TriggerEntities.ID)){
                MLIDRequestModel mlidRequestModel = new MLIDRequestModel();
                StringBuilder queryString = new StringBuilder();
                pageModel.getBlockList().forEach(block->{
                    queryString.append(block.getValue()+ System.lineSeparator());
                });
                String query = queryString.toString();
                query = query.replaceAll("[^\\x00-\\x7F]", " ");
                mlidRequestModel.setQueryString(query);
                MLIDResponseModel mlidResponseModel = mlExtractorService.process(triggerConfiguration.getBaseUri(),mlidRequestModel);

                if(!mlidResponseModel.getPatient().isEmpty()) {
                    finalout.put("patientIDExtractorRule", new SimpleExtractionField(MLExtractorConstants.TriggerEntities.patientID, 0.8f, mlidResponseModel.getPatient(), mlidResponseModel.getPatientIDTag()));
                }

                if(!mlidResponseModel.getCaseid().isEmpty()) {
                    finalout.put("caseIDExtractorRule", new SimpleExtractionField(MLExtractorConstants.TriggerEntities.caseID, 0.8f, mlidResponseModel.getCaseid(), mlidResponseModel.getCaseIDTag()));
                }

                if(!mlidResponseModel.getAccount().isEmpty()) {
                    finalout.put("accountIDExtractorRule", new SimpleExtractionField(MLExtractorConstants.TriggerEntities.accountID, 0.8f, mlidResponseModel.getAccount(), mlidResponseModel.getAccountIDTag()));
                }

                if(!mlidResponseModel.getMember().isEmpty()) {
                    finalout.put("memberIDExtractorRule", new SimpleExtractionField(MLExtractorConstants.TriggerEntities.memberID, 0.8f, mlidResponseModel.getMember(), mlidResponseModel.getMemberIDTag()));
                }

                if(!mlidResponseModel.getSubscriber().isEmpty()) {
                    finalout.put("subscriberIDExtractorRule", new SimpleExtractionField(MLExtractorConstants.TriggerEntities.subscriberID, 0.8f, mlidResponseModel.getSubscriber(), mlidResponseModel.getSubscriberIDTag()));
                }

                if(!mlidResponseModel.getAuth().isEmpty()) {
                    finalout.put("authIDExtractorRule", new SimpleExtractionField(MLExtractorConstants.TriggerEntities.authID, 0.8f, mlidResponseModel.getAuth(), mlidResponseModel.getAuthIDTag()));
                }


            }
        }

        return finalout;
    }
}
