package com.auxolabs.cvision.pipeline.utils.segmentation;

import lombok.Getter;
import java.util.ArrayList;
import java.util.List;

@Getter
public class Node<T> {
    private T obj;
    private List<Neighbour<T>> neighbours = new ArrayList<>();

    public Node(T obj){
        this.obj = obj;
    }

}
