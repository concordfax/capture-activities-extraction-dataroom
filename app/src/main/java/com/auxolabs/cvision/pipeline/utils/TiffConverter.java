package com.auxolabs.cvision.pipeline.utils;

import com.auxolabs.cvision.models.cv.PIBOXHolder;
import com.auxolabs.cvision.utils.LeptonicaUtils;
import com.sun.media.jai.codec.ByteArraySeekableStream;
import lombok.extern.slf4j.Slf4j;

import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.bytedeco.javacpp.lept.PIX;

@Slf4j
public class TiffConverter {

    private InputStream fileInputStream;
    private Integer xDpi;
    private Integer yDpi;

    public TiffConverter(InputStream inputStream, Integer xDpi, Integer yDpi) {
        this.fileInputStream = inputStream;
        this.xDpi = xDpi;
        this.yDpi = yDpi;
    }

    public List<PIBOXHolder> process() {
        List<PIBOXHolder> pages = new ArrayList<>();
        try {
            ImageInputStream input = ImageIO.createImageInputStream(this.fileInputStream);
            Iterator<ImageReader> readers = ImageIO.getImageReadersByFormatName("TIFF");
            if (!readers.hasNext()) {
                throw new RuntimeException("Tiff reader not found");
            }
            ImageReader reader = readers.next();
            reader.setInput(input);
            ImageReadParam param = reader.getDefaultReadParam();
            int numImages = reader.getNumImages(true);
            for (int i = 0; i < numImages; i++) {
                BufferedImage bufferedImage = reader.read(i, param);
                bufferedImage = removeAlphaChannel(bufferedImage);
                PIX pix = LeptonicaUtils.getInstance().convertBufferedImageToPIX(bufferedImage, xDpi, yDpi);
                pages.add(new PIBOXHolder(pix));
                bufferedImage.flush();
            }
            input.close();
            reader.dispose();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            try {
                this.fileInputStream.close();
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }
        return pages;
    }

    private BufferedImage removeAlphaChannel(BufferedImage img) {
        BufferedImage copy = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = copy.createGraphics();
        g2d.setColor(Color.WHITE); // Or what ever fill color you want...
        g2d.fillRect(0, 0, copy.getWidth(), copy.getHeight());
        g2d.drawImage(img, 0, 0, null);
        g2d.dispose();
        img.flush();
        return copy;
    }

    private ByteArraySeekableStream convertISToBASS(InputStream in) {
        try {
            byte[] buff = new byte[8096];
            int bytesRead = 0;
            ByteArrayOutputStream bao = new ByteArrayOutputStream();
            while ((bytesRead = in.read(buff)) != -1) {
                bao.write(buff, 0, bytesRead);
            }
            byte[] data = bao.toByteArray();
            bao.flush();
            bao.close();
            ByteArraySeekableStream seekableStream = new ByteArraySeekableStream(data);
            in.close();
            return seekableStream;
        } catch (Exception e) {
            log.error("Error", e);
        }
        return null;
    }

}
