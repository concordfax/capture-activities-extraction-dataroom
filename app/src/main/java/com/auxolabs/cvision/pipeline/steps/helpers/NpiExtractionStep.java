package com.auxolabs.cvision.pipeline.steps.helpers;

import com.auxolabs.cvision.models.nlp.PageModel;
import com.auxolabs.cvision.pipeline.ExtractionPipelineMessage;
import com.concordfax.capture.core.logging.ILogger;
import com.concordfax.capture.core.logging.LogLevels;
import com.concordfax.capture.core.models.document.result.extraction.ExtractionField;
import com.concordfax.capture.core.models.extraction.output.ExtractorType;
import com.concordfax.capture.core.pipeline.AbstractMessage;
import com.concordfax.capture.core.pipeline.PipelineStep;
import edu.stanford.nlp.util.Timing;

import javax.inject.Inject;
import java.util.HashMap;

public class NpiExtractionStep implements PipelineStep {
    private ILogger log;
    private NpiExtractionHelper npiExtractionHelper;

    @Inject
    public NpiExtractionStep(ILogger log) {
        this.log = log;
        this.npiExtractionHelper = new NpiExtractionHelper();
    }
    @Override
    public AbstractMessage process(AbstractMessage message) {

        ExtractionPipelineMessage extractionPipelineMessage = (ExtractionPipelineMessage) message;
        Timing timing = new Timing();

        for (PageModel pageModel : extractionPipelineMessage.getPages()) {
            HashMap<String, ExtractionField> triggeredFields;
            try {

                triggeredFields = this.npiExtractionHelper.npiFinder(pageModel);
                pageModel.getExtractorToRuleField().put(ExtractorType.NPIExtractionLuhnFormula, triggeredFields);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        log.trace(String.format("completed NPI extraction in %d ms", timing.stop()), LogLevels.Verbose, extractionPipelineMessage.getLogMaker().getProperties());
        return message;
    }
}
