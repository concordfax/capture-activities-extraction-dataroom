package com.auxolabs.cvision.pipeline.utils.field.modifiers;


import com.concordfax.capture.core.models.document.result.extraction.ExtractionField;
import com.concordfax.capture.core.models.document.result.extraction.FieldNames;
import com.concordfax.capture.core.models.document.result.extraction.SimpleExtractionField;
import com.concordfax.capture.core.models.extraction.output.ExtractorModel;
import com.concordfax.capture.core.models.extraction.output.ExtractorResultModifier;
import com.concordfax.capture.core.models.extraction.output.ExtractorType;

import java.util.HashMap;
import java.util.List;

public class ExtendNameByLength implements FieldModifier<ExtractionField, String> {
    @Override
    public ExtractionField modify(String currentResult, ExtractorResultModifier extractorResultModifier, List<ExtractorModel> extractors, HashMap<ExtractorType, HashMap<String, ExtractionField>> fieldRuleMap) {
        SimpleExtractionField simpleExtractionField = new SimpleExtractionField(FieldNames.patient_name.toString(), 0.95f, currentResult, "");
        for (ExtractorModel extracterModel : extractors) {
            if (extractorResultModifier.getPriorityLevel() == extracterModel.getPriority()) {
                SimpleExtractionField resultFromExtractor = (SimpleExtractionField) fieldRuleMap.get(extracterModel.getType()).get(extracterModel.getRule());
                if (resultFromExtractor != null && !resultFromExtractor.getValue().isEmpty()) {
                    if (currentResult == null || currentResult.isEmpty()) {
                        simpleExtractionField.setValue(resultFromExtractor.getValue());
                    } else if (checkIfContains(resultFromExtractor.getValue(), currentResult) && resultFromExtractor.getValue().length() > currentResult.length()) {
                        simpleExtractionField.setValue(resultFromExtractor.getValue());
                    }
                }
            }
        }
        return simpleExtractionField;
    }

    // TODO: 2019-04-19 perform this cleanup if possible only once
    private boolean checkIfContains(String largerCandidate, String smallerCandidate){
        String preProcessedLarger = largerCandidate.replaceAll("[^a-zA-Z]"," ").replaceAll("\\s+"," ").trim();
        String preProcessedSmaller = smallerCandidate.replaceAll("[^a-zA-Z]"," ").replaceAll("\\s+"," ").trim();
        return preProcessedLarger.contains(preProcessedSmaller);
    }
}
