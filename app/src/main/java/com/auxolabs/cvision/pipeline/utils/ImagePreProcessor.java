package com.auxolabs.cvision.pipeline.utils;

import com.auxolabs.cvision.models.cv.PIBOXHolder;
import com.auxolabs.cvision.models.cv.ScaleModel;
import com.auxolabs.cvision.utils.LeptonicaUtils;
import org.bytedeco.javacpp.FloatPointer;
import org.bytedeco.javacpp.Loader;
import org.bytedeco.javacpp.lept;

import static org.bytedeco.javacpp.lept.*;

public class ImagePreProcessor {
    private static final Float deg2rad = (float) (3.14159 / 180.);
    private static final Float errorThreshold = 100f;
    private static final Float defaultWidth = 1700f;
    private static final Float defaultHeight = 2200f;

    public ImagePreProcessor(){
        Loader.load(lept.class);
    }

    public void convertTo32bpp(PIBOXHolder pixs) {
        if (pixs.getPix().d() != 32) {
            PIX tempPix = pixConvertTo32(pixs.getPix());
            LeptonicaUtils.getInstance().safeDeletePIX(pixs.getPix());
            pixs.setPix(tempPix);
        }
//        PIX pixs8bpp = pixConvertTo8(pixs.getPix(), 1);
//        PIX pixDeskewed = deskew(pixs8bpp); // returns 8bpp image
//        PIX pixsRotated = correctOrientation(pixDeskewed); // returns 8bpp image

//        PIX tempPix = pixConvertTo32(pixDeskewed);

//        LeptonicaUtils.getInstance().safeDeletePIX(pixs8bpp);
//        LeptonicaUtils.getInstance().safeDeletePIX(pixDeskewed);
//        LeptonicaUtils.getInstance().safeDeletePIX(pixsRotated);
//        LeptonicaUtils.getInstance().safeDeletePIX(pixs.getPix());

//        pixs.setPix(tempPix);
    }

    public void scaleIfNeeded(PIBOXHolder pixs, ScaleModel scaleModel) {
        if (pixs.getPix().w() - defaultWidth > 50f || pixs.getPix().h() - defaultHeight > 50f) {
            if (pixs.getPix().w() > pixs.getPix().h()) {
                float scaleX = defaultWidth / pixs.getPix().w();
                PIX tempPix = pixScale(pixs.getPix(), scaleX, scaleX);
                LeptonicaUtils.getInstance().safeDeletePIX(pixs.getPix());
                pixs.setPix(tempPix);
                scaleModel.setScalingFactor(scaleX);
            } else {
                float scaleY = defaultHeight / pixs.getPix().h();
                PIX tempPix = pixScale(pixs.getPix(), scaleY, scaleY);
                LeptonicaUtils.getInstance().safeDeletePIX(pixs.getPix());
                pixs.setPix(tempPix);
                scaleModel.setScalingFactor(scaleY);
            }
            scaleModel.setScaled(true);
        } else if (defaultWidth - pixs.getPix().w() > 50f || defaultHeight - pixs.getPix().h() > 50f) {
            if (pixs.getPix().w() > pixs.getPix().h()) {
                float scaleX = defaultWidth / pixs.getPix().w();
                PIX tempPix = pixScale(pixs.getPix(), scaleX, scaleX);
                LeptonicaUtils.getInstance().safeDeletePIX(pixs.getPix());
                pixs.setPix(tempPix);
                scaleModel.setScalingFactor(scaleX);
            } else {
                float scaleY = defaultHeight / pixs.getPix().h();
                PIX tempPix = pixScale(pixs.getPix(), scaleY, scaleY);
                LeptonicaUtils.getInstance().safeDeletePIX(pixs.getPix());
                pixs.setPix(tempPix);
                scaleModel.setScalingFactor(scaleY);
            }
            scaleModel.setScaled(true);
        }
    }

    private PIX convertToBinary(PIX pixs8bpp) {
        PIX pixs1bpp = pixThresholdToBinary(pixs8bpp, 130);
        return pixs1bpp;
    }

    private PIX correctOrientation(PIX pixb) {
        /*
            http://search.cpan.org/~zmughal/Image-Leptonica/lib/Image/Leptonica/Func/flipdetect.pm
            0 deg :           upconf >> 1,    abs(upconf) >> abs(leftconf)
            90 deg :          leftconf >> 1,  abs(leftconf) >> abs(upconf)
            180 deg :         upconf << -1,   abs(upconf) >> abs(leftconf)
            270 deg :         leftconf << -1, abs(leftconf) >> abs(upconf)
        */
        PIX tempPix1bpp = convertToBinary(pixb);
        FloatPointer upconf = new FloatPointer(1), leftconf = new FloatPointer(1);
        pixOrientDetect(tempPix1bpp, upconf, leftconf, 0, 0);
        PIX pixd;
        float upconfAbs = Math.abs(upconf.get());
        float leftconfAbs = Math.abs(leftconf.get());
        Integer orientation;
        if (upconf.get() > 3 && upconfAbs - leftconfAbs >= 5.0f) {
            // angle at 0 deg
            orientation = 0;
            pixd = pixCopy(null, pixb);
        } else if (leftconf.get() > 3 && leftconfAbs - upconfAbs >= 5.0f) {
            // angle at 90 deg
            orientation = 90;
            pixd = pixRotateOrth(pixb, 1);
        } else if (upconf.get() < -3 && upconfAbs - leftconfAbs >= 5.0f) {
            // angle @ 180 deg
            orientation = 180;
            pixd = pixRotateOrth(pixb, 2);
        } else if (leftconf.get() < -3 && leftconfAbs - upconfAbs >= 5.0f) {
            // angle @ 270 deg
            orientation = 270;
            pixd = pixRotateOrth(pixb, 3);
        } else {
            orientation = 0;
            pixd = pixCopy(null, pixb);
        }
//        System.out.println("Orientation: " + orientation);
        LeptonicaUtils.getInstance().safeDeletePIX(tempPix1bpp);
        upconf.close();
        leftconf.close();
        return pixd;
    }

    private PIX deskew(PIX pixb) {
        float angle, conf;
        PIX tempPix1bpp = convertToBinary(pixb);
        /* find the skew angle and deskew using an interpolated
         * rotator for anti-aliasing (to avoid jaggies) */
        FloatPointer pangle = new FloatPointer(1);
        FloatPointer pconf = new FloatPointer(1);
        pixFindSkew(tempPix1bpp, pangle, pconf);
        angle = pangle.get();
        conf = pconf.get();
        PIX pixDeskewed = pixRotateAMGray(pixb, (float) (deg2rad * angle), (byte) 255);
        LeptonicaUtils.getInstance().safeDeletePIX(tempPix1bpp);
        pangle.close();
        pconf.close();
        return pixDeskewed;
    }

    public PIBOXHolder convertToBinary(PIBOXHolder pixs) {
        PIX pixs8bpp = pixConvertTo8(pixs.getPix(), 1);
        PIX pixs1bpp = pixThresholdToBinary(pixs8bpp, 130);
        PIBOXHolder binaryImage = new PIBOXHolder();
        binaryImage.setPix(pixs1bpp);
        LeptonicaUtils.getInstance().safeDeletePIX(pixs8bpp);
        return binaryImage;
    }
}
