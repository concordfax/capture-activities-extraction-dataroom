package com.auxolabs.cvision.pipeline.utils;

import com.auxolabs.cvision.models.cv.PIBOXHolder;
import com.auxolabs.cvision.streamlining.models.ImageSegment;
import com.auxolabs.cvision.utils.LeptonicaUtils;
import org.bytedeco.javacpp.IntPointer;
import org.bytedeco.javacpp.Loader;
import org.bytedeco.javacpp.lept;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.bytedeco.javacpp.lept.*;

/**
 * Takes in an image path and then emits the segments in the image with the bounding boxes.
 */
public class Segmentation {

    private static final Integer MIN_HEIGHT = 3;

    public Segmentation() {
        Loader.load(lept.class);
    }

    public List<ImageSegment> getTextSegmentsPositions(PIBOXHolder pixb) {

        /* Invert and identify vertical gutters between text columns */
        PIX pix6 = pixReduceRankBinaryCascade(pixb.getPix(), 1, 1, 0, 0);
        PIX pix11 = pixMorphSequence(pix6, "o1.3", 0);
        LeptonicaUtils.getInstance().safeDeletePIX(pix6);
        PIX pixInv = pixInvert(null, pix11);
        LeptonicaUtils.getInstance().safeDeletePIX(pix11);

        PIX pix7 = pixMorphSequence(pixInv, "o5.5 + o1.50", 0);
        LeptonicaUtils.getInstance().safeDeletePIX(pixInv);
        PIX pix8 = pixExpandBinaryPower2(pix7, 4);  /* gutter mask */
        LeptonicaUtils.getInstance().safeDeletePIX(pix7);

        /* Solidify text blocks */
        PIX pix9 = pixMorphSequence(pixb.getPix(), "c50.1 + c5.1", 0);
        PIX pix12 = pixSubtract(null, pix9, pix8);  /* preserve gutter */
        LeptonicaUtils.getInstance().safeDeletePIX(pix8);
        LeptonicaUtils.getInstance().safeDeletePIX(pix9);
        PIX pix10 = pixMorphSequence(pix12, "d5.2", 0);
        LeptonicaUtils.getInstance().safeDeletePIX(pix12);

        BOXA boxa = pixConnCompBB(pix10, 4);
        LeptonicaUtils.getInstance().safeDeletePIX(pix10);

        List<ImageSegment> imageSegments = new ArrayList<>();
        if (boxa != null) {
            for (int i = 0; i < boxa.n(); i++) {
                BOX box = boxa.box(i);
                ImageSegment imageSegment = ImageSegment.CreateEmpty(box);
                imageSegments.add(imageSegment);
            }
        }
        boxaDestroy(boxa);
        return imageSegments;
    }

}
