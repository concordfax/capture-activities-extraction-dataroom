package com.auxolabs.cvision.pipeline.steps.helpers;

import com.auxolabs.capture.positional.PositionalService;
import com.auxolabs.capture.positional.models.*;
import com.auxolabs.capture.positional.utils.ops.BasePositionalOp;
import com.auxolabs.capture.positional.utils.ops.PositionalOpsFactory;
import com.auxolabs.cvision.models.nlp.PageModel;
import com.concordfax.capture.core.logging.ILogger;
import com.concordfax.capture.core.models.document.result.extraction.ExtractionField;
import com.concordfax.capture.core.models.document.result.extraction.SimpleExtractionField;
import com.concordfax.capture.core.models.extraction.annotators.ExtractionAnnotatorConfiguration;
import com.concordfax.capture.core.models.extraction.positional.PositionalExtractorConfiguration;
import com.concordfax.capture.core.models.extraction.positional.output.PositionalExtractorOutputConfiguration;
import com.concordfax.capture.core.models.extraction.positional.output.PositionalPropertyTypes;
import com.auxolabs.cvision.streamlining.constants.PositionalConstants;
import edu.stanford.nlp.util.Timing;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * The PositionalHelper class implements the following:
 * <ul>
 * <li>Merge boxes based on Image Properties to have logically related words in a single box.</li>
 * <li>Identifies tokens from the image</li>
 * <li>The configuration for the extractor and tokens are send to positional extractor.</li>
 * <li>Positional extractor engine returns the rules triggered, results from the rules and operations.</li>
 * <li>Identifies the required fields and populated in PageModel class of Capture Document Model class.</li>
 * </ul>
 */
@Slf4j
public class PositionalHelper {
    public static ArrayList<String> temp = new ArrayList<>();
    private ILogger logger;
    private PositionalService positionalService;
    private PositionalMappingHelper positionalMappingHelper;
    private PositionalAnnotationHelper annotationHelper;

    @Inject
    public PositionalHelper(PositionalService positionalService, ILogger iLogger) {
        this.positionalService = positionalService;
        this.logger = iLogger;
        this.positionalMappingHelper = new PositionalMappingHelper();
        this.annotationHelper = new PositionalAnnotationHelper();
    }

    public HashMap<String, ExtractionField> processPage(PositionalExtractorConfiguration extractorConfiguration,PageModel pageModel,ExtractionAnnotatorConfiguration annotatorConfiguration) throws Exception {
        PositionalMappingHelper positionalMappingHelper = new PositionalMappingHelper();
        HashMap<String,HashSet<TokenModel>> resultAnnotation=positionalMappingHelper.getTagToAnnotations(pageModel,annotatorConfiguration);
        Timing timing = new Timing();
        List<RuleOpModel> ruleOpModels = positionalService.process(extractorConfiguration.getTrigger(), resultAnnotation);
        HashMap<String, HashSet<TokenModel>> tokens = positionalMappingHelper.getTagToAnnotations(pageModel, annotatorConfiguration);
        temp.add("______________________________________________________________________________________________________");
        temp.add("PAGE NUMBER:" + (pageModel.getPageNumber() + 1));
        temp.add("List of tokens generated for Positional[TAG_NAME]:\n" + tokens.get("tag_name"));
        temp.add("List of tokens generated for Positional[PERSON_NAME]:\n" + tokens.get("person_name"));
        temp.add("______________________________________________________________________________________________________");
        tokens.forEach((k,v)-> temp.add("List of tokens generated for Positional["+ k+"]" +"\n"+v));
        HashMap<String, RuleOpModel> ruleOpModelHashMap = new HashMap<>();

        ruleOpModels.forEach(ruleModel -> {

            if (ruleModel.getResults() != null && !ruleModel.getResults().isEmpty()) {
                temp.add("______________________________________________________________________________________________________");
                for (OpResultModel _result : ruleModel.getResults()) {
                    BooleanOpResultModel result = (BooleanOpResultModel)_result;
                    if (result.getResult()) {
                        System.out.println(result.getTokens());
                        temp.add(result.getTokens().toString());
                    }
                }
                temp.add("______________________________________________________________________________________________________");
                ruleOpModelHashMap.put(ruleModel.getRuleModel().getRuleName(), ruleModel);
            }
        });

        PositionalExtractorOutputConfiguration positionalExtractorOutputConfiguration = (PositionalExtractorOutputConfiguration) extractorConfiguration.getOutputConfiguration();
        HashMap<String, ExtractionField> positionalAnnotations = new HashMap<>();
        positionalExtractorOutputConfiguration.getPositional().forEach(positionalFieldModel -> {
            if (ruleOpModelHashMap.containsKey(positionalFieldModel.getRule())) {
                PositionalFieldDimensionsModel positionalFieldDimensionsModel = new PositionalFieldDimensionsModel();
                RuleOpModel ruleOpModel = ruleOpModelHashMap.get(positionalFieldModel.getRule());
                ruleOpModel.getResults().forEach(opResultModel -> {
                    PositionalFieldResultsModel positionalFieldResultsModel = new PositionalFieldResultsModel();
                    BooleanOpResultModel booleanOpResultModel = (BooleanOpResultModel) opResultModel;
                    List<TokenModel> tokenModels = new ArrayList<>();
                    positionalFieldModel.getLexicons().getAnnotations().forEach(s -> {
                        if (booleanOpResultModel.getTokens().containsKey(s)) {
                            tokenModels.add(booleanOpResultModel.getTokens().get(s));
                        }
                    });
                    positionalFieldResultsModel.setTokenModels(tokenModels);
                    positionalFieldResultsModel.setOpResultModel(opResultModel);
                    positionalFieldDimensionsModel.getFieldResultsModels().add(positionalFieldResultsModel);
                });
                positionalFieldModel.getLexicons().getOps().forEach(positionalOpsModel -> {
                    positionalFieldDimensionsModel.setProp(positionalOpsModel.getProp());
                    BasePositionalOp basePositionalOp = PositionalOpsFactory.get(positionalOpsModel.getOp());
                    basePositionalOp.perform(positionalFieldDimensionsModel);
                });
                if (positionalFieldModel.getEmit() == 1) {
                    PositionalFieldResultsModel positionalFieldResultsModel = positionalFieldDimensionsModel.getFieldResultsModels().get(0);
                    StringBuilder answerBuilder = new StringBuilder("");
                    positionalFieldModel.getLexicons().getSelectedAnnotations().forEach(positionalAnnotationModel -> {
                        BooleanOpResultModel booleanOpResultModel = (BooleanOpResultModel) positionalFieldResultsModel.getOpResultModel();
                        TokenModel tokenModel = booleanOpResultModel.getTokens().get(positionalAnnotationModel.getAnnotation());
                        try {
                            if (positionalAnnotationModel.getProp() != null) {
                                answerBuilder.append(tokenModel.get(positionalAnnotationModel.getProp().name()));
                            } else {
                                answerBuilder.append(tokenModel.get(PositionalPropertyTypes.text.name()));
                            }
                            answerBuilder.append(" ");
                        } catch (NullPointerException npe) {
                        }
                    });
                    positionalAnnotations.put(positionalFieldModel.getKey(), new SimpleExtractionField(positionalFieldModel.getKey(), 0.75f, answerBuilder.toString(), ""));
                } else {
                    // not supported now
                    // TODO: 14/06/18 implement multi output support in output configuration builder
                }
            }
        });

        // Check if any ssn tags are present
        List<ExtractionField> values =
                positionalAnnotations.keySet().stream()
                        .filter(key -> key.contains("ssn"))
                        .map(positionalAnnotations::get)
                        .collect(Collectors.toList());

        if (0 == values.size()) {

            // Get all possible nine letter ssn
            if (resultAnnotation.containsKey("nine_letter_ssn")) {
                for (TokenModel nineLetterSSN : resultAnnotation.get("nine_letter_ssn")) {
                    String ssn = nineLetterSSN.get(PositionalPropertyTypes.text.name()).toString().replaceAll("(?i)[^-\\d|X]", "");
                    if (ssn.matches(PositionalConstants.SSN_REGEX)) {
                        positionalAnnotations.put("patient ssn identifier - nine letter",
                                new SimpleExtractionField("patient ssn identifier - nine letter",
                                        0.8f,
                                        ssn,
                                        "ssn"));
                        break;
                    }
                }
            }
        }

        return positionalAnnotations;
    }
}
