package com.auxolabs.cvision.pipeline.steps.helpers;

import com.auxolabs.capture.annotator.models.response.SentenceModel;
import com.auxolabs.capture.positional.constants.PositionalRuleConstants;
import com.auxolabs.capture.positional.models.TokenModel;
import com.auxolabs.cvision.ml.MLExtractorService;
import com.auxolabs.cvision.ml.constants.MLExtractorConstants;
import com.auxolabs.cvision.ml.models.mlfieldextractionmodels.MLFields;
import com.auxolabs.cvision.ml.models.mlfieldextractionmodels.MLRequestModel;
import com.auxolabs.cvision.ml.models.mlfieldextractionmodels.MLResponseModel;
import com.auxolabs.cvision.ml.models.request.MLExtractorRequestModel;
import com.auxolabs.cvision.ml.models.response.MLExtractorResponseModel;
import com.auxolabs.cvision.models.nlp.PageModel;
import com.auxolabs.cvision.models.ocr.Lines;
import com.auxolabs.cvision.models.ocr.Para;
import com.auxolabs.cvision.models.ocr.SegBlock;
import com.auxolabs.cvision.pipeline.ExtractionPipelineMessage;
import com.auxolabs.cvision.streamlining.models.ImageSegment;
import com.concordfax.capture.core.models.document.result.extraction.ExtractionField;
import com.concordfax.capture.core.models.document.result.extraction.SimpleExtractionField;
import com.concordfax.capture.core.models.extraction.ml.MLExtractorConfiguration;

import javax.inject.Inject;
import java.util.*;
import java.util.List;


public class MachineLearningExtractorHelper {
    private MLExtractorService mlExtractorService;

    @Inject
    public MachineLearningExtractorHelper(MLExtractorService mlExtractorService) {
        this.mlExtractorService = mlExtractorService;
    }
    public HashMap<String, ExtractionField> callMLModel(MLExtractorConfiguration nameExtractorConfiguration, PageModel pageModel)
    {
        HashMap<String, ExtractionField> mlExtractedFields;
        MLRequestModel mlRequestModel = new MLRequestModel();
        StringBuilder wholePageText= new StringBuilder();
        pageModel.getBlockList().forEach(block->{
            wholePageText.append(block.getValue()+ System.lineSeparator());
        });
        mlRequestModel.setRequestText(wholePageText.toString().replaceAll("[^\\x00-\\x7f]", " "));
        MLResponseModel mlResponseModel = mlExtractorService.requestService(nameExtractorConfiguration.getTrigger().get(0).getBaseUri(), mlRequestModel);
        mlExtractedFields = parseMLFields(mlResponseModel);
        return mlExtractedFields;
    }


    private HashMap<String, ExtractionField> parseMLFields(MLResponseModel mlResponseModel)
    {
        MLFields mlFields = new MLFields();
        StringBuilder npnames = new StringBuilder();
        HashMap<String, ExtractionField> mlExtractedFields = new HashMap<>();
        mlResponseModel.getResult().forEach(resultItem -> resultItem.getAnnotations().forEach(annotationsItem -> {
            if(annotationsItem.getWord().equals("patient_name") && annotationsItem.getAnswer()!= null)
                mlExtractedFields.put("patient_name", new SimpleExtractionField("patient_name", 0.75f,annotationsItem.getAnswer(), ""));
            if(annotationsItem.getWord().equals("non_patient_names") && annotationsItem.getAnswer()!= null)
                npnames.append(annotationsItem.getAnswer()+">");
        }));
        if(npnames.toString().length() > 0)
            mlExtractedFields.put("non_patient_names", new SimpleExtractionField("non_patient_names", 0.75f,npnames.toString(), ""));

        return mlExtractedFields;
    }
}
