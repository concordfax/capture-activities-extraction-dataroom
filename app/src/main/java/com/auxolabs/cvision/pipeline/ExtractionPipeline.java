package com.auxolabs.cvision.pipeline;


import com.concordfax.capture.core.jobmanager.CaptureJob;
import com.concordfax.capture.core.logging.ILogger;
import com.concordfax.capture.core.logging.LogMaker;
import com.concordfax.capture.core.pipeline.*;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExtractionPipeline implements Pipeline {

    private List<PipelineStep> pipelineSteps;
    private ILogger log;
    private ExecutorService executors;

    public ExtractionPipeline(ILogger logger, PipelineStep... pipelineSteps) {
        this.log = logger;
        this.pipelineSteps = Arrays.asList(pipelineSteps);
    }

    @Override
    public void start(Integer integer) {
        this.executors = Executors.newFixedThreadPool(integer);
    }

    @Override
    public List<PipelineStep> steps() {
        return pipelineSteps;
    }

    @Override
    public AbstractJob submit(AbstractJob abstractJob) {
        // create message and pass message to run object
        assert abstractJob instanceof CaptureJob;
        CaptureJob captureJob = (CaptureJob) abstractJob;
        ExtractionPipelineMessage message = new ExtractionPipelineMessage();
        message.setExtractionProfileId(captureJob.getExtractionProfileId());
        message.setDocumentId(captureJob.getDocumentId());
        message.setPartitionKey(captureJob.getPartitionKey());
        message.setJobRef(captureJob);
        message.setLogMaker(new LogMaker(captureJob.getDocumentId(),captureJob.getExtractionProfileId(), UUID.randomUUID().toString()));
        CompletableFuture.runAsync(() -> {
            this.run(message);
        }, executors).exceptionally(throwable -> {
            if (throwable.getCause() instanceof PipelineFailureException) {
                PipelineFailureException pipelineFailureException = (PipelineFailureException) throwable.getCause();
                assert pipelineFailureException.getPipelineMessage() instanceof ExtractionPipelineMessage;
                ExtractionPipelineMessage predictionServiceMessage = (ExtractionPipelineMessage) pipelineFailureException.getPipelineMessage();
                pipelineFailureException.getPipelineMessage().getExceptions().forEach(predictionServiceMessage.getJobRef()::addException);
                predictionServiceMessage.getJobRef().markJobAsCompleted();
                pipelineFailureException.getPipelineMessage().getExceptions().forEach(e -> {
                    log.error(e, predictionServiceMessage.getLogMaker().getProperties(), new HashMap<>());
                });
            } else {
                Exception e = new Exception(throwable);
                message.addException(e);
                message.getJobRef().relayMessageCompletion();
                message.getJobRef().addException(e);
                message.getJobRef().markJobAsCompleted();
                log.error(e,message.getLogMaker().getProperties(),null);
            }
            return null;
        });
        return abstractJob;
    }

    @Override
    public void stop() {

    }
}
