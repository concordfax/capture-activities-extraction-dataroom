package com.auxolabs.cvision.pipeline.steps;

import com.auxolabs.capture.annotator.AnnotatorService;
import com.auxolabs.capture.positional.PositionalService;
import com.auxolabs.cvision.models.nlp.PageModel;
import com.auxolabs.cvision.pipeline.ExtractionPipelineMessage;
import com.auxolabs.cvision.pipeline.steps.helpers.PositionalHelper;
import com.concordfax.capture.core.logging.ILogger;
import com.concordfax.capture.core.logging.LogLevels;
import com.concordfax.capture.core.models.document.result.extraction.ExtractionField;
import com.concordfax.capture.core.models.extraction.annotators.ExtractionAnnotatorConfiguration;
import com.concordfax.capture.core.models.extraction.output.ExtractorType;
import com.concordfax.capture.core.models.extraction.positional.PositionalExtractorConfiguration;
import com.concordfax.capture.core.pipeline.AbstractMessage;
import com.concordfax.capture.core.pipeline.PipelineStep;
//import com.kaiser.lex.TokenModel;
import edu.stanford.nlp.util.Timing;

import javax.inject.Inject;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;


public class PositionalExtractorStep implements PipelineStep {

    private ILogger log;
    private PositionalHelper positionalHelper;

    @Inject
    public PositionalExtractorStep(ILogger log,PositionalService positionalService) {
        this.log = log;
        this.positionalHelper = new PositionalHelper(positionalService,log);
    }

    @Override
    public AbstractMessage process(AbstractMessage message) {
        ExtractionPipelineMessage extractionPipelineMessage = (ExtractionPipelineMessage) message;
        Timing timing = new Timing();
        PositionalExtractorConfiguration extractorConfiguration = null;
        for (int i = 0; i < extractionPipelineMessage.getExtractionProfile().getExtractors().size(); i++) {
            if (extractionPipelineMessage.getExtractionProfile().getExtractors().get(i).getExtractorType().equals(ExtractorType.Positional)) {
                extractorConfiguration = (PositionalExtractorConfiguration) extractionPipelineMessage.getExtractionProfile().getExtractors().get(i);
                break;
            }
        }
        for (PageModel pageModel : extractionPipelineMessage.getPages()) {
            HashMap<String, ExtractionField> triggeredRules;
            try {
                triggeredRules = this.positionalHelper.processPage(extractorConfiguration,
                        pageModel,extractionPipelineMessage.getExtractionProfile().getAnnotators());
                if (!triggeredRules.isEmpty()) {
                    PositionalHelper.temp.add("-------------------------------------------------------------------------");
                    PositionalHelper.temp.add("Positional arguments:\n" + triggeredRules.toString());
                    PositionalHelper.temp.add("-------------------------------------------------------------------------");
                    System.out.println("Positional arguments:\n" + triggeredRules.toString());
                }
            pageModel.getExtractorToRuleField().put(ExtractorType.Positional, triggeredRules);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        log.trace(String.format("completed positional extraction in %d ms", timing.stop()), LogLevels.Verbose, extractionPipelineMessage.getLogMaker().getProperties());
        return message;
    }

    public static void writeToFile(ArrayList names, String filename_withpath) throws FileNotFoundException {
        String folder_name = "/home/nstest-admin/Documents/output/doc_ids/";
        PrintWriter writer = new PrintWriter(folder_name+filename_withpath+".txt");
        names.forEach(name->{
            writer.println(name);
        });
        writer.close();
}

}
