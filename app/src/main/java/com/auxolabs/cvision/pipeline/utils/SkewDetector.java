package com.auxolabs.cvision.pipeline.utils;

import org.bytedeco.javacpp.FloatPointer;

import static org.bytedeco.javacpp.lept.*;

public class SkewDetector {

    /* deskew */
    private static final int DESKEW_REDUCTION = 2;      /* 1, 2 or 4 */

    /* sweep only */
    private static final float SWEEP_RANGE = 10.0f;    /* degrees */
    private static final float SWEEP_DELTA = 0.2f;  /* degrees */
    private static final int SWEEP_REDUCTION = 2;     /* 1, 2, 4 or 8 */

    /* sweep and search */
    private static final float SWEEP_RANGE2 = 10.0f;  /* degrees */
    private static final float SWEEP_DELTA2 = 1.0f;     /* degrees */
    private static final int SWEEP_REDUCTION2 = 2;     /* 1, 2, 4 or 8 */
    private static final int SEARCH_REDUCTION = 2;   /* 1, 2, 4 or 8 */
    private static final float SEARCH_MIN_DELTA = 0.01f;   /* degrees */

    public static void main(String[] args) {
        PIX pixs = pixRead("deskewed.png");
        new SkewDetector().detectSkew(pixs);
    }

    public void detectSkew(PIX pixs) {
        FloatPointer angle = new FloatPointer(1);
        FloatPointer conf = new FloatPointer(1);
        FloatPointer score = new FloatPointer(1);
        PIX pix = pixConvertTo1(pixs, 130);
        pixWrite("/tmp/binarized.tif", pix, IFF_TIFF_G4);
        pixFindSkew(pix, angle, conf);
        System.out.println("pixFindSkew: " + "conf = " + conf.get() + ", angle = " + angle.get() + " degrees\n");

        pixFindSkewSweepAndSearchScorePivot(pix, angle, conf, score,
                SWEEP_REDUCTION2, SEARCH_REDUCTION,
                0.0f, SWEEP_RANGE2, SWEEP_DELTA2,
                SEARCH_MIN_DELTA,
                L_SHEAR_ABOUT_CORNER);
        System.out.println("pixFind...Pivot(about corner):" +
                "  conf = " + conf.get() + ", angle =" + angle.get() + " degrees, score = " + score.get() + "\n");

        pixFindSkewSweepAndSearchScorePivot(pix, angle, conf, score,
                SWEEP_REDUCTION2, SEARCH_REDUCTION,
                0.0f, SWEEP_RANGE2, SWEEP_DELTA2,
                SEARCH_MIN_DELTA,
                L_SHEAR_ABOUT_CENTER);
        System.out.println("pixFind...Pivot(about center):" +
                "  conf = " + conf.get() + ", angle =" + angle.get() + " degrees, score = " + score.get() + "\n");

    }

}
