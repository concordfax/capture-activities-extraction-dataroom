package com.auxolabs.cvision.pipeline.steps;

import com.auxolabs.capture.annotator.AnnotationsProcessor;
import com.auxolabs.capture.annotator.AnnotatorService;
import com.auxolabs.capture.annotator.models.Annotation;
import com.auxolabs.capture.annotator.models.TokenPosition;
import com.auxolabs.capture.annotator.models.response.SentenceModel;
import com.auxolabs.cvision.models.nlp.PageModel;
import com.auxolabs.cvision.models.ocr.Lines;
import com.auxolabs.cvision.models.ocr.Para;
import com.auxolabs.cvision.models.ocr.SegBlock;
import com.auxolabs.cvision.pipeline.ExtractionPipelineMessage;
import com.auxolabs.cvision.pipeline.steps.helpers.AnnotationHelper;
import com.auxolabs.cvision.streamlining.helpers.AssignTokenPosition;
import com.auxolabs.cvision.streamlining.models.ImageSegment;
import com.concordfax.capture.core.logging.ILogger;
import com.concordfax.capture.core.logging.LogLevels;
import com.concordfax.capture.core.models.extraction.annotators.ExtractionAnnotatorConfiguration;
import com.concordfax.capture.core.models.ocr.abbyy.Line;
import com.concordfax.capture.core.pipeline.AbstractMessage;
import com.concordfax.capture.core.pipeline.PipelineStep;
import edu.stanford.nlp.util.Timing;

import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class AnnotationAndMappingStep implements PipelineStep {
    private ILogger log;
    private AnnotationHelper annotationHelper;
    private List<ImageSegment> segments;

    @Inject
    public AnnotationAndMappingStep(ILogger log, AnnotatorService annotatorService){
        this.log = log;
        this.annotationHelper = new AnnotationHelper(annotatorService);
        this.segments = new ArrayList<>();
    }


    @Override
    public AbstractMessage process(AbstractMessage message) {
        Timing timing = new Timing();
        ExtractionPipelineMessage extractionPipelineMessage = (ExtractionPipelineMessage) message;
        try {
            for (int i = 0; i < extractionPipelineMessage.getPages().size(); i++) {
                PageModel pageModel = extractionPipelineMessage.getPages().get(i);
                //newly added document id for text generation for NPI
                annotatingAndMapping(pageModel,extractionPipelineMessage.getExtractionProfile().getAnnotators(), extractionPipelineMessage.getDocumentId());
            }
            log.trace(String.format("annotation and mapping step completed in %d ms", timing.report()), LogLevels.Verbose, extractionPipelineMessage.getLogMaker().getProperties());
        } catch (Exception e) {
            // TODO: 21/05/18 LOGGER replace the log prop manufacturer code here
            throw new RuntimeException(e);
        }
        return message;
    }

    private void annotatingAndMapping(PageModel pageModel,ExtractionAnnotatorConfiguration annotatorConfigurations, String documentId) {
        AssignTokenPosition assignTokenPosition = new AssignTokenPosition();
        //assigning token positions for each block and the mapped segments inside the blocks
        assignTokenPosition.assignTokenPositionToBlockAndSegments(pageModel.getBlockList());
        //calling entity annotator step for annotating the sentences from each block
        Map<String,List<SentenceModel>> annotatorNameToAnnotations  = annotationHelper.createTextAndFeedToAnnotator(pageModel.getBlockList(),annotatorConfigurations, pageModel.getPageNumber(), documentId);
        //mapping the token positions back into the blocks and mapped segments inside of each blocks
        setAnnotationsToBlocksAndImageSegments(pageModel.getBlockList(),annotatorNameToAnnotations);

    }

    private void setAnnotationsToBlocksAndImageSegments(List<SegBlock> blocks, Map<String,List<SentenceModel>> annotations){

        for (int i = 0; i < blocks.size(); i++) {
            SegBlock block = blocks.get(i);
            block.setBlockAnnotations(new HashMap<>());
            // iterate each entry in the result for the annotator and sentence = sentences.get(i)
            for (Map.Entry<String,List<SentenceModel>> annotationsOfEachAnnotator: annotations.entrySet()){
                block.getBlockAnnotations().put(annotationsOfEachAnnotator.getKey(), annotationsOfEachAnnotator.getValue().get(i).getAnnotations());
                for(Para para: block.getParagraphList()){
                    for(Lines line:para.getLines()){
                        for (ImageSegment imageSegment : line.getMappedSegments()){
                            mapAnnotationBackToImageSegment(imageSegment,annotationsOfEachAnnotator.getValue().get(i),
                                    annotationsOfEachAnnotator.getKey());
                        }
                    }
                }

            }
        }
    }

    private void mapAnnotationBackToImageSegment(ImageSegment imageSegment,SentenceModel sentenceModelOfBlock,String annotator){
        TokenPosition largerSegmentPosition = imageSegment.getTokenPosition();
        Map<String,SentenceModel> imageSegmentAnnotationsForLargerSegment = imageSegment.getAnnotatorNameToAnnotations();
        if (imageSegmentAnnotationsForLargerSegment == null){
            imageSegmentAnnotationsForLargerSegment = new HashMap<>();
        }
        List<Annotation> imageSegmentAnnotationsOfEachAnnotator = sentenceModelOfBlock.getAnnotations().stream().filter(
                annotation -> (annotation.getTokenPosition().getStartPosition()>= largerSegmentPosition.getStartPosition()  && largerSegmentPosition.getEndPosition() >  annotation.getTokenPosition().getStartPosition() )
                        && ( annotation.getTokenPosition().getEndPosition()<= largerSegmentPosition.getEndPosition()
                || annotation.getTokenPosition().getEndPosition() > largerSegmentPosition.getEndPosition())).collect(Collectors.toList());
        imageSegmentAnnotationsForLargerSegment.put(annotator,new SentenceModel(imageSegment.getText(),imageSegmentAnnotationsOfEachAnnotator));
        imageSegment.setAnnotatorNameToAnnotations(imageSegmentAnnotationsForLargerSegment);

    }

}
