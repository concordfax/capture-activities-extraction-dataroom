package com.auxolabs.cvision.pipeline.steps;

import com.auxolabs.cvision.di.ComponentsModule;
import com.auxolabs.cvision.pipeline.ExtractionPipelineMessage;
import com.concordfax.capture.core.dependencyresolver.FileTypes;
import com.concordfax.capture.core.dependencyresolver.GetFileStream;
import com.concordfax.capture.core.logging.ILogger;
import com.concordfax.capture.core.logging.LogLevels;
import com.concordfax.capture.core.models.environment.EnvironmentCollectionModel;
import com.concordfax.capture.core.models.mlmodel.result.Classification;
import com.concordfax.capture.core.models.mlmodel.result.ClassificationResult;
import com.concordfax.capture.core.pipeline.AbstractMessage;
import com.concordfax.capture.core.pipeline.PipelineStep;
import com.concordfax.capture.core.storage.blobstore.IBlobService;
import com.concordfax.capture.core.utils.mlmodel.*;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Performs the actual inference on the document
 */
public class DocumentClassificationStep implements PipelineStep {

    // TODO: 2019-02-20 we have hardcoded some of the logic in this to accept only the concept of text files.

    private ILogger log;
    private ModelClientService modelServicesClient;
    private IBlobService blobService;
    private static final Integer MAX_INSTANCES_IN_BATCH = 10;

    public DocumentClassificationStep(ILogger logger, ModelClientService modelServicesClient, IBlobService blobService) {
        this.log = logger;
        this.modelServicesClient = modelServicesClient;
        this.blobService = blobService;
    }

    @Override
    public AbstractMessage process(AbstractMessage abstractMessage) {
        assert abstractMessage instanceof ExtractionPipelineMessage;
        ExtractionPipelineMessage psMessage = (ExtractionPipelineMessage) abstractMessage;
        ClassificationResult classificationResult = null;
        try {

            String modelUri = psMessage.getExtractionProfile().getDocumentClassification().getBaseUri();

            List<PredictResponseModel> predictionResults = new ArrayList<>();
            // counter to keep track of number of items in each request to the actual model, so as the
            Integer toProcessCount = 0;
            Integer pageIndex = 0;
            HashMap<Integer,Integer> blankPages = new HashMap<>();

            // request model format format for the model
            PredictRequestModel<String> predictRequestModel = new PredictRequestModel<>();
            predictRequestModel.setInstances(new ArrayList<>());

            //Iterating local files instead of blob
            ArrayList<String> textPath= new ArrayList<>();
            Files.walk(Paths.get(ComponentsModule.dependencyResolver(psMessage.getDocumentId(), "text")))
                    .filter(Files::isRegularFile)
                    .forEach(filename->textPath.add(filename.toString()));


            // iterate over all the pages in the configuration and perform prediction on them and add them to the list of responses
//            for (int i = 0; i < psMessage.getOcrTextFilePaths().size(); i++) {
//                String pageFilePath = psMessage.getOcrTextFilePaths().get(i);
            java.util.Collections.sort(textPath);
            for (int i = 0; i < textPath.size(); i++) {
                String pageFilePath = textPath.get(i);

                // send request to model once max request count has reached and add response to the list in the message
                if (toProcessCount > 0 && toProcessCount % MAX_INSTANCES_IN_BATCH == 0) {
                    ResponseModel<PredictResponseModel> responseModel = performInference(modelUri,
                            predictRequestModel);
                    predictionResults.add(responseModel.getObj());
                    // reset the shared collectors
                    predictRequestModel.getInstances().clear();
                    toProcessCount = 0;
                }
                toProcessCount++;

                // get file from blob store and remove non-ascii characters from it
                FileInputStream is=new FileInputStream(pageFilePath);
                String text= IOUtils.toString((InputStream)is, String.valueOf(StandardCharsets.UTF_8));
                text = text.replaceAll("[^\\x00-\\x7f]", " ");

                predictRequestModel.getInstances().add(text);

                //remove tab and space characters
                text = text.replaceAll("\\s+","");
                //Have a index of what pages are blank. Pages with less than 60 characters are marked as blank
                if (text.length() <=60){
                    blankPages.put(pageIndex,-3);
                }
                else {
                    blankPages.put(pageIndex,0);
                }
                pageIndex++;
            }

            // check if any of the messages could not be processed in the above loop, case when page count < MAX_INSTANCES_IN_BATCH
            if(toProcessCount>0){
                ResponseModel<PredictResponseModel> responseModel = performInference(modelUri,
                        predictRequestModel);
                predictionResults.add(responseModel.getObj());
            }

            classificationResult = convertResultTypes(predictionResults);
            classificationResult = updateBlankPages(classificationResult, blankPages);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        psMessage.setDocumentClassificationResult(classificationResult);
        log.trace("document classification completed", LogLevels.Verbose, psMessage.getLogMaker().getProperties());
        return psMessage;
    }

    /**
     * Takes the baseUri to the model and the predict request model containing the list of instances
     */
    private ResponseModel<PredictResponseModel> performInference(String baseUri, PredictRequestModel<String> predictRequestModel){
        ResponseModel<PredictResponseModel> predictResponseModel = null;
        try {
            predictResponseModel = modelServicesClient.predict(baseUri, predictRequestModel);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return predictResponseModel;
    }

    private ClassificationResult convertResultTypes(List<PredictResponseModel> predictResponseModels){
        // Logging for info
        List<String> modelClasses = Arrays.asList(
                "Discharge Summary",
                "Prior Authorization",
                "Lab Reports",
                "Referrals",
                "Imaging Reports",
                "Physician Order",
                "Patient Evaluation",
                "Rx Prior Authorization",
                "Progress Notes",
                "Operative Note",
                "Triage Calls",
                "Fax Cover Sheet",
                "Unknown",
                "Medical Record Request",
                "Claims",
                "ObgynReports",
                "MedServiceAuthorization",
                "Pharmacy Follow-Up",
                "PatientAdmissions");

        // Map the classes to the Hashmap
        HashMap<Integer, String> map = new HashMap<>();
        for (int i = 0; i < modelClasses.size(); i++) {
            map.put(i, modelClasses.get(i));
        }

        ClassificationResult classificationResult = new ClassificationResult();
        classificationResult.setPredictions(new ArrayList<>());
        AtomicInteger counter = new AtomicInteger();
        for (PredictResponseModel predictResponseModel : predictResponseModels) {
            com.concordfax.capture.core.utils.mlmodel.ClassificationResult apiClassificationResult = (com.concordfax.capture.core.utils.mlmodel.ClassificationResult) predictResponseModel;
            apiClassificationResult.getPredictions().forEach(classification -> {
                classificationResult.getPredictions().add(classification);
                counter.getAndIncrement();
                System.out.println("PAGE NUMBER:" + counter + " --->" + map.get(classification.getCls()));
            });
        }
        return classificationResult;
    }

    private ClassificationResult updateBlankPages(ClassificationResult classificationResult,  HashMap<Integer,Integer> blankPages){
        //set in the final response, if the pages are blank or not
        for (int index = 0; index < classificationResult.getPredictions().size(); index++) {
            if (blankPages.get(index) == -3){
                classificationResult.getPredictions().get(index).setCls(-3);
                classificationResult.getPredictions().get(index).setConf(1.0);
            }
        }

        return classificationResult;
    }
}
