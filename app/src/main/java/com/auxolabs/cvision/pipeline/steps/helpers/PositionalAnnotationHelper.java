package com.auxolabs.cvision.pipeline.steps.helpers;

import com.auxolabs.capture.annotator.AnnotationsProcessor;
import com.auxolabs.capture.annotator.models.Annotation;
import com.auxolabs.capture.annotator.models.TokenPosition;
import com.auxolabs.capture.annotator.models.response.SentenceModel;
import com.auxolabs.capture.positional.constants.PositionalRuleConstants;
import com.auxolabs.capture.positional.models.TokenModel;
import com.auxolabs.cvision.models.cv.PIBOXHolder;
import com.auxolabs.cvision.models.nlp.PageModel;
import com.concordfax.capture.core.models.extraction.annotators.AnnotatorModifier;
import com.concordfax.capture.core.models.extraction.annotators.ExtractionAnnotatorConfiguration;
import com.concordfax.capture.core.models.extraction.annotators.SpecialProps;
import com.concordfax.capture.core.models.extraction.annotators.SpecialPropsFilter;
import com.concordfax.capture.core.models.extraction.positional.output.PositionalPropertyTypes;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class PositionalAnnotationHelper {

    private AnnotationsProcessor annotationsProcessor;
    public PositionalAnnotationHelper() {
        this.annotationsProcessor = new AnnotationsProcessor();
    }

    public HashMap<String, HashSet<TokenModel>> getTagToAnnotations(PageModel pageModel, List<PIBOXHolder> segments, ExtractionAnnotatorConfiguration annotatorConfiguration){
        String text = generateTextAndAddTokenPostionInfo(segments);
        HashMap<String,HashSet<TokenModel>> resultAnnotations = new HashMap<>();
        List<String> textList = new ArrayList<>();
        textList.add(text);
        Map<String, List<SentenceModel>> annotatorToAnnotations = annotationsProcessor.getAnnotatorToAnnotations(textList,annotatorConfiguration);
        // removing POS as its not actively used by Positional
        annotatorToAnnotations.remove("pos");
        for (Map.Entry<String, List<SentenceModel>> entry : annotatorToAnnotations.entrySet()){
            List<SentenceModel> annotatorResult = entry.getValue();
            for (SentenceModel sentenceModel: annotatorResult){
                List<Annotation> annotations = sentenceModel.getAnnotations();
                mapAnnotationToResultAnnotations(annotations,resultAnnotations,segments);
            }
        }
        HashMap<String, HashSet<TokenModel>> tempTokens = new HashMap<>();
        resultAnnotations.forEach((s, tokenModels) -> {
            tempTokens.put(s.toLowerCase().trim(), tokenModels);
        });
        positionalAnnotationPostProcessing(tempTokens,annotatorConfiguration.getModifiers());
        applySpecialProps(tempTokens,annotatorConfiguration.getSpecialProps());
        addPagePropsToken(tempTokens, pageModel);
        return tempTokens;
    }

    private List<AnnotatorModifier> createMergers() {
        AnnotatorModifier annotatorModifierD1 = new AnnotatorModifier("del", "dob", null);
        AnnotatorModifier annotatorModifier1 = new AnnotatorModifier("add+del", "direct", "dob");
        AnnotatorModifier annotatorModifier2 = new AnnotatorModifier("add+del", "personname", "person_name");
        AnnotatorModifier annotatorModifier3 = new AnnotatorModifier("add+del", "person", "person_name");
        List<AnnotatorModifier> annotatorModifiers = new ArrayList<>();
        annotatorModifiers.add(annotatorModifierD1);
        annotatorModifiers.add(annotatorModifier1);
        annotatorModifiers.add(annotatorModifier2);
        annotatorModifiers.add(annotatorModifier3);
        return annotatorModifiers;
    }

    private void positionalAnnotationPostProcessing(HashMap<String, HashSet<TokenModel>> lexiconToTokens, List<AnnotatorModifier> annotatorModifiers){
        List<AnnotatorModifier> modifiers = annotatorModifiers;
        modifiers.forEach(annotatorModifier -> {
            switch (annotatorModifier.getModifierType()){
                case "add+del":
                    if(!lexiconToTokens.containsKey(annotatorModifier.getTo())) {
                        lexiconToTokens.put(annotatorModifier.getTo(), new HashSet<>());
                    }
                    if(lexiconToTokens.containsKey(annotatorModifier.getFrom())) {
                        lexiconToTokens.get(annotatorModifier.getTo()).addAll(lexiconToTokens.get(annotatorModifier.getFrom()));
                        lexiconToTokens.remove(annotatorModifier.getFrom());
                    }
                    break;
                case "del":
                    lexiconToTokens.remove(annotatorModifier.getFrom());
                    break;
            }
        });
    }

    private void mapAnnotationToResultAnnotations(List<Annotation> annotations,
                                                  HashMap<String,HashSet<TokenModel>> resultAnnotations, List<PIBOXHolder> piboxHolders){
        HashMap<String,List<Annotation>> tagToAnnotationsMap = groupAnnotationsWithTag(annotations);
        for (Map.Entry<String,List<Annotation>> tagToAnnotation:tagToAnnotationsMap.entrySet()){
            String tag = tagToAnnotation.getKey();
            List<Annotation> annotationListOfTag = tagToAnnotation.getValue();
            HashSet<TokenModel> tokenModels = mapAnnotationsToTokenModel(annotationListOfTag,piboxHolders);
            resultAnnotations.put(tag,tokenModels);
        }
        postProcessingPersonName(annotations,resultAnnotations);
    }

    private void postProcessingPersonName(List<Annotation> annotations,
                                          HashMap<String,HashSet<TokenModel>> resultAnnotations){
        List<Annotation> negationPosAnnotations = annotations.stream().filter(annotation -> annotation.getAnswer().equals("Negation POS")).collect(Collectors.toList());
        List<String> negationPosWords = new ArrayList<>();
        for (Annotation negationPosAnnotation : negationPosAnnotations){
            negationPosWords.add(negationPosAnnotation.getWord());
        }
        if (resultAnnotations.containsKey("PersonName")) {
            HashSet<TokenModel> tokenModelsInPersonName = resultAnnotations.get("PersonName");
            for (TokenModel tokenModel : tokenModelsInPersonName) {
                boolean hasNegationPosInName = false;
                StringBuilder nameBlock = new StringBuilder();
                String personNameCandidate = (String)tokenModel.get("text");
                String[] splittedName = personNameCandidate.trim().split("\\s+");
                splittedName[splittedName.length-1] = checkIfContainsColon(splittedName[splittedName.length-1]);
                for (String splitName : splittedName){
                    if (negationPosWords.contains(splitName))
                        hasNegationPosInName = true;
                }
                for (int i = 0; i<splittedName.length-1;i++){
                    nameBlock.append(splittedName[i]);
                    nameBlock.append(" ");
                }
                if (!hasNegationPosInName){
                    nameBlock.append(splittedName[splittedName.length-1]);
                }
                tokenModel.put("text",nameBlock.toString().trim());
            }
        }
    }

    private String checkIfContainsColon(String name){
        if (name.charAt(name.length()-1)==':')
            return name.substring(0,name.length()-1);
        return name;
    }

    private HashMap<String,List<Annotation>> groupAnnotationsWithTag(List<Annotation> annotations){
        HashMap<String,List<Annotation>> tagToAnnotations = new HashMap<>();
        for (Annotation annotation: annotations){
            String tag = annotation.getAnswer();
            if (tagToAnnotations.containsKey(tag)){
                tagToAnnotations.get(tag).add(annotation);
            }
            else{
                List<Annotation> tempAnnotation = new ArrayList<>();
                tempAnnotation.add(annotation);
                tagToAnnotations.put(tag,tempAnnotation);
            }
        }
        return tagToAnnotations;
    }

    private HashSet<TokenModel> mapAnnotationsToTokenModel(List<Annotation> annotations, List<PIBOXHolder> piBoxes){
        HashSet<TokenModel> tokenModels = new HashSet<>();
        TreeSet<PIBOXHolder> annotationPiBoxes = convertSpecialAnnotationsToPiBoxes(piBoxes,annotations);
        annotationPiBoxes.forEach(piboxHolder -> {
            tokenModels.add(convertPiBoxToTokenModel(piboxHolder));
        });
        return tokenModels;
    }

    private TokenModel convertPiBoxToTokenModel(PIBOXHolder piboxHolder) {
        TokenModel tokenModel = new TokenModel();
        tokenModel.put(PositionalRuleConstants.Properties.X, piboxHolder.x());
        tokenModel.put(PositionalRuleConstants.Properties.Y, piboxHolder.y());
        tokenModel.put(PositionalRuleConstants.Properties.W, piboxHolder.w());
        tokenModel.put(PositionalRuleConstants.Properties.H, piboxHolder.h());
        tokenModel.put(PositionalRuleConstants.Properties.TEXT, piboxHolder.getText());
        tokenModel.put(PositionalRuleConstants.Properties.ID, piboxHolder.hashCode());
        tokenModel.put(PositionalRuleConstants.Properties.SPE_ANO, piboxHolder.getTempOrgText());
        tokenModel.setTokenName(piboxHolder.getText());
        return tokenModel;
    }

    private TreeSet<PIBOXHolder> convertSpecialAnnotationsToPiBoxes(List<PIBOXHolder> piBoxes, List<Annotation> annotations) {
        TreeSet<PIBOXHolder> answers = new TreeSet<>();
        annotations.forEach(annotatorResult -> {
            piBoxes.forEach(piboxHolder -> {
                try {
                    // we have added a +1 or -1 buffer on the end and start tokens to account for space
                    if (piboxHolder.getText() != null
                            && (piboxHolder.getTokenPosition().getStartPosition() <= annotatorResult.getTokenPosition().getStartPosition()
                            || Math.abs(piboxHolder.getTokenPosition().getStartPosition() - annotatorResult.getTokenPosition().getStartPosition()) <= 1)
                            && (annotatorResult.getTokenPosition().getEndPosition() <= piboxHolder.getTokenPosition().getEndPosition()
                            || Math.abs(annotatorResult.getTokenPosition().getEndPosition() - piboxHolder.getTokenPosition().getEndPosition()) <= 1)) {
                        answers.add(piboxHolder);
                        piboxHolder.getAnnotationResults().add(annotatorResult);
                        piboxHolder.setTempInfo(annotatorResult.getAnswer());
                        piboxHolder.setTempOrgText(annotatorResult.getWord());
                    }
                } catch (Exception e) {
                    // ignore here
                }
            });
        });
        return answers;
    }

    private String generateTextAndAddTokenPostionInfo(List<PIBOXHolder> piBoxes) {
        StringBuilder formattedText = new StringBuilder("");
        piBoxes.forEach(pibox -> {
            if (pibox.getLines() != null) {
                StringBuilder textInBox = new StringBuilder(pibox.getText());
                if (textInBox.length() != 0) {
                    // filter out unnecessary noise coming due to possible pen marks on paper, for dates
                    {
                        Pattern pattern = Pattern.compile("([0-9]{2,4}(/)[0-9]{2}(/)[0-9]{2})|([0-9]{2}(/)[0-9]{2}(/)[0-9]{2,4})");
                        Matcher matcher = pattern.matcher(textInBox.toString());
                        int currentEnd = 0;
                        StringBuilder currFormattedText = new StringBuilder("");
                        while (matcher.find()) {
                            String matchedText = textInBox.substring(matcher.start(), matcher.end());
                            if (matcher.end() < textInBox.length()) {
                                String nextChar = textInBox.substring(matcher.end(), matcher.end() + 1);
                                if (nextChar.matches("[a-zA-Z]")) {
                                    currFormattedText.append(textInBox.substring(currentEnd, matcher.start()));
                                    currFormattedText.append(matchedText);
                                    currFormattedText.append(" ");
                                    currentEnd = matcher.end();
                                }
                            }
                        }
                        if (currentEnd < textInBox.length()) {
                            currFormattedText.append(textInBox.substring(currentEnd, textInBox.length()));
                        }
                        pibox.setText(textInBox.toString());
                        pibox.setTokenPosition(new TokenPosition(formattedText.length(), formattedText.length() + currFormattedText.length() - 1));
                        formattedText.append(currFormattedText);
//                        textInDoc.append(textInBox);
                    }
                }
            }
        });
        return formattedText.toString();
    }

    private void addPagePropsToken(HashMap<String, HashSet<TokenModel>> tokens, PageModel pageModel) {
        TokenModel tokenModel = new TokenModel();
        tokenModel.put(PositionalRuleConstants.PageProps.PAGE_WIDTH, pageModel.getPageWidth());
        tokenModel.put(PositionalRuleConstants.PageProps.PAGE_HEIGHT, pageModel.getPageHeight());
        tokenModel.put(PositionalRuleConstants.Properties.ID, PositionalRuleConstants.PageProps.PAGE_PROPS_ID);
        tokens.put(PositionalRuleConstants.PageProps.PAGE_PROPS, new HashSet<>());
        tokens.get(PositionalRuleConstants.PageProps.PAGE_PROPS).add(tokenModel);
    }

    private void applySpecialProps(HashMap<String, HashSet<TokenModel>> tokens, List<SpecialProps> specialProps) {
        for (SpecialProps specialProp : specialProps) {
            if(tokens.containsKey(specialProp.getAnnotation())){
                applySpecialProps(tokens.get(specialProp.getAnnotation()), specialProp);
            }
        }
    }

    private void applySpecialProps(HashSet<TokenModel> tokens, SpecialProps props) {
        if (props != null) {
            if(props.getFilter()!=null)
                filterTokens(tokens, props.getFilter());
        }
    }

    private boolean checkIfRegexMatches(List<String> regex, TokenModel tokenModel){
        for (int i = 0; i < regex.size(); i++) {
            if(((String)tokenModel.get(PositionalRuleConstants.Properties.TEXT)).trim().matches(regex.get(i)))
                return true;
        }
        return false;
    }

    private boolean isValidWordCount(TokenModel tokenModel, int maxWords, int minWords){
        int wordCount = ((String) tokenModel.get(PositionalRuleConstants.Properties.TEXT)).trim().split(" ").length;
        return wordCount <= maxWords && wordCount >= minWords;
    }

    private void filterTokens(HashSet<TokenModel> tokens, SpecialPropsFilter filter){
        if (tokens != null) {
            if (filter.getRegexFilter() != null) {
                if(filter.getRegexFilter().getExclude()!=null && !filter.getRegexFilter().getExclude().isEmpty())
                    tokens.removeIf(tokenModel -> checkIfRegexMatches(filter.getRegexFilter().getExclude(), tokenModel));
                if(filter.getRegexFilter().getInclude()!=null && !filter.getRegexFilter().getInclude().isEmpty())
                    tokens.removeIf(tokenModel -> !checkIfRegexMatches(filter.getRegexFilter().getInclude(), tokenModel));
            }
            if (filter.getMaxWords() != null && filter.getMinWords() != null)
                tokens.removeIf(tokenModel -> !isValidWordCount(tokenModel,filter.getMaxWords(),filter.getMinWords()));
        }
    }
}
