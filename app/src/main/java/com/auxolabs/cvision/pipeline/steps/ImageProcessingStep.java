package com.auxolabs.cvision.pipeline.steps;

import com.auxolabs.cvision.models.cv.LDObject;
import com.auxolabs.cvision.models.cv.PIBOXHolder;
import com.auxolabs.cvision.models.cv.RDObject;
import com.auxolabs.cvision.models.cv.ScaleModel;
import com.auxolabs.cvision.models.nlp.PageModel;
import com.auxolabs.cvision.pipeline.ExtractionPipelineMessage;
import com.auxolabs.cvision.pipeline.utils.*;
import com.auxolabs.cvision.streamlining.segmentation.SegmentationModule;
import com.auxolabs.cvision.streamlining.models.ImageSegment;
import com.auxolabs.cvision.utils.LeptonicaUtils;
import com.concordfax.capture.core.logging.ILogger;
import com.concordfax.capture.core.logging.LogLevels;
import com.concordfax.capture.core.pipeline.AbstractMessage;
import com.concordfax.capture.core.pipeline.PipelineStep;
import edu.stanford.nlp.util.Timing;
import org.apache.sanselan.ImageInfo;
import org.apache.sanselan.Sanselan;
import org.bytedeco.javacpp.lept;

import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.inject.Inject;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * The multi page processor class implements the following functionality:
 * <ul>
 * <li>Fetch image properties with the help of library Sanselan</li>
 * <li>Convert media-types (like tiff, png, jpg and pdf) to a common processable model (Leptonica PIX model)</li>
 * </ul>
 */
public class ImageProcessingStep implements PipelineStep {

    private ILogger log;
    private ImagePreProcessor imagePreProcessor = new ImagePreProcessor();
    private LinesDetector linesDetector = new LinesDetector();
    private ImageDetector imageDetector = new ImageDetector();
    private Segmentation segmentation = new Segmentation();

    @Inject
    public ImageProcessingStep(ILogger log) {
        this.log = log;
    }

    @Override
    public AbstractMessage process(AbstractMessage message) {
        ExtractionPipelineMessage extractionPipelineMessage = (ExtractionPipelineMessage) message;
        Timing timing = new Timing();
        byte[] bytes = extractionPipelineMessage.getDocumentBytes();
        try {
            ImageInfo imageInfo = Sanselan.getImageInfo(bytes);
            extractionPipelineMessage.setXDpi(imageInfo.getPhysicalWidthDpi());
            extractionPipelineMessage.setYDpi(imageInfo.getPhysicalHeightDpi());
        } catch (Exception e) {
            log.trace("Image could not be decoded by ImageInfo", LogLevels.Verbose, extractionPipelineMessage.getLogMaker().getProperties());
        }
        List<PageModel> pages = process(new ByteArrayInputStream(extractionPipelineMessage.getDocumentBytes()),
                extractionPipelineMessage.getXDpi(), extractionPipelineMessage.getYDpi(), extractionPipelineMessage.getDocumentId());
        extractionPipelineMessage.setDocumentBytes(null);
        extractionPipelineMessage.setPages(pages);
        log.trace(String.format("image pre-processing completed in %d ms", timing.stop()), LogLevels.Verbose, extractionPipelineMessage.getLogMaker().getProperties());
        return message;
    }

    public List<PageModel> process(InputStream fileInputStream, Integer xDpi, Integer yDpi, String documentId) {
        List<PageModel> pageModels = new ArrayList<>();
        try {
            ImageInputStream input = ImageIO.createImageInputStream(fileInputStream);
            Iterator<ImageReader> readers = ImageIO.getImageReadersByFormatName("TIFF");
            if (!readers.hasNext()) {
                throw new RuntimeException("Tiff reader not found");
            }
            ImageReader reader = readers.next();
            reader.setInput(input);
            ImageReadParam param = reader.getDefaultReadParam();
            int numImages = reader.getNumImages(true);
            for (int i = 0; i < numImages; i++) {
                PIBOXHolder sourceImage = null;
                try {
                    BufferedImage bufferedImage = reader.read(i, param);
                    bufferedImage = removeAlphaChannel(bufferedImage);
                    lept.PIX pix = LeptonicaUtils.getInstance().convertBufferedImageToPIX(bufferedImage, xDpi, yDpi);
                    sourceImage = new PIBOXHolder(pix);
                    PageModel pageModel = new PageModel(i + 1);
                    ScaleModel scaleModel = new ScaleModel();
                    List<ImageSegment> segments = getSegments(sourceImage, scaleModel);
                    pageModel.setScaleModel(scaleModel);
                    pageModel.setImageSegments(segments);
                    pageModel.setPageProps(pix.w(), pix.h());
                    pageModels.add(pageModel);

                    // Write the image
//                    lept.PIX copyImage = pix;
//                    for(ImageSegment segment: segments){
//                        drawRectangleWithColor(copyImage,segment.x().intValue(),segment.y().intValue(),segment.w().intValue(),segment.h().intValue(),255,0,0,2);
//                    }

//                    String output_directory = "/home/nstest-admin/Documents/output/leptonica/" + documentId + "/";
//                    File directory = new File(String.valueOf(output_directory));
//
//                    if (! directory.exists()){
//                        directory.mkdir();
//                        // If you require it to make the entire directory path including parents,
//                        // use directory.mkdirs(); here instead.
//                    }
//
//                    lept.pixWrite( output_directory + String.valueOf(i)+".png",copyImage,lept.IFF_PNG);
                    bufferedImage.flush();
                }catch (Exception e){
                    return null;
                }
                finally {
                    if(sourceImage!=null)
                        LeptonicaUtils.getInstance().deletePIBOX(sourceImage);
                }
            }
            input.close();
            reader.dispose();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            try {
                fileInputStream.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return pageModels;
    }

    private BufferedImage removeAlphaChannel(BufferedImage img) {
        BufferedImage copy = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = copy.createGraphics();
        g2d.setColor(Color.WHITE); // Or what ever fill color you want...
        g2d.fillRect(0, 0, copy.getWidth(), copy.getHeight());
        g2d.drawImage(img, 0, 0, null);
        g2d.dispose();
        img.flush();
        return copy;
    }

    private List<ImageSegment> getSegments(PIBOXHolder sourceImage, ScaleModel scaleModel){
        List<ImageSegment> segments = new ArrayList<>();
        PIBOXHolder binaryImage = null;
        PIBOXHolder processingImage = null;
        PIBOXHolder linesInDocument = null;
        PIBOXHolder imagesInDocument = null;
        try {
            if (sourceImage != null) {
                imagePreProcessor.convertTo32bpp(sourceImage);
                binaryImage = imagePreProcessor.convertToBinary(sourceImage);
            }
            if (binaryImage != null) {
                LDObject ldObject = linesDetector.detectLinesInImage(binaryImage, sourceImage);
                if (ldObject != null) {
                    linesInDocument = ldObject.getLinesInImage();
                    processingImage = ldObject.getImgWithoutLines();
                }
                LeptonicaUtils.getInstance().deletePIBOX(linesInDocument);
            }
            if (processingImage != null) {
                RDObject rdObject = imageDetector.detectLogos(processingImage, sourceImage);
                if (rdObject != null) {
                    LeptonicaUtils.getInstance().deletePIBOX(processingImage);
                    imagesInDocument = rdObject.getImagesInImage();
                    LeptonicaUtils.getInstance().deletePIBOX(imagesInDocument);
                    processingImage = rdObject.getTextInImage();
                }
            }
            if (processingImage != null) {
                segments = segmentation.getTextSegmentsPositions(processingImage);
            }
        } finally {
            LeptonicaUtils.getInstance().deletePIBOX(binaryImage);
            LeptonicaUtils.getInstance().deletePIBOX(imagesInDocument);
            LeptonicaUtils.getInstance().deletePIBOX(linesInDocument);
            LeptonicaUtils.getInstance().deletePIBOX(processingImage);
        }
        return segments;
    }

    private void drawRectangleWithColor(lept.PIX pix, int x, int y, int w, int h, int r, int g, int b, int thickness){
        for (int i = y; i < y+h; i++) {
            for (int j = 0; j < thickness; j++) {
                lept.pixSetRGBPixel(pix, x+j, i, r,g,b);
                lept.pixSetRGBPixel(pix, x+w+j, i, r,g,b);
            }
        }
        for (int i = x; i < x+w; i++) {
            for (int j = 0; j < thickness; j++) {
                lept.pixSetRGBPixel(pix, i, y+j, r,g,b);
                lept.pixSetRGBPixel(pix, i, y+h+j, r,g,b);
            }
        }
    }
}
