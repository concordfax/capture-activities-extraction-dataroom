package com.auxolabs.cvision.pipeline.utils.field.normalizers;

public class SSNNormalizer implements FieldNormalizer<String, String> {
    @Override
    public String normalize(String ssn) {
        if (ssn != null) {
            ssn = ssn.trim();
            ssn = ssn.replaceAll("\\s+","");
            if (ssn.length() == 4) {
                return "XXX-XX-" + ssn;
            } else if (ssn.length() == 9) {
                if (ssn.matches("\\d{9}|X{9}")) {
                    return ssn.substring(0, 3) + "-" + ssn.substring(3, 5) + "-" + ssn.substring(5, 9);
                }
            } else if (!ssn.isEmpty()) {
                ssn = ssn.replaceAll("(\\s)*[\\-,\\\\.](\\s)*", "-");
                ssn = ssn.replaceAll("[*]", "X");
                ssn = ssn.toUpperCase();
            }
            ssn = ssn.trim();
        }
        return ssn;
    }
}
