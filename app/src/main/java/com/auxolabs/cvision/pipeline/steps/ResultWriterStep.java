package com.auxolabs.cvision.pipeline.steps;

import com.auxolabs.cvision.db.IDBService;
import com.auxolabs.cvision.pipeline.ExtractionPipelineMessage;
import com.concordfax.capture.core.events.CaptureEvent;
import com.concordfax.capture.core.events.CaptureEventTypes;
import com.concordfax.capture.core.events.EventCategory;
import com.concordfax.capture.core.jobmanager.CaptureJob;
import com.concordfax.capture.core.jobmanager.JobResponseModel;
import com.concordfax.capture.core.logging.ILogger;
import com.concordfax.capture.core.logging.LogLevels;
import com.concordfax.capture.core.models.document.activity.ActivityTypes;
import com.concordfax.capture.core.models.document.result.extraction.ExtractionActivityResult;
import com.concordfax.capture.core.models.document.result.extraction.ExtractionPage;
import com.concordfax.capture.core.pipeline.AbstractMessage;
import com.concordfax.capture.core.pipeline.PipelineStep;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * class: ActivityResultWriterProcessor
 * This performs the following actions:
 * 1. updates the activity result in DB
 * 2. insert activity status event in DB
 */
public class ResultWriterStep implements PipelineStep {

    private ILogger log;
    private IDBService db;

    @Inject
    public ResultWriterStep(ILogger log, IDBService db) {
        this.log = log;
        this.db = db;
    }

    @Override
    public AbstractMessage process(AbstractMessage message) {
        ExtractionPipelineMessage extractionPipelineMessage = (ExtractionPipelineMessage) message;
        ExtractionActivityResult extractionActivityResult = extractionPipelineMessage.getExtractionActivityResult();
//        boolean isSuccess = db.updateActivityResult(extractionPipelineMessage.getDcm().getDocumentId(),
//        extractionPipelineMessage.getExtractionActivityIdx(), extractionPipelineMessage.getPartitionKey(),extractionActivityResult);
        ((CaptureJob)extractionPipelineMessage.getJobRef()).setJobResponseModel(makeJobResponse(extractionPipelineMessage));
        log.trace("document result written successfully", LogLevels.Verbose, extractionPipelineMessage.getLogMaker().getProperties());
        return message;
    }

    private JobResponseModel makeJobResponse(ExtractionPipelineMessage extractionPipelineMessage) {
        JobResponseModel jobResponseModel = new JobResponseModel();
//        jobResponseModel.setId("00000000-0000-0000-0000-000000000000");
//        jobResponseModel.setCategory(EventCategory.Capture.getVal());
//        jobResponseModel.setName(CaptureEventTypes.ActivityStatus.toString());
//        jobResponseModel.setTimeStamp(new DateTime().toString());
//        jobResponseModel.setEnvironmentId(extractionPipelineMessage.getEnvironment().getEnvironmentId());
//        jobResponseModel.setAccountId(extractionPipelineMessage.getEnvironment().getAccountId());
//        jobResponseModel.setUserId(extractionPipelineMessage.getDcm().getStatus().getCreatedById());
//        jobResponseModel.setDocumentId(extractionPipelineMessage.getDcm().getDocumentId());
//        jobResponseModel.setPageCount(extractionPipelineMessage.getPages().size());
//        jobResponseModel.setCaptureEvent(makeEvent(extractionPipelineMessage));
        jobResponseModel.setExtractionActivityResult(extractionPipelineMessage.getExtractionActivityResult());
        return jobResponseModel;
    }

    private CaptureEvent makeEvent(ExtractionPipelineMessage extractionPipelineMessage) {
        CaptureEvent event = new CaptureEvent();
        event.setId("00000000-0000-0000-0000-000000000000");
        event.setPagesProcessed(extractionPipelineMessage.getPages().size());
        event.setActivityId(extractionPipelineMessage.getActivityId());
        event.setProcessId(extractionPipelineMessage.getDcm().getProcess().getProcessId());
        event.setProcessDefinitionId(extractionPipelineMessage.getPcm().getProcessId());
        event.setActivityType(ActivityTypes.Extraction.value());
        // TODO: 2019-04-08 correct configured count to reflect the configured fields count
        event.setConfiguredCount(extractionPipelineMessage.getPages().size());
        event.setResultCount(getFieldNames(extractionPipelineMessage.getExtractionActivityResult()).size());
        event.setDocumentId(extractionPipelineMessage.getDcm().getDocumentId());
        return event;
    }

    private List<String> getFieldNames(ExtractionActivityResult extractionActivityResult) {
        if (extractionActivityResult == null || extractionActivityResult.getExtractionDocument() == null)
            return null;
        List<String> fieldNames = new ArrayList<>();
        extractionActivityResult.getExtractionDocument().getFields().forEach(extractionField -> {
            fieldNames.add(extractionField.getName());
        });
        return new ArrayList<>(fieldNames);
    }
}
