package com.auxolabs.cvision.pipeline.utils.field.modifiers;


import com.concordfax.capture.core.models.extraction.output.ModifierType;

public class ModifierFactory {

    private ExtendNameByLength extendNameByLength;
    private ExtendDateByLength extendDateByLength;
    private ExtendSsnByLength extendSsnByLength;
    private ExtendMrnByLength extendMrnByLength;

    public ModifierFactory(){
        extendNameByLength = new ExtendNameByLength();
        extendDateByLength = new ExtendDateByLength();
        extendSsnByLength = new ExtendSsnByLength();
        extendMrnByLength = new ExtendMrnByLength();
    }

    public FieldModifier getModifier(ModifierType modifierType) {
        switch (modifierType) {
            case ExtendNameByLength:
                return extendNameByLength;
            case ExtendDobByLength:
                return extendDateByLength;
            case ExtendSsnByLength:
                return extendSsnByLength;
            case ExtendMrnByLength:
                return extendMrnByLength;
            default:
                return null;
        }
    }
}
