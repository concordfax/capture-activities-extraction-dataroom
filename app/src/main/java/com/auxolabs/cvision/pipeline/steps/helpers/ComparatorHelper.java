package com.auxolabs.cvision.pipeline.steps.helpers;

import com.auxolabs.cvision.models.namemodel.NameComparison;
import info.debatty.java.stringsimilarity.CharacterSubstitutionInterface;
import info.debatty.java.stringsimilarity.WeightedLevenshtein;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;

public class ComparatorHelper {

    public String compareResults(int iterationNo, ArrayList<NameComparison> nameComparisonArrayList, String rulesResult, String mlResult, String npNames) {
        String[] negationNames;
        negationNames =  npNames.split(">");
        rulesResult = StringUtils.normalizeSpace(rulesResult);
        mlResult = StringUtils.normalizeSpace(mlResult);
//-------------------------------------------------------------------------------------------------------------
        //if both aret extracted
        if(StringUtils.isBlank(rulesResult) && StringUtils.isBlank(mlResult)){
            String returnString = bothResultsAreBlank();
            return returnString;
        }
        //if both are equal
        else if(rulesResult.equals(mlResult)){
            String returnString = equalsScenario(rulesResult, mlResult);
            return returnString;
        }

        //if rules results are not present, and only mlResults are present take mlResults
        else if(StringUtils.isBlank(rulesResult)) {
            if(StringUtils.isNotBlank(mlResult))
            {
                String result = rulesIsEmpty(rulesResult, mlResult);
                return result;
            }
        }

        //if mlResult is not extracted, but rules have done, check with negation names
        else if(StringUtils.isBlank(mlResult) && StringUtils.isNotBlank(rulesResult)){
            boolean status = false;
            String returnString = checkAgainstNPName(rulesResult, negationNames);
            if(StringUtils.isNotBlank(returnString)){
                status = compareListOfString(returnString, npNames);
                if(status)
                    return "";
                else
                    return rulesResult;
            }
            return "";
        }

        else if(StringUtils.isNotBlank(rulesResult) && StringUtils.isNotBlank(mlResult)){
            double distance;
            distance = compareUsingDistance(rulesResult, mlResult);
            String mlComparison = checkAgainstNPName(mlResult, negationNames);
            String rulesComparison = checkAgainstNPName(rulesResult, negationNames);
            if (distance <= 4.0){
                String returnString = (rulesResult.length() > mlResult.length()) ? rulesResult : mlResult;
                return returnString;
            }
            if(compareListOfString(rulesResult, mlResult)){
                String returnString = (rulesResult.length() > mlResult.length()) ? rulesResult : mlResult;
                return returnString;
            }
            if(StringUtils.isNotBlank(mlComparison)) {
                boolean status = checkOtherPages(iterationNo, mlResult, nameComparisonArrayList);
                if (status)
                    return mlResult;
            }
            if(StringUtils.isNotBlank(rulesComparison)){
                boolean status = checkOtherPages(iterationNo, rulesResult, nameComparisonArrayList);
                if(status)
                    return rulesResult;
            }
            if(StringUtils.isNotBlank(mlComparison)) {
                return mlComparison;
            }
            if(StringUtils.isNotBlank(rulesComparison)){
                return rulesComparison;
            }
            else
                return "";
        }
        return "";
    }


    //--------------------both results are blank------------------------------------------------------------------------
    private String bothResultsAreBlank() {
        return "";
    }

    private String checkAgainstNPName(String patientName, String[] negationNames) {
        double thresold;
        double distance;
        boolean status = true;
        for(String npname : negationNames)
        {
            thresold = (patientName.length() + npname.length())*0.35/2;
            npname = npname.replaceAll("\\d", "");
            npname = npname.replaceAll("-", "");
            distance = compareUsingDistance(patientName, npname);
            if(distance < thresold){
                status = false;
                break;
            }
        }
        if(status){
            return patientName;
        }
        else
            return "";
    }

    private String rulesIsEmpty(String rulesResult, String mlResult) {
        return mlResult;
    }

    private String equalsScenario(String rulesResult, String mlResult) {
        return rulesResult;
    }

    private double compareUsingDistance(String rulesResult, String npname)
    {
        WeightedLevenshtein wl = new WeightedLevenshtein(
                new CharacterSubstitutionInterface() {
                    public double cost(char c1, char c2) {
                        return 1.0;
                    }
                });
        double distance = wl.distance(rulesResult, npname);
        return wl.distance(rulesResult, npname);
    }

    private boolean compareListOfString(String rulesResult, String mlResult){
        rulesResult = rulesResult.replaceAll("[^a-zA-Z0-9]", " ");
        mlResult = mlResult.replaceAll("[^a-zA-Z0-9]", " ");
        String[] rulesResultList = rulesResult.split(" ");
        String[] mlResultList = mlResult.split(" ");

        boolean status = false;
        for(String outerWord : rulesResultList){
            for(String innerWord : mlResultList){
                if(outerWord.equals(innerWord) && StringUtils.isNotBlank(outerWord) && StringUtils.isNotBlank(innerWord) && outerWord.length()>1 && innerWord.length()>1){
                    status = true;
                    break;
                }
            }
        }
        return status;
    }

    private boolean checkOtherPages(int iterationNo, String extractionResult, ArrayList<NameComparison> nameComparisonArrayList) {
        boolean status = false;
        for(int i=0; i<nameComparisonArrayList.size(); i++){
            if(i == iterationNo)
                continue;
            if(extractionResult.equals(nameComparisonArrayList.get(i).getMlPatientName())){
                status = true;
                break;
            }
        }
        return status;
    }
}
