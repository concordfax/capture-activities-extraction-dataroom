package com.auxolabs.cvision.pipeline.steps;

import com.auxolabs.cvision.models.nlp.PageModel;
import com.auxolabs.cvision.pipeline.ExtractionPipelineMessage;
import com.concordfax.capture.core.logging.ILogger;
import com.concordfax.capture.core.logging.LogLevels;
import com.concordfax.capture.core.models.extraction.DocumentTypeMapping;
import com.concordfax.capture.core.models.extraction.DocumentTypeMinModel;
import com.concordfax.capture.core.models.mlmodel.result.Classification;
import com.concordfax.capture.core.models.mlmodel.result.ClassificationResult;
import com.concordfax.capture.core.models.ocr.abbyy.Document;
import com.concordfax.capture.core.pipeline.AbstractMessage;
import com.concordfax.capture.core.pipeline.PipelineStep;
import edu.stanford.nlp.util.Timing;
import org.bytedeco.javacpp.lept;

import javax.inject.Inject;
import java.util.List;

/**
 * OCR Output Processor class reads Ocr Output Document, which was earlier fetched from db, and populates
 * page details into the List of PageModel class in CaptureDocument class for further use.
 */
public class OCROutputProcessorStep implements PipelineStep {

    private ILogger log;

    @Inject
    public OCROutputProcessorStep(ILogger iLogger) {
        this.log = iLogger;
    }

    @Override
    public AbstractMessage process(AbstractMessage message) {
        ExtractionPipelineMessage extractionPipelineMessage = (ExtractionPipelineMessage) message;
        try {
            Timing timing = new Timing();
            Document documentAbbyy = extractionPipelineMessage.getOcrOutputDocument();
            for (int i = 0; i < documentAbbyy.getPage().size(); i++) {
                extractionPipelineMessage.getPages().get(i).extractOCROutput(documentAbbyy.getPage().get(i), i);
            }
            // iterate over pages and assign appropriate profiling for each page based on the document class
            extractionPipelineMessage.getPages().forEach(pageModel -> {
                assignDocumentTypeMappingToPage(extractionPipelineMessage.getDocumentClassificationResult(),
                        extractionPipelineMessage.getExtractionProfile().getDocumentTypeMappings(), pageModel);
            });
            log.trace("ocr output processed", LogLevels.Verbose, extractionPipelineMessage.getLogMaker().getProperties());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return message;
    }

    private void assignDocumentTypeMappingToPage(ClassificationResult classificationResult, List<DocumentTypeMapping> documentTypeMappings, PageModel pageModel){
        Classification documentClassificationOutputForPage = classificationResult.getPredictions().get(pageModel.getPageNumber());
        for (DocumentTypeMapping documentTypeMapping : documentTypeMappings) {
            for (DocumentTypeMinModel documentType : documentTypeMapping.getDocumentTypes()) {
                if(documentType.getId().equals(documentClassificationOutputForPage.getCls())){
                    pageModel.setDocumentTypeMapping(documentTypeMapping);
                    return;
                }
            }
        }
    }
}
