package com.auxolabs.cvision.pipeline.utils.field.normalizers;

import com.auxolabs.cvision.constants.CaptureAppConstants.Field.Normalizer;

public class NormalizerFactory {

    private NameNormalizer nameNormalizer;
    private DatesNormalizer datesNormalizer;
    private SSNNormalizer ssnNormalizer;
    private MRNNormalizer mrnNormalizer;

    public NormalizerFactory(){
        nameNormalizer = new NameNormalizer();
        datesNormalizer = new DatesNormalizer();
        ssnNormalizer = new SSNNormalizer();
        mrnNormalizer = new MRNNormalizer();
    }

    public FieldNormalizer createNormalizer(String normalizerType) {
        switch (normalizerType) {
            case Normalizer.NAME:
                return nameNormalizer;
            case Normalizer.DATE:
                return datesNormalizer;
            case Normalizer.SSN:
                return ssnNormalizer;
            case Normalizer.MRN:
                return mrnNormalizer;
            default:
                return null;
        }
    }
}
