package com.auxolabs.cvision.pipeline.steps.helpers;

import com.auxolabs.capture.dateparser.DPEngineService;
import com.auxolabs.capture.dateparser.constants.DateParserUtilityConstants;
import com.auxolabs.capture.dateparser.models.TokenModel;
import com.auxolabs.capture.positional.constants.PositionalRuleConstants;
import com.auxolabs.cvision.models.nlp.PageModel;
import com.concordfax.capture.core.models.document.result.extraction.ExtractionField;

import javax.inject.Inject;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DateParserHelper {
    private DPEngineService dateParserService;

    @Inject
    public DateParserHelper(DPEngineService dpEngineService){
        this.dateParserService = dpEngineService;
    }


    public HashMap<String, ExtractionField> processPage(PageModel pageModel) {
        HashMap<String,ExtractionField> finalResult = new HashMap<>();
        HashMap<String, HashSet<TokenModel>> tagToAnnotation = getTagToAnnotations(pageModel);
        List<TokenModel> dateCandidates = new ArrayList<>();
        List<TokenModel> tagCandidates = new ArrayList<>();
        List<String> tagDob = new ArrayList<>();
        List<String> tagDoe = new ArrayList<>();
        List<String> tagDos = new ArrayList<>();
        List<String> tagOrderDate = new ArrayList<>();
        List<String> tagDod = new ArrayList<>();
        // TODO: 3/21/20 as of now direct only takes dates in numeric formats. add regex in db to have all aplphabetical format and have a new annotation
        tagToAnnotation.forEach((key,value)->{
            switch (key){
                case DateParserUtilityConstants.TagConstants.DIRECT:
                    dateCandidates.addAll(value);
                    break;
                case DateParserUtilityConstants.TagConstants.TAG_DOB:
                    tagCandidates.addAll(value);
                    value.forEach(tokenModel -> {
                        tagDob.add(tokenModel.get(PositionalRuleConstants.Properties.SPE_ANO).toString());
                    });
                    break;
                case DateParserUtilityConstants.TagConstants.TAG_DOE:
                    tagCandidates.addAll(value);
                    value.forEach(tokenModel -> {
                        tagDoe.add(tokenModel.get(PositionalRuleConstants.Properties.SPE_ANO).toString());
                    });
                    break;
                case DateParserUtilityConstants.TagConstants.TAG_DOS:
                    tagCandidates.addAll(value);
                    value.forEach(tokenModel -> {
                        //the case of "DO" is getting annotated under TAG_DOS removing that from here. need to optimize the solution
                        if(!tokenModel.get(PositionalRuleConstants.Properties.SPE_ANO).toString().equals("DO")){
                            tagDos.add(tokenModel.get(PositionalRuleConstants.Properties.SPE_ANO).toString());
                        }

                    });
                    break;
                case DateParserUtilityConstants.TagConstants.TAG_ORDER_DATE:
                    tagCandidates.addAll(value);
                    value.forEach(tokenModel -> {
                        tagOrderDate.add(tokenModel.get(PositionalRuleConstants.Properties.SPE_ANO).toString());
                    });
                    break;
                case DateParserUtilityConstants.TagConstants.TAG_DOD:
                    tagCandidates.addAll(value);
                    value.forEach(tokenModel -> {
                        tagDod.add(tokenModel.get(PositionalRuleConstants.Properties.SPE_ANO).toString());
                    });
                    break;
            }

        });
        //removing dates like 00/12/1994 or 12/00/1994 or 12/12/0000
        preprocessDateCandidates(dateCandidates);
        if(!dateCandidates.isEmpty() & !tagCandidates.isEmpty()){
            finalResult = dateParserService.process(dateCandidates,tagCandidates,tagDoe,tagDos,tagOrderDate,tagDod,tagDob);
        }
        return  finalResult;
    }

    private HashMap<String, HashSet<TokenModel>> getTagToAnnotations(PageModel pageModel){
        HashMap<String,HashSet<TokenModel>> tagToAnnotationForCandidates = new HashMap<>();
        pageModel.getBlockList().forEach(block->{
            block.getParagraphList().forEach(para->{
                para.getLines().forEach(line->{
                    line.getMappedSegments().forEach(mappedSegment->{
                        mappedSegment.getAnnotatorNameToAnnotations().values().forEach(sentenceModel -> {
                            sentenceModel.getAnnotations().forEach(annotation -> {
                                TokenModel tokenModel = new TokenModel();
                                tokenModel.put(PositionalRuleConstants.Properties.X, mappedSegment.x());
                                tokenModel.put(PositionalRuleConstants.Properties.Y, mappedSegment.y());
                                tokenModel.put(PositionalRuleConstants.Properties.W, mappedSegment.w());
                                tokenModel.put(PositionalRuleConstants.Properties.H, mappedSegment.h());
                                tokenModel.put(PositionalRuleConstants.Properties.TEXT, mappedSegment.getText());
                                tokenModel.put(PositionalRuleConstants.Properties.ID, mappedSegment.hashCode());
                                tokenModel.put(PositionalRuleConstants.Properties.SPE_ANO, annotation.getWord());
                                tokenModel.setTokenName(mappedSegment.getText());

                                if(!tagToAnnotationForCandidates.containsKey(annotation.getAnswer())) {
                                    HashSet<TokenModel> tokenModels = new HashSet<>();
                                    tokenModels.add(tokenModel);
                                    tagToAnnotationForCandidates.put(annotation.getAnswer(),tokenModels);
                                }
                                else{
                                    tagToAnnotationForCandidates.get(annotation.getAnswer()).add(tokenModel);
                                }

                            });
                        });
                    });
                });
            });
        });
        HashMap<String, HashSet<TokenModel>> tempTokens = new HashMap<>();
        tagToAnnotationForCandidates.forEach((s, tokenModels) -> {
            tempTokens.put(s.toLowerCase().trim().replaceAll("[^\\w0-9]","_"), tokenModels);
        });
        return tempTokens;
    }

    private void preprocessDateCandidates(List<TokenModel> dateCandidates) {
        String invalidDateRegex1 = "((0{1,2})[/ ,.-]([0-3]?[0-9])[/ ,.-](\\d{4}|\\d{2}))|(([0-3]?[0-9])[/ ,.-](0{1,2})[/ ,.-](\\d{4}|\\d{2}))|(([0-3]?[0-9])[/ ,.-]([0-3]?[0-9])[/ ,.-](0{1,4}))";
        String invalidDateRegex2 ="(\\d{1,2}(\\s+[/,-.]|[/,-.]|[/,-.]\\s+|\\s+)\\d{1,2}(\\s+[/,-.]|[/,-.]|[/,-.]\\s+|\\s+)(\\d{1}|\\d{3}))\\s+\\d*$";
        Pattern invalidDatePattern1  = Pattern.compile(invalidDateRegex1);
        Pattern invalidDatePattern2 = Pattern.compile(invalidDateRegex2);
        ListIterator<TokenModel> iterator = dateCandidates.listIterator();
        while (iterator.hasNext()){
            String date = (String)iterator.next().get("speAno");
            Matcher matcher1 = invalidDatePattern1.matcher(date);
            Matcher matcher2 = invalidDatePattern2.matcher(date);
            if(matcher1.matches()){
                iterator.remove();
            }
            else if(matcher2.matches()){
                iterator.remove();
            }
        }
    }


}
