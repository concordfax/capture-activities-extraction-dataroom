package com.auxolabs.cvision.pipeline.utils;

import com.auxolabs.cvision.models.cv.PBText2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class SegmentMerger {

    private ArrayList<PBText2> mergeSegments(ArrayList<PBText2> abbySegments) {
        ArrayList<PBText2> segmentscopy = new ArrayList<PBText2>(abbySegments);
        ArrayList<PBText2> new_list = new ArrayList<PBText2>();

        int x = 0;
        while (x < segmentscopy.size()) {


            if (x == segmentscopy.size() - 1) {

                new_list.add(segmentscopy.get(x));
                break;

            } else if ((segmentscopy.get(x + 1).getX() - (segmentscopy.get(x).getX() + segmentscopy.get(x).getW())) >= 0
                    && (segmentscopy.get(x + 1).getX() - (segmentscopy.get(x).getX() + segmentscopy.get(x).getW())) <= 300) {
                PBText2 new_item = new PBText2();
                new_item.setX(segmentscopy.get(x).getX());
                new_item.setY(segmentscopy.get(x).getY());
                new_item.setW(segmentscopy.get(x).getW() + segmentscopy.get(x + 1).getW());
                new_item.setH(segmentscopy.get(x).getH());
                new_item.setText((segmentscopy.get(x).isWordFirst() ? " " : "") + segmentscopy.get(x).getText()
                        + (segmentscopy.get(x + 1).isWordFirst() ? " " : "") + segmentscopy.get(x + 1).getText());
                new_list.add(new_item);

                x += 2;
            } else {
                new_list.add(segmentscopy.get(x));
                x += 1;
            }


        }
        return new_list;
    }

    private ArrayList<PBText2> iterateSegments(ArrayList<PBText2> new_list) {
        ArrayList<PBText2> iterated = new ArrayList<PBText2>();
        for (int iterator = 0; iterator < 5; iterator++) {
            iterated = mergeSegments(new_list);
        }
        return iterated;
    }

    private ArrayList<PBText2> sortIterated(ArrayList<PBText2> iterated) {
        int x = 0;
        while (x < iterated.size()) {
            Collections.sort(iterated, Comparator.comparingInt(PBText2::getY));
            x += 1;
        }
        return iterated;

    }

    private ArrayList<PBText2> refineSegments(ArrayList<PBText2> sorted) {
        int current = 0;
        ArrayList<PBText2> new_list = new ArrayList<PBText2>();

        while (current < sorted.size()) {
            int next = current + 1;

            while (next < sorted.size()) {


                if (sorted.get(current).getX() < sorted.get(next).getX() &&
                        sorted.get(current).getY() < sorted.get(next).getY() &&
                        sorted.get(current).getX() + sorted.get(current).getW() > sorted.get(next).getX() + sorted.get(next).getW() &&
                        sorted.get(current).getY() + sorted.get(current).getH() > sorted.get(next).getY() + sorted.get(next).getH()) {

                    next += 1;


                } else {
                    break;
                }
            }
            new_list.add(sorted.get(current));
            current = next;


        }

        return new_list;

    }

    public String textExtraction(List<PBText2> new_list) {
        String text;
        ArrayList<String> finalout = new ArrayList<String>();
        String extracted = null;
        for (int index = 0; index < new_list.size(); index++) {
            text = new_list.get(index).getText();
            finalout.add(text);
            extracted = String.join(" ", finalout);

        }
        return extracted;
    }


}