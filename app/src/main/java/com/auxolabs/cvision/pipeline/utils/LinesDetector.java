package com.auxolabs.cvision.pipeline.utils;

import com.auxolabs.cvision.models.cv.LDObject;
import com.auxolabs.cvision.models.cv.PIBOXHolder;
import com.auxolabs.cvision.utils.LeptonicaUtils;
import org.bytedeco.javacpp.Loader;
import org.bytedeco.javacpp.lept;

import static org.bytedeco.javacpp.lept.*;

public class LinesDetector {

    public LinesDetector(){
        Loader.load(lept.class);
    }

    public LDObject detectLinesInImage(PIBOXHolder pixb, PIBOXHolder pixs) {
        lept.PIX pix2, pix3, pix4, pix5, pix6, linesMap, linesMap32bpp;

        /* Open out the vertical lines */
        pix2 = pixMorphSequence(pixb.getPix(), "o1.50", 0);


        /* Seedfill back to get those lines in their entirety */
        pix3 = pixSeedfillBinary(null, pix2, pixb.getPix(), 8);

        linesMap = pixCopy(null, pix3);

        LeptonicaUtils.getInstance().safeDeletePIX(pix2);

        /* Remove the vertical lines (and some of the images) */
        pix2 = pixXor(null, pixb.getPix(), pix3);
        LeptonicaUtils.getInstance().safeDeletePIX(pix3);

        /* Open out the horizontal lines */
        pix4 = pixMorphSequence(pix2, "o50.1", 0);

        pix6 = pixOr(null, linesMap, pix4);
        LeptonicaUtils.getInstance().safeDeletePIX(linesMap);

        /* Seedfill back to get those lines in their entirety */
        pix5 = pixSeedfillBinary(null, pix4, pix2, 8);

        LeptonicaUtils.getInstance().safeDeletePIX(pix4);

        /* Remove the horizontal lines */
        pix4 = pixXor(null, pix2, pix5);
        LeptonicaUtils.getInstance().safeDeletePIX(pix2);
        LeptonicaUtils.getInstance().safeDeletePIX(pix5);

        linesMap32bpp = pixConvertTo32(pix6);
        LeptonicaUtils.getInstance().safeDeletePIX(pix6);

        pixRasteropFullImage(linesMap32bpp, pixs.getPix(), PIX_SRC | PIX_DST);

        PIBOXHolder linesInImage = new PIBOXHolder();
        linesInImage.setPix(linesMap32bpp);

        PIBOXHolder imgWithoutLines = new PIBOXHolder();
        imgWithoutLines.setPix(pix4);

        LDObject ldObject = new LDObject();
        ldObject.setLinesInImage(linesInImage);
        ldObject.setImgWithoutLines(imgWithoutLines);


        return ldObject;
    }

}
