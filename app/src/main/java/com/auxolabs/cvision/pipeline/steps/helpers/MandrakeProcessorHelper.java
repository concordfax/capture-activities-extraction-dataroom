package com.auxolabs.cvision.pipeline.steps.helpers;

import com.auxolabs.capture.mandrake.MandrakeService;
import com.auxolabs.capture.mandrake.models.NLPResultModel;
import com.auxolabs.capture.mandrake.models.RuleParserModel;
import com.auxolabs.capture.mandrake.models.RuleResultModel;
import com.auxolabs.cvision.models.mandrake.MandrakeSentenceModel;
import com.auxolabs.cvision.models.nlp.PageModel;
import com.concordfax.capture.core.models.document.result.extraction.ExtractionField;
import com.concordfax.capture.core.models.document.result.extraction.SimpleExtractionField;
import com.concordfax.capture.core.models.extraction.mandrake.MandrakeExtractorConfiguration;
import com.concordfax.capture.core.models.extraction.mandrake.output.MandrakeExtractorRuleModel;
import com.concordfax.capture.core.models.extraction.mandrake.output.MandrakeFieldModel;
import com.kaiser.models.ParserValue;
import com.kaiser.models.ResultAnnotation;
import com.kaiser.models.ValueModel;

import javax.inject.Inject;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class MandrakeProcessorHelper {
    private MandrakeService mandrakeService;
    private AtomicInteger sentenceId = new AtomicInteger(0);


    @Inject
    public MandrakeProcessorHelper(MandrakeService mandrakeService) {
        this.mandrakeService = mandrakeService;

    }

    private static HashMap<String, ExtractionField> stringResultToExtractionField(HashMap<String, String> stringresult, MandrakeExtractorRuleModel mandrakeExtractorRuleModel) {
        HashMap<String, ExtractionField> finalout = new HashMap<>();

        stringresult.forEach((key, value) -> {
            mandrakeExtractorRuleModel.getMandrake().forEach(mandrakeFieldModel -> {
                if (key.equals(mandrakeFieldModel.getKey())) {
                    if (value != null) {
                        finalout.put(key, new SimpleExtractionField(key, 0.75f, value, ""));
                    }
                }
            });

        });
        return finalout;
    }

    // TODO: 7/18/18 change the ocrText into old format after research on performance of blocks
//    public HashMap<String, ExtractionField> extractIntegrated(Page page, PageModel pageModel, MandrakeExtractorConfiguration mandrakeExtractorConfiguration, ExtractionAnnotatorConfiguration annotatorConfigurations) throws IOException {
    public HashMap<String, ExtractionField> extractIntegrated(PageModel pageModel, MandrakeExtractorConfiguration mandrakeExtractorConfiguration) throws IOException {
        List<MandrakeSentenceModel> mandrakeSentenceModelList = new ArrayList<>();
        PositionalHelper.temp.add("****************************************************************************");
        int pageNumber = Integer.valueOf(pageModel.getPageNumber()) + 1;
        pageModel.getBlockList().forEach(block->{
            MandrakeSentenceModel mandrakeSentenceModel = new MandrakeSentenceModel();
            mandrakeSentenceModel.setSentenceId(sentenceId.getAndIncrement());
            mandrakeSentenceModel.setSentence(block.getValue());
            mandrakeSentenceModel.setAnnotations(new ArrayList<>());
            block.getBlockAnnotations().forEach((s, annotations) -> mandrakeSentenceModel.getAnnotations().addAll(annotations));
            mandrakeSentenceModel.setAnnotationsMap(block.getBlockAnnotations());
            mandrakeSentenceModelList.add(mandrakeSentenceModel);
        });

        PositionalHelper.temp.add("PAGE NUMBER:" + pageNumber);
        PositionalHelper.temp.add("OCR Sentence:\n");
        mandrakeSentenceModelList.forEach(ocrSentence->{
            PositionalHelper.temp.add(ocrSentence.getSentence().trim());
        });
        PositionalHelper.temp.add("****************************************************************************\n");

        createParserInput(mandrakeSentenceModelList);
        NLPResultModel nlpResultModel = getInitialResult(mandrakeSentenceModelList);

        mandrakeSentenceModelList.forEach(sentenceModel -> {
            List<RuleParserModel> parserModels = mandrakeService.process(mandrakeExtractorConfiguration.getTrigger(), sentenceModel.getSentencePV());
            parserModels.forEach(ruleParserModel -> {
                nlpResultModel.addRuleResults(ruleParserModel.getResults());
                for(int index=0; index <ruleParserModel.getResults().size(); index++ ){
                    System.out.println("--------------------------------------------------------------------------------");
                    PositionalHelper.temp.add("--------------------------------------------------------------------------------");
                    System.out.println("Mandrake rule triggered:" + ruleParserModel.getResults().get(index).getRuleName());
                    PositionalHelper.temp.add("Mandrake rule triggered:" + ruleParserModel.getResults().get(index).getRuleName());
                    for(ResultAnnotation ra:ruleParserModel.getResults().get(index).getAnnotations()){
                        System.out.println("Mandrake annotated text:" +ra.getText());
                        System.out.println("Mandrake annotated tag:" +ra.getTag());
                        PositionalHelper.temp.add("Mandrake annotated text:" +ra.getText());
                        PositionalHelper.temp.add("Mandrake annotated tag:" +ra.getTag());
                    }
                    System.out.println("--------------------------------------------------------------------------------");
                    PositionalHelper.temp.add("--------------------------------------------------------------------------------");

                }
            });
        });
        return extractFieldsFromResult(nlpResultModel, mandrakeExtractorConfiguration);

    }

    private void createParserInput(List<MandrakeSentenceModel> sentenceModels){
        for (MandrakeSentenceModel sentenceModel : sentenceModels) {
            ParserValue parserValue = new ParserValue();
            addLexerAnnotations(parserValue, sentenceModel.getAnnotationsMap().get("lexer"), sentenceModel.getSentenceId());
            addPOSAnnotations(parserValue, sentenceModel.getAnnotationsMap().get("pos"), sentenceModel.getSentenceId());
            sentenceModel.setSentencePV(parserValue);
        }
    }

    private void addLexerAnnotations(ParserValue parserValue, List<com.auxolabs.capture.annotator.models.Annotation> tempAnnotations, int sentenceId) {
        tempAnnotations.forEach(annotation -> {
            String tagType = annotation.getAnswer();
            ResultAnnotation resultAnnotation = new ResultAnnotation(sentenceId, annotation.getWord(),
                    annotation.getTokenPosition().getStartPosition(), annotation.getTokenPosition().getEndPosition(), tagType, null);
            ValueModel model = new ValueModel(annotation.getWord(), resultAnnotation);
            String displayText = annotation.getWord();
            model.setDisplayText(displayText);
            model.setIsNegated(false);
            model.setStartIndex(annotation.getStartTokenIndex());
            model.setEndIndex(annotation.getEndTokenIndex());
            model.setSentenceIndex(sentenceId);
            updateKeyPhraseListInParserValue(parserValue, model, tagType);
        });
    }

    private void updateKeyPhraseListInParserValue(ParserValue parserValue, ValueModel keyPhrase, String component) {
        Object value = parserValue.get(component);
        List<ValueModel> keyPhrases;
        if (value == null || !(value instanceof ArrayList)) {
            keyPhrases = new ArrayList<>();
        } else {
            keyPhrases = (List<ValueModel>) value;
        }
        keyPhrases.add(keyPhrase);
        parserValue.put(component, keyPhrases);
    }

    private void addPOSAnnotations(ParserValue parserValue, List<com.auxolabs.capture.annotator.models.Annotation> annotations,
                                   int sentenceId) {
        annotations.forEach(annotation -> {
            String tagType = annotation.getAnswer();
            ResultAnnotation resultAnnotation = new ResultAnnotation(sentenceId, annotation.getWord(), annotation.getTokenPosition().getStartPosition(),
                    annotation.getTokenPosition().getEndPosition(), "POS TAG::" + tagType, new HashMap<>());
            ValueModel value = new ValueModel(annotation.getWord(), resultAnnotation);
            Map<String, Object> properties = new HashMap<>();
            properties.put("POS", tagType);
            value.setProperties(properties);
            value.setStartIndex(annotation.getStartTokenIndex());
            value.setEndIndex(annotation.getEndTokenIndex());
            value.setSentenceIndex(sentenceId);
            parserValue.put("POS TAG" + annotation.getWord() + annotation.getStartTokenIndex(), value);

        });
    }

    private NLPResultModel getInitialResult(List<MandrakeSentenceModel> sentences) {
        NLPResultModel result = new NLPResultModel();
        sentences.forEach(sentenceModel -> {
            result.addSentence(sentenceModel.getSentence());
            sentenceModel.getSentencePV().forEach((k, v) -> {
                if(!k.startsWith("POS TAG")) {
                    if (v instanceof ValueModel) {
                        result.addAnnotations(((ValueModel) v).getTriggerPhrases());
                    } else if (v instanceof ArrayList) {
                        List<ValueModel> keyPhrases = (ArrayList) v;
                        keyPhrases.forEach(keyPhraseModel -> {
                            result.addAnnotations(keyPhraseModel.getTriggerPhrases());
                        });
                    }
                }
            });
        });
        return result;
    }

    /**
     * This method is the abstract of overall process of MandrakeProcessorHelper class.
     * This method producing the final output in the form of rules along with corresponding values.
     *
     * @param mandrakeExtractorRuleModel is the configuration file.(mandrake-config.yml)
     * @param nlpResultModel             is the response model from the mandrake.
     * @return mandrakeExtraction This is the final output in the form of rules along with corresponding values.
     */
    private HashMap<String, ExtractionField> extractFieldsFromResult(NLPResultModel nlpResultModel, MandrakeExtractorConfiguration mandrakeExtractorRuleModel) {
        HashMap<String, String> mandrakeExtraction = new HashMap<>();
        mandrakeExtractorRuleModel.getOutputConfiguration().getMandrake().forEach(mandrakeFieldModel -> {
            mandrakeExtraction.put(mandrakeFieldModel.getKey(), extractField(nlpResultModel, mandrakeFieldModel));
        });
        return stringResultToExtractionField(mandrakeExtraction, mandrakeExtractorRuleModel.getOutputConfiguration());
    }

    /**
     * This method involves overall processes of extraction of final values as string. It includes filtering
     * of rules, indices assign operation, sorting operation and also validation of each values.
     *
     * @param nlpResultModel     The response model from mandrake
     * @param mandrakeFieldModel Each field model in the configuration file.
     * @return This return the final string output after all the processes. This can be passed to the mandrake
     * extraction output Hashmap as values to the corresponding rules.
     */

    private String extractField(NLPResultModel nlpResultModel, MandrakeFieldModel mandrakeFieldModel) {
        List<RuleResultModel> ruleResultModels = nlpResultModel.getRuleResults();
        List<RuleResultModel> extractorRules = new ArrayList<>();
        ruleResultModels.forEach(ruleResultModel -> {

            if (ruleResultModel.getRuleName().equals(mandrakeFieldModel.getRule())) {
                extractorRules.add(ruleResultModel);
            }


        });


        assignIndicesForRuleResults(extractorRules, mandrakeFieldModel.getLexicons().getValues(),
                mandrakeFieldModel.getLexicons().getTags().toArray(new String[mandrakeFieldModel.getLexicons().getTags().size()]));
        sortRuleResults(extractorRules);

        if (!extractorRules.isEmpty()) {
            RuleResultModel ruleResultModel = extractorRules.get(0);// Since the extractorRules are sorted. the first model of the list contins all the values.

            List<ResultAnnotation> annotationsInLine = nlpResultModel.getAnnotations().stream().filter(resultAnnotation ->
                    resultAnnotation.getSentenceIndex() == ruleResultModel.getSentIndex()).collect(Collectors.toList());
            List<ResultAnnotation> annotationsInRule = annotationsInLine.stream().filter(resultAnnotation ->
                    resultAnnotation.getStartIndex() >= ruleResultModel.getValueStartIndex() &&
                            (resultAnnotation.getEndIndex() <= ruleResultModel.getValueEndIndex()
                                    || Math.abs(resultAnnotation.getEndIndex() - ruleResultModel.getValueEndIndex()) <= 2))// second case added for rogue last letters like 's' which may not be part of POS
                    .collect(Collectors.toList());

            StringBuilder extractionValue = new StringBuilder("");
            HashSet<String> dupList = new HashSet<>();
            List<ResultAnnotation> sortedAnnotations = ruleResultModel.getAnnotations().stream().sorted(Comparator.comparingInt(ResultAnnotation::getStartIndex)).collect(Collectors.toList());

            for (ResultAnnotation resultAnnotation : sortedAnnotations) {

                if (mandrakeFieldModel.getLexicons().getSelectedValues().contains(resultAnnotation.getTag())
                        && !checkIfAnnotationAlsoHasAEndValueTag(mandrakeFieldModel.getLexicons().getEndValues(), annotationsInRule, resultAnnotation)) {
                    if (!dupList.contains(resultAnnotation.getText().trim())) {
                        dupList.add(resultAnnotation.getText().trim());
                        extractionValue.append(resultAnnotation.getText());
                        extractionValue.append(" ");
                    }
                }
            }
            return extractionValue.toString();
        }
        return null;
    }

    /**
     * This method is to find whether the current tag is present or not in the filed of endvalues in configuration file.
     * If it is present we have to neglect those values from the final output.
     *
     * @param endValues         The endvalue field in the configuration file.
     * @param annotationsInRule The annotation that present in the triggered rule.
     * @param currentAnnotation The annotation which we want to check.
     * @return This returns boolean whether the tag present or not.(True or False)
     */

    private boolean checkIfAnnotationAlsoHasAEndValueTag(List<String> endValues, List<ResultAnnotation> annotationsInRule, ResultAnnotation currentAnnotation) {
        if (endValues != null) {
            for (ResultAnnotation resultAnnotation : annotationsInRule) {
                if (resultAnnotation.getStartIndex() == currentAnnotation.getStartIndex()
                        && (resultAnnotation.getEndIndex() == currentAnnotation.getEndIndex()
                        || resultAnnotation.getEndIndex() - currentAnnotation.getEndIndex() <= 2) // cases to adjust POS for "'s"
                        && endValues.contains(resultAnnotation.getTag())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * This method is to set the start index and end index for values and Tags in in ruleResultModel respectively.
     * The values are set in ruleResultModel according to the values that obtained from the annotationlist.
     *
     * @param ruleResultModels The response model from the mandrake
     * @param possibleValues   The values that present in the field of Values in the configuration file.
     * @param tags             The tags that present in the field of Tags in the configuration file.
     */

    private void assignIndicesForRuleResults(List<RuleResultModel> ruleResultModels, List<String> possibleValues, String... tags) {
        ruleResultModels.forEach(ruleResultModel -> {
            ruleResultModel.setValueStartIndex(-1);
            ruleResultModel.setTagStartIndex(-1);
            List<ResultAnnotation> annotationList = new ArrayList<>(ruleResultModel.getAnnotations());
            annotationList.sort(Comparator.comparing(ResultAnnotation::getStartIndex));
            for (ResultAnnotation resultAnnotation : annotationList) {
                ruleResultModel.setSentIndex(resultAnnotation.getSentenceIndex());
                if (resultAnnotation.getText() != null && !resultAnnotation.getText().isEmpty()) {
                    if (possibleValues.contains(resultAnnotation.getTag())) {
                        if (ruleResultModel.getValueStartIndex() == -1)
                            ruleResultModel.setValueStartIndex(resultAnnotation.getStartIndex());
                        ruleResultModel.setValueEndIndex(resultAnnotation.getEndIndex());
                    } else if (checkIfResultAnnotationTagInTags(tags, resultAnnotation.getTag()) && ruleResultModel.getTagStartIndex() == -1) {
                        ruleResultModel.setTagStartIndex(resultAnnotation.getStartIndex());
                        ruleResultModel.setTagEndIndex(resultAnnotation.getEndIndex());

                    }
                }
            }
            ruleResultModel.getAnnotations().clear();
            ruleResultModel.getAnnotations().addAll(annotationList);
        });
    }

    /**
     * This method is to check whether the tags in the annotaion list is present in the tags field of configuration file.
     *
     * @param tags        This is the tags obtained from the annotaion list
     * @param expectedTag This is the tags that present in the configuration file
     * @return isFound This is a Boolean equal to True or False.
     */

    private Boolean checkIfResultAnnotationTagInTags(String[] tags, String expectedTag) {
        boolean isFound = false;
        for (String tag : tags) {
            if (tag.equalsIgnoreCase(expectedTag)) {
                isFound = true;
                break;
            }
        }
        return isFound;
    }

    /**
     * This method is to sort the rule results based on the start index values of sentence index.
     *
     * @param ruleResultModels The response model from mandrake.
     */
    private void sortRuleResults(List<RuleResultModel> ruleResultModels) {
        ruleResultModels.sort((o1, o2) -> {
            if (o1.getSentIndex() == o2.getSentIndex()) {
                if (o1.getTagStartIndex() == o2.getTagStartIndex()) {
                    if (o1.getValueStartIndex() == o2.getValueStartIndex()) {
                        return Integer.compare(o2.getValueEndIndex(), o1.getValueEndIndex());
                    } else if (o1.getValueStartIndex() > o2.getValueStartIndex()) {
                        return 1;
                    } else {
                        return -1;
                    }
                } else if (o1.getTagStartIndex() > o2.getTagStartIndex())
                    return 1;
                else
                    return -1;
            } else if (o1.getSentIndex() > o2.getSentIndex()) {
                return 1;
            } else {
                return -1;
            }

        });


    }

    public String preprocess(String s) {
        s = preprocessPossibleSSN(s);
        s = preprocessPossibleDOB(s);
        s = preprocessPossibleDates(s);
        s = preprocessErroneousTexts(s);
        s = s.replaceAll("[^\\\\a-zA-Z0-9/.\\s-#@,']", " ");
        s = s.replaceAll("(\\s)+", " ");
        return s;
    }

    private String preprocessErroneousTexts(String text) {
        String textRegex = "Enc.(\\s)+Date";
        text = processRegex(text, textRegex, textRegex, "Enc Date");
        return text;
    }

    private String preprocessPossibleSSN(String text) {
        String ssnRegex = "( ([\\dXx*#]{3}\\s*)([\\.\\-\\,])(\\s*[\\dXx*#]{2}\\s*)([\\.\\-\\,])(\\s*\\d{4}) )|[\\s:]([*]{9}) ";
        text = processRegex(text, ssnRegex, "(\\s)*[\\-,\\\\.](\\s)*", "-");
        text = processRegex(text, ssnRegex, "[:]", ": ");
        text = processRegex(text, ssnRegex, "[*]", "X");
        text = processRegex(text, ssnRegex, "[#]", "X");
        return text;
    }

    private String preprocessPossibleDOB(String text) {
        String dobRegex = " (\\d{1,2}\\s*)([\\.\\-\\,/])(\\s*\\d{1,2}\\s*)([\\.\\-\\,/])(\\s*\\d{4}) ";
        text = processRegex(text, dobRegex, "(\\s)*[\\-,\\/\\.](\\s)*", "/");
        return text;
    }

    private String preprocessPossibleDates(String text) {
        String dobRegex = " (((\\d{1,2}\\s*)([\\.\\,/]))?(\\s*\\d{1,2}\\s*)([\\.\\,/]))?(\\s*\\d{4})-";
        text = processRegex(text, dobRegex, "(\\s)*[\\-](\\s)*", " - ");
        return text;
    }

    private String processRegex(String s, String regex, String originalPattern, String replaceText) {
        StringBuilder formattedText = new StringBuilder("");
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(s);
        int currentEnd = 0;
        while (matcher.find()) {
            String matchedText = s.substring(matcher.start(), matcher.end());
            matchedText = matchedText.replaceAll(originalPattern, replaceText);
            matchedText = matchedText.toUpperCase();
            if (matcher.end() < s.length()) {
                formattedText.append(s.substring(currentEnd, matcher.start()));
                formattedText.append(matchedText);
                formattedText.append(" ");
                currentEnd = matcher.end();
            }
        }
        if (currentEnd < s.length()) {
            formattedText.append(s.substring(currentEnd, s.length()));
        }
        return formattedText.toString();
    }
}
