package com.auxolabs.cvision.pipeline.steps;

import com.auxolabs.capture.annotator.AnnotatorService;
import com.auxolabs.capture.mandrake.MandrakeService;
import com.auxolabs.cvision.models.nlp.PageModel;
import com.auxolabs.cvision.pipeline.ExtractionPipelineMessage;
import com.auxolabs.cvision.pipeline.steps.helpers.MandrakeProcessorHelper;
import com.auxolabs.cvision.pipeline.steps.helpers.PositionalHelper;
import com.concordfax.capture.core.logging.ILogger;
import com.concordfax.capture.core.logging.LogLevels;
import com.concordfax.capture.core.models.document.result.extraction.ExtractionField;
import com.concordfax.capture.core.models.document.result.extraction.SimpleExtractionField;
import com.concordfax.capture.core.models.extraction.DocumentTypeMapping;
import com.concordfax.capture.core.models.extraction.mandrake.MandrakeExtractorConfiguration;
import com.concordfax.capture.core.models.extraction.mandrake.trigger.MandrakeTriggerConfiguration;
import com.concordfax.capture.core.models.extraction.output.ExtractorType;
import com.concordfax.capture.core.pipeline.AbstractMessage;
import com.concordfax.capture.core.pipeline.PipelineStep;
import edu.stanford.nlp.util.Timing;

import javax.inject.Inject;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;

public class MandrakeExtractorStep implements PipelineStep {

    private ILogger log;
    private MandrakeProcessorHelper mandrakeProcessorHelper;

    @Inject
    public MandrakeExtractorStep(ILogger log, MandrakeService mandrakeService) {
        this.log = log;
        this.mandrakeProcessorHelper = new MandrakeProcessorHelper(mandrakeService);

    }

    @Override
    public AbstractMessage process(AbstractMessage message) {
        Timing timing = new Timing();
        ExtractionPipelineMessage extractionPipelineMessage = (ExtractionPipelineMessage) message;
        try {
            MandrakeExtractorConfiguration extractorConfiguration = null;
            for (int i = 0; i < extractionPipelineMessage.getExtractionProfile().getExtractors().size(); i++) {
                if (extractionPipelineMessage.getExtractionProfile().getExtractors().get(i).getExtractorType().equals(ExtractorType.Mandrake)) {
                    extractorConfiguration = (MandrakeExtractorConfiguration) extractionPipelineMessage.getExtractionProfile().getExtractors().get(i);
                    break;
                }
            }
            for (int i = 0; i < extractionPipelineMessage.getPages().size(); i++) {
                PageModel pageModel = extractionPipelineMessage.getPages().get(i);
                HashMap<String, ExtractionField> mandrakeExtraction = this.mandrakeProcessorHelper.extractIntegrated (pageModel, extractorConfiguration);
                pageModel.setExtractorToRuleField(new HashMap<>());
                pageModel.getExtractorToRuleField().put(ExtractorType.Mandrake, mandrakeExtraction);
                int pageNumber = Integer.valueOf(pageModel.getPageNumber() + 1);
                if(mandrakeExtraction.size() > 0) {
                    System.out.println("--------------------------------------------------------------------------------");
                    PositionalHelper.temp.add("--------------------------------------------------------------------------------");
                    System.out.println("Mandrake Extraction results for page:" + pageNumber);
                    mandrakeExtraction.forEach((key, value) -> {
                        SimpleExtractionField s = (SimpleExtractionField)value;
                        System.out.println("Mandrake rule : " + key + "\nResult : " + s.getValue());
                    });
                    System.out.println("--------------------------------------------------------------------------------");
                    PositionalHelper.temp.add("--------------------------------------------------------------------------------");
                }
            }
            log.trace(String.format("mandrake processing completed in %d ms", timing.report()), LogLevels.Verbose, extractionPipelineMessage.getLogMaker().getProperties());
        } catch (Exception e) {
            // TODO: 21/05/18 LOGGER replace the log prop manufacturer code here
            throw new RuntimeException(e);
        }
        return message;
    }

    // TODO: 2019-04-08 use this to optimize the number of rule conditions evaluated for each page
    private MandrakeExtractorConfiguration shallowCopyForPage(MandrakeExtractorConfiguration extractorConfiguration, DocumentTypeMapping documentTypeMapping){
        MandrakeExtractorConfiguration tempExtractorConfig = new MandrakeExtractorConfiguration();
        tempExtractorConfig.setOutputConfiguration(extractorConfiguration.getOutputConfiguration());
        tempExtractorConfig.setTrigger(new MandrakeTriggerConfiguration());
        tempExtractorConfig.getTrigger().setRules(new ArrayList<>());
        if(documentTypeMapping.getExtractors().containsKey(ExtractorType.Mandrake.toString())) {
            tempExtractorConfig.getTrigger().getRules().addAll(extractorConfiguration.getTrigger().getRules());
            tempExtractorConfig.getTrigger().getRules().removeIf(ruleModel -> documentTypeMapping.getExtractors().get(ExtractorType.Mandrake.toString()).contains(ruleModel.getRuleId()));
        }
        tempExtractorConfig.getTrigger().setTriggerId(extractorConfiguration.getTrigger().getTriggerId());
        tempExtractorConfig.getTrigger().setTriggerName(extractorConfiguration.getTrigger().getTriggerName());
        return tempExtractorConfig;
    }
}
