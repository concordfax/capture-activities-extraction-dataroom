package com.auxolabs.cvision.pipeline.steps.helpers;

import com.auxolabs.capture.annotator.models.Annotation;
import com.auxolabs.capture.annotator.models.response.SentenceModel;
import com.auxolabs.capture.positional.constants.PositionalRuleConstants;
import com.auxolabs.capture.positional.models.TokenModel;
import com.auxolabs.cvision.models.nlp.PageModel;
import com.auxolabs.cvision.models.ocr.SegBlock;
import com.concordfax.capture.core.models.extraction.annotators.AnnotatorModifier;
import com.concordfax.capture.core.models.extraction.annotators.ExtractionAnnotatorConfiguration;
import com.concordfax.capture.core.models.extraction.annotators.SpecialProps;
import com.concordfax.capture.core.models.extraction.annotators.SpecialPropsFilter;

import java.util.*;
import java.util.stream.Collectors;

public class PositionalMappingHelper {
    private HashMap<String,HashSet<TokenModel>> resultAnnotations = new HashMap<>();

    public HashMap<String,HashSet<TokenModel>> getTagToAnnotations (PageModel pageModel,ExtractionAnnotatorConfiguration annotatorConfiguration) {
        pageModel.getBlockList().forEach(block->{
            block.getParagraphList().forEach(para->{
                para.getLines().forEach(line->{
                    line.getMappedSegments().forEach(mappedSegment->{
                        mappedSegment.getAnnotatorNameToAnnotations().values().forEach(sentenceModel -> {
                            sentenceModel.getAnnotations().forEach(annotation -> {
                                TokenModel tokenModel = new TokenModel();
                                tokenModel.put(PositionalRuleConstants.Properties.X, mappedSegment.x());
                                tokenModel.put(PositionalRuleConstants.Properties.Y, mappedSegment.y());
                                tokenModel.put(PositionalRuleConstants.Properties.W, mappedSegment.w());
                                tokenModel.put(PositionalRuleConstants.Properties.H, mappedSegment.h());
                                tokenModel.put(PositionalRuleConstants.Properties.TEXT, mappedSegment.getText());
                                tokenModel.put(PositionalRuleConstants.Properties.ID, mappedSegment.hashCode());
                                tokenModel.put(PositionalRuleConstants.Properties.SPE_ANO, annotation.getWord());
                                tokenModel.setTokenName(mappedSegment.getText());
                                if(!resultAnnotations.containsKey(annotation.getAnswer())) {
                                    HashSet<TokenModel> tokenModels = new HashSet<>();
                                    tokenModels.add(tokenModel);
                                    resultAnnotations.put(annotation.getAnswer(),tokenModels);
                                }
                                else{
                                    resultAnnotations.get(annotation.getAnswer()).add(tokenModel);
                                }

                            });
                            postProcessingPersonName(sentenceModel.getAnnotations(),resultAnnotations);
                        });
                    });
                });


            });
        });
        HashMap<String, HashSet<TokenModel>> tempTokens = new HashMap<>();
        resultAnnotations.forEach((s, tokenModels) -> {
            tempTokens.put(s.toLowerCase().trim().replaceAll("[^\\w0-9]","_"), tokenModels);
        });
        positionalAnnotationPostProcessing(tempTokens,annotatorConfiguration.getModifiers());
        applySpecialProps(tempTokens,annotatorConfiguration.getSpecialProps());
        addPagePropsToken(tempTokens, pageModel);
        return tempTokens;
    }

    private void postProcessingPersonName(List<Annotation> annotations,
                                          HashMap<String,HashSet<TokenModel>> resultAnnotations){
        List<Annotation> negationPosAnnotations = annotations.stream().filter(annotation -> annotation.getAnswer().equals("Negation POS")).collect(Collectors.toList());
        List<String> negationPosWords = new ArrayList<>();
        HashSet<TokenModel> tokenModelsInPersonName = new HashSet<>();
        for (Annotation negationPosAnnotation : negationPosAnnotations){
            negationPosWords.add(negationPosAnnotation.getWord().toLowerCase());
        }
        if (resultAnnotations.containsKey("PersonName")) {
            tokenModelsInPersonName = resultAnnotations.get("PersonName");
        } else if (resultAnnotations.containsKey("PERSON")) {
            tokenModelsInPersonName = resultAnnotations.get("PERSON");
        }
        for (TokenModel tokenModel : tokenModelsInPersonName) {
            StringBuilder nameBlock = new StringBuilder();
            String personNameCandidate = (String)tokenModel.get("text");
            String[] splittedName = personNameCandidate.trim().split("\\s+");
            if(splittedName.length!=1) {
                splittedName[splittedName.length - 1] = checkIfContainsColon(splittedName[splittedName.length - 1]);
            }

            /***
             * 1. To handle post processing of names like John Smith DOB
             * 2. To handle post processing of names like Name Wendt Courtney
             */
            for (int i = 0; i<splittedName.length; ++i) {
                if (!negationPosWords.contains(splittedName[i].toLowerCase())) {
                    nameBlock.append(splittedName[i]);
                    nameBlock.append(" ");
                }
            }

            // Appends only valid blocks
            if(!nameBlock.toString().trim().isEmpty()) {
                tokenModel.put("text", nameBlock.toString().trim());
            }
        }
    }

    private String checkIfContainsColon(String name){
        if (name.charAt(name.length()-1)==':')
            return name.substring(0,name.length()-1);
        return name;
    }

    private void positionalAnnotationPostProcessing(HashMap<String, HashSet<TokenModel>> lexiconToTokens, List<AnnotatorModifier> annotatorModifiers){
        List<AnnotatorModifier> modifiers = annotatorModifiers;
        modifiers.forEach(annotatorModifier -> {
            switch (annotatorModifier.getModifierType()){
                case "add+del":
                    if(!lexiconToTokens.containsKey(annotatorModifier.getTo())) {
                        lexiconToTokens.put(annotatorModifier.getTo(), new HashSet<>());
                    }
                    if(lexiconToTokens.containsKey(annotatorModifier.getFrom())) {
                        lexiconToTokens.get(annotatorModifier.getTo()).addAll(lexiconToTokens.get(annotatorModifier.getFrom()));
                        lexiconToTokens.remove(annotatorModifier.getFrom());
                    }
                    break;
                case "del":
                    lexiconToTokens.remove(annotatorModifier.getFrom());
                    break;
            }
        });
    }

    private void applySpecialProps(HashMap<String, HashSet<TokenModel>> tokens, List<SpecialProps> specialProps) {
        for (SpecialProps specialProp : specialProps) {
            if(tokens.containsKey(specialProp.getAnnotation())){
                applySpecialProps(tokens.get(specialProp.getAnnotation()), specialProp);
            }
        }
    }

    private void applySpecialProps(HashSet<TokenModel> tokens, SpecialProps props) {
        if (props != null) {
            if(props.getFilter()!=null)
                filterTokens(tokens, props.getFilter());
        }
    }

    private void filterTokens(HashSet<TokenModel> tokens, SpecialPropsFilter filter){
        if (tokens != null) {
            if (filter.getRegexFilter() != null) {
                if(filter.getRegexFilter().getExclude()!=null && !filter.getRegexFilter().getExclude().isEmpty())
                    tokens.removeIf(tokenModel -> checkIfRegexMatches(filter.getRegexFilter().getExclude(), tokenModel));
                if(filter.getRegexFilter().getInclude()!=null && !filter.getRegexFilter().getInclude().isEmpty())
                    tokens.removeIf(tokenModel -> !checkIfRegexMatches(filter.getRegexFilter().getInclude(), tokenModel));
            }
            if (filter.getMaxWords() != null && filter.getMinWords() != null)
                tokens.removeIf(tokenModel -> !isValidWordCount(tokenModel,filter.getMaxWords(),filter.getMinWords()));
        }
    }
    private boolean checkIfRegexMatches(List<String> regex, TokenModel tokenModel){
        for (int i = 0; i < regex.size(); i++) {
            if(((String)tokenModel.get(PositionalRuleConstants.Properties.TEXT)).trim().matches(regex.get(i)))
                return true;
        }
        return false;
    }

    private boolean isValidWordCount(TokenModel tokenModel, int maxWords, int minWords){
        int wordCount = ((String) tokenModel.get(PositionalRuleConstants.Properties.TEXT)).trim().split(" ").length;
        return wordCount <= maxWords && wordCount >= minWords;
    }
    private void addPagePropsToken(HashMap<String, HashSet<TokenModel>> tokens, PageModel pageModel) {
        TokenModel tokenModel = new TokenModel();
        tokenModel.put(PositionalRuleConstants.PageProps.PAGE_WIDTH, pageModel.getPageWidth());
        tokenModel.put(PositionalRuleConstants.PageProps.PAGE_HEIGHT, pageModel.getPageHeight());
        tokenModel.put(PositionalRuleConstants.Properties.ID, PositionalRuleConstants.PageProps.PAGE_PROPS_ID);
        tokens.put(PositionalRuleConstants.PageProps.PAGE_PROPS, new HashSet<>());
        tokens.get(PositionalRuleConstants.PageProps.PAGE_PROPS).add(tokenModel);
    }

}
