package com.auxolabs.cvision.pipeline.steps;

import com.auxolabs.cvision.constants.CaptureAppConstants;
import com.auxolabs.cvision.di.ComponentsModule;
import com.auxolabs.cvision.exceptions.ActivityNotFoundException;
import com.auxolabs.cvision.exceptions.ExtractionException;
import com.auxolabs.cvision.exceptions.FileRevisionNotFoundException;
import com.auxolabs.cvision.exceptions.OCRActivityResultNotFound;
import com.auxolabs.cvision.db.IDBService;
import com.auxolabs.cvision.models.nlp.PageModel;
import com.auxolabs.cvision.pipeline.ExtractionPipelineMessage;
import com.concordfax.capture.core.dependencyresolver.FileTypes;
import com.concordfax.capture.core.dependencyresolver.GetFileStream;
import com.concordfax.capture.core.logging.ILogger;
import com.concordfax.capture.core.logging.LogLevels;
import com.concordfax.capture.core.models.document.CommonDocumentFiles;
import com.concordfax.capture.core.models.document.DocumentCollectionModel;
import com.concordfax.capture.core.models.document.DocumentFiles;
import com.concordfax.capture.core.models.document.ProcessModel;
import com.concordfax.capture.core.models.document.activity.ActivitiesModel;
import com.concordfax.capture.core.models.document.result.OCRActivityResult;
import com.concordfax.capture.core.models.environment.EnvironmentCollectionModel;
import com.concordfax.capture.core.models.extraction.DocumentTypeMapping;
import com.concordfax.capture.core.models.extraction.ExtractionProfileCollection;
import com.concordfax.capture.core.models.mlmodel.result.Classification;
import com.concordfax.capture.core.models.mlmodel.result.ClassificationResult;
import com.concordfax.capture.core.models.ocr.abbyy.Document;
import com.concordfax.capture.core.models.process.ActivityConfiguration;
import com.concordfax.capture.core.models.process.ProcessCollectionModel;
import com.concordfax.capture.core.models.process.configuration.ExtractionConfiguration;
import com.concordfax.capture.core.pipeline.AbstractMessage;
import com.concordfax.capture.core.pipeline.PipelineStep;
import com.concordfax.capture.core.storage.blobstore.IBlobService;
import com.concordfax.capture.core.utils.dependency.DependencyResolverApiResponse;
import com.concordfax.capture.core.utils.dependency.DependencyResolverClient;
import com.concordfax.capture.core.utils.dependency.IDependencyResolverClient;

import com.google.gson.internal.LinkedTreeMap;
import edu.stanford.nlp.util.Timing;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.bytedeco.javacpp.presets.opencv_core;

import javax.inject.Inject;
import javax.validation.constraints.Null;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * The document props fetcher class implements the following functionality:
 * <ul>
 * <li>Fetch document object from DB based on documentId</li>
 * <li>Fetch the related process definition object from the DB based on the processDefId field in the process field of the document</li>
 * <li>Fetch the environment object from the db based on the environment id in the process field of the document</li>
 * <li>Next we get the extraction configuration model from the process definition object</li>
 * <li>Next we get the file from blob store based on the fileId field in the document</li>
 * <li>Next we get the ocr activity result from the OCR activity in the process field of the document</li>
 * <li>We get the the OCR output result from the blob store based on the blob id in the ocr activity result</li>
 * <li>We deserialize the file from the ocr xml output and convert it into a abbyy document</li>
 * <li>The original file input stream, the document object, the extraction configuration, the environment object and the ocr output model is stored to the ExtractionPipelineMessage</li>
 * </ul>
 */
public class DocumentPropsFetcherStep implements PipelineStep {

    private ILogger log;
    private IDBService db;
    private IBlobService fileStore;
    private IDependencyResolverClient dependencyResolverClient;

    @Inject
    public DocumentPropsFetcherStep(ILogger log, IDBService db, IBlobService filestore, IDependencyResolverClient dependencyResolverClient) {
        this.log = log;
        this.db = db;
        this.fileStore = filestore;
        this.dependencyResolverClient = dependencyResolverClient;
    }

    @Override
    public AbstractMessage process(AbstractMessage message) {
        Timing timing = new Timing();
        ExtractionPipelineMessage extractionPipelineMessage = (ExtractionPipelineMessage) message;
        ExtractionProfileCollection extractionProfile = db.getExtractionProfile(extractionPipelineMessage.getExtractionProfileId());
        log.trace("document id -->"+ extractionPipelineMessage.getDocumentId(), LogLevels.Verbose, extractionPipelineMessage.getLogMaker().getProperties());
        DocumentCollectionModel documentCollectionModel = db.cccGetDocumentById(extractionPipelineMessage.getDocumentId(), extractionPipelineMessage.getPartitionKey());
        ProcessModel documentProcess = documentCollectionModel.getProcess();
        documentProcess.setProcessDefinitionId("5d134fdb74fa1c401111bb9b");
        documentProcess.setVersion(1.0);
        ProcessCollectionModel processCollectionModel = db.cccGetProcessById(documentProcess.getProcessDefinitionId(),documentProcess.getVersion());
        EnvironmentCollectionModel environmentCollectionModel = db.cccGetEnvironmentById(documentCollectionModel.getEnvironmentId());
        extractionPipelineMessage.setActivityId("a6bbbeb2-f485-46e0-85ae-7a4fbc295088");
        Integer extractionActivityIdx = getActivityIndex(processCollectionModel.getActivities(), extractionPipelineMessage.getActivityId());

        ActivityConfiguration activityConfiguration = processCollectionModel.getActivities().get(extractionActivityIdx).getConfiguration();
        // validate if document has proper config set
        assert activityConfiguration instanceof ExtractionConfiguration;
        ExtractionConfiguration extractionConfiguration = (ExtractionConfiguration) activityConfiguration;
        extractionConfiguration.setExtractionProfileId(extractionPipelineMessage.getExtractionProfileId());

        // fetch document from blob store
        try {
            //mock imaged from azure locally
            try {
                Files.walk(Paths.get(ComponentsModule.dependencyResolver(documentCollectionModel.getDocumentId(),"images")))
                        .filter(Files::isRegularFile)
                        .forEach(filename->{
                            try {
                                File fi = new File(filename.toString());
                                byte[] fileContent = Files.readAllBytes(fi.toPath());
                                extractionPipelineMessage.setDocumentBytes(fileContent);
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            Document document = null;
            InputStream temp= new FileInputStream(new File(ComponentsModule.dependencyResolver(documentCollectionModel.getDocumentId(), "xml")));

            // TODO: 21/05/18 can be optimized by preventing multiple conversion, check "Document" class cleanup functions as well before refactoring
            document = Document.getDocumentObjectFromXML(IOUtils.toString(temp, StandardCharsets.UTF_8));

            extractionPipelineMessage.setOcrOutputDocument(document);
            extractionPipelineMessage.setExtractionProfile(extractionProfile);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        extractionPipelineMessage.setDcm(documentCollectionModel);
        extractionPipelineMessage.setExtractionActivityIdx(extractionActivityIdx);
        extractionPipelineMessage.setExtractionConfiguration(extractionConfiguration);
        extractionPipelineMessage.setEnvironment(environmentCollectionModel);
        extractionPipelineMessage.setPcm(processCollectionModel);
        extractionPipelineMessage.getLogMaker().addUser(extractionPipelineMessage.getDcm().getStatus().getCreatedById());

        log.trace(String.format("fetched document from db and filestore successfully in %d ms", timing.report()) , LogLevels.Verbose, extractionPipelineMessage.getLogMaker().getProperties());
        return message;
    }

    private Integer getActivityIndex(List<ActivitiesModel> activities, String reqActivityId) throws ExtractionException {
        for (int i = 0; i < activities.size(); i++) {
            if (activities.get(i).getActivityId().equals(reqActivityId)) {
                return i;
            }
        }
        throw new ActivityNotFoundException(reqActivityId);
    }


}
