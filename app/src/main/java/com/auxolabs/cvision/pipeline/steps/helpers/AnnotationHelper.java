package com.auxolabs.cvision.pipeline.steps.helpers;

import com.auxolabs.capture.annotator.AnnotationsProcessor;
import com.auxolabs.capture.annotator.AnnotatorService;
import com.auxolabs.capture.annotator.models.response.SentenceModel;
import com.auxolabs.cvision.models.ocr.SegBlock;
import com.concordfax.capture.core.models.extraction.annotators.ExtractionAnnotatorConfiguration;

import javax.inject.Inject;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AnnotationHelper {
    private AnnotatorService annotationsProcessor;

    @Inject
    public AnnotationHelper(AnnotatorService annotationsProcessor) {
        this.annotationsProcessor = annotationsProcessor;
    }

    public Map<String,List<SentenceModel>> createTextAndFeedToAnnotator(List<SegBlock> blocks, ExtractionAnnotatorConfiguration annotatorConfigurations, int pageNumber, String docId) {
        List<String> requestText = new ArrayList<>();
        blocks.forEach(block -> {
            requestText.add(block.getValue());
        });

        try {
//        //to create folder and save the text into the folder for text generation for NPI
            File file = new File("/home/nstest-admin/Documents/output/segments/" + docId);
            if (!file.exists()) {
                file.mkdir();
                FileWriter writer = new FileWriter(file + "/" + (pageNumber +1) + ".txt");
                for (String str : requestText) {
                    writer.write(str + System.lineSeparator());
                }
                writer.flush();
                writer.close();


            } else {
                FileWriter writer = new FileWriter(file + "/" + pageNumber + ".txt");
                for (String str : requestText) {
                    writer.write(str + System.lineSeparator());
                }
                writer.flush();
                writer.close();

            }
        } catch (IOException ie){

        }

        Map<String,List<SentenceModel>> annotatorNameToAnnotations =annotationsProcessor.getAnnotatorToAnnotations(requestText,annotatorConfigurations);
        return  annotatorNameToAnnotations;
    }
}
