package com.auxolabs.cvision.pipeline.utils.field.modifiers;


import com.concordfax.capture.core.models.document.result.extraction.ExtractionField;
import com.concordfax.capture.core.models.document.result.extraction.FieldNames;
import com.concordfax.capture.core.models.document.result.extraction.SimpleExtractionField;
import com.concordfax.capture.core.models.extraction.output.ExtractorModel;
import com.concordfax.capture.core.models.extraction.output.ExtractorResultModifier;
import com.concordfax.capture.core.models.extraction.output.ExtractorType;

import java.util.HashMap;
import java.util.List;

public class ExtendMrnByLength implements FieldModifier<ExtractionField, String> {
    @Override
    public ExtractionField modify(String currentResult, ExtractorResultModifier extractorResultModifier, List<ExtractorModel> extractors, HashMap<ExtractorType, HashMap<String, ExtractionField>> fieldRuleMap) {
        return new SimpleExtractionField(FieldNames.mrn.toString(), 0.95f, currentResult, "");
    }
}
