package com.auxolabs.cvision.pipeline.steps.helpers;

import com.auxolabs.cvision.models.namemodel.NameComparison;
import com.auxolabs.cvision.pipeline.utils.field.normalizers.NormalizerFactory;
import com.auxolabs.cvision.pipeline.utils.field.modifiers.ModifierFactory;
import com.concordfax.capture.core.models.document.result.extraction.ExtractionField;
import com.concordfax.capture.core.models.document.result.extraction.SimpleExtractionField;
import com.concordfax.capture.core.models.extraction.ExtractionOutputConfigurationModel;
import com.concordfax.capture.core.models.extraction.output.ExtractorModel;
import com.concordfax.capture.core.models.extraction.output.ExtractorResultModifier;
import com.concordfax.capture.core.models.extraction.output.ExtractorType;
import com.concordfax.capture.core.models.extraction.output.FieldFeatureModel;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

public class ActivityResultHelper {
    public static ArrayList<NameComparison> nameComparisonArrayList = new ArrayList<>();
    private NormalizerFactory normalizerFactory;
    private ModifierFactory modifierFactory;

    public ActivityResultHelper(){
        normalizerFactory = new NormalizerFactory();
        modifierFactory = new ModifierFactory();
    }

    public List<ExtractionField> process(ExtractionOutputConfigurationModel extractionOutputConfigurationModel, HashMap<ExtractorType, HashMap<String, ExtractionField>> fieldRuleMap) {
        List<ExtractionField> fieldToValueResults = new ArrayList<>();
        extractionOutputConfigurationModel.getFields().forEach(fieldFeatureModel -> {
            ExtractionField extractionField = processEachField(fieldFeatureModel, fieldRuleMap);
            if (extractionField!=null && !extractionField.getName().equals("patient_name")){
                if(StringUtils.isNotBlank(extractionField.getValue()))
                    fieldToValueResults.add(extractionField);
            }
        });
        return fieldToValueResults;
    }

    private ExtractionField processEachField(FieldFeatureModel fieldFeatureModel, HashMap<ExtractorType, HashMap<String, ExtractionField>> fieldRuleMap) {
        SimpleExtractionField finalNormalizedResult = (SimpleExtractionField) getFinalNormalizedResult(fieldFeatureModel,
                fieldRuleMap);
        try {
            if ((null == finalNormalizedResult.getValue()) ||
                    (null == finalNormalizedResult.getValue().trim()) ||
                    finalNormalizedResult.getValue().trim().isEmpty()) {
                return new SimpleExtractionField(fieldFeatureModel.getKey(), 0.8f,
                        "", fieldFeatureModel.getKey());
//                return null;
            } else if(fieldFeatureModel.getKey().matches("patient_id|case_id|member_id|subscriber_id|account_id|authorization_id")) {
                return new SimpleExtractionField(fieldFeatureModel.getKey(), 0.8f,
                        finalNormalizedResult.getValue(), finalNormalizedResult.getTag());
            }
            else if(fieldFeatureModel.getKey().matches("patient_dob|dos|order_date|doe|dod")){
                return new SimpleExtractionField(fieldFeatureModel.getKey(), 0.8f,
                        finalNormalizedResult.getValue(), finalNormalizedResult.getTag());
            }

            else return new SimpleExtractionField(fieldFeatureModel.getKey(), 0.8f,
                        finalNormalizedResult.getValue(), "");
        } catch (NullPointerException exception) {
            return null;
        }
    }

    private ExtractionField getFinalNormalizedResult(FieldFeatureModel fieldFeatureModel, HashMap<ExtractorType, HashMap<String, ExtractionField>> fieldRuleMap) {
        if (fieldFeatureModel.getExtractors().size() == 0)
            return null;

        ExtractionField extractedResultOfCurrentHighestPriority = null;
        SimpleExtractionField currentResult = new SimpleExtractionField();
        currentResult.setValue("");
        // sort priorities in ascending order, so that the priorities are from 1,2,3,4
        fieldFeatureModel.getExtractors().sort(Comparator.comparingInt(ExtractorModel::getPriority));

        for (ExtractorModel currentExtractor : fieldFeatureModel.getExtractors()) {
            if (fieldRuleMap.containsKey(currentExtractor.getType()) && fieldRuleMap.get(currentExtractor.getType()).containsKey(currentExtractor.getRule())) {
                extractedResultOfCurrentHighestPriority = fieldRuleMap.get(currentExtractor.getType()).get(currentExtractor.getRule());
            }
            currentResult = (SimpleExtractionField) extractedResultOfCurrentHighestPriority;
            try{
                if (currentResult != null || !currentResult.getValue().isEmpty()) {
                    List<ExtractorResultModifier> modifiers = currentExtractor.getModifier();
                    if (modifiers != null && modifiers.size() > 0) {
                        for (ExtractorResultModifier extractorResultModifier : modifiers) {
                            currentResult = (SimpleExtractionField) modifierFactory.getModifier(extractorResultModifier.getModifierType())
                                    .modify(currentResult.getValue(), extractorResultModifier, fieldFeatureModel.getExtractors(), fieldRuleMap);
                        }
                    }
                    break;
                }
            }catch(NullPointerException npe){

            }
        }
        if (currentResult == null || currentResult.getValue().isEmpty()) {
            currentResult = new SimpleExtractionField(fieldFeatureModel.getKey(), 0.75f, "", "");
        }
        try{
            ExtractionField patientNameField;
            ExtractionField nonPatientNameField;
            patientNameField = (ExtractionField) ((Map)fieldRuleMap.get(ExtractorType.MachineLearningName)).get( "patient_name" );
            nonPatientNameField = (ExtractionField) ((Map)fieldRuleMap.get(ExtractorType.MachineLearningName)).get( "non_patient_names" );
            String patientName = (patientNameField==null) ? "" : patientNameField.getValue();
            String nonPatientName = (nonPatientNameField==null) ? "" : nonPatientNameField.getValue();
            if(StringUtils.isNotBlank(currentResult.getName())){
                if(fieldFeatureModel.getKey().equals("patient_name"))
                    nameComparisonArrayList.add(new NameComparison(currentResult.getValue(), patientName, nonPatientName));
            }

        }catch (NullPointerException npe){
            npe.printStackTrace();
        }
        currentResult = (SimpleExtractionField) normalize(fieldFeatureModel.getNormalizer(), currentResult);
        return currentResult;

    }

    public ExtractionField normalize(String normalizer, SimpleExtractionField value) {
        SimpleExtractionField normalizedValue = new SimpleExtractionField();
        if(normalizer==null){
            return value;
        }
        if (value != null && !normalizer.isEmpty())
            normalizedValue.setValue((String) normalizerFactory.createNormalizer(normalizer).normalize(value.getValue()));
        return normalizedValue;
    }

}
