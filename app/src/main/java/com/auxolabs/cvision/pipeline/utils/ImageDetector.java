package com.auxolabs.cvision.pipeline.utils;

import com.auxolabs.cvision.models.cv.PIBOXHolder;
import com.auxolabs.cvision.models.cv.RDObject;
import com.auxolabs.cvision.utils.LeptonicaUtils;
import org.bytedeco.javacpp.IntPointer;
import org.bytedeco.javacpp.Loader;
import org.bytedeco.javacpp.lept;

import static org.bytedeco.javacpp.lept.*;

/**
 * Detects segments and then identifies the type of region
 */

// -----------------------------------------------------------------------------
// Takes two pix as input, generated from the same original image:
//   1. pixb   - a binary thresholded image
//   2. piximg - a full color or grayscale image
// and segments them by finding the areas that contain color or grayscale
// graphics, removing those areas from the binary image, and doing the
// opposite for the full color/grayscale image.  The upshot is that after
// this routine has been run, the binary image contains only text and the
// full color image contains only the graphics.
//
// Both input images are modified by this procedure.  If no text is found,
// pixb is set to NULL.  If no graphics is found, piximg is set to NULL.
//
// Thanks to Dan Bloomberg for this
// -----------------------------------------------------------------------------

public class ImageDetector {

    private static final String segment_mask_sequence = "r11";
    private static final String segment_seed_sequence = "r22 + o6.6"; /* maybe o6.6 */
    private static final String segment_dilation_sequence = "d3.3";

    public ImageDetector() {
        Loader.load(lept.class);
    }

    public RDObject detectLogos(PIBOXHolder pixb, PIBOXHolder pixs) {
        RDObject rdObject = new RDObject();

        // Make seed and mask, and fill seed into mask
        PIX pixmask4 = pixMorphSequence(pixb.getPix(), segment_mask_sequence, 0);
        PIX pixseed4 = pixMorphSequence(pixb.getPix(), segment_seed_sequence, 0);
        PIX pixsf4 = pixSeedfillBinary(null, pixseed4, pixmask4, 8);
        PIX pixd4 = pixMorphSequence(pixsf4, segment_dilation_sequence, 0);
        PIX pixd = pixExpandBinaryPower2(pixd4, 4);

        LeptonicaUtils.getInstance().safeDeletePIX(pixd4);
        LeptonicaUtils.getInstance().safeDeletePIX(pixsf4);
        LeptonicaUtils.getInstance().safeDeletePIX(pixseed4);
        LeptonicaUtils.getInstance().safeDeletePIX(pixmask4);

        PIX pixSub = pixSubtract(null, pixb.getPix(), pixd);

        // now see what we got from the segmentation
        IntPointer pcount = new IntPointer(1);

        // if no image portion was found, set the image pointer to NULL and return
        pixCountPixels(pixd, pcount, null);
        if (pcount.get() < 100) {
            LeptonicaUtils.getInstance().safeDeletePIX(pixd);
            LeptonicaUtils.getInstance().safeDeletePIX(pixSub);
            pcount.close();
            PIBOXHolder textOnlyImage = new PIBOXHolder();
            PIX pix = pixCopy(null, pixb.getPix());
            textOnlyImage.setPix(pix);
            rdObject.setTextInImage(textOnlyImage);
            return rdObject;
        }

        // if no text portion found, set the binary pointer to NULL
        pixCountPixels(pixSub, pcount, null);
        if (pcount.get() < 100) {
            LeptonicaUtils.getInstance().safeDeletePIX(pixd);
            LeptonicaUtils.getInstance().safeDeletePIX(pixSub);
            pcount.close();
            PIBOXHolder imgOnlyImage = new PIBOXHolder();
            PIX pix = pixCopy(null, pixs.getPix());
            imgOnlyImage.setPix(pix);
            rdObject.setTextInImage(imgOnlyImage);
            return rdObject;
        }

        pcount.close();

        PIX piximg1;
        if (pixs.getPix().d() == 1 || pixs.getPix().d() == 8 || pixs.getPix().d() == 32) {
            piximg1 = pixCopy(null, pixs.getPix());
        } else if (pixs.getPix().d() > 8) {
            piximg1 = pixConvertTo32(pixs.getPix());
        } else {
            piximg1 = pixConvertTo8(pixs.getPix(), FALSE);
        }


        PIX pixd1;
        if (piximg1.d() == 32) {
            pixd1 = pixConvertTo32(pixd);
        } else if (piximg1.d() == 8) {
            pixd1 = pixConvertTo8(pixd, FALSE);
        } else {
            pixd1 = pixCopy(null, pixd);
        }

        LeptonicaUtils.getInstance().safeDeletePIX(pixd);

        pixRasteropFullImage(pixd1, piximg1, PIX_SRC | PIX_DST);
        LeptonicaUtils.getInstance().safeDeletePIX(piximg1);

        PIBOXHolder textOnlyImage = new PIBOXHolder();
        textOnlyImage.setPix(pixSub);
        rdObject.setTextInImage(textOnlyImage);
        PIBOXHolder imgOnlyImage = new PIBOXHolder();
        imgOnlyImage.setPix(pixd1);
        rdObject.setImagesInImage(imgOnlyImage);

        return rdObject;
    }

}
