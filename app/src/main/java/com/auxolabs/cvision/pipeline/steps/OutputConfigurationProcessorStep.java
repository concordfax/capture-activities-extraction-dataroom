package com.auxolabs.cvision.pipeline.steps;

import com.auxolabs.capture.dateparser.exceptions.DateParserInvalidFormat;
import com.auxolabs.capture.dateparser.models.OutputDateModel;
import com.auxolabs.capture.dateparser.utils.formatter.DateNormalizer;
import com.auxolabs.cvision.models.namemodel.NameComparison;
import com.auxolabs.cvision.models.nlp.PageModel;
import com.auxolabs.cvision.pipeline.ExtractionPipelineMessage;
import com.auxolabs.cvision.pipeline.steps.helpers.ActivityResultHelper;
import com.auxolabs.cvision.pipeline.steps.helpers.ComparatorHelper;
import com.auxolabs.cvision.pipeline.steps.helpers.PositionalHelper;
import com.concordfax.capture.core.logging.ILogger;
import com.concordfax.capture.core.logging.LogLevels;
import com.concordfax.capture.core.models.document.result.extraction.*;
import com.concordfax.capture.core.models.extraction.ExtractionOutputConfigurationModel;
import com.concordfax.capture.core.models.extraction.output.FieldFeatureModel;
import com.concordfax.capture.core.models.mlmodel.result.Classification;
import com.concordfax.capture.core.pipeline.AbstractMessage;
import com.concordfax.capture.core.pipeline.PipelineStep;
import edu.stanford.nlp.util.Timing;
import org.apache.commons.lang3.StringUtils;
import javax.inject.Inject;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.*;

/**
 * The Activity Result Processor implements the following functionalities:
 * <ul>
 * <li>Identifies the required field values from extractors according to the profile configuration.</li>
 * <li>Normalizes the field results.</li>
 * <li>Populates the results to Extraction Activity Result model in Capture Document Model class</li>
 * </ul>
 */
public class OutputConfigurationProcessorStep implements PipelineStep {

    private ILogger log;
    private ActivityResultHelper activityResultHelper;
    private ComparatorHelper comparatorHelper;

    @Inject
    public OutputConfigurationProcessorStep(ILogger log) {
        this.log = log;
        this.activityResultHelper = new ActivityResultHelper();
        this.comparatorHelper = new ComparatorHelper();
    }

    @Override
    public AbstractMessage process(AbstractMessage message) {
        ExtractionPipelineMessage extractionPipelineMessage = (ExtractionPipelineMessage) message;
        try {
            Timing timing = new Timing();
            extractionConfigurationToExtractionActivityResult(extractionPipelineMessage);
            log.trace(String.format("extraction result formatting done in %d ms", timing.stop()), LogLevels.Verbose, extractionPipelineMessage.getLogMaker().getProperties());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return message;
    }

    private void extractionConfigurationToExtractionActivityResult(ExtractionPipelineMessage extractionPipelineMessage) throws FileNotFoundException {
        ExtractionActivityResult extractionActivityResult = new ExtractionActivityResult();
        extractionActivityResult.setPages(new ArrayList<>());
        for (PageModel pageModel : extractionPipelineMessage.getPages()) {
            ExtractionPage currentExtractionPage = new ExtractionPage();
            currentExtractionPage.setPageNumber(pageModel.getPageNumber() + 1);
            currentExtractionPage.setFields(new ArrayList<>());
            List<ExtractionField> fieldToResult = activityResultHelper.process(pageModel.getDocumentTypeMapping().getOutputConfiguration(), pageModel.getExtractorToRuleField());
            fieldToResult.forEach(value -> {
                currentExtractionPage.getFields().add(value);
            });
            currentExtractionPage.setDocumentType(extractionPipelineMessage.getDocumentClassificationResult().getPredictions().get(pageModel.getPageNumber()));
            extractionActivityResult.getPages().add(currentExtractionPage);
        }
        NameComparison nameComparison;
        String patientName;
        for (int i = 0; i < extractionActivityResult.getPages().size(); i++) {
            nameComparison = ActivityResultHelper.nameComparisonArrayList.get(i);
            //comparing rules and ml results
            patientName = comparatorHelper.compareResults(i, ActivityResultHelper.nameComparisonArrayList, nameComparison.getRulesPatientName(), nameComparison.getMlPatientName(), nameComparison.getMlNonPatientName());
            if (StringUtils.isNotBlank(patientName)) {
                SimpleExtractionField currentResult = new SimpleExtractionField("patient_name", 0.8f, patientName, "");
                currentResult = (SimpleExtractionField) activityResultHelper.normalize("Name", currentResult);
                currentResult.setName("patient_name");
                currentResult.setConfidence(0.8f);
                currentResult.setTag("patient_name");
                extractionActivityResult.getPages().get(i).getFields().add(currentResult);
            }
        }
        extractionPipelineMessage.setExtractionActivityResult(extractionActivityResult);
        generateDocumentExtractionResult(extractionPipelineMessage);
        try {
            PositionalExtractorStep.writeToFile(PositionalHelper.temp, extractionPipelineMessage.getDocumentId());
        } catch (FileNotFoundException ex) {
            // insert code to run when exception occurs
            System.out.println("File write failed");
        } catch (Exception ex1) {
            // insert code to run when exception occurs
            System.out.println("File write failed");
        } finally {

            // clear the array after writing to the file
            PositionalHelper.temp.clear();
        }
        aggregateNameId(extractionPipelineMessage, extractionPipelineMessage.getExtractionActivityResult());
        ActivityResultHelper.nameComparisonArrayList.clear();
    }

    private void generateDocumentExtractionResult(ExtractionPipelineMessage extractionPipelineMessage) {
        ArrayList<Classification> predictions = (ArrayList<Classification>) extractionPipelineMessage.getDocumentClassificationResult().getPredictions();
        int selectedPageIdx = -1;
        ExtractionPage extractionDocument = new ExtractionPage();

        // If document having multiple fax cover sheet in it. take last page as selected page
        for (int i = 0; i < predictions.size(); i++) {
            if (extractionPipelineMessage.getExtractionActivityResult().getPages().get(i).getFields().size() > 0) {
                if ((predictions.get(i).getCls() != 11) &&
                        (extractionPipelineMessage.getExtractionActivityResult().getPages().get(i).getFields()
                                .stream()
                                .anyMatch(field -> field.getName().matches("patient_name|patient_dob|patient_ssn")))) {
                    // Select the page which has valid extraction results from any of the below fields
                    // Name (or) DOB (or) SSN
                    selectedPageIdx = i;
                    break;
                }
            }
        }

        /*
         * Aggregate all values values from pages prior to Fax cover sheet except Patient Name
         * 1. Aggregate all fields present till the current page at the selected index
         * 2. If there patient name gets aggregated from the non fax cover sheet, remove it
         */
        if (selectedPageIdx > 0) {
            for (int iterationIndex = 0; iterationIndex <= selectedPageIdx; iterationIndex++) {
                if (extractionPipelineMessage.getExtractionActivityResult().getPages().get(iterationIndex).getFields().size() > 0) {
                    if (null == extractionDocument.getFields()) {
                        extractionDocument = new ExtractionPage(extractionPipelineMessage.getExtractionActivityResult().getPages().get(iterationIndex));

                        // Remove patient name aggregated from the "Fax Cover sheet" page
                        extractionDocument.getFields().removeIf(field -> field.getName().equals("patient_name"));

                        // Skip the current page as the results are already aggregated
                        continue;
                    }

                    for (ExtractionField localExtractionField : extractionPipelineMessage.getExtractionActivityResult().getPages().get(iterationIndex).getFields()) {
                        for (ListIterator<ExtractionField> iterator = extractionDocument.getFields().listIterator(); iterator.hasNext(); ) {

                            ExtractionField aggregatedField = iterator.next();

                            // Remove patient name aggregated from the subsequent "Fax Cover sheet" page
                            if (aggregatedField.getName().equals("patient_name") &&
                                    (iterationIndex != selectedPageIdx) &&
                                    (11 == predictions.get(iterationIndex).getCls())) {
                                iterator.remove();
                                continue;
                            }

                            // add missing fields
                            if (null == checkIfFieldIsExtractedFromPage(localExtractionField.getName(), extractionDocument.getFields())) {
                                iterator.add(localExtractionField);
                            }
                        }
                    }
                }

                // Break the loop once the selected page index is reached
                if (iterationIndex == selectedPageIdx) {

                    // Update the document type at the selected index
                    extractionDocument.setPageNumber(extractionPipelineMessage.getExtractionActivityResult().getPages().get(selectedPageIdx).getPageNumber());
                    extractionDocument.setDocumentType(extractionPipelineMessage.getExtractionActivityResult().getPages().get(selectedPageIdx).getDocumentType());
                    break;
                }
            }
        }

        /*
         * If no valid index is selected from the above logic
         * 1. Aggregate results from page that has maximum extraction results
         * 2. If only one page document, aggregate the results from the page
         */
        int pageCount = extractionPipelineMessage.getExtractionActivityResult().getPages().size();
        if ((-1 == selectedPageIdx) && (pageCount > 1)) {
            selectedPageIdx = getPageIndexWithMaxResults(extractionPipelineMessage.getExtractionActivityResult().getPages());
        } else if (-1 == selectedPageIdx) {
            //if document has only one page in it taking extraction field result of the single page
            selectedPageIdx = predictions.size() - 1;
        }

        // set the results for the entire document by aggregating the results
        aggregateResults(selectedPageIdx, extractionPipelineMessage.getExtractionActivityResult().getPages(),
                extractionPipelineMessage.getExtractionActivityResult(),
                extractionPipelineMessage.getPages().get(selectedPageIdx).getDocumentTypeMapping().getOutputConfiguration(),
                extractionDocument);
    }

    // TODO: 2019-05-16 long term we would want to aggregate based on the different sections in the document
    private void aggregateResults(Integer selectedPageIdx, List<ExtractionPage> extractionPages,
                                  ExtractionActivityResult extractionActivityResult,
                                  ExtractionOutputConfigurationModel extractionOutputConfiguration,
                                  ExtractionPage tempExtractionDocument) {
        // create a copy of the results from the selected extraction page so that they can be upgraded
        ExtractionPage extractionDocument;
        if ((null == tempExtractionDocument.getFields()) || (0 == tempExtractionDocument.getFields().size())) {
            extractionDocument = new ExtractionPage(extractionPages.get(selectedPageIdx));
        } else {
            extractionDocument = tempExtractionDocument;
        }

        //creating Document level extraction for DOS,DOE,OD and DD
        List<ExtractionField> doeCands = new ArrayList<>();
        List<ExtractionField> dosCands = new ArrayList<>();
        List<ExtractionField> dodCands = new ArrayList<>();
        List<ExtractionField> odCands = new ArrayList<>();
        extractionPages.forEach(page -> {
            if (!page.getFields().isEmpty()) {
                page.getFields().forEach(field -> {
                    switch (field.getName()) {
                        case "doe":
                            doeCands.add(field);
                            break;
                        case "dos":
                            dosCands.add(field);
                            break;
                        case "dod":
                            dodCands.add(field);
                            break;
                        case "order_date":
                            odCands.add(field);
                            break;
                    }

                });
            }

        });

        // get fields to be extracted for that document type
        List<FieldFeatureModel> fields = extractionOutputConfiguration.getFields();

        // check if results for all the fields are present in the selected page
        fields.forEach(fieldFeatureModel -> {

            // check if field is present in the fields of the extracted page, if not check if next pages contain field
            if (checkIfFieldIsExtractedFromPage(fieldFeatureModel.getKey(), extractionDocument.getFields()) == null) {

                // iterate over all the pages starting from the page of the selectedPage
                for (int i = selectedPageIdx + 1; i < extractionPages.size(); i++) {
                    ExtractionPage curPage = extractionPages.get(i);
                    curPage.getFields().forEach(field -> {
                        if (!field.getName().matches("patient_id|case_id|member_id|subscriber_id|account_id|authorization_id")) {
                            field.setTag(field.getName());
                        }
                    });

                    // ensure that next page is of same document type
                    if (!curPage.getDocumentType().getCls().equals(extractionDocument.getDocumentType().getCls())) {
                        break;
                    }

                    // check if field present in current page
                    ExtractionField field = checkIfFieldIsExtractedFromPage(fieldFeatureModel.getKey(), curPage.getFields());

                    // add field from page to final result
                    if (field != null) {
                        extractionDocument.getFields().add(field);
                        break;
                    }
                }
            }
        });

        // set the result for the entire document
        extractionActivityResult.setExtractionDocument(extractionDocument);
        npiAggregationLogic(extractionActivityResult, extractionDocument);

        // aggregate the missing fields
        if (extractionPages.size() > 1 && (selectedPageIdx < extractionPages.size() - 1)) {
            // add missing fields to the document level aggregation
            aggregateMissingResultsToDocument(extractionDocument, extractionPages, selectedPageIdx, extractionOutputConfiguration.getFields().size());
            npiAggregationLogic(extractionActivityResult, extractionDocument);
        }

        if (!doeCands.isEmpty()) {
            extractionDocument.getFields().add(getLowestDate(doeCands));
        }
        if (!dosCands.isEmpty()) {
            extractionDocument.getFields().add(getLowestDate(dosCands));
        }
        if (!odCands.isEmpty()) {
            extractionDocument.getFields().add(getLowestDate(odCands));
        }
        if (!dodCands.isEmpty()) {
            extractionDocument.getFields().add(getLowestDate(dodCands));
        }

    }

    private ExtractionField checkIfFieldIsExtractedFromPage(String fieldName, List<ExtractionField> fieldsInPage) {
        for (ExtractionField extractionField : fieldsInPage) {
            if (extractionField.getName().equals(fieldName)) {
                return extractionField;
            }
        }
        return null;
    }

    /**
     * @param extractionDocument:   Current extraction page document selected for aggregation
     * @param extractionPages:      List of extraction results across all pages
     * @param selectedPageIdx:      Current extraction page index selected for aggregation
     * @param extractionFieldCount: Total count of Extraction fields
     */
    private void aggregateMissingResultsToDocument(ExtractionPage extractionDocument,
                                                   List<ExtractionPage> extractionPages,
                                                   int selectedPageIdx, int extractionFieldCount) {
        int pageCount = extractionPages.size();

        /*
         * Iterate through rest of the pages from the selected page
         * 1. Select the Page which is non-fax cover sheet
         * 2. Has atleast more than 1 field extracted
         */
        for (int index = selectedPageIdx + 1; index < pageCount; index++) {
            if (extractionPages.get(index).getFields().size() > 0) {

                // If all the fields are aggregated, break the loop
                if (extractionDocument.getFields().size() == extractionFieldCount) {
                    break;
                }

                // Proceed with logic
                boolean matchedField = false;
                for (ExtractionField extractionField : extractionDocument.getFields()) {
                    for (ExtractionField comparingField : extractionPages.get(index).getFields()) {
                        try {

                            // Check if any of the following fields matches in the subsequent pages
                            // Name, SSN or DOB
                            // TODO: Replace stream with better logic for faster performance
                            ExtractionField checkingField = extractionDocument.getFields()
                                    .stream()
                                    .filter(field -> field.getName().matches("patient_name|patient_dob|patient_ssn"))
                                    .filter(field -> field.getName().equals(comparingField.getName())).findAny().get();

                            // Match for substring in both strings
                            if ((extractionField.getName().equals(checkingField.getName()))
                                    && (extractionField.getValue().toLowerCase().contains(checkingField.getValue().toLowerCase())
                                    || checkingField.getValue().contains(extractionField.getValue()))) {
                                matchedField = true;
                                break;
                            }
                        } catch (NoSuchElementException nse) {

                            // If the comparing field isn't available in the existing aggregation, skip the exception
                            // and still add the new field
                            matchedField = true;
                            break;
                        }
                    }
                }

                if (matchedField) {
                    for (ExtractionField comparingField : extractionPages.get(index).getFields()) {
                        boolean addMissingField = true;

                        for (Iterator<ExtractionField> iterator = extractionDocument.getFields().iterator(); iterator.hasNext(); ) {
                            ExtractionField aggregatedField = iterator.next();

                            if (aggregatedField.getName().equals(comparingField.getName())) {
                                addMissingField = false;
                            }

                            // Validate invalid mrn in already aggregated fields and remove them
                            if (aggregatedField.getName().equals("mrn") && aggregatedField.getValue().length() < 4) {
                                iterator.remove();
                            } else if (aggregatedField.getName().equals("dos") ||
                                    aggregatedField.getName().equals("doe") ||
                                    aggregatedField.getName().equals("order_date") ||
                                    aggregatedField.getName().equals("dod")) {
                                iterator.remove();
                            }
                        }

                        // Add the missing field to the Aggregation results
                        if (addMissingField) {

                            // Check for valid MRN
                            if (comparingField.getName().equals("mrn") && comparingField.getValue().length() < 4) {
                                break;
                            }
                            if (!comparingField.getName().equals("dos") &&
                                    !comparingField.getName().equals("doe") &&
                                    !comparingField.getName().equals("order_date") &&
                                    !comparingField.getName().equals("dod")) {
                                extractionDocument.getFields().add(comparingField);
                                continue;
                            }

                        }
                    }
                }
            }
        }
    }

    private void npiAggregationLogic(ExtractionActivityResult extractionActivityResult, ExtractionPage extractionDocument) {
        ArrayList<String> npiList = new ArrayList<String>();
        Map<String, Integer> wordMap = new HashMap<>();
        ExtractionField fieldToBeRemoved = null;
        for (ExtractionField extractionField : extractionDocument.getFields()) {
            if (extractionField.getName().equals("npi")) {
                fieldToBeRemoved = extractionField;
            }
        }
        if (fieldToBeRemoved != null) {
            extractionDocument.getFields().remove(fieldToBeRemoved);
        }

        for (int i = 0; i < extractionActivityResult.getPages().size(); i++) {
            for (ExtractionField extractionField : extractionActivityResult.getPages().get(i).getFields()) {
                if (extractionField.getName().equals("npi")) {
                    npiList.add(extractionField.getValue());
                }
            }
        }

        if (npiList.size() != 0 && npiList.size() < 3) {
            SimpleExtractionField finalResult = new SimpleExtractionField("npi", 0.8f, npiList.get(0), "npi");
            extractionDocument.getFields().add(finalResult);
        } else if (npiList.size() > 3) {
            for (String st : npiList) {
                String input = st.toLowerCase();
                if (wordMap.get(input) != null) {
                    Integer count = wordMap.get(input) + 1;
                    wordMap.put(input, count);
                } else {
                    wordMap.put(input, 1);
                }
            }
            Object maxEntry = Collections.max(wordMap.entrySet(), Map.Entry.comparingByValue()).getKey();
            SimpleExtractionField finalResult = new SimpleExtractionField("npi", 0.8f, maxEntry.toString(), "npi");
            extractionDocument.getFields().add(finalResult);

        }


    }

    private int getPageIndexWithMaxResults(List<ExtractionPage> extractionPages) {
        int pageCount = extractionPages.size();
        int maxIndex = 0;

        // Get the page with maximum extraction results
        for (int index = 0; index < pageCount; index++) {
            if (extractionPages.get(maxIndex).getFields().size() < extractionPages.get(index).getFields().size()) {
                maxIndex = index;
            }
        }
        // Get the index of the last number
        return maxIndex;
    }

    private static ExtractionField getLowestDate(List<ExtractionField> dateCandidateList) {
        DateNormalizer dateNormalizer = new DateNormalizer();
        List<OutputDateModel> listOfODModel = new ArrayList<>();
        dateCandidateList.forEach(extractionField -> {
            try {
                OutputDateModel outputDateModel = new OutputDateModel();
                outputDateModel.setExtractionField(extractionField);
                outputDateModel.setNormalizedDate(dateNormalizer.dateFieldFormatter(extractionField.getValue()));
                listOfODModel.add(outputDateModel);
            } catch (ParseException e) {
                throw new DateParserInvalidFormat(e.getMessage());
            }
        });
        Comparator<OutputDateModel> byYear = Comparator.comparing(dateModel -> Integer.parseInt(dateModel.getNormalizedDate().substring(6)));
        Comparator<OutputDateModel> byMonth = Comparator.comparing(dateModel -> Integer.parseInt(dateModel.getNormalizedDate().substring(0, 2)));
        Comparator<OutputDateModel> byDay = Comparator.comparing(dateModel -> Integer.parseInt(dateModel.getNormalizedDate().substring(3, 5)));
        listOfODModel.sort(byYear.thenComparing(byMonth).thenComparing(byDay));
        return listOfODModel.get(0).getExtractionField();
    }

    private void aggregateNameId(ExtractionPipelineMessage extractionPipelineMessage, ExtractionActivityResult extractionActivityResult) {
        ArrayList<ExtractionField> documentAccountId = new ArrayList<>();
        ArrayList<ExtractionField> documentPatientId = new ArrayList<>();
        String documentLevelPatientName = "";
        ArrayList<String> listPatientNames = new ArrayList<>();
        ArrayList<ExtractionField> listAccountId = new ArrayList<>();
        ArrayList<ExtractionField> listPatientId = new ArrayList<>();
        ArrayList<ExtractionField> listMemberId = new ArrayList<>();
        ArrayList<ExtractionField> listSubscriberId = new ArrayList<>();
        ArrayList<ExtractionField> listAuthorizationId = new ArrayList<>();
        ArrayList<ExtractionField> listCaseId = new ArrayList<>();
        ArrayList<ExtractionField> fieldsToRemove = new ArrayList<>();

        for (ExtractionField extractionField : extractionActivityResult.getExtractionDocument().getFields()) {
            if (extractionField.getName().equals("patient_name")) {
                documentLevelPatientName = extractionField.getValue();
                fieldsToRemove.add(extractionField);
            } else if (extractionField.getName().equals("account_id"))
                documentAccountId.add(extractionField);
            else if (extractionField.getName().equals("patient_id"))
                documentPatientId.add(extractionField);
            else if (extractionField.getName().equals("member_id"))
                fieldsToRemove.add(extractionField);
            else if (extractionField.getName().equals("subscriber_id"))
                fieldsToRemove.add(extractionField);
            else if (extractionField.getName().equals("authorization_id")) {
                fieldsToRemove.add(extractionField);
            } else if (extractionField.getName().equals("case_id")) {
                fieldsToRemove.add(extractionField);
            }
        }

        ArrayList<String> similarPatientNames = new ArrayList<>();
        for (int i = 0; i < extractionActivityResult.getPages().size(); i++) {
            for (ExtractionField extractionField : extractionActivityResult.getPages().get(i).getFields()) {
                if (extractionField.getName().equals("patient_name"))
                    listPatientNames.add(extractionField.getValue());
                else if (extractionField.getName().equals("account_id"))
                    listAccountId.add(extractionField);
                else if (extractionField.getName().equals("patient_id"))
                    listPatientId.add(extractionField);
                else if (extractionField.getName().equals("member_id"))
                    listMemberId.add(extractionField);
                else if (extractionField.getName().equals("subscriber_id"))
                    listSubscriberId.add(extractionField);
                else if (extractionField.getName().equals("authorization_id"))
                    listAuthorizationId.add(extractionField);
                else if (extractionField.getName().equals("case_id"))
                    listCaseId.add(extractionField);
            }
        }

        for (ExtractionField extractionField : fieldsToRemove) {
            extractionActivityResult.getExtractionDocument().getFields().remove(extractionField);
        }
        if (StringUtils.isNotEmpty(documentLevelPatientName)) {

            for (String name : listPatientNames) {
                Boolean isSimilar = compareListOfString(documentLevelPatientName, name);
                if (isSimilar) {
                    similarPatientNames.add(name);
                }
            }
            Collections.sort(similarPatientNames, Comparator.comparing(String::length));
            documentLevelPatientName = similarPatientNames.get((similarPatientNames.size()) - 1);
        } else if (listPatientNames.size() > 0) {
            documentLevelPatientName = listPatientNames.get(0);
        }
        if (StringUtils.isNotEmpty(documentLevelPatientName)) {
            ExtractionField newField = new SimpleExtractionField("patient_name", 0.8f, documentLevelPatientName, "patient_name");
            extractionPipelineMessage.getExtractionActivityResult().getExtractionDocument().getFields().add(newField);
        }

        if (documentAccountId.size() == 0 && listAccountId.size() > 0)
            extractionPipelineMessage.getExtractionActivityResult().getExtractionDocument().getFields().add(listAccountId.get(0));
        if (documentPatientId.size() == 0 && listPatientId.size() > 0)
            extractionPipelineMessage.getExtractionActivityResult().getExtractionDocument().getFields().add(listPatientId.get(0));
        if (listMemberId.size() > 0) {
            extractionPipelineMessage.getExtractionActivityResult().getExtractionDocument().getFields().add(listMemberId.get(0));
        }
        if (listSubscriberId.size() > 0) {
            extractionPipelineMessage.getExtractionActivityResult().getExtractionDocument().getFields().add(listSubscriberId.get(0));
        }
        if (listAuthorizationId.size() > 0) {
            extractionPipelineMessage.getExtractionActivityResult().getExtractionDocument().getFields().add(listAuthorizationId.get(0));
        }
        if (listCaseId.size() > 0) {
            extractionPipelineMessage.getExtractionActivityResult().getExtractionDocument().getFields().add(listCaseId.get(0));
        }
    }

    private static boolean compareListOfString(String firstInstance, String secondInstance) {
        firstInstance = firstInstance.toLowerCase();
        secondInstance = secondInstance.toLowerCase();
        firstInstance = firstInstance.replaceAll("[^a-zA-Z0-9]", " ");
        secondInstance = secondInstance.replaceAll("[^a-zA-Z0-9]", " ");
        String[] rulesResultList = firstInstance.split(" ");
        String[] mlResultList = secondInstance.split(" ");

        boolean status = false;
        for (String outerWord : rulesResultList) {
            for (String innerWord : mlResultList) {
                if (outerWord.equals(innerWord) && StringUtils.isNotBlank(outerWord) && StringUtils.isNotBlank(innerWord) && outerWord.length() > 1 && innerWord.length() > 1) {
                    status = true;
                    break;
                }
            }
        }
        return status;
    }
}