package com.auxolabs.cvision.pipeline.utils.field.normalizers;

public class MRNNormalizer implements FieldNormalizer<String, String> {
    @Override
    public String normalize(String mrn) {
        return mrn.toUpperCase().trim();
    }
}
