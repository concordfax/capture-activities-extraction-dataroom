package com.auxolabs.cvision.pipeline.utils.segmentation;

import com.auxolabs.cvision.models.ocr.Lines;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Groups the rows in the form of a table into an image
 */
public class RowGrouper {

    // accepted error in boxes on y plane for a row
    private final double thresholdY;

    // accepted error in boxes on x plane for a column
    private final double thresholdX;

    // accepted offsets for table
    private final double offsetX;
    private final double offsetY;

    public RowGrouper(double thresholdX, double thresholdY, double offsetX, double offsetY) {
        this.thresholdX = thresholdX;
        this.thresholdY = thresholdY;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
    }

    public List<Table> getTablesFromImageSegments(List<Lines> piBoxes) {
        List<Row> rows = findAllPossibleRows(piBoxes); // may contain duplicate
        rows.sort(Comparator.comparingDouble(Row::y));
        List<Row> dedupedRows = dedupRows(rows);
        List<Table> tables = findPossibleTables(dedupedRows);
        List<Table> result = removeSubTables(tables);
        result.forEach(Table::sortRows);
        return result;
    }

//    private List<Lines> convertToLinesObjects(HashMap<lept.PIX, lept.BOX> segments) {
//        List<Lines> piBoxes = new ArrayList<>();
//        segments.forEach((pix, box) -> {
//            piBoxes.add(new Lines(pix, box));
//        });
////		for (int i = 0; i < piBoxes.size(); i++) {
////			System.out.println("Box: "+i+" -> "+piBoxes.get(i));
////			pixWrite("box_"+i+".png", piBoxes.get(i).getPix(), IFF_PNG);
////		}
//        return piBoxes;
//    }

    private List<Row> findAllPossibleRows(List<Lines> piBoxes) {
        List<Row> rows = new ArrayList<>();
        piBoxes.forEach(piBox -> {
            List<Lines> boxesInARow = findRow(piBoxes, piBox);
            if (boxesInARow.size() > 1)
                rows.add(new Row(boxesInARow));
        });
//		for (int i = 0; i < rows.size(); i++) {
//			System.out.println("Row: "+i+" -> "+rows.get(i)+" size: "+rows.get(i).getBoxes().size());
//		}

//		rows.forEach(row -> {
//			row.getBoxes().forEach(Lines -> {
//				pixDisplayWithTitle(Lines.getPix(), 400, 100, "halftone seed", 1);
//			});
//		});
        return rows;
    }

    private List<Table> findPossibleTables(List<Row> rows) {
        List<Table> tables = new ArrayList<>();
        List<Row> rowsAlreadyInTable = new ArrayList<>();
        rows.forEach(row -> {
            if (!rowsAlreadyInTable.contains(row)) {
                List<Row> possibleTable = new ArrayList<Row>();
                possibleTable.add(row);
                rows.forEach(row1 -> {
                    if (!rowsAlreadyInTable.contains(row1)) {
                        if (!row.equals(row1) && isPossibleTable(row, row1) && !possibleTable.contains(row1)) {
                            possibleTable.add(row1);
                        }
                    }
                });
                if (possibleTable.size() > 1) {
                    tables.add(new Table(new ArrayList<Row>(possibleTable)));
                    rowsAlreadyInTable.addAll(possibleTable);
                }
            }
        });
        return tables;
    }

    private void addRowsWithMissingValuesToTable(List<Table> tables, List<Row> rows){
//        tab
    }

    private List<Row> dedupRows(List<Row> rows) {
        List<Row> result = new ArrayList<>();
        Row[] arr = rows.toArray(new Row[0]);
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != null) {
                for (int j = i + 1; j < arr.length; j++) {
                    if (arr[j] != null) {
                        if (arr[i].equals(arr[j])) {
                            arr[j] = null;
                        }
                    }
                }
            }
        }
        for (Row row : arr) {
            if (row != null) {
                result.add(row);
            }
        }
        return result;
    }

    private List<Table> removeSubTables(List<Table> tables) {
        List<Table> result = new ArrayList<>();
        if (tables.size() > 0) {
            Table[] arr = tables.toArray(new Table[tables.size()]);
            for (int i = 0; i < arr.length; i++) {
                if (arr[i] != null) {
                    for (int j = i + 1; j < arr.length; j++) {
                        if (arr[j] != null) {
                            if (arr[i].equals(arr[j])) {
                                arr[j] = null;
                            } else if (arr[i].contains(arr[j])) {
                                arr[j] = null;
                            } else if (arr[j].contains(arr[i])) {
                                arr[i] = null;
                                break;
                            }
                        }
                    }
                }
            }
            for (Table table : arr) {
                if (table != null) {
                    result.add(table);
                }
            }
        }
        return result;
    }

    private boolean isPossibleTable(Row row1, Row row2) {
        if (row1.getBoxes().size() == row2.getBoxes().size()) {
            Double distMultiplier = (Math.abs(row1.getRect().getY() - row2.getRect().getY()) / (row1.getRect().getHeight() + offsetY));
            return Math.abs(row1.getRect().getX() - row2.getRect().getX()) <= thresholdX
                    && Math.abs(row1.getRect().getWidth() - row2.getRect().getWidth()) <= thresholdX
                    && (Math.abs(row1.getRect().getY() - row2.getRect().getY()) % (row1.getRect().getHeight() + offsetY) <= distMultiplier * thresholdY)
//				&& Math.abs(row1.getRect().getHeight() - row2.getRect().getHeight()) <= thresholdY
                    ;
        }
        return false;
    }

    private List<Lines> findRow(List<Lines> segments, Lines box) {
        List<Lines> acceptedRow = new ArrayList<>();
//		System.out.println(box.toString());
        segments.forEach(piBoxT -> {
            if (!box.equals(piBoxT) && isBoxInRangeForRow(box, piBoxT)) {
                acceptedRow.add(piBoxT);
//				System.out.println("\t" + piBoxT);
            }
        });
        if(acceptedRow.size()>0) {
            acceptedRow.add(box);
            acceptedRow.sort(Comparator.comparingInt(Lines::getL));
        }
        return acceptedRow;
    }

    // assumption that target.x >= source.x
    private boolean isBoxInRangeForRow(Lines source, Lines target) {
        return source.x() < target.x()
                && Math.abs(source.y() - target.y()) <= thresholdY
//			&& Math.abs(source.w() - target.w()) <= thresholdX
//			&& Math.abs(source.h() - target.h()) <= thresholdY
//			&& Math.abs(source.x() + source.w() - target.x()) >= offsetX
                ;
    }
}
