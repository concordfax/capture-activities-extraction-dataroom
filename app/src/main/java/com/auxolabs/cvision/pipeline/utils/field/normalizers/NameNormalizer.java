package com.auxolabs.cvision.pipeline.utils.field.normalizers;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static edu.stanford.nlp.util.StringUtils.capitalize;


/**
 * Normalize names to Nextstep Name format
 * Please note to avoid sending names with period in them
 */
public class NameNormalizer implements FieldNormalizer<String, String> {

    private static final String[] salutations = {"mr","dr","ms","jr","mrs","sr"};
    private static final String[] namePrefixes = {"mc"};

    private static String[] nameSplit;
    private List<String> nameSuffixes = Arrays.asList("name", "dob", "mrn", "ssn", "doe", "npi", "id", "patient");


    private String cleanName(String name){
        name = name.replaceAll("[^a-zA-Z,]"," ").replaceAll("\\s+"," ").trim();
        nameSplit = name.split(" ");
        for(String token: nameSuffixes){
            for(String split: nameSplit) {
                if (split.toLowerCase().equals(token)) {
                    name = name.replace(split, "");
                }
            }
        }
        return StringUtils.normalizeSpace(name);
    }
    public String normalize(String name){

        String salutation = null;
        if (name == null || name.isEmpty()){
            return name;
        }

        name = cleanName(name);

        if (nameContainsHyphen(name)){
            name = normalizeHyphenInName(name);
        }

        if ((salutation = nameContainsSalutations(name)) != null){
            name = name.replace(salutation, " ").trim();
        }

        String commaNormalizedName = name;
        if (nameContainsComma(name)){
            commaNormalizedName = normalizeComma(name);
        }

        String normalizedName = normalizeForInitials(commaNormalizedName);
        return addSalutation(normalizedName, salutation);
    }

    private String checkIfWordContainsNamePrefixes(String name){
        for (String namePrefix : namePrefixes) {
            if(name.startsWith(namePrefix)){
                return namePrefix;
            }
        }
        return null;
    }

    private boolean nameContainsComma(String name){
        return name.contains(",");
    }

    private String normalizeComma(String name){
        String[] wordsInName = name.split(",");
        formatWordsInName(wordsInName);
        if (wordsInName.length == 2){
            swapWords(wordsInName);
        }
        return appendSpacesBetween(wordsInName);
    }

    private String[] formatWordsInName(String[] wordsInName){
        for (int i = 0; i<wordsInName.length; i++){
            wordsInName[i] = wordsInName[i].trim();
            wordsInName[i] = wordsInName[i].toLowerCase();
        }
        return wordsInName;
    }

    private void swapWords(String[] wordsInName){
        String temp = wordsInName[0];
        wordsInName[0] = wordsInName[1];
        wordsInName[1] = temp;
    }

    private String normalizeForInitials(String name){
        List<String> wordsList = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i<name.length();i++){
            // TODO: 2019-01-30 optimise this to use Character.isAlpha
            if ((name.charAt(i)>=65 && name.charAt(i)<=90)||(name.charAt(i)>=97&&(name.charAt(i)<=122))){
                sb.append(name.charAt(i));
            }
            else if ((name.charAt(i) == 160 || name.charAt(i) == 32 ) && sb.length() !=0 ) {
                wordsList.add(sb.toString());
                sb.delete(0,sb.length());
            }
        }
        if (sb.length()>0) {
            wordsList.add(sb.toString());
        }
        String[] spaceSplittedWords = wordsList.toArray(new String[wordsList.size()]);

        if (spaceSplittedWords.length == 3 && !nameHasInitial(spaceSplittedWords)){
            spaceSplittedWords[1] = spaceSplittedWords[1].charAt(0)+"";
        }

        addPeriodForAllInitials(spaceSplittedWords);

        return appendSpacesBetween(capitalizeName(formatWordsInName(spaceSplittedWords)));
    }

    private void addPeriodForAllInitials(String[] partsOfName){
        for (int i = 0; i < partsOfName.length; i++) {
            if(partsOfName[i].length()==1)
                partsOfName[i] = partsOfName[i]+".";
        }
    }

    private String appendSpacesBetween(String[] words){
        StringBuilder spaceSeperatedWords = new StringBuilder();
        for (String word : words){
            spaceSeperatedWords.append(word);
            spaceSeperatedWords.append(" ");
        }
        return spaceSeperatedWords.toString().trim();
    }

    private boolean nameContainsHyphen(String name){
        return name.contains("-");
    }

    private String normalizeHyphenInName(String name){
        return name.replaceAll("-"," ");
    }

    private String nameContainsSalutations(String name){
        String lowerCasedName = name.toLowerCase();
        String[] words = lowerCasedName.split("\\s+");
        List<String> salutationList = Arrays.asList(salutations);
        for (String salutation : salutationList){
            if (words[0].equals(salutation) || words[0].equals(salutation.substring(0,salutation.length()-1))){
                return name.substring(0,words[0].length());
            }
        }
        return null;
    }

    private String addSalutation(String name, String salutation){
        return salutation != null ? formatSalutation(salutation)+" "+name : name;
    }

    private String formatSalutation(String salutation){
        String tempSalutation = salutation.toLowerCase();
        return capitalize(tempSalutation)+".";
    }

    private boolean nameHasInitial(String[] spaceSplittedWords){
        for (int i = 0; i < spaceSplittedWords.length; i++) {
            String s = spaceSplittedWords[i];
            int characterCount = 0;
            for (int j = 0; j < s.length(); j++) {
                if (Character.isLetter(s.charAt(j))) {
                    characterCount++;
                }
            }
            if (characterCount == 1) {
                spaceSplittedWords[i] = s.charAt(0) + ".";
                return true;
            }
        }
        return false;
    }

    private String[] capitalizeName(String[] wordsInName){
        for (int i = 0; i < wordsInName.length; i++) {
            String namePrefix = null;
            if((namePrefix=checkIfWordContainsNamePrefixes(wordsInName[i]))!=null){
                //skipping word have only 2 letters eg:MC
                if(wordsInName[i].length()!=2){
                    wordsInName[i] = capitalize(namePrefix)+capitalize(wordsInName[i].substring(namePrefix.length()));
                }
                //capitalizing two two lettered word eg:Mc
                else {
                    wordsInName[i] = capitalize(namePrefix);
                }
            }else if (Character.isLetter(wordsInName[i].charAt(0))){
                wordsInName[i] = capitalize(wordsInName[i]);
            }
        }
        return wordsInName;
    }
}
