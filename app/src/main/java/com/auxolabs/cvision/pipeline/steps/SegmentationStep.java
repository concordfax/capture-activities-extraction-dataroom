package com.auxolabs.cvision.pipeline.steps;

import com.auxolabs.cvision.models.nlp.PageModel;
import com.auxolabs.cvision.pipeline.ExtractionPipelineMessage;
import com.auxolabs.cvision.streamlining.segmentation.SegmentationModule;
import com.concordfax.capture.core.logging.ILogger;
import com.concordfax.capture.core.logging.LogLevels;
import com.concordfax.capture.core.models.ocr.abbyy.Page;
import com.concordfax.capture.core.pipeline.AbstractMessage;
import com.concordfax.capture.core.pipeline.PipelineStep;
import edu.stanford.nlp.util.Timing;
import org.bytedeco.javacpp.lept;

import javax.inject.Inject;
import java.io.IOException;

public class SegmentationStep implements PipelineStep {
    private ILogger log;

    @Inject
    public SegmentationStep(ILogger logger){
        this.log = logger;
    }

    @Override
    public AbstractMessage process(AbstractMessage message) {
        Timing timing = new Timing();
        ExtractionPipelineMessage extractionPipelineMessage = (ExtractionPipelineMessage) message;
        try {
            for (int i = 0; i < extractionPipelineMessage.getPages().size(); i++) {
                PageModel pageModel = extractionPipelineMessage.getPages().get(i);
                segmentationProcess(extractionPipelineMessage.getOcrOutputDocument().getPage().get(i), pageModel);
            }
            log.trace(String.format("segmentation step completed in %d ms", timing.report()), LogLevels.Verbose, extractionPipelineMessage.getLogMaker().getProperties());
        } catch (Exception e) {
            // TODO: 21/05/18 LOGGER replace the log prop manufacturer code here
            throw new RuntimeException(e);
        }
        return message;
    }

    private void segmentationProcess(Page page,PageModel pageModel) throws IOException {
        SegmentationModule segmentationModule = new SegmentationModule(pageModel.getImageSegments());
        pageModel.setBlockList(segmentationModule.getBlocks(page));
    }

}
