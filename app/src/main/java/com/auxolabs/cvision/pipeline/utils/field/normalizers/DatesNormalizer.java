package com.auxolabs.cvision.pipeline.utils.field.normalizers;

import java.io.IOException;
import java.text.ParseException;

public class DatesNormalizer implements FieldNormalizer<String, String> {

    @Override
    public String normalize(String date) {
        CaptureDateNormalizer dateNormalizer = new CaptureDateNormalizer();
        try {
            return dateNormalizer.dateFieldFormatter(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
