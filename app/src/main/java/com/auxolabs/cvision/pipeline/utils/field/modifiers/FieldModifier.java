package com.auxolabs.cvision.pipeline.utils.field.modifiers;

import com.concordfax.capture.core.models.document.result.extraction.ExtractionField;
import com.concordfax.capture.core.models.extraction.output.ExtractorModel;
import com.concordfax.capture.core.models.extraction.output.ExtractorResultModifier;
import com.concordfax.capture.core.models.extraction.output.ExtractorType;

import java.util.HashMap;
import java.util.List;

public interface FieldModifier<T, V> {
    T modify(V currentResult, ExtractorResultModifier extractorResultModifier, List<ExtractorModel> extractors, HashMap<ExtractorType, HashMap<String, ExtractionField>> fieldRuleMap);
}
