package com.auxolabs.cvision.pipeline.utils.field.normalizers;

public interface FieldNormalizer<T, V> {
    V normalize(T field);
}
