package com.auxolabs.cvision.pipeline;

import com.auxolabs.cvision.models.nlp.PageModel;
import com.concordfax.capture.core.jobmanager.JobResponseModel;
import com.concordfax.capture.core.logging.LogMaker;
import com.concordfax.capture.core.models.document.DocumentCollectionModel;
import com.concordfax.capture.core.models.document.result.extraction.ExtractionActivityResult;
import com.concordfax.capture.core.models.environment.EnvironmentCollectionModel;
import com.concordfax.capture.core.models.extraction.ExtractionProfileCollection;
import com.concordfax.capture.core.models.mlmodel.MLModelCollection;
import com.concordfax.capture.core.models.mlmodel.result.ClassificationResult;
import com.concordfax.capture.core.models.ocr.abbyy.Document;
import com.concordfax.capture.core.models.process.ProcessCollectionModel;
import com.concordfax.capture.core.models.process.configuration.ExtractionConfiguration;
import com.concordfax.capture.core.pipeline.AbstractMessage;
import edu.stanford.nlp.util.Timing;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class ExtractionPipelineMessage extends AbstractMessage{

    private String requestId;
    private String activityId;
    private String extractionProfileId;
    private String documentId;
    private String partitionKey;

    private Integer activityIndex;

    private byte[] documentBytes;

    private ClassificationResult documentClassificationResult;
    private List<String> ocrTextFilePaths;

    private List<PageModel> pages;
    private Integer xDpi;
    private Integer yDpi;
    private String fileName;

    private DocumentCollectionModel dcm;
    private ProcessCollectionModel pcm;
    private ExtractionConfiguration extractionConfiguration;
    private EnvironmentCollectionModel environment;
    private ExtractionProfileCollection extractionProfile;

    private Document ocrOutputDocument;

    private ExtractionActivityResult extractionActivityResult;

    private JobResponseModel jobResponseModel;
    private Integer extractionActivityIdx;

    private LogMaker logMaker;

    private Timing timing;

    public ExtractionPipelineMessage() {
        this.timing = new Timing();
    }

    public void cleanup() {
        // TODO: 21/05/18 implement common cleanup code here recheck
        documentBytes = null;
        if (pages != null && !pages.isEmpty())
            pages.forEach(PageModel::cleanup);

    }

    @Override
    public void close() throws Exception {
        cleanup();
    }
}
