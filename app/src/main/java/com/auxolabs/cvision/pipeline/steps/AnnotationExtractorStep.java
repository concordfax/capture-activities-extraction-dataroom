package com.auxolabs.cvision.pipeline.steps;

import com.auxolabs.cvision.ml.MLExtractorService;
import com.auxolabs.cvision.models.nlp.PageModel;
import com.auxolabs.cvision.pipeline.ExtractionPipelineMessage;
import com.auxolabs.cvision.pipeline.steps.helpers.AnnotationExtractorHelper;
import com.auxolabs.cvision.pipeline.steps.helpers.MachineLearningExtractorHelper;
import com.concordfax.capture.core.logging.ILogger;
import com.concordfax.capture.core.logging.LogLevels;
import com.concordfax.capture.core.models.document.result.extraction.ExtractionField;
import com.concordfax.capture.core.models.extraction.ml.MLExtractorConfiguration;
import com.concordfax.capture.core.models.extraction.output.ExtractorType;
import com.concordfax.capture.core.pipeline.AbstractMessage;
import com.concordfax.capture.core.pipeline.PipelineStep;
import edu.stanford.nlp.util.Timing;

import javax.inject.Inject;
import java.util.HashMap;

public class AnnotationExtractorStep implements PipelineStep {
    private ILogger log;
    private AnnotationExtractorHelper annotationExtractorHelper;


    @Inject
    public AnnotationExtractorStep(ILogger log, MLExtractorService mlExtractorService) {
        this.log = log;
        this.annotationExtractorHelper = new AnnotationExtractorHelper(mlExtractorService);

    }
    @Override
    public AbstractMessage process(AbstractMessage message) {
        ExtractionPipelineMessage extractionPipelineMessage = (ExtractionPipelineMessage) message;
        Timing timing = new Timing();
        MLExtractorConfiguration extractorConfiguration = null;
        for (int i = 0; i < extractionPipelineMessage.getExtractionProfile().getExtractors().size(); i++) {
            if (extractionPipelineMessage.getExtractionProfile().getExtractors().get(i).getExtractorType().equals(ExtractorType.MachineLearningID)) {
                extractorConfiguration = (MLExtractorConfiguration) extractionPipelineMessage.getExtractionProfile().getExtractors().get(i);
                break;
            }
        }
        for (PageModel pageModel : extractionPipelineMessage.getPages()) {
            HashMap<String, ExtractionField> triggeredFields;
            try {
                triggeredFields = this.annotationExtractorHelper.processPage(extractorConfiguration,pageModel);
                pageModel.getExtractorToRuleField().put(ExtractorType.MachineLearningID, triggeredFields);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        log.trace(String.format("completed machine learning extraction for id in %d ms", timing.stop()), LogLevels.Verbose, extractionPipelineMessage.getLogMaker().getProperties());
        return message;
    }

}
