package com.auxolabs.cvision.pipeline.utils.segmentation;

import java.awt.geom.Rectangle2D;
import java.util.Collections;
import java.util.List;

public class Table {

    private List<Row> rows;
    private Rectangle2D.Double rect;

    public Table(List<Row> rows) {
        this.rows = rows;
        Row xMinRow = null;
        Row xMaxRow = null;
        Row yMinRow = null;
        Row yMaxRow = null;
        Double width = 0.0;
        for (Row box : this.rows) {
            if (xMinRow == null)
                xMinRow = box;
            if (xMaxRow == null)
                xMaxRow = box;
            if (yMinRow == null)
                yMinRow = box;
            if (yMaxRow == null)
                yMaxRow = box;
            if (xMinRow.x() > box.x()) {
                xMinRow = box;
            } else if (xMaxRow.x() < box.x()) {
                xMaxRow = box;
            }
            if (yMinRow.y() > box.y()) {
                yMinRow = box;
            } else if (yMaxRow.y() < box.y()) {
                yMaxRow = box;
            }
            if (width < box.w()) {
                width = box.w();
            }
        }
        if (xMinRow != null && xMaxRow != null && yMinRow != null && yMaxRow != null) {
            this.rect = new Rectangle2D.Double(xMinRow.x(), yMaxRow.y(), width, yMaxRow.y() - yMinRow.y());
        } else {
            this.rect = new Rectangle2D.Double(0, 0, 0, 0);
        }
    }

    public List<Row> getRows() {
        return rows;
    }

    public void setRows(List<Row> rows) {
        this.rows = rows;
    }

    public Rectangle2D getRect() {
        return rect;
    }

    public void setRect(Rectangle2D.Double rect) {
        this.rect = rect;
    }

    public boolean equals(Table o) {
        return rect.equals(o.rect);
    }

    public boolean contains(Table o) {
        return rect.contains(o.rect);
    }

    @Override
    public String toString() {
        return "t - x: " + rect.getX() + ", y: " + rect.getY() + ", w: " + rect.getWidth() + ", h: " + rect.getHeight();
    }

    public void sortRows() {
        if (this.rows != null && !this.rows.isEmpty()) {
            this.rows.forEach(Row::sort);
            Collections.sort(this.rows, (row1, row2) -> {
                if (row1.y() > row2.y()) {
                    return 1;
                } else if (row1.y() < row2.y()) {
                    return -1;
                }
                return 0;
            });
        }
    }


}
