package com.auxolabs.cvision.pipeline.utils.segmentation;

import com.auxolabs.cvision.models.ocr.Lines;

import java.awt.geom.Rectangle2D;
import java.util.Collections;
import java.util.List;

public class Row {

    private List<Lines> boxes;
    private Rectangle2D.Double rect;

    public Row(List<Lines> boxes) {
        this.boxes = boxes;
        Lines xMinBox = null;
        Lines xMaxBox = null;
        Lines yMinBox = null;
        Lines yMaxBox = null;
        Double height = 0.0;
        for (Lines box : this.boxes) {
            if (xMinBox == null)
                xMinBox = box;
            if (xMaxBox == null)
                xMaxBox = box;
            if (yMinBox == null)
                yMinBox = box;
            if (yMaxBox == null)
                yMaxBox = box;
            if (xMinBox.x() > box.x()) {
                xMinBox = box;
            } else if (xMaxBox.x() < box.x()) {
                xMaxBox = box;
            }
            if (yMinBox.y() > box.y()) {
                yMinBox = box;
            } else if (yMaxBox.y() < box.y()) {
                yMaxBox = box;
            }
            if (height < box.h()) {
                height = box.h();
            }
        }
        if (xMinBox != null && xMaxBox != null && yMinBox != null && yMaxBox != null) {
            this.rect = new Rectangle2D.Double(xMinBox.x(), yMaxBox.y(), xMaxBox.x() - xMinBox.x(), height);
        } else {
            this.rect = new Rectangle2D.Double(0, 0, 0, 0);
        }
    }

    public List<Lines> getBoxes() {
        return boxes;
    }

    public void setBoxes(List<Lines> boxes) {
        this.boxes = boxes;
    }

    public Rectangle2D getRect() {
        return rect;
    }

    public void setRect(Rectangle2D.Double rect) {
        this.rect = rect;
    }

    // if this.boxes contains all boxes from o.boxes and are bigger in size, return true
    public boolean contains(Row o) {
        if (boxes.isEmpty() || o.boxes.isEmpty()) {
            return false;
        }
        if (this.equals(o)) {
            return false;
        }
        if (this.boxes.size() > o.boxes.size()) {
            boolean isSubset = true;
            for (Lines box : o.boxes) {
                if (!this.boxes.contains(box)) {
                    isSubset = false;
                    break;
                }
            }
            return isSubset;
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Row that = (Row) o;

        if (boxes != null && that.boxes != null && !boxes.isEmpty() && !that.boxes.isEmpty() && this.boxes.size() == that.boxes.size()) {
            boolean isEqual = true;
            for (Lines box : that.boxes) {
                if (!this.boxes.contains(box)) {
                    isEqual = false;
                    break;
                }
            }
            return isEqual;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return (rect.getX() + ":" + rect.getY() + ":" + rect.getWidth() + ":" + rect.getHeight()).hashCode();
    }

    public Double x() {
        return rect.getX();
    }

    public Double y() {
        return rect.getY();
    }

    public Double w() {
        return rect.getWidth();
    }

    public Double h() {
        return rect.getHeight();
    }

    @Override
    public String toString() {
        return "r - x: " + rect.getX() + ", y: " + rect.getY() + ", w: " + rect.getWidth() + ", h: " + rect.getHeight();
    }

    public void sort() {
        if (this.boxes != null && !this.boxes.isEmpty()) {
            Collections.sort(this.boxes, (box1, box2) -> {
                if (box1.x() > box2.x()) {
                    return 1;
                } else if (box1.x() < box2.x()) {
                    return -1;
                }
                return 0;
            });
        }
    }
}
