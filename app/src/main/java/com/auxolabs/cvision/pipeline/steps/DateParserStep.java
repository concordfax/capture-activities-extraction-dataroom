package com.auxolabs.cvision.pipeline.steps;

import com.auxolabs.capture.dateparser.DPEngineService;
import com.auxolabs.cvision.models.nlp.PageModel;
import com.auxolabs.cvision.pipeline.ExtractionPipelineMessage;
import com.auxolabs.cvision.pipeline.steps.helpers.DateParserHelper;
import com.concordfax.capture.core.logging.ILogger;
import com.concordfax.capture.core.logging.LogLevels;
import com.concordfax.capture.core.models.document.result.extraction.ExtractionField;
import com.concordfax.capture.core.models.extraction.output.ExtractorType;
import com.concordfax.capture.core.models.extraction.rexyd.RexyDExtractorConfiguration;
import com.concordfax.capture.core.pipeline.AbstractMessage;
import com.concordfax.capture.core.pipeline.PipelineStep;
import com.google.gson.Gson;
import edu.stanford.nlp.util.Timing;

import javax.inject.Inject;
import java.util.HashMap;

public class DateParserStep implements PipelineStep {
    private ILogger log;
    private DateParserHelper dateParserHelper;
    private Gson gson;

    @Inject
    public DateParserStep(ILogger log, DPEngineService dpEngineService) {
        this.log = log;
        this.dateParserHelper = new DateParserHelper(dpEngineService);
        this.gson = new Gson();

    }
    @Override
    public AbstractMessage process(AbstractMessage message) {
        ExtractionPipelineMessage extractionPipelineMessage = (ExtractionPipelineMessage) message;
        Timing timing = new Timing();
        RexyDExtractorConfiguration extractorConfiguration = null;
        for (int i = 0; i < extractionPipelineMessage.getExtractionProfile().getExtractors().size(); i++) {
            if (extractionPipelineMessage.getExtractionProfile().getExtractors().get(i).getExtractorType().equals(ExtractorType.REXYD)) {
                extractorConfiguration = (RexyDExtractorConfiguration) extractionPipelineMessage.getExtractionProfile().getExtractors().get(i);
                break;
            }
        }
        for (PageModel pageModel : extractionPipelineMessage.getPages()) {
            try{
                HashMap<String, ExtractionField> tagValuePair = this.dateParserHelper.processPage(pageModel);
                pageModel.getExtractorToRuleField().put(ExtractorType.REXYD, tagValuePair);
            }
            catch (Exception e){
                throw new RuntimeException(e);
            }

        }
        log.trace(String.format("completed date candidates extraction in %d ms", timing.stop()), LogLevels.Verbose, extractionPipelineMessage.getLogMaker().getProperties());
        return message;
    }
}
