package com.auxolabs.cvision.ml.models.mlfieldextractionmodels;

public class AnnotationsItem{

	private String answer;

	private String word;

	public void setAnswer(String answer){
		this.answer = answer;
	}

	public String getAnswer(){
		return answer;
	}

	public void setWord(String word){
		this.word = word;
	}

	public String getWord(){
		return word;
	}

	@Override
 	public String toString(){
		return 
			"AnnotationsItem{" + 
			"answer = '" + answer + '\'' + 
			",word = '" + word + '\'' + 
			"}";
		}
}