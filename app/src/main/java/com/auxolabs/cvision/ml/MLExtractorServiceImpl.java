package com.auxolabs.cvision.ml;

import com.auxolabs.cvision.ml.models.mlfieldextractionmodels.MLRequestModel;
import com.auxolabs.cvision.ml.models.mlfieldextractionmodels.MLResponseModel;
import com.auxolabs.cvision.ml.models.request.MLExtractorRequestModel;
import com.auxolabs.cvision.ml.models.request.MLIDRequestModel;
import com.auxolabs.cvision.ml.models.response.MLExtractorResponseModel;
import com.auxolabs.cvision.ml.models.response.MLIDResponseModel;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;

public class MLExtractorServiceImpl implements MLExtractorService {
    private Gson gson;
    private CloseableHttpClient httpClient;

    @Inject
    public MLExtractorServiceImpl(){
        this.gson = new Gson();
        this.httpClient = HttpClients.createDefault();
    }

    private static final Type mlResponse = new TypeToken<MLResponseModel>() {
    }.getType();

    @Override
    public MLExtractorResponseModel process(String endPoint, MLExtractorRequestModel mlExtractorRequestModel) {

            HttpPost httpPost = null;
            CloseableHttpResponse response = null;
            try{
                httpPost = new HttpPost(endPoint);
                httpPost.setEntity(new StringEntity(gson.toJson(mlExtractorRequestModel)));
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");
                response = this.httpClient.execute(httpPost);
                HttpEntity responseEntity = response.getEntity();
                MLExtractorResponseModel responseModel = this.gson.fromJson(new InputStreamReader(responseEntity.getContent()), MLExtractorResponseModel.class);
                return responseModel;

            }catch (Exception e){
                throw new RuntimeException(e.getMessage());
            }finally {
                if (response != null) {
                    try {
                        response.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (httpPost != null) {
                    httpPost.releaseConnection();
                }
            }

        }
    @Override
    public MLIDResponseModel process (String endPoint, MLIDRequestModel mlidRequestModel){
        HttpPost httpPost = null;
        CloseableHttpResponse response = null;
        try{
            httpPost = new HttpPost(endPoint);
            httpPost.setEntity(new StringEntity(gson.toJson(mlidRequestModel)));
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            response = this.httpClient.execute(httpPost);
            HttpEntity responseEntity = response.getEntity();
            MLIDResponseModel responseModel = this.gson.fromJson(new InputStreamReader(responseEntity.getContent()), MLIDResponseModel.class);
            return responseModel;

        }catch (Exception e){
            throw new RuntimeException(e.getMessage());
        }finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (httpPost != null) {
                httpPost.releaseConnection();
            }
        }
    }
    @Override
    public MLResponseModel requestService(String endPoint, MLRequestModel mlRequestModel) {
        HttpPost httpPost = null;
        CloseableHttpResponse response = null;
        try {
            httpPost = new HttpPost(endPoint);
            httpPost.setEntity(new StringEntity(gson.toJson(mlRequestModel)));
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            response = this.httpClient.execute(httpPost);
            HttpEntity responseEntity = response.getEntity();
            String entityOutput = EntityUtils.toString(responseEntity);
            MLResponseModel mlResponseModel = this.gson.fromJson(entityOutput,mlResponse);
            return mlResponseModel;
        } catch (Exception e){
            throw new RuntimeException(e.getMessage());
        }finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (httpPost != null) {
                httpPost.releaseConnection();
            }
        }
    }
}


