package com.auxolabs.cvision.ml.models.response;

import lombok.Data;

@Data
public class MLPredictResponseModel {
    private String npiValue;
    private float confidence;
}
