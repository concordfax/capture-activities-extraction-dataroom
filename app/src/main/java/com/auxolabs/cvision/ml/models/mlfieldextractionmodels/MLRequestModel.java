package com.auxolabs.cvision.ml.models.mlfieldextractionmodels;

import lombok.Data;

@Data
public class MLRequestModel {

    private String requestText;
}
