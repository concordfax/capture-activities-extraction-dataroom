package com.auxolabs.cvision.ml.models.mlfieldextractionmodels;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;
import java.util.List;

public class MLResponseModel{

	private List<ResultItem> result;

	public void setResult(List<ResultItem> result){
		this.result = result;
	}

	public List<ResultItem> getResult(){
		return result;
	}

	@Override
 	public String toString(){
		return 
			"MLResponseModel{" + 
			"result = '" + result + '\'' + 
			"}";
		}
}