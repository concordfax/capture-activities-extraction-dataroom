package com.auxolabs.cvision.ml.models.response;

import lombok.Data;

import java.util.List;

@Data
public class MLExtractorResponseModel {
    private List<MLPredictResponseModel> prediction;
}
