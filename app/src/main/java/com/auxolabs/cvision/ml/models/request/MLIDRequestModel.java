package com.auxolabs.cvision.ml.models.request;

import lombok.Data;

@Data
public class MLIDRequestModel {
    private String queryString;
}
