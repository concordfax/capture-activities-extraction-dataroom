package com.auxolabs.cvision.ml.models.response;

import lombok.Data;

@Data
public class MLIDResponseModel {
    private String patient;
    private  String member;
    private  String caseid;
    private  String auth;
    private  String subscriber;
    private  String account;
    private  String other;
    private  String noTag;
    private String patientIDTag;
    private  String memberIDTag;
    private  String caseIDTag;
    private  String authIDTag;
    private  String subscriberIDTag;
    private  String accountIDTag;
    private  String otherIDTag;
}
