package com.auxolabs.cvision.ml.models.mlfieldextractionmodels;

import lombok.Data;

@Data
public class MLFields {
    private String patient_name;
    private String patient_dob;
    private String patient_ssn;
    private String patient_mrn;
    private String doe;
    private String npi;
    private String id;
    private String non_patient_names;

}
