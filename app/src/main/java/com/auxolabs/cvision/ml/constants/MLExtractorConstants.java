package com.auxolabs.cvision.ml.constants;

public class MLExtractorConstants {

    public class TriggerEntities{
        public static final String NPI = "npi";
        public static final String ID = "id";
        public static final String patientID = "patient_id";
        public static final String caseID = "case_id";
        public static final String memberID = "member_id";
        public static final String subscriberID = "subscriber_id";
        public static final String accountID = "account_id";
        public static final String authID = "auth_id";
//        public static final String noTagID = "notag_id";
    }

    public class SpecialParams{
        public static final int PROXIMITY_THRESHOLD = 300;
    }
}
