package com.auxolabs.cvision.ml.models.request;

import com.auxolabs.capture.positional.models.TokenModel;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class MLExtractorRequestModel {
    private double pageWidth;
    private double pageHeight;
    private List<TokenModel> predictRequest;

    public MLExtractorRequestModel(){
        this.predictRequest = new ArrayList<>();
    }
}
