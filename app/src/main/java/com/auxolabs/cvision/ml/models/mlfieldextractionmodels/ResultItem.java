package com.auxolabs.cvision.ml.models.mlfieldextractionmodels;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;
import java.util.List;

public class ResultItem{

	private List<AnnotationsItem> annotations;

	private String queryString;

	public void setAnnotations(List<AnnotationsItem> annotations){
		this.annotations = annotations;
	}

	public List<AnnotationsItem> getAnnotations(){
		return annotations;
	}

	public void setQueryString(String queryString){
		this.queryString = queryString;
	}

	public String getQueryString(){
		return queryString;
	}

	@Override
 	public String toString(){
		return 
			"ResultItem{" + 
			"annotations = '" + annotations + '\'' + 
			",queryString = '" + queryString + '\'' + 
			"}";
		}
}