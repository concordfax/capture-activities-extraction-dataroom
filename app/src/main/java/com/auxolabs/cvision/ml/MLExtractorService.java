package com.auxolabs.cvision.ml;

import com.auxolabs.cvision.ml.models.mlfieldextractionmodels.MLRequestModel;
import com.auxolabs.cvision.ml.models.mlfieldextractionmodels.MLResponseModel;
import com.auxolabs.cvision.ml.models.request.MLExtractorRequestModel;
import com.auxolabs.cvision.ml.models.request.MLIDRequestModel;
import com.auxolabs.cvision.ml.models.response.MLExtractorResponseModel;
import com.auxolabs.cvision.ml.models.response.MLIDResponseModel;

public interface MLExtractorService {

    MLExtractorResponseModel process(String endPoint, MLExtractorRequestModel mlExtractorRequestModel);
    MLIDResponseModel process (String endPOint, MLIDRequestModel mlidRequestModel);
    MLResponseModel requestService(String endPoint, MLRequestModel mlRequestModel);
}
