package com.auxolabs.cvision.di;
//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//


import com.concordfax.capture.core.web.helpers.HealthWatchable;
import com.google.gson.Gson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;

@Path("health-check")
@Api("Health Check Resource")
public class HealthcheckResourceDropwizard {
    private Gson gson = new Gson();
    private HealthWatchable[] healthWatchables;

    @Inject
    public HealthcheckResourceDropwizard(HealthWatchable... healthWatchables) {
        if (healthWatchables != null) {
            this.healthWatchables = healthWatchables;
        }

    }

    @Path("metrics")
    @GET
    @Produces({"application/json"})
    @ApiOperation("Metrics")
    public Response ping() throws UnsupportedEncodingException {
        int httpStatus = 200;
//        TreeMap<String, Map> responseMap = new TreeMap();
//        if (this.healthWatchables != null && this.healthWatchables.length > 0) {
//            HealthWatchable[] var3 = this.healthWatchables;
//            int var4 = var3.length;
//
//            for(int var5 = 0; var5 < var4; ++var5) {
//                HealthWatchable healthWatchable = var3[var5];
//                responseMap.put(healthWatchable.name(), healthWatchable.result());
//                if (!healthWatchable.isHealthy()) {
//                    httpStatus = healthWatchable.httpStatus();
//                    break;
//                }
//            }
//        }



    return Response.status(httpStatus).build();
    }
}
