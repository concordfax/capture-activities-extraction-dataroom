package com.auxolabs.cvision.di;

import com.auxolabs.cvision.CaptureApplication;
import com.auxolabs.cvision.web.config.CaptureConfig;
import com.concordfax.capture.core.db.MongoConfigModel;
import com.concordfax.capture.core.jobmanager.JobManagerConfig;
import com.concordfax.capture.core.logging.LogConfig;
import com.concordfax.capture.core.logging.azure.AppInsightsLogConfig;
import dagger.Module;
import dagger.Provides;

@Module
public class ConfigModule {

    @Provides
    CaptureConfig providesConfig() {
        return CaptureApplication.getCaptureConfig();
    }

    @Provides
    MongoConfigModel providesDBConfig(CaptureConfig captureConfig) {
        MongoConfigModel mongoConfigModel = captureConfig.getDb();
        String dbName = System.getenv("Environment__DatabaseName");
        String dbConnString = System.getenv("Environment__DatabaseConnectionString");

        if (dbName != null && dbConnString != null) {
            mongoConfigModel = new MongoConfigModel();
            mongoConfigModel.setDbName(dbName);
            mongoConfigModel.setConnString(dbConnString);
        }
        return mongoConfigModel;
    }

    @Provides
    LogConfig providesLoggerConfig(CaptureConfig captureConfig) {
        LogConfig logConfig = captureConfig.getLogger();
        String instrumentationKey = System.getenv("ApplicationInsights__InstrumentationKey");
        if (instrumentationKey != null) {
            logConfig = new AppInsightsLogConfig(instrumentationKey);
        }
        return logConfig;
    }

    @Provides
    JobManagerConfig providesJobManagerConfig(CaptureConfig captureConfig) {
        JobManagerConfig jobManagerConfig = captureConfig.getJobManager();
        String ip = System.getenv("MY_POD_IP");
        if(null == ip)
            throw new RuntimeException("Pod ip not found");
        jobManagerConfig.setProcessorUrl(String.format("http://%s/job-manager/getJob", ip));
        String maxActiveJobs = System.getenv("MaximumActiveJob_Count");
        if(maxActiveJobs != null){
            jobManagerConfig.setMaxActiveJobs(Integer.parseInt(maxActiveJobs));
        }
        return jobManagerConfig;
    }

}
