package com.auxolabs.cvision.di;

import com.auxolabs.cvision.web.init.CaptureInitializer;
import com.auxolabs.cvision.web.resources.ExtendedHealthCheckResource;
import com.auxolabs.cvision.web.resources.ExtendedJobManagerResource;
import dagger.Component;

import javax.inject.Singleton;

@Component(modules = {ConfigModule.class, ComponentsModule.class, MetricsComponentsModule.class})
@Singleton
public interface ICapture {
    CaptureInitializer ocrInit();
    ExtendedJobManagerResource jobManagerResource();
    ExtendedHealthCheckResource healthCheckResource();

}
