package com.auxolabs.cvision.di;

import com.auxolabs.capture.dateparser.DPEngineService;
import com.auxolabs.capture.mandrake.MandrakeService;
import com.auxolabs.capture.positional.PositionalService;
import com.auxolabs.cvision.metrics.DBHealthWatchable;
import com.auxolabs.cvision.metrics.pipeline.*;
import com.auxolabs.cvision.ml.MLExtractorService;
import com.auxolabs.cvision.pipeline.steps.*;
import com.concordfax.capture.core.jobmanager.JobManagerHealthCheck;
import com.concordfax.capture.core.pipeline.Pipeline;
import com.concordfax.capture.core.web.helpers.HealthWatchable;
import dagger.Module;
import dagger.Provides;

import javax.inject.Named;
import javax.inject.Singleton;

import static com.auxolabs.cvision.constants.CaptureAppConstants.Pipeline.METRICS_PIPELINE;

@Module(includes = {ConfigModule.class})
public class MetricsComponentsModule {

    @Provides
    @Singleton
    HealthWatchable[] providesHealthWatchables(JobManagerHealthCheck jobManagerHealthCheck, DBHealthWatchable dbHealthWatchable, PipelineHealthWatchable pipelineHealthWatchable){
        HealthWatchable[] healthWatchables = new HealthWatchable[3];
        healthWatchables[0] = jobManagerHealthCheck;
        healthWatchables[1] = dbHealthWatchable;
        healthWatchables[2] = pipelineHealthWatchable;
        return healthWatchables;
    }

    @Provides
    @Named(METRICS_PIPELINE)
    Pipeline providesPipeline(MetricsLogger logger, MetricsDB idbService, MetricsBlobService blobService, MLExtractorService mlExtractorService,
                              MandrakeService mandrakeService, PositionalService positionalService, DPEngineService dpEngineService,
                              MetricsModelServicesClient modelServicesClient, MetricsDependencyResolver dependencyResolverClient , MetricsAnnotationsService metricsAnnotationsService) {
        return new HealthCheckPipeline(
                new DocumentPropsFetcherStep(logger, idbService, blobService, dependencyResolverClient),
                new DocumentClassificationStep(logger, modelServicesClient, blobService),
                new ImageProcessingStep(logger),
                new OCROutputProcessorStep(logger),
                new SegmentationStep(logger),
                new AnnotationAndMappingStep(logger,metricsAnnotationsService),
                new MandrakeExtractorStep(logger, mandrakeService),
                new PositionalExtractorStep(logger, positionalService),
                new MachineLearningExtractorStep(logger,mlExtractorService),
                new AnnotationExtractorStep(logger,mlExtractorService),
                new DateParserStep(logger,dpEngineService),
                new AnnotationExtractorStep(logger,mlExtractorService),
                new OutputConfigurationProcessorStep(logger),
                new ResultWriterStep(logger, idbService));
    }
}
