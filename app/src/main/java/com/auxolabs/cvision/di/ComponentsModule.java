package com.auxolabs.cvision.di;

import com.auxolabs.capture.annotator.AnnotationsProcessor;
import com.auxolabs.capture.annotator.AnnotatorService;
import com.auxolabs.capture.dateparser.DPEngineService;
import com.auxolabs.capture.dateparser.application.DateParserServiceImpl;
import com.auxolabs.cvision.db.AzureCosmosDBService;
import com.auxolabs.cvision.db.IDBService;
import com.auxolabs.cvision.ml.MLExtractorService;
import com.auxolabs.cvision.ml.MLExtractorServiceImpl;
import com.auxolabs.cvision.pipeline.ExtractionPipeline;
import com.auxolabs.cvision.pipeline.steps.*;
import com.auxolabs.cvision.pipeline.steps.helpers.NpiExtractionStep;
import com.auxolabs.cvision.utils.ResourceCleanup;
import com.auxolabs.capture.mandrake.MandrakeService;
import com.auxolabs.capture.mandrake.application.MandrakeServiceImpl;
import com.auxolabs.capture.positional.PositionalService;
import com.auxolabs.capture.positional.application.PositionalServiceImpl;
import com.auxolabs.cvision.utils.TempLogger;
import com.auxolabs.cvision.web.config.CaptureConfig;
import com.concordfax.capture.core.jobmanager.JobManager;
import com.concordfax.capture.core.jobmanager.JobManagerConfig;
import com.concordfax.capture.core.logging.ILogger;
import com.concordfax.capture.core.logging.LogConfig;
import com.concordfax.capture.core.logging.azure.AppInsightsLogConfig;
import com.concordfax.capture.core.logging.azure.AzureAppInsightsLogging;
import com.concordfax.capture.core.pipeline.Pipeline;
import com.concordfax.capture.core.storage.blobstore.IBlobService;
import com.concordfax.capture.core.storage.blobstore.azure.AzureBlobService;
import com.concordfax.capture.core.utils.dependency.DependencyResolverClient;
import com.concordfax.capture.core.utils.dependency.IDependencyResolverClient;
import com.concordfax.capture.core.utils.mlmodel.ModelClientService;
import com.concordfax.capture.core.utils.mlmodel.ModelServicesClient;
import dagger.Module;
import dagger.Provides;

import javax.inject.Named;
import javax.inject.Singleton;

import static com.concordfax.capture.core.constants.CaptureAppConstants.JobManager.JOB_MANAGER_PIPELINE;


@Module(includes = {ConfigModule.class})
public class ComponentsModule {
    public static String dependencyResolver(String docId, String type){
        String document_folder_path ="/home/nstest-admin/Documents/prod_docs/";
        if(type.equals("text"))
        return document_folder_path+docId+"/"+type;
        else if(type.equals("images"))
            return document_folder_path+docId+"/"+type;
        else if(type.equals("xml"))
            return document_folder_path+docId+"/xml/metadata.xml";
        else
            return "";
    }

    @Provides
    @Singleton
    IBlobService providesFileStore(CaptureConfig captureConfig) {
        AzureBlobService azureBlobService = new AzureBlobService();
        return azureBlobService;
    }

    @Provides
    @Singleton
    ILogger providesLogger(LogConfig logConfig) {
        AzureAppInsightsLogging azureAppInsightsLogging = new AzureAppInsightsLogging((AppInsightsLogConfig)logConfig);
//        return azureAppInsightsLogging;
        return new TempLogger();
    }

    @Provides
    @Singleton
    JobManager providesJobManager(JobManagerConfig jobManagerConfig, @Named(JOB_MANAGER_PIPELINE) Pipeline pipeline){
        return new JobManager(jobManagerConfig, pipeline);
    }

    @Provides
    @Singleton
    AnnotatorService providesAnnotatorService(AnnotationsProcessor annotationsProcessor){
        return annotationsProcessor;
    }

    @Provides
    @Singleton
    IDBService providesDB(AzureCosmosDBService mockDBService) {
        return mockDBService;
    }

    @Provides
    @Singleton
    MandrakeService providesMandrake(MandrakeServiceImpl mandrakeService) {
        return mandrakeService;
    }

    @Provides
    @Singleton
    PositionalService providesPositionalEngine(PositionalServiceImpl positionalService) {
        return positionalService;
    }

    @Provides
    @Singleton
    DPEngineService providesDateParserEngine(DateParserServiceImpl dateParserService){return dateParserService;}

    @Provides
    @Singleton
    ResourceCleanup providesCleaner() {
        return new ResourceCleanup();
    }

    @Provides
    @Singleton
    ModelClientService providesModelServicesClient(ModelServicesClient modelServicesClient){
        return modelServicesClient;
    }

    @Provides
    @Singleton
    IDependencyResolverClient providesDependencyResolver(){
        return new DependencyResolverClient();
    }

    @Provides
    @Singleton
    MLExtractorService providesMLExtractorServiceClient(MLExtractorServiceImpl mlExtractorServiceImpl){
        return mlExtractorServiceImpl;
    }

    @Provides
    @Named(JOB_MANAGER_PIPELINE)
    Pipeline providesPipeline(ILogger logger, IDBService idbService, IBlobService blobService, MandrakeService mandrakeService,MLExtractorService mlExtractorService,
                              IDependencyResolverClient dependencyResolverClient,DPEngineService dpEngineService,
                              ModelClientService modelServicesClient, PositionalService positionalService,AnnotatorService annotatorService){
        return new ExtractionPipeline(logger,
                new DocumentPropsFetcherStep(logger, idbService, blobService, dependencyResolverClient),
                new DocumentClassificationStep(logger, modelServicesClient, blobService),
                new ImageProcessingStep(logger),
                new OCROutputProcessorStep(logger),
                new SegmentationStep(logger),
                new AnnotationAndMappingStep(logger,annotatorService),
                new MandrakeExtractorStep(logger, mandrakeService),
                new PositionalExtractorStep(logger, positionalService),
                new MachineLearningExtractorStep(logger,mlExtractorService),
                new AnnotationExtractorStep(logger,mlExtractorService),
                new DateParserStep(logger,dpEngineService),
                new NpiExtractionStep(logger),
                new AnnotationExtractorStep(logger,mlExtractorService),
                new OutputConfigurationProcessorStep(logger),
                new ResultWriterStep(logger, idbService));
    }

}
