package com.auxolabs.cvision.web.init;

import com.auxolabs.cvision.constants.CaptureAppConstants;
import com.auxolabs.cvision.utils.ResourceCleanup;
import com.auxolabs.capture.positional.init.PositionalInitializer;
import com.concordfax.capture.core.logging.ILogger;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.stream.Stream;

@Singleton
public class CaptureInitializer {

    private ILogger log;

    @Inject
    public CaptureInitializer(ILogger logger, ResourceCleanup cleanup) {
        this.log = logger;

        Runtime runtime = Runtime.getRuntime();
        runtime.addShutdownHook(cleanup);

//        cleanup.registerCleanupFunction(this::tmpDirCleanup);

        new PositionalInitializer();
    }

}
