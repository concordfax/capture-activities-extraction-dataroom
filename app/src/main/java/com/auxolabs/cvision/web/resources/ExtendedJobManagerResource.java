package com.auxolabs.cvision.web.resources;

import com.auxolabs.cvision.db.IDBService;
import com.concordfax.capture.core.jobmanager.JobManager;
import com.concordfax.capture.core.logging.ILogger;
import com.concordfax.capture.core.logging.LogLevels;
import com.concordfax.capture.core.models.account.AccountProfileIdModel;
import com.concordfax.capture.core.web.resources.JobManagerResourceDropwizard;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.eclipse.jetty.http.HttpStatus;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;

@Path("job-manager")
@Api("JobManager Resource")
public class ExtendedJobManagerResource extends JobManagerResourceDropwizard {
    private IDBService db;
    private ILogger log;

    @Inject
    public ExtendedJobManagerResource(JobManager jobManager, ILogger logger, IDBService idbService, ILogger iLogger) {
        super(jobManager, logger);
        this.db = idbService;
        this.log = iLogger;
    }
    @Path("resolveIdUrl/{accountId}/{environmentId}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response resolveIdUrl(@ApiParam(value="get extraction profile mapping")@PathParam("accountId")String accountId,@PathParam("environmentId") String envId) {
        try {
            AccountProfileIdModel apidModel = db.cccGetProfileIdForAccountId(accountId,envId);
            this.log.trace(String.format("Fetched the extraction url:%s and profile:%s against the Account id:%s",
                    apidModel.getExtractionUrl(), apidModel.getExtractionProfileId(),
                    apidModel.getAccountId()) , LogLevels.Verbose, null);
            return Response.status(HttpStatus.OK_200).entity(apidModel).build();
        } catch (Exception e) {
            this.log.error(e, new HashMap<>(), new HashMap<>());
            return Response.status(HttpStatus.INTERNAL_SERVER_ERROR_500).entity(e.getMessage()).build();
        }
    }
}
