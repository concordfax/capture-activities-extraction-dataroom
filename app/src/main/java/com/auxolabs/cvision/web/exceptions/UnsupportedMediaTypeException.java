package com.auxolabs.cvision.web.exceptions;

import com.auxolabs.cvision.exceptions.ExtractionException;

public class UnsupportedMediaTypeException extends ExtractionException {
    public UnsupportedMediaTypeException(String mediaType) {
        super(String.format("found %s media type, supported are png, jpeg, pdf, tiff.", mediaType));
    }
}
