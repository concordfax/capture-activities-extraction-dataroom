package com.auxolabs.cvision.web.config;

import com.concordfax.capture.core.db.MongoConfigModel;
import com.concordfax.capture.core.logging.azure.AppInsightsLogConfig;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;
import com.concordfax.capture.core.jobmanager.JobManagerConfig;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CaptureConfig extends Configuration {

    @JsonProperty("swagger")
    private SwaggerBundleConfiguration swaggerBundleConfiguration;

    @JsonProperty
    private MongoConfigModel db;

    @JsonProperty
    private AppInsightsLogConfig logger;

    @JsonProperty
    private JobManagerConfig jobManager;
}
