package com.auxolabs.cvision.web.config;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class CacheControlFilter implements Filter {

    private static final List<String> escapeExtensions = Arrays.asList(".png", ".jpg", ".jpeg", ".bmp",
            ".ico", ".gif", ".tiff", ".tga", ".midi", ".mp3", ".avi", ".ogg", ".mkv", ".mp4", ".flv", ".wav",
            ".js", ".css", ".fnt", ".svg", ".ttf", ".eot", "woff");

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        String requestUri = req.getRequestURI().trim();
        String ext = null;
        Boolean shouldEscape = false;

        int loc = requestUri.lastIndexOf(".");
        if (0 <= loc)
            ext = requestUri.substring(loc);

        if (escapeExtensions.contains(ext))
            shouldEscape = true;

        if (!shouldEscape) {
            resp.setHeader("Cache-Control", "no-cache, no-store");
            resp.setHeader("pragma", "no-cache");
            resp.setHeader("expires", "0");
        }
        chain.doFilter(request, response);
    }

    public void destroy() {
    }

    public void init(FilterConfig arg0) throws ServletException {
    }
}

