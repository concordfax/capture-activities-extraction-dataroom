package com.auxolabs.cvision.web.resources;

import com.concordfax.capture.core.web.resources.HealthCheckResourceDropwizard;
import io.swagger.annotations.Api;

import javax.inject.Inject;
import javax.ws.rs.Path;

@Path("health-check")
@Api("Health Check Resource")
public class ExtendedHealthCheckResource extends HealthCheckResourceDropwizard {
    @Inject
    public ExtendedHealthCheckResource(){}
}
