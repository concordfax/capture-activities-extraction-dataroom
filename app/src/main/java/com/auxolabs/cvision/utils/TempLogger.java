package com.auxolabs.cvision.utils;

import com.concordfax.capture.core.logging.ILogger;
import com.concordfax.capture.core.logging.LogLevels;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Slf4j
public class TempLogger implements ILogger {
    @Override
    public void trace(String s, LogLevels logLevels, Map<String, String> map) {
        log.info(s);
    }

    @Override
    public void error(Exception e, Map<String, String> map, Map<String, Double> map1) {
        log.error("!! error !!",e);
    }

    @Override
    public void event(String s, Map<String, String> map, Map<String, Double> map1) {
        log.warn(s);
    }
}
