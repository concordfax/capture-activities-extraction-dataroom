package com.auxolabs.cvision.utils;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@Singleton
public class ResourceCleanup extends Thread {
    private List<Consumer<Object>> cleanupFunctions;

    public ResourceCleanup() {
        System.out.println(this.hashCode());
        this.cleanupFunctions = new ArrayList<>();
    }

    public void registerCleanupFunction(Consumer consumer) {
        cleanupFunctions.add(consumer);
    }

    public void run() {
        cleanupFunctions.forEach(consumer -> {
            consumer.accept(null);
        });
    }
}
