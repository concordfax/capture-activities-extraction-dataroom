package com.auxolabs.cvision.utils;

import com.auxolabs.cvision.models.cv.PIBOXHolder;
import com.sun.media.jai.codec.ByteArraySeekableStream;
import com.sun.media.jai.codec.TIFFDecodeParam;
import com.sun.media.jai.codec.TIFFEncodeParam;
import com.sun.media.jai.codec.TIFFField;
import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.javacpp.SizeTPointer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import javax.media.jai.JAI;
import javax.media.jai.RenderedOp;
import java.awt.image.BufferedImage;
import java.awt.image.renderable.ParameterBlock;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.UUID;

import static org.bytedeco.javacpp.lept.*;

public class LeptonicaUtils {
    private static final Object lock = new Object();
    private static LeptonicaUtils instance;

    private LeptonicaUtils() {
    }

    public static LeptonicaUtils getInstance() {
        synchronized (lock) {
            if (instance == null) {
                instance = new LeptonicaUtils();
            }
        }
        return instance;
    }

    public Boolean safeDeletePIX(PIX pix) {
        if (pix != null && pix.address() != 0) {
            pix.refcount(1);
            pixDestroy(pix);
            return true;
        }
        return false;
    }

    public Boolean safeDeleteBOX(BOX box) {
        if (box != null && box.address() != 0) {
            box.refcount(1);
            boxDestroy(box);
            return true;
        }
        return false;
    }

    public Boolean deletePIBOX(PIBOXHolder piboxHolder) {
        if (piboxHolder != null) {
            if (piboxHolder.getPix() != null) {
                safeDeletePIX(piboxHolder.getPix());
            }
            if (piboxHolder.getBox() != null) {
                safeDeleteBOX(piboxHolder.getBox());
            }
            return true;
        }
        return false;
    }

    public String printPIXToFile(String folder, PIX pixs) {
        try {
            if (pixs != null) {
                String fileName = folder + "/" + UUID.randomUUID().toString() + ".png";
                pixWrite(fileName, pixs, IFF_PNG);
                return fileName;
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return null;
    }

    public byte[] convertBufferedImageToBA(BufferedImage bufferedImage) {
        try {
            if (bufferedImage == null)
                return null;
            byte[] result = null;
            if (OsUtils.OS.isWindows()) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                baos.flush();
                TIFFEncodeParam params = new TIFFEncodeParam();
                JAI.create("encode", bufferedImage, baos, "TIFF", params);
                result = baos.toByteArray();
                baos.close();
            } else {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                baos.flush();
                ImageIO.write(bufferedImage, "png", baos);
                result = baos.toByteArray();
                baos.close();
            }
            return result;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    public PIX convertBufferedImageToPIX(BufferedImage bufferedImage, Integer xDpi, Integer yDpi) {
        PIX pix = null;
        try {
//            if (xDpi == -1)
//                xDpi = 204;
//            if (yDpi == -1)
//                yDpi = 196;
            if (bufferedImage == null)
                return null;

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            baos.flush();
            TIFFEncodeParam params = new TIFFEncodeParam();
            if (xDpi != -1 && yDpi != -1) {
                TIFFField[] extras = new TIFFField[2];
                extras[0] = new TIFFField(282, TIFFField.TIFF_RATIONAL, 1, (Object) new long[][]{{(long) xDpi, (long) 1},
                        {(long) 0, (long) 0}});
                extras[1] = new TIFFField(283, TIFFField.TIFF_RATIONAL, 1, (Object) new long[][]{{(long) yDpi, (long) 1},
                        {(long) 0, (long) 0}});
                params.setExtraFields(extras);
            }
            JAI.create("encode", bufferedImage, baos, "TIFF", params);
            byte[] imageInByte = baos.toByteArray();
            baos.close();
            pix = pixReadMem(imageInByte, imageInByte.length);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return pix;
    }

    public BufferedImage convertPIXToBufferedImage(PIX pixs) {
        try {
            if (pixs == null)
                return null;
            BufferedImage resultImage = null;
            BytePointer bytePointer = new BytePointer();
            SizeTPointer sizeTPointer = new SizeTPointer(1);
            if (OsUtils.OS.isWindows()) {
                int success = pixWriteMem(bytePointer, sizeTPointer, pixs, IFF_TIFF);
                if (success == 0) { // as per leptonica docs success = 0;
                    byte[] req = new byte[(int) sizeTPointer.get()];
                    for (int i = 0; i < sizeTPointer.get(); i++) {
                        req[i] = bytePointer.get(i);
                    }
                    ByteArraySeekableStream seekableStream = new ByteArraySeekableStream(req);
                    TIFFDecodeParam decodeParam = new TIFFDecodeParam();
                    decodeParam.setDecodePaletteAsShorts(true);
                    ParameterBlock params = new ParameterBlock();
                    params.add(seekableStream);
                    RenderedOp image1 = JAI.create("tiff", params);
                    resultImage = image1.getAsBufferedImage();
                    image1.dispose();
                    seekableStream.close();
                }
            } else {
                int success = pixWriteMem(bytePointer, sizeTPointer, pixs, IFF_PNG);
                if (success == 0) { // as per leptonica docs success = 0;
                    byte[] req = new byte[(int) sizeTPointer.get()];
                    for (int i = 0; i < sizeTPointer.get(); i++) {
                        req[i] = bytePointer.get(i);
                    }
                    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(req);
                    resultImage = ImageIO.read(byteArrayInputStream);
                    byteArrayInputStream.close();
                }
            }
            lept_free(bytePointer);
            bytePointer.close();
            sizeTPointer.close();
            return resultImage;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    public byte[] convertPIXToBA(PIX pixs) {
        try {
            if (pixs == null)
                return null;
            byte[] result = null;
            BytePointer bytePointer = new BytePointer();
            SizeTPointer sizeTPointer = new SizeTPointer(1);
            int success = pixWriteMem(bytePointer, sizeTPointer, pixs, IFF_TIFF_ZIP);
            if (success == 0) { // as per leptonica docs success = 0;
                result = new byte[(int) sizeTPointer.get()];
                for (int i = 0; i < sizeTPointer.get(); i++) {
                    result[i] = bytePointer.get(i);
                }
            }
            lept_free(bytePointer);
            bytePointer.close();
            sizeTPointer.close();
            return result;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
