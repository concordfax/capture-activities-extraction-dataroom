package com.auxolabs.cvision.utils;

import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.javacpp.SizeTPointer;
import org.bytedeco.javacpp.opencv_imgproc;

import static org.bytedeco.javacpp.lept.*;
import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_imgcodecs.*;
import static org.bytedeco.javacpp.opencv_imgproc.*;

public class OpenCVUtils {

    public static void main(String[] args) throws Exception {
        Mat img = imread("skewed_sample_2.jpg");
        for (int i = 0; i < 200000; i++) {
            System.out.print("\riter: " + i);
            deskew2(img);
        }
    }

    public static void deskew2(Mat img) {
        Mat src_gray = new Mat();
        opencv_imgproc.cvtColor(img, src_gray, opencv_imgproc.COLOR_BGR2GRAY);
        bitwise_not(src_gray, src_gray);
        Mat thresdholdedImg = new Mat();
        double threshold = threshold(src_gray, thresdholdedImg, 0, 255,
                opencv_imgproc.CV_THRESH_BINARY | opencv_imgproc.CV_THRESH_OTSU);
        Mat nonZeroPoints = new Mat();
        findNonZero(thresdholdedImg, nonZeroPoints);
        RotatedRect box = minAreaRect(nonZeroPoints);
        float angle = 0.0f;
        if (box.angle() < -45) {
            angle = ((90 + box.angle()));
        } else {
            angle = ((box.angle()));
        }

        Point2f center = new Point2f(img.size().width() / 2.0f, img.size().height() / 2.0f);
        Mat rot_mat = getRotationMatrix2D(center, angle, 1);
        Mat rotated = new Mat();
        warpAffine(img, rotated, rot_mat, img.size());

        src_gray.release();
        thresdholdedImg.release();
        nonZeroPoints.release();
        rot_mat.release();
        rotated.release();
        box.close();
        center.close();

//        cvRelease(src_gray);
//        cvRelease(thresdholdedImg);
//        cvRelease(nonZeroPoints);
//        cvRelease(rot_mat);
//        cvRelease(rotated);
    }

    public static Mat covertPIXToMat(PIX pixs) {
        BytePointer bytePointer = new BytePointer(0);
        SizeTPointer sizeTPointer = new SizeTPointer(1);
        int success = pixWriteMem(bytePointer, sizeTPointer, pixs, IFF_PNG);
        if (success == 0) {
            byte[] req = new byte[(int) sizeTPointer.get()];
            for (int i = 0; i < sizeTPointer.get(); i++) {
                req[i] = bytePointer.get(i);
            }
            Mat img = imdecode(new Mat(req), CV_LOAD_IMAGE_UNCHANGED);
            imwrite("abcd2.png", img);
            return img;
        }
        return null;
    }

}
