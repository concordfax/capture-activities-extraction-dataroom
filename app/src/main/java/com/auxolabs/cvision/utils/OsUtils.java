package com.auxolabs.cvision.utils;

public final class OsUtils {

    public static void showSysSettings() {
        System.err.println("----------------------------------------");
        System.err.println("OS: " + OS.name());
        System.err.println("JVM-Arch: " + JVM.arch());
        System.err.println("----------------------------------------");
    }

    public static class JVM {
        private static String arch = null;

        public static synchronized String arch() {
            if (JVM.arch == null)
                JVM.arch = System.getProperty("sun.arch.data.model");
            return JVM.arch;
        }
    }

    public static class OS {
        private static String name = null;

        public static boolean isWindows() {
            return name().startsWith("Windows");
        }

        public static synchronized String name() {
            if (OS.name == null) {
                OS.name = System.getProperty("os.name");
            }
            return OS.name;
        }

        public static boolean isUnix() {
            if (!OS.isWindows()) {
                return true;
            }
            return false;
        }
    }
}
