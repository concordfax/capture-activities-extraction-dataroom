package com.auxolabs.cvision.exceptions;

public class ActivityNotFoundException extends ExtractionException {

    public ActivityNotFoundException(String activityId) {
        super(String.format("activity with id %s not found", activityId));

    }
}
