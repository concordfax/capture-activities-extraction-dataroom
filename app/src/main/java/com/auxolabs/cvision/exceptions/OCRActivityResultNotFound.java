package com.auxolabs.cvision.exceptions;

public class OCRActivityResultNotFound extends ExtractionException {
    public OCRActivityResultNotFound() {
        super("OCR activity result not found, empty or null list");
    }
}
