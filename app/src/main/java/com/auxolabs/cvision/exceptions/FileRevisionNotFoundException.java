package com.auxolabs.cvision.exceptions;

public class FileRevisionNotFoundException extends ExtractionException {
    public FileRevisionNotFoundException(String type) {
        super(String.format("revision with type: %s not found", type));
    }

    public FileRevisionNotFoundException(Throwable throwable) {
        super(throwable);
    }
}
