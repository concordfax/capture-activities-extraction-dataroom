package com.auxolabs.cvision.exceptions;

public class ExtractionException extends RuntimeException {

    public ExtractionException(String message) {
        super("CaptureExtractionException:" + message);
    }

    public ExtractionException(Throwable throwable) {
        super(throwable);
    }
}
