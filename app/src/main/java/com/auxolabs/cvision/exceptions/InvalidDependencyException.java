package com.auxolabs.cvision.exceptions;

public class InvalidDependencyException extends ExtractionException{
    public InvalidDependencyException() {
        super("Invalid dependency resolution, please view activity configuration");
    }

    public InvalidDependencyException(Throwable throwable) {
        super(throwable);
    }
}
