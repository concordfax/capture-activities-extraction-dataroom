<!DOCTYPE html>
<html style="height:100%">
<head>
    <title>Concord</title>
    <meta charset="UTF-8">
    <meta name="description" content="Auxo Concord OCR detector for images with width and height in the range of 2000.">
    <meta name="keywords" content="Concord,Auxo,OCR,Medical">
    <meta name="author" content="ASK@Auxo">
    <link rel="apple-touch-icon" sizes="57x57" href="athena/images/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="athena/images/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="athena/images/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="athena/images/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="athena/images/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="athena/images/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="athena/images/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="athena/images/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="athena/images/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="athena/images/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="athena/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="athena/images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="athena/images/favicon-16x16.png">
    <link rel="manifest" href="athena/images/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="athena/images/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="athena/css/ladda.min.css">
    <link rel="stylesheet" href="athena/css/bootstrap.min.css">
    <link rel="stylesheet" href="athena/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="athena/css/bootstrap-toggle.min.css">
    <style type="text/css">
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        li {
            cursor: pointer;
            cursor: hand;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        .cont {
            margin: 5px;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }

        #result {
            margin: 5px;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }

        #plus {
            cursor: pointer;
        }

        #plus:hover {
            content: url("athena/images/plus_7.png");
        }

        #clear {
            cursor: pointer;
            margin: auto;
        }

        #clear:hover {
            content: url("athena/images/clear_hover.png");
        }

        button {
            margin: 5px;
        }

        button:hover {
            margin: 5px;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }

        #image_file {
            opacity: 0.0;
            cursor: pointer;
            border: none;
        }

        #terms {
            color: #bbb;
            font-weight: normal;
        }
    </style>
</head>
<body style="height:100%">
<div class="container-fluid" style="height:100%">
    <section>
        <div style="width:70%;margin:auto;" class="input-group input-group-lg">
            <form id="uploadForm" enctype="multipart/form-data" method="POST">
                <input id="textbox" type="file" class="form-control" name="file"/>
            </form>
            <button class="ladda-button" data-color="green" data-style="expand-right" id="submitButton"
                    style="width:100%;">
                <span class="ladda-label">Analyze</span>
            </button>
        </div>
    </section>
    <hr>
    <div class="row zoomContainer" style="height:100%">
        <div id="paginationContainer" style="margin:auto;" class="input-group input-group-lg">
        </div>
        <div class="col col-md-6 col-xs-12" style="height:100%;padding:10px;">
            <div class="cont" style="height:100%;width:100%;">
                <img id="preview" style="max-height:100%;max-width:100%;margin-left: auto;margin-right: auto;">
                <img id="preview" style="max-height:100%;max-width:100%;margin-left: auto;margin-right: auto;">
                <img id="preview" style="max-height:100%;max-width:100%;margin-left: auto;margin-right: auto;">
                <img id="preview" style="max-height:100%;max-width:100%;margin-left: auto;margin-right: auto;">
                <img id="preview" style="max-height:100%;max-width:100%;margin-left: auto;margin-right: auto;">
            </div>
        </div>
        <div id="toPrint" class="col col-md-6 col-xs-12" style="height:100%;padding:10px;line-height:0.75em">
            <div class="zoomContainer" id="result" style="height:100%;width:100%;position:relative;">
            </div>
        </div>
    </div>
    <hr style="width:100%;">
</div>

<div class="wrapper">
    <hr style="width:100%;">
    <div id="annotated">

    </div>
    <div id="terms">
        <center>Concord &copy; 2017</center>
    </div>
</div>

<script src="athena/js/jquery.min.js"></script>
<script src="athena/js/bootstrap.min.js"></script>
<script src="athena/js/jquery.zoomooz.min.js"></script>
<script src="athena/js/jquery.lazyload.min.js"></script>
<script src="athena/js/quickfit.js"></script>
<script src="athena/js/spin.min.js"></script>
<script src="athena/js/ladda.min.js"></script>
<script src="athena/js/bootstrap-toggle.min.js"></script>
<script src="athena/js/script.js"></script>
</body>
</html>