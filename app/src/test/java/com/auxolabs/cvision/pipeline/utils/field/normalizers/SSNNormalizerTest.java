package com.auxolabs.cvision.pipeline.utils.field.normalizers;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SSNNormalizerTest {

    @Test
    public void normalize() {
        SSNNormalizer ssnNormalizer = new SSNNormalizer();
        assertEquals("XXX-XX-1234", ssnNormalizer.normalize("1234"));
        assertEquals("123-45-6789", ssnNormalizer.normalize("123456789"));
        assertEquals("123-45-6789", ssnNormalizer.normalize("123 456 789"));
        assertEquals("XXX-XX-XXXX", ssnNormalizer.normalize("xxx-xx-xxxx"));
    }
}