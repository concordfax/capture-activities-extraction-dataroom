package com.auxolabs.cvision.pipeline.steps.helpers;

import com.concordfax.capture.core.models.document.result.extraction.ExtractionField;
import com.concordfax.capture.core.models.document.result.extraction.SimpleExtractionField;
import com.concordfax.capture.core.models.extraction.ExtractionOutputConfigurationModel;
import com.concordfax.capture.core.models.extraction.output.ExtractorType;
import org.junit.Test;
import org.yaml.snakeyaml.Yaml;

import java.util.HashMap;

import static org.junit.Assert.assertEquals;

public class ActivityResultHelperTest {
   // @Test
    public void process() throws Exception {
        Yaml yaml = new Yaml();
        ExtractionOutputConfigurationModel extractionOutputConfigurationModel = yaml.loadAs(this.getClass().getClassLoader().getResourceAsStream("com/auxolabs/capture/app/field-config.yml"), ExtractionOutputConfigurationModel.class);

        HashMap<ExtractorType, HashMap<String, ExtractionField>> fieldRuleMap = new HashMap<>();
        HashMap<String, ExtractionField> ruleToResult = new HashMap<>();
        SimpleExtractionField simpleExtractionFieldMandrake = new SimpleExtractionField(null,0.95f,"", "");
        ruleToResult.put("Patient Name Identifier", simpleExtractionFieldMandrake);
        fieldRuleMap.put(ExtractorType.Mandrake, ruleToResult);

        HashMap<String, ExtractionField> ruleToResultPositional = new HashMap<>();
        SimpleExtractionField simpleExtractionFieldPositional = new SimpleExtractionField(null,0.95f,"John", "");
        ruleToResultPositional.put("Patient Name Identifier", simpleExtractionFieldPositional);
        fieldRuleMap.put(ExtractorType.Positional, ruleToResultPositional);

        ruleToResult.put("Ex Patient Name Identifier",new SimpleExtractionField(null,0.95f,"", ""));
        fieldRuleMap.put(ExtractorType.Mandrake, ruleToResult);

        ActivityResultHelper activityResultHelper = new ActivityResultHelper();
        SimpleExtractionField patientName = (SimpleExtractionField) activityResultHelper.process(extractionOutputConfigurationModel,fieldRuleMap).get(0);
        assertEquals("John",patientName.getValue());
    }

    //@Test
    public void getFinalNormalizedResult() throws Exception {

    }

}