package com.auxolabs.cvision.pipeline.utils.field.normalizers;

import org.junit.Test;

import java.io.IOException;
import java.text.ParseException;

import static org.junit.Assert.assertEquals;

public class CaptureDateNormalizerTest {

    @Test
    public void dateFieldFormatter() throws IOException, ParseException {
        CaptureDateNormalizer captureDateNormalizer = new CaptureDateNormalizer();
        assertEquals("10/10/1929", captureDateNormalizer.dateFieldFormatter("10/10/29"));
        assertEquals("10/10/1929", captureDateNormalizer.dateFieldFormatter("10-10-29"));
        assertEquals("10/10/1929", captureDateNormalizer.dateFieldFormatter("10.10.29"));
        assertEquals("10/10/2019", captureDateNormalizer.dateFieldFormatter("10/10/19"));
        assertEquals("11/29/1938", captureDateNormalizer.dateFieldFormatter("11 / 29 / 38"));
        assertEquals("06/10/2018", captureDateNormalizer.dateFieldFormatter("10 Jun 2018"));
        assertEquals("06/10/2018", captureDateNormalizer.dateFieldFormatter("10 June 2018"));
        assertEquals("06/10/2018", captureDateNormalizer.dateFieldFormatter("June 10 2018"));
        assertEquals("06/10/2018", captureDateNormalizer.dateFieldFormatter("June-10-2018"));
        assertEquals("06/10/2018", captureDateNormalizer.dateFieldFormatter("10-June-2018"));
        assertEquals("05/02/1994", captureDateNormalizer.dateFieldFormatter("02-May- 1994"));
        assertEquals("10/20/1989", captureDateNormalizer.dateFieldFormatter("10/ 20 / 89"));
        assertEquals("01/29/1987", captureDateNormalizer.dateFieldFormatter("29 Jan 87"));
        assertEquals("06/10/2018", captureDateNormalizer.dateFieldFormatter("10 June 18"));
        assertEquals("01/29/2000", captureDateNormalizer.dateFieldFormatter("29 Jan 00"));
        assertEquals("04/30/1985", captureDateNormalizer.dateFieldFormatter("04/30/1 985"));
        assertEquals("04/30/1985", captureDateNormalizer.dateFieldFormatter("0 4/ 30/1 98 5"));
        assertEquals("04/30/1985", captureDateNormalizer.dateFieldFormatter("0 4/ 30/8 5"));
        assertEquals("12/14/2000", captureDateNormalizer.dateFieldFormatter("Dec- 14-2000"));
    }
}
