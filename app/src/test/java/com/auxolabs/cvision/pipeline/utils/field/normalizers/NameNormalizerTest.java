package com.auxolabs.cvision.pipeline.utils.field.normalizers;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NameNormalizerTest {

    @Test
    public void normalize() {
        NameNormalizer nameNormalizer = new NameNormalizer();
        assertEquals("John McDonald", nameNormalizer.normalize("JOHN MCDONALD"));
        assertEquals("Mr. Kelly A. Brook", nameNormalizer.normalize("Mr Kelly Audrey-Brook"));
        assertEquals("Mr. Alpha S. John", nameNormalizer.normalize("MR John, Alpha Smith"));
        assertEquals("Eliquis", nameNormalizer.normalize("• Eliquis"));
        assertEquals("Serena Cassady M. Chan", nameNormalizer.normalize("Serena Cassady M Chan"));
        assertEquals("Mc Ed", nameNormalizer.normalize("MC ED"));

    }
}