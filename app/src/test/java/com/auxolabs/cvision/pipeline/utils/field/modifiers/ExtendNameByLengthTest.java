package com.auxolabs.cvision.pipeline.utils.field.modifiers;

import com.concordfax.capture.core.models.document.result.extraction.ExtractionField;
import com.concordfax.capture.core.models.document.result.extraction.SimpleExtractionField;
import com.concordfax.capture.core.models.extraction.output.ExtractorModel;
import com.concordfax.capture.core.models.extraction.output.ExtractorResultModifier;
import com.concordfax.capture.core.models.extraction.output.ExtractorType;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ExtendNameByLengthTest {

    ExtendNameByLength extendNameByLength;
    Gson gson;
    Type listExtractorType;
    TypeReference mapExtractorTypeToExtractorOutput;
    ObjectMapper objectMapper;

    @Before
    public void setUp() throws Exception {
        extendNameByLength = new ExtendNameByLength();
        gson = new Gson();
        objectMapper = new ObjectMapper();
        listExtractorType = new TypeToken<List<ExtractorModel>>(){}.getType();
        mapExtractorTypeToExtractorOutput = new TypeReference<HashMap<ExtractorType, HashMap<String, ExtractionField>>>() {};
    }

    @Test
    public void modify() throws IOException {
        ExtractorResultModifier extractorResultModifier = gson.fromJson(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("com/auxolabs/cvision/pipeline/utils/field/name/extractor-result-modifier.json")), ExtractorResultModifier.class);
        List<ExtractorModel> extractors = gson.fromJson(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("com/auxolabs/cvision/pipeline/utils/field/name/name-extractors.json")), listExtractorType);
        HashMap<ExtractorType, HashMap<String, ExtractionField>> fieldRuleMap = objectMapper.readValue(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("com/auxolabs/cvision/pipeline/utils/field/name/field-rule-map.json")), mapExtractorTypeToExtractorOutput);
        String currentResult = "LYDIA , ABIGAIL";

        assertEquals("ADDISON LYDIA, ABIGAIL", ((SimpleExtractionField)extendNameByLength.modify(currentResult,extractorResultModifier, extractors, fieldRuleMap)).getValue());
    }
}