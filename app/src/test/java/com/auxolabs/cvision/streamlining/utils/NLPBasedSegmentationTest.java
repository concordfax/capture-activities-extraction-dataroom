package com.auxolabs.cvision.streamlining.utils;

import com.auxolabs.cvision.streamlining.models.ImageSegment;
import com.concordfax.capture.core.models.ocr.abbyy.CharParams;
import com.concordfax.capture.core.models.ocr.abbyy.Formatting;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class NLPBasedSegmentationTest {

    Type listCharParams;
    Gson gson;
    NLPBasedSegmentation nlpBasedSegmentation;
    ImageSegment imageSegment1;
    ImageSegment imageSegment2;
    ImageSegment imageSegment3;
    ImageSegment imageSegment4;
    Formatting formatting1;
    Formatting formatting2;
    Formatting formatting3;
    Formatting formatting4;

    //@Before
    public void setUp() throws Exception {
        this.listCharParams = new TypeToken<List<CharParams>>(){}.getType();
        this.gson = new Gson();
        this.nlpBasedSegmentation = new NLPBasedSegmentation();
        // create some mock segments
        this.imageSegment1 = new ImageSegment();
        this.imageSegment2 = new ImageSegment();
        this.imageSegment3 = new ImageSegment();
        this.imageSegment4 = new ImageSegment();

        List<CharParams> seg1CharParams = new ArrayList<>();
        this.formatting1 = new Formatting();
        formatting1.setCharParams(seg1CharParams);
        List<CharParams> seg2CharParams = new ArrayList<>();
        this.formatting2 = new Formatting();
        formatting2.setCharParams(seg2CharParams);
        List<CharParams> seg3CharParams = new ArrayList<>();
        this.formatting3 = new Formatting();
        formatting3.setCharParams(seg3CharParams);
        List<CharParams> seg4CharParams = new ArrayList<>();
        this.formatting4 = new Formatting();
        formatting4.setCharParams(seg4CharParams);

        imageSegment1.setText("PATIENT");
        imageSegment2.setText(" NAME");
        imageSegment3.setText("John");
        imageSegment4.setText("Smith");
        imageSegment1.setRect(100,100,50,16);
        imageSegment2.setRect(170,101,40,17);
        imageSegment3.setRect(230,100,40,17);
        imageSegment4.setRect(300,102,40,16);
        imageSegment1.setFormatting(formatting1);
        imageSegment2.setFormatting(formatting2);
        imageSegment3.setFormatting(formatting3);
        imageSegment4.setFormatting(formatting4);

    }

    //@Test
    public void createSegmentsByMergingWordsWithAllCapitalLetters() {
        List<ImageSegment> segmentsFromOcr = new ArrayList<>(Arrays.asList(imageSegment1, imageSegment2, imageSegment3, imageSegment4));
        nlpBasedSegmentation.createSegmentsByMergingWordsWithAllCapitalLetters(segmentsFromOcr);

        imageSegment1.setText("PATIENT NAME");
        imageSegment1.setRect(100,100,110,17);
        imageSegment1.setVisited(true);
        imageSegment1.setFormatting(formatting1);
        imageSegment3.setText("John");
        imageSegment3.setRect(230,100,40,17);
        imageSegment3.setVisited(true);
        imageSegment3.setFormatting(formatting3);
        imageSegment4.setText("Smith");
        imageSegment4.setRect(300,102,40,16);
        imageSegment4.setVisited(true);
        imageSegment4.setFormatting(formatting4);
        List<ImageSegment> expectedList = new ArrayList<>(Arrays.asList(imageSegment1, imageSegment3, imageSegment4));

        assertEquals(expectedList,segmentsFromOcr);


    }

    /**
     * 1. Starts with ":"
     * 2. Ends with ":"
     * 3. Single ":" in b/w
     * 4. Multiple ":" in b/w
     * 5. In b/w digits
     * 6. Single
     */
    //@Test
    public void splitSegmentsBasedOnSegmentSplitters() {
        // Single ":" in b/w
        List<CharParams> charparamsList = gson.fromJson(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("com/auxolabs/cvision/streamlining/utils/nlp/charparams/mock-charparams-list-single-column.json")), listCharParams);
        ImageSegment segment = new ImageSegment();
        Formatting formatting = new Formatting();
        formatting.setCharParams(charparamsList);
        segment.setFormatting(formatting);
        segment.setText("Patient name :John Smith");
        List<ImageSegment> imageSegments = new ArrayList<>();
        imageSegments.add(segment);

        ImageSegment expectedSplit1 = new ImageSegment();
        ImageSegment expectedSplit2 = new ImageSegment();
        expectedSplit1.setText("Patient name :");
        expectedSplit2.setText("John Smith");
        List<ImageSegment> expectedListOfSegments = new ArrayList<>(Arrays.asList(expectedSplit1,expectedSplit2));

        nlpBasedSegmentation.splitSegmentsBasedOnSegmentSplitters(imageSegments);
        for(int i=0;i<imageSegments.size();i++){
            assertEquals(expectedListOfSegments.get(i).getText(),imageSegments.get(i).getText());
        }
        imageSegments.clear();

        // Multiple ":" in b/w
        List<CharParams> charparamsList2 = gson.fromJson(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("com/auxolabs/cvision/streamlining/utils/nlp/charparams/mock-charparams-list-single-column.json")), listCharParams);
        formatting.setCharParams(charparamsList2);
        segment.setFormatting(formatting);
        segment.setText("Sign: Date: 05/18");
        imageSegments.add(segment);

        ImageSegment split1 = new ImageSegment();
        ImageSegment split2 = new ImageSegment();
        ImageSegment split3 = new ImageSegment();
        split1.setText("Sign:");
        split2.setText(" Date:");
        split3.setText(" 05/18");
        List<ImageSegment> expectedListOfSegments1 = new ArrayList<>(Arrays.asList(split1,split2,split3));

        nlpBasedSegmentation.splitSegmentsBasedOnSegmentSplitters(imageSegments);
        for(int i=0;i<imageSegments.size();i++){
            assertEquals(expectedListOfSegments1.get(i).getText(),imageSegments.get(i).getText());
        }
        imageSegments.clear();

        //starts with :
        ImageSegment inputSegment = new ImageSegment();
        inputSegment.setText(":05/19 sign");
        imageSegments.add(inputSegment);

        nlpBasedSegmentation.splitSegmentsBasedOnSegmentSplitters(imageSegments);
        for(int i=0;i<imageSegments.size();i++){
            assertEquals(":05/19 sign",imageSegments.get(i).getText());
        }
        imageSegments.clear();

        //Ends with ":"
        ImageSegment inputSegment1 = new ImageSegment();
        inputSegment1.setText("Signed Date:");
        imageSegments.add(inputSegment1);

        nlpBasedSegmentation.splitSegmentsBasedOnSegmentSplitters(imageSegments);
        for(int i=0;i<imageSegments.size();i++){
            assertEquals("Signed Date:",imageSegments.get(i).getText());
        }
        imageSegments.clear();

        //in b/w digits
        ImageSegment inputSegment3 = new ImageSegment();
        inputSegment3.setText("05:31:1998");
        imageSegments.add(inputSegment3);

        nlpBasedSegmentation.splitSegmentsBasedOnSegmentSplitters(imageSegments);
        for(int i=0;i<imageSegments.size();i++){
            assertEquals("05:31:1998",imageSegments.get(i).getText());
        }
        imageSegments.clear();

        //single ":" as a single segment
        ImageSegment inputSegment4 = new ImageSegment();
        inputSegment4.setText(":");
        imageSegments.add(inputSegment4);

        nlpBasedSegmentation.splitSegmentsBasedOnSegmentSplitters(imageSegments);
        for(int i=0;i<imageSegments.size();i++){
            assertEquals(":",imageSegments.get(i).getText());
        }
        imageSegments.clear();
    }
}