//package com.auxolabs.cvision.streamlining.utils;
//
//import com.auxolabs.cvision.streamlining.models.ImageSegment;
//import org.junit.Before;
//import org.junit.Test;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import static org.junit.Assert.*;
//
//public class SortingUtilityTest {
//    SortingUtility sortingUtility;
//
//    @Before
//    public void setUp(){
//        sortingUtility = new SortingUtility();
//    }
//
//    @Test
//    //sorting segments from different lines
//    public void sortMappedSegmentsIfSegmentsAreInDifferentLines() {
//        List<ImageSegment> mockList = new ArrayList<>();
//        List<ImageSegment> expected = new ArrayList<>();
//
//        ImageSegment imageSegment1 = new ImageSegment();
//        imageSegment1.setText("Patient ");
//        imageSegment1.setRect(100,100,50,10);
//        ImageSegment imageSegment2 = new ImageSegment();
//        imageSegment2.setText(" John");
//        imageSegment2.setRect(125,110,30,11);
//        ImageSegment imageSegment3 = new ImageSegment();
//        imageSegment3.setText("name ");
//        imageSegment3.setRect(150,100,30,10);
//
//        mockList.add(imageSegment1);
//        mockList.add(imageSegment2);
//        mockList.add(imageSegment3);
//
//        sortingUtility.sortMappedSegments(mockList);
//
//        expected.add(imageSegment1);
//        expected.add(imageSegment3);
//        expected.add(imageSegment2);
//        assertEquals(expected,mockList);
//
//    }
//
//    @Test
//    //sorting segments from same line
//    public void sortMappedSegmentsIfSegmentsAreInSameLine(){
//        List<ImageSegment> mockList = new ArrayList<>();
//        List<ImageSegment> expected = new ArrayList<>();
//
//        ImageSegment imageSegment1 = new ImageSegment();
//        imageSegment1.setText(" john");
//        imageSegment1.setRect(300,100,50,10);
//        ImageSegment imageSegment2 = new ImageSegment();
//        imageSegment2.setText("patient ");
//        imageSegment2.setRect(100,101,30,11);
//        ImageSegment imageSegment3 = new ImageSegment();
//        imageSegment3.setText(" name");
//        imageSegment3.setRect(200,100,30,10);
//
//        mockList.add(imageSegment1);
//        mockList.add(imageSegment2);
//        mockList.add(imageSegment3);
//
//        sortingUtility.sortMappedSegments(mockList);
//
//        expected.add(imageSegment2);
//        expected.add(imageSegment3);
//        expected.add(imageSegment1);
//        assertEquals(expected,mockList);
//    }
//
//
//}
