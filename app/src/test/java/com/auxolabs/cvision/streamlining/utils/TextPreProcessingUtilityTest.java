package com.auxolabs.cvision.streamlining.utils;

import org.junit.Test;

import static org.junit.Assert.*;

public class TextPreProcessingUtilityTest {

    @Test
    public void preprocess() {
        // name with colon is trimmed to remove colon
        assertEquals("Patient John Smith",TextPreProcessingUtility.preprocess("Patient: John Smith"));

//        // date formats with hypen/periods is converted into slash to standardize to standard format
//        assertEquals("DOB 31/01/2018 ",TextPreProcessingUtility.preprocess("DOB: 31-01-2018"));
//        assertEquals("DOB 31/01/2018 ",TextPreProcessingUtility.preprocess("DOB: 31 . 01 . 2018"));
//
//        // dates that are ignored to characters near it, considering that such cases are rare and may create false positives;
//        // we have seen them mostly occur due to OCR issues
//        assertEquals("DOB31 . 01 . 2018",TextPreProcessingUtility.preprocess("DOB31 . 01 . 2018"));
//        assertEquals("DOB 31/01 / 2018123",TextPreProcessingUtility.preprocess("DOB 31/01 / 2018123"));

        // ssn formats
        assertEquals("SSN 333-22-4444 ",TextPreProcessingUtility.preprocess("SSN 333.22.4444"));
        assertEquals("SSN 333-22-4444 ",TextPreProcessingUtility.preprocess("SSN 333 - 22-4444"));
    }
}
