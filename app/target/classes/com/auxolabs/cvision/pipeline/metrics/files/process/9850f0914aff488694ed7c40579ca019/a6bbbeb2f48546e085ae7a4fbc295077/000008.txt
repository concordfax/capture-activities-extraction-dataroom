PAGE 1/003
Health Advocates
288 Hanover St., Ste B
Rocklin, California 95677
FAX
H-A
H t'-ALl'l i ADVOCATES
vf ■! V•	•
To:
MEDICAL RECORDS
GOLDEN VALLEY HEALTH CENTER
209-508-1006
Company:
Fax:
Phone:
Veronica Dean Johnny
(818) 201-3066
(818) 405-1027
verondj@healthadvocates.com
From:
Fax:
Phone:
E-mail:
NOTES:
Please find attached, the Request for Medical Records and HIPAA compliant
authorization.Please note that our preferred method of delivery is through electronic fax or
email at ssidocs@healthadvocates.com
Commitment • Experience • Compassion
HEALTH ADVOCATES CONFIDENTIALITY NOTICE - Tliis e-mail, including attachments, is intended for
the sola use of the incfividual(s) to whom it is addressed, and may contain Protected Health information (PH!)
that is privileged, confidential, and/or exempt from disclosure under applicable law. If you are not the intended
recipient, you are hereby notified that an.y unauthorised use, disclosure, copying or distribution of any PH! is
strict!*/prohibited and punishable under Federal and State laws. If you have received this e-mail In error, please
notify the sc-oiler by reply s-rnall or by telephone at (*18) 9*5-95*5, and destroy this message and its
attachments immediately, Thank you tor vour cooperation.
If you do not receive all of the pages, please call our office at (818) 405-1027 as soon as possible. If
you wish to respond by fax, please use (818) 201-3066.
Date and time of transmission: 1/14/2019 3:57:04 PM
Number of pages including this cover sheet: 3
