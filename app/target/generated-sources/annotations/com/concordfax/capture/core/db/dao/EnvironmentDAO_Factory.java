package com.concordfax.capture.core.db.dao;

import com.concordfax.capture.core.db.MongoUtil;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class EnvironmentDAO_Factory implements Factory<EnvironmentDAO> {
  private final Provider<MongoUtil> mongoUtilProvider;

  public EnvironmentDAO_Factory(Provider<MongoUtil> mongoUtilProvider) {
    assert mongoUtilProvider != null;
    this.mongoUtilProvider = mongoUtilProvider;
  }

  @Override
  public EnvironmentDAO get() {
    return new EnvironmentDAO(mongoUtilProvider.get());
  }

  public static Factory<EnvironmentDAO> create(Provider<MongoUtil> mongoUtilProvider) {
    return new EnvironmentDAO_Factory(mongoUtilProvider);
  }
}
