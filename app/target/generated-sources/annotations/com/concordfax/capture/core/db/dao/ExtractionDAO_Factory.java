package com.concordfax.capture.core.db.dao;

import com.concordfax.capture.core.db.MongoUtil;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ExtractionDAO_Factory implements Factory<ExtractionDAO> {
  private final Provider<MongoUtil> mongoUtilProvider;

  public ExtractionDAO_Factory(Provider<MongoUtil> mongoUtilProvider) {
    assert mongoUtilProvider != null;
    this.mongoUtilProvider = mongoUtilProvider;
  }

  @Override
  public ExtractionDAO get() {
    return new ExtractionDAO(mongoUtilProvider.get());
  }

  public static Factory<ExtractionDAO> create(Provider<MongoUtil> mongoUtilProvider) {
    return new ExtractionDAO_Factory(mongoUtilProvider);
  }
}
