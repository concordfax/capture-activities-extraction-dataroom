package com.concordfax.capture.core.db.dao;

import com.concordfax.capture.core.db.MongoUtil;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class DocumentDAO_Factory implements Factory<DocumentDAO> {
  private final Provider<MongoUtil> mongoUtilProvider;

  public DocumentDAO_Factory(Provider<MongoUtil> mongoUtilProvider) {
    assert mongoUtilProvider != null;
    this.mongoUtilProvider = mongoUtilProvider;
  }

  @Override
  public DocumentDAO get() {
    return new DocumentDAO(mongoUtilProvider.get());
  }

  public static Factory<DocumentDAO> create(Provider<MongoUtil> mongoUtilProvider) {
    return new DocumentDAO_Factory(mongoUtilProvider);
  }
}
