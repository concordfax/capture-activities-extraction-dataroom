package com.concordfax.capture.core.db;

import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class MongoUtil_Factory implements Factory<MongoUtil> {
  private final Provider<MongoConfigModel> mongoProvider;

  public MongoUtil_Factory(Provider<MongoConfigModel> mongoProvider) {
    assert mongoProvider != null;
    this.mongoProvider = mongoProvider;
  }

  @Override
  public MongoUtil get() {
    return new MongoUtil(mongoProvider.get());
  }

  public static Factory<MongoUtil> create(Provider<MongoConfigModel> mongoProvider) {
    return new MongoUtil_Factory(mongoProvider);
  }
}
