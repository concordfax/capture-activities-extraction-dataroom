package com.concordfax.capture.core.db.dao;

import com.concordfax.capture.core.db.MongoUtil;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class MLModelDAO_Factory implements Factory<MLModelDAO> {
  private final Provider<MongoUtil> mongoUtilProvider;

  public MLModelDAO_Factory(Provider<MongoUtil> mongoUtilProvider) {
    assert mongoUtilProvider != null;
    this.mongoUtilProvider = mongoUtilProvider;
  }

  @Override
  public MLModelDAO get() {
    return new MLModelDAO(mongoUtilProvider.get());
  }

  public static Factory<MLModelDAO> create(Provider<MongoUtil> mongoUtilProvider) {
    return new MLModelDAO_Factory(mongoUtilProvider);
  }
}
