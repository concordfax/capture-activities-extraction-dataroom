package com.concordfax.capture.core.db.dao;

import com.concordfax.capture.core.db.MongoUtil;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ProcessDAO_Factory implements Factory<ProcessDAO> {
  private final Provider<MongoUtil> mongoUtilProvider;

  public ProcessDAO_Factory(Provider<MongoUtil> mongoUtilProvider) {
    assert mongoUtilProvider != null;
    this.mongoUtilProvider = mongoUtilProvider;
  }

  @Override
  public ProcessDAO get() {
    return new ProcessDAO(mongoUtilProvider.get());
  }

  public static Factory<ProcessDAO> create(Provider<MongoUtil> mongoUtilProvider) {
    return new ProcessDAO_Factory(mongoUtilProvider);
  }
}
