package com.concordfax.capture.core.utils.mlmodel;

import com.concordfax.capture.core.logging.ILogger;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ModelServicesClient_Factory implements Factory<ModelServicesClient> {
  private final Provider<ILogger> loggerProvider;

  public ModelServicesClient_Factory(Provider<ILogger> loggerProvider) {
    assert loggerProvider != null;
    this.loggerProvider = loggerProvider;
  }

  @Override
  public ModelServicesClient get() {
    return new ModelServicesClient(loggerProvider.get());
  }

  public static Factory<ModelServicesClient> create(Provider<ILogger> loggerProvider) {
    return new ModelServicesClient_Factory(loggerProvider);
  }
}
