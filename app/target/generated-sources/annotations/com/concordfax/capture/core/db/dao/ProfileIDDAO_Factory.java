package com.concordfax.capture.core.db.dao;

import com.concordfax.capture.core.db.MongoUtil;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ProfileIDDAO_Factory implements Factory<ProfileIDDAO> {
  private final Provider<MongoUtil> mongoUtilProvider;

  public ProfileIDDAO_Factory(Provider<MongoUtil> mongoUtilProvider) {
    assert mongoUtilProvider != null;
    this.mongoUtilProvider = mongoUtilProvider;
  }

  @Override
  public ProfileIDDAO get() {
    return new ProfileIDDAO(mongoUtilProvider.get());
  }

  public static Factory<ProfileIDDAO> create(Provider<MongoUtil> mongoUtilProvider) {
    return new ProfileIDDAO_Factory(mongoUtilProvider);
  }
}
