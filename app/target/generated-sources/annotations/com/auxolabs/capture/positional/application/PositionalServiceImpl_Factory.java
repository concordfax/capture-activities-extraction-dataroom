package com.auxolabs.capture.positional.application;

import dagger.internal.Factory;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class PositionalServiceImpl_Factory implements Factory<PositionalServiceImpl> {
  private static final PositionalServiceImpl_Factory INSTANCE = new PositionalServiceImpl_Factory();

  @Override
  public PositionalServiceImpl get() {
    return new PositionalServiceImpl();
  }

  public static Factory<PositionalServiceImpl> create() {
    return INSTANCE;
  }
}
