package com.auxolabs.capture.mandrake.application;

import dagger.internal.Factory;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class MandrakeServiceImpl_Factory implements Factory<MandrakeServiceImpl> {
  private static final MandrakeServiceImpl_Factory INSTANCE = new MandrakeServiceImpl_Factory();

  @Override
  public MandrakeServiceImpl get() {
    return new MandrakeServiceImpl();
  }

  public static Factory<MandrakeServiceImpl> create() {
    return INSTANCE;
  }
}
