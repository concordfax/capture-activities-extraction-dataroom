package com.auxolabs.capture.annotator;

import dagger.internal.Factory;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class AnnotationsProcessor_Factory implements Factory<AnnotationsProcessor> {
  private static final AnnotationsProcessor_Factory INSTANCE = new AnnotationsProcessor_Factory();

  @Override
  public AnnotationsProcessor get() {
    return new AnnotationsProcessor();
  }

  public static Factory<AnnotationsProcessor> create() {
    return INSTANCE;
  }
}
