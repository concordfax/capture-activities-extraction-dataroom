package com.auxolabs.capture.dateparser.application;

import dagger.internal.Factory;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class DateParserServiceImpl_Factory implements Factory<DateParserServiceImpl> {
  private static final DateParserServiceImpl_Factory INSTANCE = new DateParserServiceImpl_Factory();

  @Override
  public DateParserServiceImpl get() {
    return new DateParserServiceImpl();
  }

  public static Factory<DateParserServiceImpl> create() {
    return INSTANCE;
  }
}
