package com.auxolabs.cvision.ml;

import dagger.internal.Factory;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class MLExtractorServiceImpl_Factory implements Factory<MLExtractorServiceImpl> {
  private static final MLExtractorServiceImpl_Factory INSTANCE =
      new MLExtractorServiceImpl_Factory();

  @Override
  public MLExtractorServiceImpl get() {
    return new MLExtractorServiceImpl();
  }

  public static Factory<MLExtractorServiceImpl> create() {
    return INSTANCE;
  }
}
