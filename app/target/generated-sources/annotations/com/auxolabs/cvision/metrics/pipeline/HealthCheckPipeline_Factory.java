package com.auxolabs.cvision.metrics.pipeline;

import com.concordfax.capture.core.pipeline.PipelineStep;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class HealthCheckPipeline_Factory implements Factory<HealthCheckPipeline> {
  private final Provider<PipelineStep[]> pipelineStepsProvider;

  public HealthCheckPipeline_Factory(Provider<PipelineStep[]> pipelineStepsProvider) {
    assert pipelineStepsProvider != null;
    this.pipelineStepsProvider = pipelineStepsProvider;
  }

  @Override
  public HealthCheckPipeline get() {
    return new HealthCheckPipeline(pipelineStepsProvider.get());
  }

  public static Factory<HealthCheckPipeline> create(
      Provider<PipelineStep[]> pipelineStepsProvider) {
    return new HealthCheckPipeline_Factory(pipelineStepsProvider);
  }
}
