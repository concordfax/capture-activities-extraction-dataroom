package com.auxolabs.cvision.metrics.pipeline;

import com.concordfax.capture.core.jobmanager.JobManagerConfig;
import com.concordfax.capture.core.pipeline.Pipeline;
import dagger.MembersInjector;
import dagger.internal.Factory;
import dagger.internal.MembersInjectors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class MetricsJobManager_Factory implements Factory<MetricsJobManager> {
  private final MembersInjector<MetricsJobManager> metricsJobManagerMembersInjector;

  private final Provider<JobManagerConfig> jobManagerConfigProvider;

  private final Provider<Pipeline> healthCheckPipelineProvider;

  public MetricsJobManager_Factory(
      MembersInjector<MetricsJobManager> metricsJobManagerMembersInjector,
      Provider<JobManagerConfig> jobManagerConfigProvider,
      Provider<Pipeline> healthCheckPipelineProvider) {
    assert metricsJobManagerMembersInjector != null;
    this.metricsJobManagerMembersInjector = metricsJobManagerMembersInjector;
    assert jobManagerConfigProvider != null;
    this.jobManagerConfigProvider = jobManagerConfigProvider;
    assert healthCheckPipelineProvider != null;
    this.healthCheckPipelineProvider = healthCheckPipelineProvider;
  }

  @Override
  public MetricsJobManager get() {
    return MembersInjectors.injectMembers(
        metricsJobManagerMembersInjector,
        new MetricsJobManager(jobManagerConfigProvider.get(), healthCheckPipelineProvider.get()));
  }

  public static Factory<MetricsJobManager> create(
      MembersInjector<MetricsJobManager> metricsJobManagerMembersInjector,
      Provider<JobManagerConfig> jobManagerConfigProvider,
      Provider<Pipeline> healthCheckPipelineProvider) {
    return new MetricsJobManager_Factory(
        metricsJobManagerMembersInjector, jobManagerConfigProvider, healthCheckPipelineProvider);
  }
}
