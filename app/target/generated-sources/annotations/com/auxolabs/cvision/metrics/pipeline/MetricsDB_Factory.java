package com.auxolabs.cvision.metrics.pipeline;

import dagger.internal.Factory;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class MetricsDB_Factory implements Factory<MetricsDB> {
  private static final MetricsDB_Factory INSTANCE = new MetricsDB_Factory();

  @Override
  public MetricsDB get() {
    return new MetricsDB();
  }

  public static Factory<MetricsDB> create() {
    return INSTANCE;
  }
}
