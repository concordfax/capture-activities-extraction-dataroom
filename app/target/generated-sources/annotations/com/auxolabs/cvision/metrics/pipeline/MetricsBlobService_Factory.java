package com.auxolabs.cvision.metrics.pipeline;

import dagger.internal.Factory;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class MetricsBlobService_Factory implements Factory<MetricsBlobService> {
  private static final MetricsBlobService_Factory INSTANCE = new MetricsBlobService_Factory();

  @Override
  public MetricsBlobService get() {
    return new MetricsBlobService();
  }

  public static Factory<MetricsBlobService> create() {
    return INSTANCE;
  }
}
