package com.auxolabs.cvision.metrics.pipeline;

import dagger.internal.Factory;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class MetricsAnnotationsService_Factory implements Factory<MetricsAnnotationsService> {
  private static final MetricsAnnotationsService_Factory INSTANCE =
      new MetricsAnnotationsService_Factory();

  @Override
  public MetricsAnnotationsService get() {
    return new MetricsAnnotationsService();
  }

  public static Factory<MetricsAnnotationsService> create() {
    return INSTANCE;
  }
}
