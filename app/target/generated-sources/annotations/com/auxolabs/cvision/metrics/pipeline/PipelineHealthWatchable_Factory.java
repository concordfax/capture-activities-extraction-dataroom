package com.auxolabs.cvision.metrics.pipeline;

import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class PipelineHealthWatchable_Factory implements Factory<PipelineHealthWatchable> {
  private final Provider<MetricsJobManager> metricsJobManagerProvider;

  public PipelineHealthWatchable_Factory(Provider<MetricsJobManager> metricsJobManagerProvider) {
    assert metricsJobManagerProvider != null;
    this.metricsJobManagerProvider = metricsJobManagerProvider;
  }

  @Override
  public PipelineHealthWatchable get() {
    return new PipelineHealthWatchable(metricsJobManagerProvider.get());
  }

  public static Factory<PipelineHealthWatchable> create(
      Provider<MetricsJobManager> metricsJobManagerProvider) {
    return new PipelineHealthWatchable_Factory(metricsJobManagerProvider);
  }
}
