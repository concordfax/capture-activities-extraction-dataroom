package com.auxolabs.cvision.metrics.pipeline;

import dagger.internal.Factory;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class MetricsLogger_Factory implements Factory<MetricsLogger> {
  private static final MetricsLogger_Factory INSTANCE = new MetricsLogger_Factory();

  @Override
  public MetricsLogger get() {
    return new MetricsLogger();
  }

  public static Factory<MetricsLogger> create() {
    return INSTANCE;
  }
}
