package com.auxolabs.cvision.metrics.pipeline;

import dagger.internal.Factory;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class MetricsDependencyResolver_Factory implements Factory<MetricsDependencyResolver> {
  private static final MetricsDependencyResolver_Factory INSTANCE =
      new MetricsDependencyResolver_Factory();

  @Override
  public MetricsDependencyResolver get() {
    return new MetricsDependencyResolver();
  }

  public static Factory<MetricsDependencyResolver> create() {
    return INSTANCE;
  }
}
