package com.auxolabs.cvision.metrics;

import dagger.internal.Factory;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class DBHealthWatchable_Factory implements Factory<DBHealthWatchable> {
  private static final DBHealthWatchable_Factory INSTANCE = new DBHealthWatchable_Factory();

  @Override
  public DBHealthWatchable get() {
    return new DBHealthWatchable();
  }

  public static Factory<DBHealthWatchable> create() {
    return INSTANCE;
  }
}
