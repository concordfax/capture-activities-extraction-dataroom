package com.auxolabs.cvision.metrics.pipeline;

import dagger.internal.Factory;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class MetricsModelServicesClient_Factory
    implements Factory<MetricsModelServicesClient> {
  private static final MetricsModelServicesClient_Factory INSTANCE =
      new MetricsModelServicesClient_Factory();

  @Override
  public MetricsModelServicesClient get() {
    return new MetricsModelServicesClient();
  }

  public static Factory<MetricsModelServicesClient> create() {
    return INSTANCE;
  }
}
