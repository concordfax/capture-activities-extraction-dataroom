package com.auxolabs.cvision.db;

import com.concordfax.capture.core.db.dao.DocumentDAO;
import com.concordfax.capture.core.db.dao.EnvironmentDAO;
import com.concordfax.capture.core.db.dao.ExtractionDAO;
import com.concordfax.capture.core.db.dao.MLModelDAO;
import com.concordfax.capture.core.db.dao.ProcessDAO;
import com.concordfax.capture.core.db.dao.ProfileIDDAO;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class AzureCosmosDBService_Factory implements Factory<AzureCosmosDBService> {
  private final Provider<DocumentDAO> documentDAOProvider;

  private final Provider<ProcessDAO> processDAOProvider;

  private final Provider<EnvironmentDAO> environmentDAOProvider;

  private final Provider<MLModelDAO> mlModelDAOProvider;

  private final Provider<ExtractionDAO> extractionDAOProvider;

  private final Provider<ProfileIDDAO> profileIDDAOProvider;

  public AzureCosmosDBService_Factory(
      Provider<DocumentDAO> documentDAOProvider,
      Provider<ProcessDAO> processDAOProvider,
      Provider<EnvironmentDAO> environmentDAOProvider,
      Provider<MLModelDAO> mlModelDAOProvider,
      Provider<ExtractionDAO> extractionDAOProvider,
      Provider<ProfileIDDAO> profileIDDAOProvider) {
    assert documentDAOProvider != null;
    this.documentDAOProvider = documentDAOProvider;
    assert processDAOProvider != null;
    this.processDAOProvider = processDAOProvider;
    assert environmentDAOProvider != null;
    this.environmentDAOProvider = environmentDAOProvider;
    assert mlModelDAOProvider != null;
    this.mlModelDAOProvider = mlModelDAOProvider;
    assert extractionDAOProvider != null;
    this.extractionDAOProvider = extractionDAOProvider;
    assert profileIDDAOProvider != null;
    this.profileIDDAOProvider = profileIDDAOProvider;
  }

  @Override
  public AzureCosmosDBService get() {
    return new AzureCosmosDBService(
        documentDAOProvider.get(),
        processDAOProvider.get(),
        environmentDAOProvider.get(),
        mlModelDAOProvider.get(),
        extractionDAOProvider.get(),
        profileIDDAOProvider.get());
  }

  public static Factory<AzureCosmosDBService> create(
      Provider<DocumentDAO> documentDAOProvider,
      Provider<ProcessDAO> processDAOProvider,
      Provider<EnvironmentDAO> environmentDAOProvider,
      Provider<MLModelDAO> mlModelDAOProvider,
      Provider<ExtractionDAO> extractionDAOProvider,
      Provider<ProfileIDDAO> profileIDDAOProvider) {
    return new AzureCosmosDBService_Factory(
        documentDAOProvider,
        processDAOProvider,
        environmentDAOProvider,
        mlModelDAOProvider,
        extractionDAOProvider,
        profileIDDAOProvider);
  }
}
