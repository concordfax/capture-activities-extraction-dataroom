package com.auxolabs.cvision.pipeline.steps.helpers;

import com.concordfax.capture.core.logging.ILogger;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class NpiExtractionStep_Factory implements Factory<NpiExtractionStep> {
  private final Provider<ILogger> logProvider;

  public NpiExtractionStep_Factory(Provider<ILogger> logProvider) {
    assert logProvider != null;
    this.logProvider = logProvider;
  }

  @Override
  public NpiExtractionStep get() {
    return new NpiExtractionStep(logProvider.get());
  }

  public static Factory<NpiExtractionStep> create(Provider<ILogger> logProvider) {
    return new NpiExtractionStep_Factory(logProvider);
  }
}
