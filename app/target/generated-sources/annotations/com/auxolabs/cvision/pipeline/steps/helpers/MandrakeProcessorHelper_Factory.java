package com.auxolabs.cvision.pipeline.steps.helpers;

import com.auxolabs.capture.mandrake.MandrakeService;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class MandrakeProcessorHelper_Factory implements Factory<MandrakeProcessorHelper> {
  private final Provider<MandrakeService> mandrakeServiceProvider;

  public MandrakeProcessorHelper_Factory(Provider<MandrakeService> mandrakeServiceProvider) {
    assert mandrakeServiceProvider != null;
    this.mandrakeServiceProvider = mandrakeServiceProvider;
  }

  @Override
  public MandrakeProcessorHelper get() {
    return new MandrakeProcessorHelper(mandrakeServiceProvider.get());
  }

  public static Factory<MandrakeProcessorHelper> create(
      Provider<MandrakeService> mandrakeServiceProvider) {
    return new MandrakeProcessorHelper_Factory(mandrakeServiceProvider);
  }
}
