package com.auxolabs.cvision.pipeline.steps;

import com.concordfax.capture.core.logging.ILogger;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class OutputConfigurationProcessorStep_Factory
    implements Factory<OutputConfigurationProcessorStep> {
  private final Provider<ILogger> logProvider;

  public OutputConfigurationProcessorStep_Factory(Provider<ILogger> logProvider) {
    assert logProvider != null;
    this.logProvider = logProvider;
  }

  @Override
  public OutputConfigurationProcessorStep get() {
    return new OutputConfigurationProcessorStep(logProvider.get());
  }

  public static Factory<OutputConfigurationProcessorStep> create(Provider<ILogger> logProvider) {
    return new OutputConfigurationProcessorStep_Factory(logProvider);
  }
}
