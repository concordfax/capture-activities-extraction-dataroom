package com.auxolabs.cvision.pipeline.steps;

import com.auxolabs.capture.positional.PositionalService;
import com.concordfax.capture.core.logging.ILogger;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class PositionalExtractorStep_Factory implements Factory<PositionalExtractorStep> {
  private final Provider<ILogger> logProvider;

  private final Provider<PositionalService> positionalServiceProvider;

  public PositionalExtractorStep_Factory(
      Provider<ILogger> logProvider, Provider<PositionalService> positionalServiceProvider) {
    assert logProvider != null;
    this.logProvider = logProvider;
    assert positionalServiceProvider != null;
    this.positionalServiceProvider = positionalServiceProvider;
  }

  @Override
  public PositionalExtractorStep get() {
    return new PositionalExtractorStep(logProvider.get(), positionalServiceProvider.get());
  }

  public static Factory<PositionalExtractorStep> create(
      Provider<ILogger> logProvider, Provider<PositionalService> positionalServiceProvider) {
    return new PositionalExtractorStep_Factory(logProvider, positionalServiceProvider);
  }
}
