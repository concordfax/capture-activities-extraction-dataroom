package com.auxolabs.cvision.pipeline.steps;

import com.auxolabs.cvision.ml.MLExtractorService;
import com.concordfax.capture.core.logging.ILogger;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class AnnotationExtractorStep_Factory implements Factory<AnnotationExtractorStep> {
  private final Provider<ILogger> logProvider;

  private final Provider<MLExtractorService> mlExtractorServiceProvider;

  public AnnotationExtractorStep_Factory(
      Provider<ILogger> logProvider, Provider<MLExtractorService> mlExtractorServiceProvider) {
    assert logProvider != null;
    this.logProvider = logProvider;
    assert mlExtractorServiceProvider != null;
    this.mlExtractorServiceProvider = mlExtractorServiceProvider;
  }

  @Override
  public AnnotationExtractorStep get() {
    return new AnnotationExtractorStep(logProvider.get(), mlExtractorServiceProvider.get());
  }

  public static Factory<AnnotationExtractorStep> create(
      Provider<ILogger> logProvider, Provider<MLExtractorService> mlExtractorServiceProvider) {
    return new AnnotationExtractorStep_Factory(logProvider, mlExtractorServiceProvider);
  }
}
