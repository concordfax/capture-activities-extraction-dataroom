package com.auxolabs.cvision.pipeline.steps;

import com.auxolabs.cvision.db.IDBService;
import com.concordfax.capture.core.logging.ILogger;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ResultWriterStep_Factory implements Factory<ResultWriterStep> {
  private final Provider<ILogger> logProvider;

  private final Provider<IDBService> dbProvider;

  public ResultWriterStep_Factory(Provider<ILogger> logProvider, Provider<IDBService> dbProvider) {
    assert logProvider != null;
    this.logProvider = logProvider;
    assert dbProvider != null;
    this.dbProvider = dbProvider;
  }

  @Override
  public ResultWriterStep get() {
    return new ResultWriterStep(logProvider.get(), dbProvider.get());
  }

  public static Factory<ResultWriterStep> create(
      Provider<ILogger> logProvider, Provider<IDBService> dbProvider) {
    return new ResultWriterStep_Factory(logProvider, dbProvider);
  }
}
