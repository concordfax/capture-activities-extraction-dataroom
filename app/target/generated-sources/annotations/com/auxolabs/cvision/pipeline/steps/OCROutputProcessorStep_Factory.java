package com.auxolabs.cvision.pipeline.steps;

import com.concordfax.capture.core.logging.ILogger;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class OCROutputProcessorStep_Factory implements Factory<OCROutputProcessorStep> {
  private final Provider<ILogger> iLoggerProvider;

  public OCROutputProcessorStep_Factory(Provider<ILogger> iLoggerProvider) {
    assert iLoggerProvider != null;
    this.iLoggerProvider = iLoggerProvider;
  }

  @Override
  public OCROutputProcessorStep get() {
    return new OCROutputProcessorStep(iLoggerProvider.get());
  }

  public static Factory<OCROutputProcessorStep> create(Provider<ILogger> iLoggerProvider) {
    return new OCROutputProcessorStep_Factory(iLoggerProvider);
  }
}
