package com.auxolabs.cvision.pipeline.steps;

import com.auxolabs.cvision.db.IDBService;
import com.concordfax.capture.core.logging.ILogger;
import com.concordfax.capture.core.storage.blobstore.IBlobService;
import com.concordfax.capture.core.utils.dependency.IDependencyResolverClient;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class DocumentPropsFetcherStep_Factory implements Factory<DocumentPropsFetcherStep> {
  private final Provider<ILogger> logProvider;

  private final Provider<IDBService> dbProvider;

  private final Provider<IBlobService> filestoreProvider;

  private final Provider<IDependencyResolverClient> dependencyResolverClientProvider;

  public DocumentPropsFetcherStep_Factory(
      Provider<ILogger> logProvider,
      Provider<IDBService> dbProvider,
      Provider<IBlobService> filestoreProvider,
      Provider<IDependencyResolverClient> dependencyResolverClientProvider) {
    assert logProvider != null;
    this.logProvider = logProvider;
    assert dbProvider != null;
    this.dbProvider = dbProvider;
    assert filestoreProvider != null;
    this.filestoreProvider = filestoreProvider;
    assert dependencyResolverClientProvider != null;
    this.dependencyResolverClientProvider = dependencyResolverClientProvider;
  }

  @Override
  public DocumentPropsFetcherStep get() {
    return new DocumentPropsFetcherStep(
        logProvider.get(),
        dbProvider.get(),
        filestoreProvider.get(),
        dependencyResolverClientProvider.get());
  }

  public static Factory<DocumentPropsFetcherStep> create(
      Provider<ILogger> logProvider,
      Provider<IDBService> dbProvider,
      Provider<IBlobService> filestoreProvider,
      Provider<IDependencyResolverClient> dependencyResolverClientProvider) {
    return new DocumentPropsFetcherStep_Factory(
        logProvider, dbProvider, filestoreProvider, dependencyResolverClientProvider);
  }
}
