package com.auxolabs.cvision.pipeline.steps;

import com.concordfax.capture.core.logging.ILogger;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class SegmentationStep_Factory implements Factory<SegmentationStep> {
  private final Provider<ILogger> loggerProvider;

  public SegmentationStep_Factory(Provider<ILogger> loggerProvider) {
    assert loggerProvider != null;
    this.loggerProvider = loggerProvider;
  }

  @Override
  public SegmentationStep get() {
    return new SegmentationStep(loggerProvider.get());
  }

  public static Factory<SegmentationStep> create(Provider<ILogger> loggerProvider) {
    return new SegmentationStep_Factory(loggerProvider);
  }
}
