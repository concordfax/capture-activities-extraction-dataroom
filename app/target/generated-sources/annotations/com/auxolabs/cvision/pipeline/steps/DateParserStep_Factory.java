package com.auxolabs.cvision.pipeline.steps;

import com.auxolabs.capture.dateparser.DPEngineService;
import com.concordfax.capture.core.logging.ILogger;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class DateParserStep_Factory implements Factory<DateParserStep> {
  private final Provider<ILogger> logProvider;

  private final Provider<DPEngineService> dpEngineServiceProvider;

  public DateParserStep_Factory(
      Provider<ILogger> logProvider, Provider<DPEngineService> dpEngineServiceProvider) {
    assert logProvider != null;
    this.logProvider = logProvider;
    assert dpEngineServiceProvider != null;
    this.dpEngineServiceProvider = dpEngineServiceProvider;
  }

  @Override
  public DateParserStep get() {
    return new DateParserStep(logProvider.get(), dpEngineServiceProvider.get());
  }

  public static Factory<DateParserStep> create(
      Provider<ILogger> logProvider, Provider<DPEngineService> dpEngineServiceProvider) {
    return new DateParserStep_Factory(logProvider, dpEngineServiceProvider);
  }
}
