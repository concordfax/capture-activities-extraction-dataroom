package com.auxolabs.cvision.pipeline.steps;

import com.auxolabs.capture.mandrake.MandrakeService;
import com.concordfax.capture.core.logging.ILogger;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class MandrakeExtractorStep_Factory implements Factory<MandrakeExtractorStep> {
  private final Provider<ILogger> logProvider;

  private final Provider<MandrakeService> mandrakeServiceProvider;

  public MandrakeExtractorStep_Factory(
      Provider<ILogger> logProvider, Provider<MandrakeService> mandrakeServiceProvider) {
    assert logProvider != null;
    this.logProvider = logProvider;
    assert mandrakeServiceProvider != null;
    this.mandrakeServiceProvider = mandrakeServiceProvider;
  }

  @Override
  public MandrakeExtractorStep get() {
    return new MandrakeExtractorStep(logProvider.get(), mandrakeServiceProvider.get());
  }

  public static Factory<MandrakeExtractorStep> create(
      Provider<ILogger> logProvider, Provider<MandrakeService> mandrakeServiceProvider) {
    return new MandrakeExtractorStep_Factory(logProvider, mandrakeServiceProvider);
  }
}
