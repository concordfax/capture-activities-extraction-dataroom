package com.auxolabs.cvision.pipeline.steps;

import com.auxolabs.cvision.ml.MLExtractorService;
import com.concordfax.capture.core.logging.ILogger;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class MachineLearningExtractorStep_Factory
    implements Factory<MachineLearningExtractorStep> {
  private final Provider<ILogger> logProvider;

  private final Provider<MLExtractorService> mlExtractorServiceProvider;

  public MachineLearningExtractorStep_Factory(
      Provider<ILogger> logProvider, Provider<MLExtractorService> mlExtractorServiceProvider) {
    assert logProvider != null;
    this.logProvider = logProvider;
    assert mlExtractorServiceProvider != null;
    this.mlExtractorServiceProvider = mlExtractorServiceProvider;
  }

  @Override
  public MachineLearningExtractorStep get() {
    return new MachineLearningExtractorStep(logProvider.get(), mlExtractorServiceProvider.get());
  }

  public static Factory<MachineLearningExtractorStep> create(
      Provider<ILogger> logProvider, Provider<MLExtractorService> mlExtractorServiceProvider) {
    return new MachineLearningExtractorStep_Factory(logProvider, mlExtractorServiceProvider);
  }
}
