package com.auxolabs.cvision.pipeline.steps.helpers;

import com.auxolabs.cvision.ml.MLExtractorService;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class MachineLearningExtractorHelper_Factory
    implements Factory<MachineLearningExtractorHelper> {
  private final Provider<MLExtractorService> mlExtractorServiceProvider;

  public MachineLearningExtractorHelper_Factory(
      Provider<MLExtractorService> mlExtractorServiceProvider) {
    assert mlExtractorServiceProvider != null;
    this.mlExtractorServiceProvider = mlExtractorServiceProvider;
  }

  @Override
  public MachineLearningExtractorHelper get() {
    return new MachineLearningExtractorHelper(mlExtractorServiceProvider.get());
  }

  public static Factory<MachineLearningExtractorHelper> create(
      Provider<MLExtractorService> mlExtractorServiceProvider) {
    return new MachineLearningExtractorHelper_Factory(mlExtractorServiceProvider);
  }
}
