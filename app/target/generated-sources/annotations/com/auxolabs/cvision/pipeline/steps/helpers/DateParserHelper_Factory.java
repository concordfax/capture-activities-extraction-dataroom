package com.auxolabs.cvision.pipeline.steps.helpers;

import com.auxolabs.capture.dateparser.DPEngineService;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class DateParserHelper_Factory implements Factory<DateParserHelper> {
  private final Provider<DPEngineService> dpEngineServiceProvider;

  public DateParserHelper_Factory(Provider<DPEngineService> dpEngineServiceProvider) {
    assert dpEngineServiceProvider != null;
    this.dpEngineServiceProvider = dpEngineServiceProvider;
  }

  @Override
  public DateParserHelper get() {
    return new DateParserHelper(dpEngineServiceProvider.get());
  }

  public static Factory<DateParserHelper> create(
      Provider<DPEngineService> dpEngineServiceProvider) {
    return new DateParserHelper_Factory(dpEngineServiceProvider);
  }
}
