package com.auxolabs.cvision.pipeline.steps;

import com.auxolabs.capture.annotator.AnnotatorService;
import com.concordfax.capture.core.logging.ILogger;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class AnnotationAndMappingStep_Factory implements Factory<AnnotationAndMappingStep> {
  private final Provider<ILogger> logProvider;

  private final Provider<AnnotatorService> annotatorServiceProvider;

  public AnnotationAndMappingStep_Factory(
      Provider<ILogger> logProvider, Provider<AnnotatorService> annotatorServiceProvider) {
    assert logProvider != null;
    this.logProvider = logProvider;
    assert annotatorServiceProvider != null;
    this.annotatorServiceProvider = annotatorServiceProvider;
  }

  @Override
  public AnnotationAndMappingStep get() {
    return new AnnotationAndMappingStep(logProvider.get(), annotatorServiceProvider.get());
  }

  public static Factory<AnnotationAndMappingStep> create(
      Provider<ILogger> logProvider, Provider<AnnotatorService> annotatorServiceProvider) {
    return new AnnotationAndMappingStep_Factory(logProvider, annotatorServiceProvider);
  }
}
