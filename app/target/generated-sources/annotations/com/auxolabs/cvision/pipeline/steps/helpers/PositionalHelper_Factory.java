package com.auxolabs.cvision.pipeline.steps.helpers;

import com.auxolabs.capture.positional.PositionalService;
import com.concordfax.capture.core.logging.ILogger;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class PositionalHelper_Factory implements Factory<PositionalHelper> {
  private final Provider<PositionalService> positionalServiceProvider;

  private final Provider<ILogger> iLoggerProvider;

  public PositionalHelper_Factory(
      Provider<PositionalService> positionalServiceProvider, Provider<ILogger> iLoggerProvider) {
    assert positionalServiceProvider != null;
    this.positionalServiceProvider = positionalServiceProvider;
    assert iLoggerProvider != null;
    this.iLoggerProvider = iLoggerProvider;
  }

  @Override
  public PositionalHelper get() {
    return new PositionalHelper(positionalServiceProvider.get(), iLoggerProvider.get());
  }

  public static Factory<PositionalHelper> create(
      Provider<PositionalService> positionalServiceProvider, Provider<ILogger> iLoggerProvider) {
    return new PositionalHelper_Factory(positionalServiceProvider, iLoggerProvider);
  }
}
