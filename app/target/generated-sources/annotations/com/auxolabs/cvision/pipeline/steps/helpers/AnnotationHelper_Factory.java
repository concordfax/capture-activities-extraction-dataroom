package com.auxolabs.cvision.pipeline.steps.helpers;

import com.auxolabs.capture.annotator.AnnotatorService;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class AnnotationHelper_Factory implements Factory<AnnotationHelper> {
  private final Provider<AnnotatorService> annotationsProcessorProvider;

  public AnnotationHelper_Factory(Provider<AnnotatorService> annotationsProcessorProvider) {
    assert annotationsProcessorProvider != null;
    this.annotationsProcessorProvider = annotationsProcessorProvider;
  }

  @Override
  public AnnotationHelper get() {
    return new AnnotationHelper(annotationsProcessorProvider.get());
  }

  public static Factory<AnnotationHelper> create(
      Provider<AnnotatorService> annotationsProcessorProvider) {
    return new AnnotationHelper_Factory(annotationsProcessorProvider);
  }
}
