package com.auxolabs.cvision.pipeline.steps;

import com.concordfax.capture.core.logging.ILogger;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ImageProcessingStep_Factory implements Factory<ImageProcessingStep> {
  private final Provider<ILogger> logProvider;

  public ImageProcessingStep_Factory(Provider<ILogger> logProvider) {
    assert logProvider != null;
    this.logProvider = logProvider;
  }

  @Override
  public ImageProcessingStep get() {
    return new ImageProcessingStep(logProvider.get());
  }

  public static Factory<ImageProcessingStep> create(Provider<ILogger> logProvider) {
    return new ImageProcessingStep_Factory(logProvider);
  }
}
