package com.auxolabs.cvision.pipeline.steps.helpers;

import com.auxolabs.cvision.ml.MLExtractorService;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class AnnotationExtractorHelper_Factory implements Factory<AnnotationExtractorHelper> {
  private final Provider<MLExtractorService> mlExtractorServiceProvider;

  public AnnotationExtractorHelper_Factory(
      Provider<MLExtractorService> mlExtractorServiceProvider) {
    assert mlExtractorServiceProvider != null;
    this.mlExtractorServiceProvider = mlExtractorServiceProvider;
  }

  @Override
  public AnnotationExtractorHelper get() {
    return new AnnotationExtractorHelper(mlExtractorServiceProvider.get());
  }

  public static Factory<AnnotationExtractorHelper> create(
      Provider<MLExtractorService> mlExtractorServiceProvider) {
    return new AnnotationExtractorHelper_Factory(mlExtractorServiceProvider);
  }
}
