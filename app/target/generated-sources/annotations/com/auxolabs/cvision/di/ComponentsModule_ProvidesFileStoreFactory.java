package com.auxolabs.cvision.di;

import com.auxolabs.cvision.web.config.CaptureConfig;
import com.concordfax.capture.core.storage.blobstore.IBlobService;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ComponentsModule_ProvidesFileStoreFactory implements Factory<IBlobService> {
  private final ComponentsModule module;

  private final Provider<CaptureConfig> captureConfigProvider;

  public ComponentsModule_ProvidesFileStoreFactory(
      ComponentsModule module, Provider<CaptureConfig> captureConfigProvider) {
    assert module != null;
    this.module = module;
    assert captureConfigProvider != null;
    this.captureConfigProvider = captureConfigProvider;
  }

  @Override
  public IBlobService get() {
    return Preconditions.checkNotNull(
        module.providesFileStore(captureConfigProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<IBlobService> create(
      ComponentsModule module, Provider<CaptureConfig> captureConfigProvider) {
    return new ComponentsModule_ProvidesFileStoreFactory(module, captureConfigProvider);
  }

  /** Proxies {@link ComponentsModule#providesFileStore(CaptureConfig)}. */
  public static IBlobService proxyProvidesFileStore(
      ComponentsModule instance, CaptureConfig captureConfig) {
    return instance.providesFileStore(captureConfig);
  }
}
