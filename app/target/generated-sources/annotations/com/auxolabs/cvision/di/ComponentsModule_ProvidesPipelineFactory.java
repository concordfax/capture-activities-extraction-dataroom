package com.auxolabs.cvision.di;

import com.auxolabs.capture.annotator.AnnotatorService;
import com.auxolabs.capture.dateparser.DPEngineService;
import com.auxolabs.capture.mandrake.MandrakeService;
import com.auxolabs.capture.positional.PositionalService;
import com.auxolabs.cvision.db.IDBService;
import com.auxolabs.cvision.ml.MLExtractorService;
import com.concordfax.capture.core.logging.ILogger;
import com.concordfax.capture.core.pipeline.Pipeline;
import com.concordfax.capture.core.storage.blobstore.IBlobService;
import com.concordfax.capture.core.utils.dependency.IDependencyResolverClient;
import com.concordfax.capture.core.utils.mlmodel.ModelClientService;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ComponentsModule_ProvidesPipelineFactory implements Factory<Pipeline> {
  private final ComponentsModule module;

  private final Provider<ILogger> loggerProvider;

  private final Provider<IDBService> idbServiceProvider;

  private final Provider<IBlobService> blobServiceProvider;

  private final Provider<MandrakeService> mandrakeServiceProvider;

  private final Provider<MLExtractorService> mlExtractorServiceProvider;

  private final Provider<IDependencyResolverClient> dependencyResolverClientProvider;

  private final Provider<DPEngineService> dpEngineServiceProvider;

  private final Provider<ModelClientService> modelServicesClientProvider;

  private final Provider<PositionalService> positionalServiceProvider;

  private final Provider<AnnotatorService> annotatorServiceProvider;

  public ComponentsModule_ProvidesPipelineFactory(
      ComponentsModule module,
      Provider<ILogger> loggerProvider,
      Provider<IDBService> idbServiceProvider,
      Provider<IBlobService> blobServiceProvider,
      Provider<MandrakeService> mandrakeServiceProvider,
      Provider<MLExtractorService> mlExtractorServiceProvider,
      Provider<IDependencyResolverClient> dependencyResolverClientProvider,
      Provider<DPEngineService> dpEngineServiceProvider,
      Provider<ModelClientService> modelServicesClientProvider,
      Provider<PositionalService> positionalServiceProvider,
      Provider<AnnotatorService> annotatorServiceProvider) {
    assert module != null;
    this.module = module;
    assert loggerProvider != null;
    this.loggerProvider = loggerProvider;
    assert idbServiceProvider != null;
    this.idbServiceProvider = idbServiceProvider;
    assert blobServiceProvider != null;
    this.blobServiceProvider = blobServiceProvider;
    assert mandrakeServiceProvider != null;
    this.mandrakeServiceProvider = mandrakeServiceProvider;
    assert mlExtractorServiceProvider != null;
    this.mlExtractorServiceProvider = mlExtractorServiceProvider;
    assert dependencyResolverClientProvider != null;
    this.dependencyResolverClientProvider = dependencyResolverClientProvider;
    assert dpEngineServiceProvider != null;
    this.dpEngineServiceProvider = dpEngineServiceProvider;
    assert modelServicesClientProvider != null;
    this.modelServicesClientProvider = modelServicesClientProvider;
    assert positionalServiceProvider != null;
    this.positionalServiceProvider = positionalServiceProvider;
    assert annotatorServiceProvider != null;
    this.annotatorServiceProvider = annotatorServiceProvider;
  }

  @Override
  public Pipeline get() {
    return Preconditions.checkNotNull(
        module.providesPipeline(
            loggerProvider.get(),
            idbServiceProvider.get(),
            blobServiceProvider.get(),
            mandrakeServiceProvider.get(),
            mlExtractorServiceProvider.get(),
            dependencyResolverClientProvider.get(),
            dpEngineServiceProvider.get(),
            modelServicesClientProvider.get(),
            positionalServiceProvider.get(),
            annotatorServiceProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<Pipeline> create(
      ComponentsModule module,
      Provider<ILogger> loggerProvider,
      Provider<IDBService> idbServiceProvider,
      Provider<IBlobService> blobServiceProvider,
      Provider<MandrakeService> mandrakeServiceProvider,
      Provider<MLExtractorService> mlExtractorServiceProvider,
      Provider<IDependencyResolverClient> dependencyResolverClientProvider,
      Provider<DPEngineService> dpEngineServiceProvider,
      Provider<ModelClientService> modelServicesClientProvider,
      Provider<PositionalService> positionalServiceProvider,
      Provider<AnnotatorService> annotatorServiceProvider) {
    return new ComponentsModule_ProvidesPipelineFactory(
        module,
        loggerProvider,
        idbServiceProvider,
        blobServiceProvider,
        mandrakeServiceProvider,
        mlExtractorServiceProvider,
        dependencyResolverClientProvider,
        dpEngineServiceProvider,
        modelServicesClientProvider,
        positionalServiceProvider,
        annotatorServiceProvider);
  }

  /**
   * Proxies {@link ComponentsModule#providesPipeline(ILogger, IDBService, IBlobService,
   * MandrakeService, MLExtractorService, IDependencyResolverClient, DPEngineService,
   * ModelClientService, PositionalService, AnnotatorService)}.
   */
  public static Pipeline proxyProvidesPipeline(
      ComponentsModule instance,
      ILogger logger,
      IDBService idbService,
      IBlobService blobService,
      MandrakeService mandrakeService,
      MLExtractorService mlExtractorService,
      IDependencyResolverClient dependencyResolverClient,
      DPEngineService dpEngineService,
      ModelClientService modelServicesClient,
      PositionalService positionalService,
      AnnotatorService annotatorService) {
    return instance.providesPipeline(
        logger,
        idbService,
        blobService,
        mandrakeService,
        mlExtractorService,
        dependencyResolverClient,
        dpEngineService,
        modelServicesClient,
        positionalService,
        annotatorService);
  }
}
