package com.auxolabs.cvision.di;

import com.auxolabs.capture.positional.PositionalService;
import com.auxolabs.capture.positional.application.PositionalServiceImpl;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ComponentsModule_ProvidesPositionalEngineFactory
    implements Factory<PositionalService> {
  private final ComponentsModule module;

  private final Provider<PositionalServiceImpl> positionalServiceProvider;

  public ComponentsModule_ProvidesPositionalEngineFactory(
      ComponentsModule module, Provider<PositionalServiceImpl> positionalServiceProvider) {
    assert module != null;
    this.module = module;
    assert positionalServiceProvider != null;
    this.positionalServiceProvider = positionalServiceProvider;
  }

  @Override
  public PositionalService get() {
    return Preconditions.checkNotNull(
        module.providesPositionalEngine(positionalServiceProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<PositionalService> create(
      ComponentsModule module, Provider<PositionalServiceImpl> positionalServiceProvider) {
    return new ComponentsModule_ProvidesPositionalEngineFactory(module, positionalServiceProvider);
  }

  /** Proxies {@link ComponentsModule#providesPositionalEngine(PositionalServiceImpl)}. */
  public static PositionalService proxyProvidesPositionalEngine(
      ComponentsModule instance, PositionalServiceImpl positionalService) {
    return instance.providesPositionalEngine(positionalService);
  }
}
