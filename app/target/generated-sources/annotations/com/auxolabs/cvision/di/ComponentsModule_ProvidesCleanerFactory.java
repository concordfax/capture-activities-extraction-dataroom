package com.auxolabs.cvision.di;

import com.auxolabs.cvision.utils.ResourceCleanup;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ComponentsModule_ProvidesCleanerFactory implements Factory<ResourceCleanup> {
  private final ComponentsModule module;

  public ComponentsModule_ProvidesCleanerFactory(ComponentsModule module) {
    assert module != null;
    this.module = module;
  }

  @Override
  public ResourceCleanup get() {
    return Preconditions.checkNotNull(
        module.providesCleaner(), "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<ResourceCleanup> create(ComponentsModule module) {
    return new ComponentsModule_ProvidesCleanerFactory(module);
  }

  /** Proxies {@link ComponentsModule#providesCleaner()}. */
  public static ResourceCleanup proxyProvidesCleaner(ComponentsModule instance) {
    return instance.providesCleaner();
  }
}
