package com.auxolabs.cvision.di;

import com.concordfax.capture.core.web.helpers.HealthWatchable;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class HealthcheckResourceDropwizard_Factory
    implements Factory<HealthcheckResourceDropwizard> {
  private final Provider<HealthWatchable[]> healthWatchablesProvider;

  public HealthcheckResourceDropwizard_Factory(
      Provider<HealthWatchable[]> healthWatchablesProvider) {
    assert healthWatchablesProvider != null;
    this.healthWatchablesProvider = healthWatchablesProvider;
  }

  @Override
  public HealthcheckResourceDropwizard get() {
    return new HealthcheckResourceDropwizard(healthWatchablesProvider.get());
  }

  public static Factory<HealthcheckResourceDropwizard> create(
      Provider<HealthWatchable[]> healthWatchablesProvider) {
    return new HealthcheckResourceDropwizard_Factory(healthWatchablesProvider);
  }
}
