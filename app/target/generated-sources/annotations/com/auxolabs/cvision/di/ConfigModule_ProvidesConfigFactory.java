package com.auxolabs.cvision.di;

import com.auxolabs.cvision.web.config.CaptureConfig;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ConfigModule_ProvidesConfigFactory implements Factory<CaptureConfig> {
  private final ConfigModule module;

  public ConfigModule_ProvidesConfigFactory(ConfigModule module) {
    assert module != null;
    this.module = module;
  }

  @Override
  public CaptureConfig get() {
    return Preconditions.checkNotNull(
        module.providesConfig(), "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<CaptureConfig> create(ConfigModule module) {
    return new ConfigModule_ProvidesConfigFactory(module);
  }

  /** Proxies {@link ConfigModule#providesConfig()}. */
  public static CaptureConfig proxyProvidesConfig(ConfigModule instance) {
    return instance.providesConfig();
  }
}
