package com.auxolabs.cvision.di;

import com.auxolabs.cvision.db.AzureCosmosDBService;
import com.auxolabs.cvision.db.IDBService;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ComponentsModule_ProvidesDBFactory implements Factory<IDBService> {
  private final ComponentsModule module;

  private final Provider<AzureCosmosDBService> mockDBServiceProvider;

  public ComponentsModule_ProvidesDBFactory(
      ComponentsModule module, Provider<AzureCosmosDBService> mockDBServiceProvider) {
    assert module != null;
    this.module = module;
    assert mockDBServiceProvider != null;
    this.mockDBServiceProvider = mockDBServiceProvider;
  }

  @Override
  public IDBService get() {
    return Preconditions.checkNotNull(
        module.providesDB(mockDBServiceProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<IDBService> create(
      ComponentsModule module, Provider<AzureCosmosDBService> mockDBServiceProvider) {
    return new ComponentsModule_ProvidesDBFactory(module, mockDBServiceProvider);
  }

  /** Proxies {@link ComponentsModule#providesDB(AzureCosmosDBService)}. */
  public static IDBService proxyProvidesDB(
      ComponentsModule instance, AzureCosmosDBService mockDBService) {
    return instance.providesDB(mockDBService);
  }
}
