package com.auxolabs.cvision.di;

import com.auxolabs.cvision.metrics.DBHealthWatchable;
import com.auxolabs.cvision.metrics.pipeline.PipelineHealthWatchable;
import com.concordfax.capture.core.jobmanager.JobManagerHealthCheck;
import com.concordfax.capture.core.web.helpers.HealthWatchable;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class MetricsComponentsModule_ProvidesHealthWatchablesFactory
    implements Factory<HealthWatchable[]> {
  private final MetricsComponentsModule module;

  private final Provider<JobManagerHealthCheck> jobManagerHealthCheckProvider;

  private final Provider<DBHealthWatchable> dbHealthWatchableProvider;

  private final Provider<PipelineHealthWatchable> pipelineHealthWatchableProvider;

  public MetricsComponentsModule_ProvidesHealthWatchablesFactory(
      MetricsComponentsModule module,
      Provider<JobManagerHealthCheck> jobManagerHealthCheckProvider,
      Provider<DBHealthWatchable> dbHealthWatchableProvider,
      Provider<PipelineHealthWatchable> pipelineHealthWatchableProvider) {
    assert module != null;
    this.module = module;
    assert jobManagerHealthCheckProvider != null;
    this.jobManagerHealthCheckProvider = jobManagerHealthCheckProvider;
    assert dbHealthWatchableProvider != null;
    this.dbHealthWatchableProvider = dbHealthWatchableProvider;
    assert pipelineHealthWatchableProvider != null;
    this.pipelineHealthWatchableProvider = pipelineHealthWatchableProvider;
  }

  @Override
  public HealthWatchable[] get() {
    return Preconditions.checkNotNull(
        module.providesHealthWatchables(
            jobManagerHealthCheckProvider.get(),
            dbHealthWatchableProvider.get(),
            pipelineHealthWatchableProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<HealthWatchable[]> create(
      MetricsComponentsModule module,
      Provider<JobManagerHealthCheck> jobManagerHealthCheckProvider,
      Provider<DBHealthWatchable> dbHealthWatchableProvider,
      Provider<PipelineHealthWatchable> pipelineHealthWatchableProvider) {
    return new MetricsComponentsModule_ProvidesHealthWatchablesFactory(
        module,
        jobManagerHealthCheckProvider,
        dbHealthWatchableProvider,
        pipelineHealthWatchableProvider);
  }

  /**
   * Proxies {@link MetricsComponentsModule#providesHealthWatchables(JobManagerHealthCheck,
   * DBHealthWatchable, PipelineHealthWatchable)}.
   */
  public static HealthWatchable[] proxyProvidesHealthWatchables(
      MetricsComponentsModule instance,
      JobManagerHealthCheck jobManagerHealthCheck,
      DBHealthWatchable dbHealthWatchable,
      PipelineHealthWatchable pipelineHealthWatchable) {
    return instance.providesHealthWatchables(
        jobManagerHealthCheck, dbHealthWatchable, pipelineHealthWatchable);
  }
}
