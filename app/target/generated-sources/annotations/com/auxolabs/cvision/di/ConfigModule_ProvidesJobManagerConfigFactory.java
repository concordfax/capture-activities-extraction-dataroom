package com.auxolabs.cvision.di;

import com.auxolabs.cvision.web.config.CaptureConfig;
import com.concordfax.capture.core.jobmanager.JobManagerConfig;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ConfigModule_ProvidesJobManagerConfigFactory
    implements Factory<JobManagerConfig> {
  private final ConfigModule module;

  private final Provider<CaptureConfig> captureConfigProvider;

  public ConfigModule_ProvidesJobManagerConfigFactory(
      ConfigModule module, Provider<CaptureConfig> captureConfigProvider) {
    assert module != null;
    this.module = module;
    assert captureConfigProvider != null;
    this.captureConfigProvider = captureConfigProvider;
  }

  @Override
  public JobManagerConfig get() {
    return Preconditions.checkNotNull(
        module.providesJobManagerConfig(captureConfigProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<JobManagerConfig> create(
      ConfigModule module, Provider<CaptureConfig> captureConfigProvider) {
    return new ConfigModule_ProvidesJobManagerConfigFactory(module, captureConfigProvider);
  }

  /** Proxies {@link ConfigModule#providesJobManagerConfig(CaptureConfig)}. */
  public static JobManagerConfig proxyProvidesJobManagerConfig(
      ConfigModule instance, CaptureConfig captureConfig) {
    return instance.providesJobManagerConfig(captureConfig);
  }
}
