package com.auxolabs.cvision.di;

import com.concordfax.capture.core.utils.dependency.IDependencyResolverClient;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ComponentsModule_ProvidesDependencyResolverFactory
    implements Factory<IDependencyResolverClient> {
  private final ComponentsModule module;

  public ComponentsModule_ProvidesDependencyResolverFactory(ComponentsModule module) {
    assert module != null;
    this.module = module;
  }

  @Override
  public IDependencyResolverClient get() {
    return Preconditions.checkNotNull(
        module.providesDependencyResolver(),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<IDependencyResolverClient> create(ComponentsModule module) {
    return new ComponentsModule_ProvidesDependencyResolverFactory(module);
  }

  /** Proxies {@link ComponentsModule#providesDependencyResolver()}. */
  public static IDependencyResolverClient proxyProvidesDependencyResolver(
      ComponentsModule instance) {
    return instance.providesDependencyResolver();
  }
}
