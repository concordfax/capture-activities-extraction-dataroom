package com.auxolabs.cvision.di;

import com.auxolabs.cvision.ml.MLExtractorService;
import com.auxolabs.cvision.ml.MLExtractorServiceImpl;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ComponentsModule_ProvidesMLExtractorServiceClientFactory
    implements Factory<MLExtractorService> {
  private final ComponentsModule module;

  private final Provider<MLExtractorServiceImpl> mlExtractorServiceImplProvider;

  public ComponentsModule_ProvidesMLExtractorServiceClientFactory(
      ComponentsModule module, Provider<MLExtractorServiceImpl> mlExtractorServiceImplProvider) {
    assert module != null;
    this.module = module;
    assert mlExtractorServiceImplProvider != null;
    this.mlExtractorServiceImplProvider = mlExtractorServiceImplProvider;
  }

  @Override
  public MLExtractorService get() {
    return Preconditions.checkNotNull(
        module.providesMLExtractorServiceClient(mlExtractorServiceImplProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<MLExtractorService> create(
      ComponentsModule module, Provider<MLExtractorServiceImpl> mlExtractorServiceImplProvider) {
    return new ComponentsModule_ProvidesMLExtractorServiceClientFactory(
        module, mlExtractorServiceImplProvider);
  }

  /** Proxies {@link ComponentsModule#providesMLExtractorServiceClient(MLExtractorServiceImpl)}. */
  public static MLExtractorService proxyProvidesMLExtractorServiceClient(
      ComponentsModule instance, MLExtractorServiceImpl mlExtractorServiceImpl) {
    return instance.providesMLExtractorServiceClient(mlExtractorServiceImpl);
  }
}
