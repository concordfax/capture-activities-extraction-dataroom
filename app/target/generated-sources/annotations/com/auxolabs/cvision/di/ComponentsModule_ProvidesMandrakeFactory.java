package com.auxolabs.cvision.di;

import com.auxolabs.capture.mandrake.MandrakeService;
import com.auxolabs.capture.mandrake.application.MandrakeServiceImpl;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ComponentsModule_ProvidesMandrakeFactory implements Factory<MandrakeService> {
  private final ComponentsModule module;

  private final Provider<MandrakeServiceImpl> mandrakeServiceProvider;

  public ComponentsModule_ProvidesMandrakeFactory(
      ComponentsModule module, Provider<MandrakeServiceImpl> mandrakeServiceProvider) {
    assert module != null;
    this.module = module;
    assert mandrakeServiceProvider != null;
    this.mandrakeServiceProvider = mandrakeServiceProvider;
  }

  @Override
  public MandrakeService get() {
    return Preconditions.checkNotNull(
        module.providesMandrake(mandrakeServiceProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<MandrakeService> create(
      ComponentsModule module, Provider<MandrakeServiceImpl> mandrakeServiceProvider) {
    return new ComponentsModule_ProvidesMandrakeFactory(module, mandrakeServiceProvider);
  }

  /** Proxies {@link ComponentsModule#providesMandrake(MandrakeServiceImpl)}. */
  public static MandrakeService proxyProvidesMandrake(
      ComponentsModule instance, MandrakeServiceImpl mandrakeService) {
    return instance.providesMandrake(mandrakeService);
  }
}
