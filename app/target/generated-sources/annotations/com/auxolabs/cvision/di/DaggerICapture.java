package com.auxolabs.cvision.di;

import com.auxolabs.capture.annotator.AnnotationsProcessor_Factory;
import com.auxolabs.capture.annotator.AnnotatorService;
import com.auxolabs.capture.dateparser.DPEngineService;
import com.auxolabs.capture.dateparser.application.DateParserServiceImpl_Factory;
import com.auxolabs.capture.mandrake.MandrakeService;
import com.auxolabs.capture.mandrake.application.MandrakeServiceImpl_Factory;
import com.auxolabs.capture.positional.PositionalService;
import com.auxolabs.capture.positional.application.PositionalServiceImpl_Factory;
import com.auxolabs.cvision.db.AzureCosmosDBService;
import com.auxolabs.cvision.db.AzureCosmosDBService_Factory;
import com.auxolabs.cvision.db.IDBService;
import com.auxolabs.cvision.ml.MLExtractorService;
import com.auxolabs.cvision.ml.MLExtractorServiceImpl_Factory;
import com.auxolabs.cvision.utils.ResourceCleanup;
import com.auxolabs.cvision.web.config.CaptureConfig;
import com.auxolabs.cvision.web.init.CaptureInitializer;
import com.auxolabs.cvision.web.init.CaptureInitializer_Factory;
import com.auxolabs.cvision.web.resources.ExtendedHealthCheckResource;
import com.auxolabs.cvision.web.resources.ExtendedHealthCheckResource_Factory;
import com.auxolabs.cvision.web.resources.ExtendedJobManagerResource;
import com.auxolabs.cvision.web.resources.ExtendedJobManagerResource_Factory;
import com.concordfax.capture.core.db.MongoConfigModel;
import com.concordfax.capture.core.db.MongoUtil;
import com.concordfax.capture.core.db.MongoUtil_Factory;
import com.concordfax.capture.core.db.dao.DocumentDAO;
import com.concordfax.capture.core.db.dao.DocumentDAO_Factory;
import com.concordfax.capture.core.db.dao.EnvironmentDAO;
import com.concordfax.capture.core.db.dao.EnvironmentDAO_Factory;
import com.concordfax.capture.core.db.dao.ExtractionDAO;
import com.concordfax.capture.core.db.dao.ExtractionDAO_Factory;
import com.concordfax.capture.core.db.dao.MLModelDAO;
import com.concordfax.capture.core.db.dao.MLModelDAO_Factory;
import com.concordfax.capture.core.db.dao.ProcessDAO;
import com.concordfax.capture.core.db.dao.ProcessDAO_Factory;
import com.concordfax.capture.core.db.dao.ProfileIDDAO;
import com.concordfax.capture.core.db.dao.ProfileIDDAO_Factory;
import com.concordfax.capture.core.jobmanager.JobManager;
import com.concordfax.capture.core.jobmanager.JobManagerConfig;
import com.concordfax.capture.core.logging.ILogger;
import com.concordfax.capture.core.logging.LogConfig;
import com.concordfax.capture.core.pipeline.Pipeline;
import com.concordfax.capture.core.storage.blobstore.IBlobService;
import com.concordfax.capture.core.utils.dependency.IDependencyResolverClient;
import com.concordfax.capture.core.utils.mlmodel.ModelClientService;
import com.concordfax.capture.core.utils.mlmodel.ModelServicesClient;
import com.concordfax.capture.core.utils.mlmodel.ModelServicesClient_Factory;
import dagger.internal.DoubleCheck;
import dagger.internal.MembersInjectors;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class DaggerICapture implements ICapture {
  private Provider<CaptureConfig> providesConfigProvider;

  private Provider<LogConfig> providesLoggerConfigProvider;

  private Provider<ILogger> providesLoggerProvider;

  private Provider<ResourceCleanup> providesCleanerProvider;

  private Provider<CaptureInitializer> captureInitializerProvider;

  private Provider<JobManagerConfig> providesJobManagerConfigProvider;

  private Provider<MongoConfigModel> providesDBConfigProvider;

  private Provider<MongoUtil> mongoUtilProvider;

  private Provider<DocumentDAO> documentDAOProvider;

  private Provider<ProcessDAO> processDAOProvider;

  private Provider<EnvironmentDAO> environmentDAOProvider;

  private Provider<MLModelDAO> mLModelDAOProvider;

  private Provider<ExtractionDAO> extractionDAOProvider;

  private Provider<ProfileIDDAO> profileIDDAOProvider;

  private Provider<AzureCosmosDBService> azureCosmosDBServiceProvider;

  private Provider<IDBService> providesDBProvider;

  private Provider<IBlobService> providesFileStoreProvider;

  private Provider<MandrakeService> providesMandrakeProvider;

  private Provider<MLExtractorService> providesMLExtractorServiceClientProvider;

  private Provider<IDependencyResolverClient> providesDependencyResolverProvider;

  private Provider<DPEngineService> providesDateParserEngineProvider;

  private Provider<ModelServicesClient> modelServicesClientProvider;

  private Provider<ModelClientService> providesModelServicesClientProvider;

  private Provider<PositionalService> providesPositionalEngineProvider;

  private Provider<AnnotatorService> providesAnnotatorServiceProvider;

  private Provider<Pipeline> providesPipelineProvider;

  private Provider<JobManager> providesJobManagerProvider;

  private Provider<ExtendedJobManagerResource> extendedJobManagerResourceProvider;

  private Provider<ExtendedHealthCheckResource> extendedHealthCheckResourceProvider;

  private DaggerICapture(Builder builder) {
    assert builder != null;
    initialize(builder);
  }

  public static Builder builder() {
    return new Builder();
  }

  public static ICapture create() {
    return new Builder().build();
  }

  @SuppressWarnings("unchecked")
  private void initialize(final Builder builder) {

    this.providesConfigProvider = ConfigModule_ProvidesConfigFactory.create(builder.configModule);

    this.providesLoggerConfigProvider =
        ConfigModule_ProvidesLoggerConfigFactory.create(
            builder.configModule, providesConfigProvider);

    this.providesLoggerProvider =
        DoubleCheck.provider(
            ComponentsModule_ProvidesLoggerFactory.create(
                builder.componentsModule, providesLoggerConfigProvider));

    this.providesCleanerProvider =
        DoubleCheck.provider(
            ComponentsModule_ProvidesCleanerFactory.create(builder.componentsModule));

    this.captureInitializerProvider =
        DoubleCheck.provider(
            CaptureInitializer_Factory.create(providesLoggerProvider, providesCleanerProvider));

    this.providesJobManagerConfigProvider =
        ConfigModule_ProvidesJobManagerConfigFactory.create(
            builder.configModule, providesConfigProvider);

    this.providesDBConfigProvider =
        ConfigModule_ProvidesDBConfigFactory.create(builder.configModule, providesConfigProvider);

    this.mongoUtilProvider = MongoUtil_Factory.create(providesDBConfigProvider);

    this.documentDAOProvider = DocumentDAO_Factory.create(mongoUtilProvider);

    this.processDAOProvider = ProcessDAO_Factory.create(mongoUtilProvider);

    this.environmentDAOProvider = EnvironmentDAO_Factory.create(mongoUtilProvider);

    this.mLModelDAOProvider = MLModelDAO_Factory.create(mongoUtilProvider);

    this.extractionDAOProvider = ExtractionDAO_Factory.create(mongoUtilProvider);

    this.profileIDDAOProvider = ProfileIDDAO_Factory.create(mongoUtilProvider);

    this.azureCosmosDBServiceProvider =
        AzureCosmosDBService_Factory.create(
            documentDAOProvider,
            processDAOProvider,
            environmentDAOProvider,
            mLModelDAOProvider,
            extractionDAOProvider,
            profileIDDAOProvider);

    this.providesDBProvider =
        DoubleCheck.provider(
            ComponentsModule_ProvidesDBFactory.create(
                builder.componentsModule, azureCosmosDBServiceProvider));

    this.providesFileStoreProvider =
        DoubleCheck.provider(
            ComponentsModule_ProvidesFileStoreFactory.create(
                builder.componentsModule, providesConfigProvider));

    this.providesMandrakeProvider =
        DoubleCheck.provider(
            ComponentsModule_ProvidesMandrakeFactory.create(
                builder.componentsModule, MandrakeServiceImpl_Factory.create()));

    this.providesMLExtractorServiceClientProvider =
        DoubleCheck.provider(
            ComponentsModule_ProvidesMLExtractorServiceClientFactory.create(
                builder.componentsModule, MLExtractorServiceImpl_Factory.create()));

    this.providesDependencyResolverProvider =
        DoubleCheck.provider(
            ComponentsModule_ProvidesDependencyResolverFactory.create(builder.componentsModule));

    this.providesDateParserEngineProvider =
        DoubleCheck.provider(
            ComponentsModule_ProvidesDateParserEngineFactory.create(
                builder.componentsModule, DateParserServiceImpl_Factory.create()));

    this.modelServicesClientProvider = ModelServicesClient_Factory.create(providesLoggerProvider);

    this.providesModelServicesClientProvider =
        DoubleCheck.provider(
            ComponentsModule_ProvidesModelServicesClientFactory.create(
                builder.componentsModule, modelServicesClientProvider));

    this.providesPositionalEngineProvider =
        DoubleCheck.provider(
            ComponentsModule_ProvidesPositionalEngineFactory.create(
                builder.componentsModule, PositionalServiceImpl_Factory.create()));

    this.providesAnnotatorServiceProvider =
        DoubleCheck.provider(
            ComponentsModule_ProvidesAnnotatorServiceFactory.create(
                builder.componentsModule, AnnotationsProcessor_Factory.create()));

    this.providesPipelineProvider =
        ComponentsModule_ProvidesPipelineFactory.create(
            builder.componentsModule,
            providesLoggerProvider,
            providesDBProvider,
            providesFileStoreProvider,
            providesMandrakeProvider,
            providesMLExtractorServiceClientProvider,
            providesDependencyResolverProvider,
            providesDateParserEngineProvider,
            providesModelServicesClientProvider,
            providesPositionalEngineProvider,
            providesAnnotatorServiceProvider);

    this.providesJobManagerProvider =
        DoubleCheck.provider(
            ComponentsModule_ProvidesJobManagerFactory.create(
                builder.componentsModule,
                providesJobManagerConfigProvider,
                providesPipelineProvider));

    this.extendedJobManagerResourceProvider =
        ExtendedJobManagerResource_Factory.create(
            MembersInjectors.<ExtendedJobManagerResource>noOp(),
            providesJobManagerProvider,
            providesLoggerProvider,
            providesDBProvider);

    this.extendedHealthCheckResourceProvider =
        ExtendedHealthCheckResource_Factory.create(
            MembersInjectors.<ExtendedHealthCheckResource>noOp());
  }

  @Override
  public CaptureInitializer ocrInit() {
    return captureInitializerProvider.get();
  }

  @Override
  public ExtendedJobManagerResource jobManagerResource() {
    return extendedJobManagerResourceProvider.get();
  }

  @Override
  public ExtendedHealthCheckResource healthCheckResource() {
    return extendedHealthCheckResourceProvider.get();
  }

  public static final class Builder {
    private ConfigModule configModule;

    private ComponentsModule componentsModule;

    private Builder() {}

    public ICapture build() {
      if (configModule == null) {
        this.configModule = new ConfigModule();
      }
      if (componentsModule == null) {
        this.componentsModule = new ComponentsModule();
      }
      return new DaggerICapture(this);
    }

    public Builder configModule(ConfigModule configModule) {
      this.configModule = Preconditions.checkNotNull(configModule);
      return this;
    }

    public Builder componentsModule(ComponentsModule componentsModule) {
      this.componentsModule = Preconditions.checkNotNull(componentsModule);
      return this;
    }

    /**
     * @deprecated This module is declared, but an instance is not used in the component. This
     *     method is a no-op. For more, see https://google.github.io/dagger/unused-modules.
     */
    @Deprecated
    public Builder metricsComponentsModule(MetricsComponentsModule metricsComponentsModule) {
      Preconditions.checkNotNull(metricsComponentsModule);
      return this;
    }
  }
}
