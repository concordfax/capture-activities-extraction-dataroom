package com.auxolabs.cvision.di;

import com.concordfax.capture.core.utils.mlmodel.ModelClientService;
import com.concordfax.capture.core.utils.mlmodel.ModelServicesClient;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ComponentsModule_ProvidesModelServicesClientFactory
    implements Factory<ModelClientService> {
  private final ComponentsModule module;

  private final Provider<ModelServicesClient> modelServicesClientProvider;

  public ComponentsModule_ProvidesModelServicesClientFactory(
      ComponentsModule module, Provider<ModelServicesClient> modelServicesClientProvider) {
    assert module != null;
    this.module = module;
    assert modelServicesClientProvider != null;
    this.modelServicesClientProvider = modelServicesClientProvider;
  }

  @Override
  public ModelClientService get() {
    return Preconditions.checkNotNull(
        module.providesModelServicesClient(modelServicesClientProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<ModelClientService> create(
      ComponentsModule module, Provider<ModelServicesClient> modelServicesClientProvider) {
    return new ComponentsModule_ProvidesModelServicesClientFactory(
        module, modelServicesClientProvider);
  }

  /** Proxies {@link ComponentsModule#providesModelServicesClient(ModelServicesClient)}. */
  public static ModelClientService proxyProvidesModelServicesClient(
      ComponentsModule instance, ModelServicesClient modelServicesClient) {
    return instance.providesModelServicesClient(modelServicesClient);
  }
}
