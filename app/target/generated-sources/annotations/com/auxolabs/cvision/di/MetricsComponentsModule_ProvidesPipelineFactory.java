package com.auxolabs.cvision.di;

import com.auxolabs.capture.dateparser.DPEngineService;
import com.auxolabs.capture.mandrake.MandrakeService;
import com.auxolabs.capture.positional.PositionalService;
import com.auxolabs.cvision.metrics.pipeline.MetricsAnnotationsService;
import com.auxolabs.cvision.metrics.pipeline.MetricsBlobService;
import com.auxolabs.cvision.metrics.pipeline.MetricsDB;
import com.auxolabs.cvision.metrics.pipeline.MetricsDependencyResolver;
import com.auxolabs.cvision.metrics.pipeline.MetricsLogger;
import com.auxolabs.cvision.metrics.pipeline.MetricsModelServicesClient;
import com.auxolabs.cvision.ml.MLExtractorService;
import com.concordfax.capture.core.pipeline.Pipeline;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class MetricsComponentsModule_ProvidesPipelineFactory implements Factory<Pipeline> {
  private final MetricsComponentsModule module;

  private final Provider<MetricsLogger> loggerProvider;

  private final Provider<MetricsDB> idbServiceProvider;

  private final Provider<MetricsBlobService> blobServiceProvider;

  private final Provider<MLExtractorService> mlExtractorServiceProvider;

  private final Provider<MandrakeService> mandrakeServiceProvider;

  private final Provider<PositionalService> positionalServiceProvider;

  private final Provider<DPEngineService> dpEngineServiceProvider;

  private final Provider<MetricsModelServicesClient> modelServicesClientProvider;

  private final Provider<MetricsDependencyResolver> dependencyResolverClientProvider;

  private final Provider<MetricsAnnotationsService> metricsAnnotationsServiceProvider;

  public MetricsComponentsModule_ProvidesPipelineFactory(
      MetricsComponentsModule module,
      Provider<MetricsLogger> loggerProvider,
      Provider<MetricsDB> idbServiceProvider,
      Provider<MetricsBlobService> blobServiceProvider,
      Provider<MLExtractorService> mlExtractorServiceProvider,
      Provider<MandrakeService> mandrakeServiceProvider,
      Provider<PositionalService> positionalServiceProvider,
      Provider<DPEngineService> dpEngineServiceProvider,
      Provider<MetricsModelServicesClient> modelServicesClientProvider,
      Provider<MetricsDependencyResolver> dependencyResolverClientProvider,
      Provider<MetricsAnnotationsService> metricsAnnotationsServiceProvider) {
    assert module != null;
    this.module = module;
    assert loggerProvider != null;
    this.loggerProvider = loggerProvider;
    assert idbServiceProvider != null;
    this.idbServiceProvider = idbServiceProvider;
    assert blobServiceProvider != null;
    this.blobServiceProvider = blobServiceProvider;
    assert mlExtractorServiceProvider != null;
    this.mlExtractorServiceProvider = mlExtractorServiceProvider;
    assert mandrakeServiceProvider != null;
    this.mandrakeServiceProvider = mandrakeServiceProvider;
    assert positionalServiceProvider != null;
    this.positionalServiceProvider = positionalServiceProvider;
    assert dpEngineServiceProvider != null;
    this.dpEngineServiceProvider = dpEngineServiceProvider;
    assert modelServicesClientProvider != null;
    this.modelServicesClientProvider = modelServicesClientProvider;
    assert dependencyResolverClientProvider != null;
    this.dependencyResolverClientProvider = dependencyResolverClientProvider;
    assert metricsAnnotationsServiceProvider != null;
    this.metricsAnnotationsServiceProvider = metricsAnnotationsServiceProvider;
  }

  @Override
  public Pipeline get() {
    return Preconditions.checkNotNull(
        module.providesPipeline(
            loggerProvider.get(),
            idbServiceProvider.get(),
            blobServiceProvider.get(),
            mlExtractorServiceProvider.get(),
            mandrakeServiceProvider.get(),
            positionalServiceProvider.get(),
            dpEngineServiceProvider.get(),
            modelServicesClientProvider.get(),
            dependencyResolverClientProvider.get(),
            metricsAnnotationsServiceProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<Pipeline> create(
      MetricsComponentsModule module,
      Provider<MetricsLogger> loggerProvider,
      Provider<MetricsDB> idbServiceProvider,
      Provider<MetricsBlobService> blobServiceProvider,
      Provider<MLExtractorService> mlExtractorServiceProvider,
      Provider<MandrakeService> mandrakeServiceProvider,
      Provider<PositionalService> positionalServiceProvider,
      Provider<DPEngineService> dpEngineServiceProvider,
      Provider<MetricsModelServicesClient> modelServicesClientProvider,
      Provider<MetricsDependencyResolver> dependencyResolverClientProvider,
      Provider<MetricsAnnotationsService> metricsAnnotationsServiceProvider) {
    return new MetricsComponentsModule_ProvidesPipelineFactory(
        module,
        loggerProvider,
        idbServiceProvider,
        blobServiceProvider,
        mlExtractorServiceProvider,
        mandrakeServiceProvider,
        positionalServiceProvider,
        dpEngineServiceProvider,
        modelServicesClientProvider,
        dependencyResolverClientProvider,
        metricsAnnotationsServiceProvider);
  }

  /**
   * Proxies {@link MetricsComponentsModule#providesPipeline(MetricsLogger, MetricsDB,
   * MetricsBlobService, MLExtractorService, MandrakeService, PositionalService, DPEngineService,
   * MetricsModelServicesClient, MetricsDependencyResolver, MetricsAnnotationsService)}.
   */
  public static Pipeline proxyProvidesPipeline(
      MetricsComponentsModule instance,
      MetricsLogger logger,
      MetricsDB idbService,
      MetricsBlobService blobService,
      MLExtractorService mlExtractorService,
      MandrakeService mandrakeService,
      PositionalService positionalService,
      DPEngineService dpEngineService,
      MetricsModelServicesClient modelServicesClient,
      MetricsDependencyResolver dependencyResolverClient,
      MetricsAnnotationsService metricsAnnotationsService) {
    return instance.providesPipeline(
        logger,
        idbService,
        blobService,
        mlExtractorService,
        mandrakeService,
        positionalService,
        dpEngineService,
        modelServicesClient,
        dependencyResolverClient,
        metricsAnnotationsService);
  }
}
