package com.auxolabs.cvision.di;

import com.auxolabs.cvision.web.config.CaptureConfig;
import com.concordfax.capture.core.logging.LogConfig;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ConfigModule_ProvidesLoggerConfigFactory implements Factory<LogConfig> {
  private final ConfigModule module;

  private final Provider<CaptureConfig> captureConfigProvider;

  public ConfigModule_ProvidesLoggerConfigFactory(
      ConfigModule module, Provider<CaptureConfig> captureConfigProvider) {
    assert module != null;
    this.module = module;
    assert captureConfigProvider != null;
    this.captureConfigProvider = captureConfigProvider;
  }

  @Override
  public LogConfig get() {
    return Preconditions.checkNotNull(
        module.providesLoggerConfig(captureConfigProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<LogConfig> create(
      ConfigModule module, Provider<CaptureConfig> captureConfigProvider) {
    return new ConfigModule_ProvidesLoggerConfigFactory(module, captureConfigProvider);
  }

  /** Proxies {@link ConfigModule#providesLoggerConfig(CaptureConfig)}. */
  public static LogConfig proxyProvidesLoggerConfig(
      ConfigModule instance, CaptureConfig captureConfig) {
    return instance.providesLoggerConfig(captureConfig);
  }
}
