package com.auxolabs.cvision.di;

import com.concordfax.capture.core.logging.ILogger;
import com.concordfax.capture.core.logging.LogConfig;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ComponentsModule_ProvidesLoggerFactory implements Factory<ILogger> {
  private final ComponentsModule module;

  private final Provider<LogConfig> logConfigProvider;

  public ComponentsModule_ProvidesLoggerFactory(
      ComponentsModule module, Provider<LogConfig> logConfigProvider) {
    assert module != null;
    this.module = module;
    assert logConfigProvider != null;
    this.logConfigProvider = logConfigProvider;
  }

  @Override
  public ILogger get() {
    return Preconditions.checkNotNull(
        module.providesLogger(logConfigProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<ILogger> create(
      ComponentsModule module, Provider<LogConfig> logConfigProvider) {
    return new ComponentsModule_ProvidesLoggerFactory(module, logConfigProvider);
  }

  /** Proxies {@link ComponentsModule#providesLogger(LogConfig)}. */
  public static ILogger proxyProvidesLogger(ComponentsModule instance, LogConfig logConfig) {
    return instance.providesLogger(logConfig);
  }
}
