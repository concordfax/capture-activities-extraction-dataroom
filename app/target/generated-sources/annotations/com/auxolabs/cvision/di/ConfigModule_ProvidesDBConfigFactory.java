package com.auxolabs.cvision.di;

import com.auxolabs.cvision.web.config.CaptureConfig;
import com.concordfax.capture.core.db.MongoConfigModel;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ConfigModule_ProvidesDBConfigFactory implements Factory<MongoConfigModel> {
  private final ConfigModule module;

  private final Provider<CaptureConfig> captureConfigProvider;

  public ConfigModule_ProvidesDBConfigFactory(
      ConfigModule module, Provider<CaptureConfig> captureConfigProvider) {
    assert module != null;
    this.module = module;
    assert captureConfigProvider != null;
    this.captureConfigProvider = captureConfigProvider;
  }

  @Override
  public MongoConfigModel get() {
    return Preconditions.checkNotNull(
        module.providesDBConfig(captureConfigProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<MongoConfigModel> create(
      ConfigModule module, Provider<CaptureConfig> captureConfigProvider) {
    return new ConfigModule_ProvidesDBConfigFactory(module, captureConfigProvider);
  }

  /** Proxies {@link ConfigModule#providesDBConfig(CaptureConfig)}. */
  public static MongoConfigModel proxyProvidesDBConfig(
      ConfigModule instance, CaptureConfig captureConfig) {
    return instance.providesDBConfig(captureConfig);
  }
}
