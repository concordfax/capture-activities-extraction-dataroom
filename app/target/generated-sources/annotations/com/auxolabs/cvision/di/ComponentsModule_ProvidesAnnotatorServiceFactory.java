package com.auxolabs.cvision.di;

import com.auxolabs.capture.annotator.AnnotationsProcessor;
import com.auxolabs.capture.annotator.AnnotatorService;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ComponentsModule_ProvidesAnnotatorServiceFactory
    implements Factory<AnnotatorService> {
  private final ComponentsModule module;

  private final Provider<AnnotationsProcessor> annotationsProcessorProvider;

  public ComponentsModule_ProvidesAnnotatorServiceFactory(
      ComponentsModule module, Provider<AnnotationsProcessor> annotationsProcessorProvider) {
    assert module != null;
    this.module = module;
    assert annotationsProcessorProvider != null;
    this.annotationsProcessorProvider = annotationsProcessorProvider;
  }

  @Override
  public AnnotatorService get() {
    return Preconditions.checkNotNull(
        module.providesAnnotatorService(annotationsProcessorProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<AnnotatorService> create(
      ComponentsModule module, Provider<AnnotationsProcessor> annotationsProcessorProvider) {
    return new ComponentsModule_ProvidesAnnotatorServiceFactory(
        module, annotationsProcessorProvider);
  }

  /** Proxies {@link ComponentsModule#providesAnnotatorService(AnnotationsProcessor)}. */
  public static AnnotatorService proxyProvidesAnnotatorService(
      ComponentsModule instance, AnnotationsProcessor annotationsProcessor) {
    return instance.providesAnnotatorService(annotationsProcessor);
  }
}
