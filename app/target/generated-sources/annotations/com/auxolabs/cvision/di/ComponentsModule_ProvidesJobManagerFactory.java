package com.auxolabs.cvision.di;

import com.concordfax.capture.core.jobmanager.JobManager;
import com.concordfax.capture.core.jobmanager.JobManagerConfig;
import com.concordfax.capture.core.pipeline.Pipeline;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ComponentsModule_ProvidesJobManagerFactory implements Factory<JobManager> {
  private final ComponentsModule module;

  private final Provider<JobManagerConfig> jobManagerConfigProvider;

  private final Provider<Pipeline> pipelineProvider;

  public ComponentsModule_ProvidesJobManagerFactory(
      ComponentsModule module,
      Provider<JobManagerConfig> jobManagerConfigProvider,
      Provider<Pipeline> pipelineProvider) {
    assert module != null;
    this.module = module;
    assert jobManagerConfigProvider != null;
    this.jobManagerConfigProvider = jobManagerConfigProvider;
    assert pipelineProvider != null;
    this.pipelineProvider = pipelineProvider;
  }

  @Override
  public JobManager get() {
    return Preconditions.checkNotNull(
        module.providesJobManager(jobManagerConfigProvider.get(), pipelineProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<JobManager> create(
      ComponentsModule module,
      Provider<JobManagerConfig> jobManagerConfigProvider,
      Provider<Pipeline> pipelineProvider) {
    return new ComponentsModule_ProvidesJobManagerFactory(
        module, jobManagerConfigProvider, pipelineProvider);
  }

  /** Proxies {@link ComponentsModule#providesJobManager(JobManagerConfig, Pipeline)}. */
  public static JobManager proxyProvidesJobManager(
      ComponentsModule instance, JobManagerConfig jobManagerConfig, Pipeline pipeline) {
    return instance.providesJobManager(jobManagerConfig, pipeline);
  }
}
