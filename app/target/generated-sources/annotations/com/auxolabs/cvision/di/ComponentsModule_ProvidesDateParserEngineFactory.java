package com.auxolabs.cvision.di;

import com.auxolabs.capture.dateparser.DPEngineService;
import com.auxolabs.capture.dateparser.application.DateParserServiceImpl;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ComponentsModule_ProvidesDateParserEngineFactory
    implements Factory<DPEngineService> {
  private final ComponentsModule module;

  private final Provider<DateParserServiceImpl> dateParserServiceProvider;

  public ComponentsModule_ProvidesDateParserEngineFactory(
      ComponentsModule module, Provider<DateParserServiceImpl> dateParserServiceProvider) {
    assert module != null;
    this.module = module;
    assert dateParserServiceProvider != null;
    this.dateParserServiceProvider = dateParserServiceProvider;
  }

  @Override
  public DPEngineService get() {
    return Preconditions.checkNotNull(
        module.providesDateParserEngine(dateParserServiceProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<DPEngineService> create(
      ComponentsModule module, Provider<DateParserServiceImpl> dateParserServiceProvider) {
    return new ComponentsModule_ProvidesDateParserEngineFactory(module, dateParserServiceProvider);
  }

  /** Proxies {@link ComponentsModule#providesDateParserEngine(DateParserServiceImpl)}. */
  public static DPEngineService proxyProvidesDateParserEngine(
      ComponentsModule instance, DateParserServiceImpl dateParserService) {
    return instance.providesDateParserEngine(dateParserService);
  }
}
