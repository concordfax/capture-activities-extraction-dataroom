package com.auxolabs.cvision.web.init;

import com.auxolabs.cvision.utils.ResourceCleanup;
import com.concordfax.capture.core.logging.ILogger;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class CaptureInitializer_Factory implements Factory<CaptureInitializer> {
  private final Provider<ILogger> loggerProvider;

  private final Provider<ResourceCleanup> cleanupProvider;

  public CaptureInitializer_Factory(
      Provider<ILogger> loggerProvider, Provider<ResourceCleanup> cleanupProvider) {
    assert loggerProvider != null;
    this.loggerProvider = loggerProvider;
    assert cleanupProvider != null;
    this.cleanupProvider = cleanupProvider;
  }

  @Override
  public CaptureInitializer get() {
    return new CaptureInitializer(loggerProvider.get(), cleanupProvider.get());
  }

  public static Factory<CaptureInitializer> create(
      Provider<ILogger> loggerProvider, Provider<ResourceCleanup> cleanupProvider) {
    return new CaptureInitializer_Factory(loggerProvider, cleanupProvider);
  }
}
