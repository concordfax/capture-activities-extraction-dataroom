package com.auxolabs.cvision.web.resources;

import com.auxolabs.cvision.db.IDBService;
import com.concordfax.capture.core.jobmanager.JobManager;
import com.concordfax.capture.core.logging.ILogger;
import dagger.MembersInjector;
import dagger.internal.Factory;
import dagger.internal.MembersInjectors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ExtendedJobManagerResource_Factory
    implements Factory<ExtendedJobManagerResource> {
  private final MembersInjector<ExtendedJobManagerResource>
      extendedJobManagerResourceMembersInjector;

  private final Provider<JobManager> jobManagerProvider;

  private final Provider<ILogger> iLoggerAndLoggerProvider;

  private final Provider<IDBService> idbServiceProvider;

  public ExtendedJobManagerResource_Factory(
      MembersInjector<ExtendedJobManagerResource> extendedJobManagerResourceMembersInjector,
      Provider<JobManager> jobManagerProvider,
      Provider<ILogger> iLoggerAndLoggerProvider,
      Provider<IDBService> idbServiceProvider) {
    assert extendedJobManagerResourceMembersInjector != null;
    this.extendedJobManagerResourceMembersInjector = extendedJobManagerResourceMembersInjector;
    assert jobManagerProvider != null;
    this.jobManagerProvider = jobManagerProvider;
    assert iLoggerAndLoggerProvider != null;
    this.iLoggerAndLoggerProvider = iLoggerAndLoggerProvider;
    assert idbServiceProvider != null;
    this.idbServiceProvider = idbServiceProvider;
  }

  @Override
  public ExtendedJobManagerResource get() {
    return MembersInjectors.injectMembers(
        extendedJobManagerResourceMembersInjector,
        new ExtendedJobManagerResource(
            jobManagerProvider.get(),
            iLoggerAndLoggerProvider.get(),
            idbServiceProvider.get(),
            iLoggerAndLoggerProvider.get()));
  }

  public static Factory<ExtendedJobManagerResource> create(
      MembersInjector<ExtendedJobManagerResource> extendedJobManagerResourceMembersInjector,
      Provider<JobManager> jobManagerProvider,
      Provider<ILogger> iLoggerAndLoggerProvider,
      Provider<IDBService> idbServiceProvider) {
    return new ExtendedJobManagerResource_Factory(
        extendedJobManagerResourceMembersInjector,
        jobManagerProvider,
        iLoggerAndLoggerProvider,
        idbServiceProvider);
  }
}
