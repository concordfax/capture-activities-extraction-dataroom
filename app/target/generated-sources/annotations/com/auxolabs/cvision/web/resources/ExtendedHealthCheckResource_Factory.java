package com.auxolabs.cvision.web.resources;

import dagger.MembersInjector;
import dagger.internal.Factory;
import dagger.internal.MembersInjectors;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ExtendedHealthCheckResource_Factory
    implements Factory<ExtendedHealthCheckResource> {
  private final MembersInjector<ExtendedHealthCheckResource>
      extendedHealthCheckResourceMembersInjector;

  public ExtendedHealthCheckResource_Factory(
      MembersInjector<ExtendedHealthCheckResource> extendedHealthCheckResourceMembersInjector) {
    assert extendedHealthCheckResourceMembersInjector != null;
    this.extendedHealthCheckResourceMembersInjector = extendedHealthCheckResourceMembersInjector;
  }

  @Override
  public ExtendedHealthCheckResource get() {
    return MembersInjectors.injectMembers(
        extendedHealthCheckResourceMembersInjector, new ExtendedHealthCheckResource());
  }

  public static Factory<ExtendedHealthCheckResource> create(
      MembersInjector<ExtendedHealthCheckResource> extendedHealthCheckResourceMembersInjector) {
    return new ExtendedHealthCheckResource_Factory(extendedHealthCheckResourceMembersInjector);
  }
}
