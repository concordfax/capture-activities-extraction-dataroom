# Capture Extraction Activity - For Dataroom 

## Installation

1. Run the maven packaging script from the project home directory `mvn clean package`
1. The app is now packaged and is available in the app/target directory; to run the app use `java -Xmx2g -Djava.io.tmpdir=tmp -jar app/target/app-1.0-SNAPSHOT.jar server dev.config.yaml`
1. While deploying to production run with assertions disabled using `-da` flag

### Requirements
1. Java 1.8 ver: >=120
1. Maven ver: >=3.3.3
1. if IDE == "IntelliJ" then use "Lombok IntelliJ Plugin" and enable annotations processing in `Preferences | Build, Execution, Deployment | Compiler | Annotation Processors `

### Configuration
1. Check the configurations file `dev.config.yaml` for relevant configuration like ports, db, logging key.
1. Check the dependency inject code to change dependencies like logger type (ie FileLogger or AzureApplicationInsights), filestore type, db type
1. The various implementation for the components can be found in their respective modules from there they can packaged and easily reused in other projects


**NO SECURITY INFORMATION SHOULD BE MENTIONED IN THE CONFIG, USE INTELLIJ ENV PROPERTIES TO STORE AND USE THAT**

### Dependencies
1. capture-core-java
1. OCR activity result can be found in the result field for the activity

### Docker

1. Important !! Set the storage account credentials in the `Dockerfile`
1. Important !! configurations while using docker are the `dev.config.yaml`
1. Important - Production/Testing !! Pass in the environment variables as needed for each environment:
    1. `Environment__DatabaseName` : main db to be used for extraction
    1. `Environment__DatabaseConnectionString` : the connection string for the main db
    1. `ApplicationInsights__InstrumentationKey` : the application insights logging apiKey
    1. `MaximumActiveJob_Count` : the maximum active job count for extraction
1. Starting as a docker container is extremely easy just use the command `docker build -t nextstep/extraction:v1 .` from within the project directory or wherever the Dockerfile is.
1. Once image is created container can be started using `docker run --name capt-ex-v1 -p 80:80 -d nextstep/extraction:v1` . This will automatically start the extraction and mandrake apps from within the container.

## Contributions
1. Contributions can be made by filling in missing features that are available as TODOs in the code 


