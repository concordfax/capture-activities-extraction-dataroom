#!/bin/bash

cd /usr/local/extraction
mkdir tmp
java -Xmx2g -Xms1g -XX:+UseG1GC -XX:MaxMetaspaceSize=256m -Djava.io.tmpdir=tmp -jar extraction.jar server config.yaml
