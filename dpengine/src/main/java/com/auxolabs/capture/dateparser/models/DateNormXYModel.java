package com.auxolabs.capture.dateparser.models;

import lombok.Data;

@Data
public class DateNormXYModel {
    private String orgValue;
    private String normValue;
    private String tagValue;
    private double x;
    private double y;
    private double h;
    private double w;
    private boolean isFirstOccurence = false;
}

