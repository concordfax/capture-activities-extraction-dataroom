package com.auxolabs.capture.dateparser.models;

import com.concordfax.capture.core.models.document.result.extraction.ExtractionField;
import lombok.Data;

@Data
public class OutputDateModel {
    private String normalizedDate;
    private ExtractionField extractionField;
    private String originalDateValue;
}
