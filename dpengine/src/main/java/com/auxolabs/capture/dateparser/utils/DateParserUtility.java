package com.auxolabs.capture.dateparser.utils;

import com.auxolabs.capture.dateparser.constants.DateParserUtilityConstants;
import com.auxolabs.capture.dateparser.exceptions.DateParserInvalidFormat;
import com.auxolabs.capture.dateparser.models.TokenModel;
import com.auxolabs.capture.dateparser.utils.formatter.DateNormalizer;


import javax.inject.Inject;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class DateParserUtility {
    private final int LINE_THRESHOLD = 4;
    private final int SAME_COLUMN_THRESHOLD = 3;
    private final int PROXIMITY_THRESHOLD_UPPER = 50;
    private final int PROXIMITY_THRESHOLD_LOWER = -50;

    @Inject
    public DateParserUtility(){}

    public  List<TokenModel> parseAndGetSorted(List<TokenModel> dateCandidates){
        DateNormalizer dateNormalizer = new DateNormalizer();
        dateCandidates.forEach(tokenModel->{
            try {
                tokenModel.put(DateParserUtilityConstants.Properties.NORM_DATE, dateNormalizer.dateFieldFormatter((String)tokenModel.get("speAno")));
            } catch (ParseException e) {
                throw new DateParserInvalidFormat(e.getMessage());
            }
        });
        //considering only dates that between 1920 and current date
        dateCandidates = removeFalseDates(dateCandidates);

        Comparator<TokenModel> byYear = Comparator.comparing(tokenModel->Integer.parseInt(tokenModel.get("normDate").toString().substring(6)));
        Comparator<TokenModel> byMonth = Comparator.comparing(tokenModel -> Integer.parseInt(tokenModel.get("normDate").toString().substring(0,2)));
        Comparator<TokenModel> byDay = Comparator.comparing(tokenModel -> Integer.parseInt(tokenModel.get("normDate").toString().substring(3,5)));
        Collections.sort(dateCandidates, byYear.thenComparing(byMonth).thenComparing(byDay));
        return dateCandidates;
    }

    //Function to check both date and tag are residing in the same segment
    public TokenModel checkIfSameSegmentWithTag(TokenModel dateCandidate, List<TokenModel> tagCandidates){
        TokenModel tagForDate =new TokenModel();
        for(TokenModel tagCandidate: tagCandidates){
            if(isInSameSegment(dateCandidate,tagCandidate)){
                    tagForDate = tagCandidate;

            }
        }
        return tagForDate;
    }
    //function to check tag and date in same segment has "12/25/69 d/c" format. Then we need to check 12/258/69 is value for any other tag
//    private boolean isValueOfNearTag(List<TokenModel> tagCandidates,TokenModel dateCandidate){
//        boolean isValueOfNearTag = false;
//        for(TokenModel tagCandidate:tagCandidates){
//
//        }
//   }

    private boolean isInSameSegment(TokenModel dateCandidate, TokenModel tagCandidate){
        boolean inSameSegment = false;
        if(Math.abs((double)dateCandidate.get(DateParserUtilityConstants.Properties.X) - (double)tagCandidate.get(DateParserUtilityConstants.Properties.X)) == 0 &&
                Math.abs((double)dateCandidate.get(DateParserUtilityConstants.Properties.Y) - (double)tagCandidate.get(DateParserUtilityConstants.Properties.Y)) == 0 &&
                Math.abs((double)dateCandidate.get(DateParserUtilityConstants.Properties.W) - (double)tagCandidate.get(DateParserUtilityConstants.Properties.W)) == 0  &&
                Math.abs((double)dateCandidate.get(DateParserUtilityConstants.Properties.H) - (double)tagCandidate.get(DateParserUtilityConstants.Properties.H)) == 0 ){
            inSameSegment = true;
        }
        return inSameSegment;
    }

    //function to get all the same line tag tokenmodels for a date candidate
    public List<TokenModel> getAllInLineTags(TokenModel dateCandidate , List<TokenModel> tagCandidates){
        List<TokenModel> sameLineTags = new ArrayList<>();
        tagCandidates.forEach(tokenModel -> {
            if (Math.abs((double)dateCandidate.get(DateParserUtilityConstants.Properties.Y) - (double) tokenModel.get(DateParserUtilityConstants.Properties.Y)) <= LINE_THRESHOLD){
                sameLineTags.add(tokenModel);
            }
        });
        if(!sameLineTags.isEmpty())
            return sameLineTags;
        else
            return null;
    }

    //function to get all the tag tokenmodels that residing leftside of the date candidate
    public List<TokenModel> getAllLeftTags(TokenModel dateCandidate, List<TokenModel> tagCandidates){
        List<TokenModel> leftSideTags = new ArrayList<>();
        tagCandidates.forEach(tokenModel -> {
            if(((double)dateCandidate.get(DateParserUtilityConstants.Properties.X)) - ((double) tokenModel.get(DateParserUtilityConstants.Properties.X)+(double)tokenModel.get(DateParserUtilityConstants.Properties.W)) >= 0 &&
                    ((double)dateCandidate.get(DateParserUtilityConstants.Properties.X)) - ((double) tokenModel.get(DateParserUtilityConstants.Properties.X)+(double)tokenModel.get(DateParserUtilityConstants.Properties.W)) < 250 ){

                leftSideTags.add(tokenModel);
            }
        });
        if(!leftSideTags.isEmpty())
            return leftSideTags;
        else
            return null;
    }

    //function to get all the tag tokenmodels that residing rightside of the date candidate
    public List<TokenModel> getAllRightTags(TokenModel dateCandidate, List<TokenModel> tagCandidates){
        List<TokenModel> rightSideTags = new ArrayList<>();
        tagCandidates.forEach(tokenModel -> {
            if(((double)dateCandidate.get(DateParserUtilityConstants.Properties.X)+(double)dateCandidate.get(DateParserUtilityConstants.Properties.W)) - (double) tokenModel.get(DateParserUtilityConstants.Properties.X) <= 0 &
                    ((double)dateCandidate.get(DateParserUtilityConstants.Properties.X)+(double)dateCandidate.get(DateParserUtilityConstants.Properties.W)) - (double) tokenModel.get(DateParserUtilityConstants.Properties.X) > -250){
                rightSideTags.add(tokenModel);
            }
        });
        if(!rightSideTags.isEmpty())
            return rightSideTags;
        else
            return null;
    }
    //function to get all the tag tokenmodel that residing on top of date candidate
    public List<TokenModel> getAllUpperTags(TokenModel dateCandidate, List<TokenModel> tagCandidates){
        List<TokenModel> upperTags = new ArrayList<>();
        tagCandidates.forEach(tokenModel -> {
            if(((double) dateCandidate.get(DateParserUtilityConstants.Properties.Y) -((double)tokenModel.get(DateParserUtilityConstants.Properties.Y)+ (double)tokenModel.get(DateParserUtilityConstants.Properties.H)) >= 0 &
                    ((double) dateCandidate.get(DateParserUtilityConstants.Properties.Y) -((double)tokenModel.get(DateParserUtilityConstants.Properties.Y)+ (double)tokenModel.get(DateParserUtilityConstants.Properties.H)) < PROXIMITY_THRESHOLD_UPPER))){
//                    &
//                    (Math.abs((double)tokenModel.get(DateParserUtilityConstants.Properties.X) -
//                    (double) dateCandidate.get(DateParserUtilityConstants.Properties.X)) <= SAME_COLUMN_THRESHOLD)){
                upperTags.add(tokenModel);
            }
        });
        if(!upperTags.isEmpty())
            return upperTags;
        else
            return null;

    }

    //function to get all the tag tokenmodel that residing on bottom of date candidate
    public List<TokenModel> getAllLowerTags(TokenModel dateCandidate, List<TokenModel> tagCandidates){
        List<TokenModel> lowerTags = new ArrayList<>();
        tagCandidates.forEach(tokenModel -> {
            if(((double)dateCandidate.get(DateParserUtilityConstants.Properties.Y)+ (double)dateCandidate.get(DateParserUtilityConstants.Properties.H)) -
                    (double) tokenModel.get(DateParserUtilityConstants.Properties.Y) <= 0 &
                    ((double)dateCandidate.get(DateParserUtilityConstants.Properties.Y)+ (double)dateCandidate.get(DateParserUtilityConstants.Properties.H)) -
                            (double) tokenModel.get(DateParserUtilityConstants.Properties.Y) > PROXIMITY_THRESHOLD_LOWER){
//                    &
//                    (Math.abs((double)tokenModel.get(DateParserUtilityConstants.Properties.X) -
//                    (double) dateCandidate.get(DateParserUtilityConstants.Properties.X)) <= SAME_COLUMN_THRESHOLD)){
                lowerTags.add(tokenModel);
            }
        });
        if(!lowerTags.isEmpty())
            return lowerTags;
        else
            return null;

    }

    //function to get the tag that closest to the date candidate
    public TokenModel getClosestTagFromRow(TokenModel dateCandidate, List<TokenModel> tagCandidates){
        Comparator<TokenModel> byX = Comparator.comparingDouble(tokenModel -> Math.abs((double)tokenModel.get(DateParserUtilityConstants.Properties.X) - (double) dateCandidate.get(DateParserUtilityConstants.Properties.X)));
        Comparator<TokenModel> byY = Comparator.comparingDouble(tokenModel -> Math.abs((double)tokenModel.get(DateParserUtilityConstants.Properties.Y) - (double) dateCandidate.get(DateParserUtilityConstants.Properties.Y)));
//        tagCandidates.sort(Comparator.comparingDouble(tokenModel -> Math.abs((double)tokenModel.get(DateParserUtilityConstants.Properties.X) - (double) dateCandidate.get(DateParserUtilityConstants.Properties.X))));
        Collections.sort(tagCandidates,byX.thenComparing(byY));
        return tagCandidates.get(0);
    }
    public TokenModel getClosestTagFromColumn(TokenModel dateCandidate, List<TokenModel> tagCandidates){
        Comparator<TokenModel> byY = Comparator.comparingDouble(tokenModel -> Math.abs((double)tokenModel.get(DateParserUtilityConstants.Properties.Y) - (double) dateCandidate.get(DateParserUtilityConstants.Properties.Y)));
        Comparator<TokenModel> byX = Comparator.comparingDouble(tokenModel -> Math.abs((double)tokenModel.get(DateParserUtilityConstants.Properties.X) - (double) dateCandidate.get(DateParserUtilityConstants.Properties.X)));
//        tagCandidates.sort(Comparator.comparingDouble(tokenModel -> Math.abs((double)tokenModel.get(DateParserUtilityConstants.Properties.Y) - (double) dateCandidate.get(DateParserUtilityConstants.Properties.Y))));
        Collections.sort(tagCandidates,byY.thenComparing(byX));
        if(Math.abs((double)dateCandidate.get(DateParserUtilityConstants.Properties.X) - (double) tagCandidates.get(0).get(DateParserUtilityConstants.Properties.X)) > 150.0){
            return null;
        }
        return tagCandidates.get(0);
    }


    public List<TokenModel> removeFalseDates(List<TokenModel> dateCandidates){
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        List<TokenModel> faultYearCandidates = new ArrayList<>();
        //regex to identify the normalized date in the format of mm/dd/yyyy
        String regex = "^(1[0-2]|0[1-9])/(3[01]|[12][0-9]|0[1-9])/[0-9]{4}$";
        Pattern pattern = Pattern.compile(regex);
        for(TokenModel date:dateCandidates){
            Matcher matcher = pattern.matcher(date.get(DateParserUtilityConstants.Properties.NORM_DATE).toString());
            if(!matcher.matches() || date.get(DateParserUtilityConstants.Properties.NORM_DATE).toString().length()!=10 ){
                faultYearCandidates.add(date);
                continue;
            }
            else if(Math.abs(Integer.parseInt(date.get(DateParserUtilityConstants.Properties.NORM_DATE).toString().substring(6)) - currentYear) > 100){
                faultYearCandidates.add(date);
            }
        }
        dateCandidates.removeAll(faultYearCandidates);
        return dateCandidates;
    }



    public static void main(String[] args) throws FileNotFoundException {
        DateParserUtility dateParserUtility = new DateParserUtility();
        List<TokenModel> test = new ArrayList<>();
        TokenModel t1 = new TokenModel();
        TokenModel t2 = new TokenModel();
        TokenModel t3 = new TokenModel();
        TokenModel t4 = new TokenModel();
        TokenModel dateCandidate = new TokenModel();
        t1.put("speAno","31 may 94");
        t1.put("x",150);
        t2.put("speAno","28 may 94");
        t2.put("x",80);
        t3.put("speAno","31-05-2000");
        t3.put("x",40);
        t4.put("x",20);

        dateCandidate.put("speAno","1/01/2019");
        dateCandidate.put("x",50);
        test.add(t1);
        test.add(t2);
        test.add(t3);
        test.add(t4);

        dateParserUtility.getClosestTagFromRow(dateCandidate,test);



    }
}
