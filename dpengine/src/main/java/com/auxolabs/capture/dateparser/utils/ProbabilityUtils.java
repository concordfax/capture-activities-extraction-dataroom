package com.auxolabs.capture.dateparser.utils;


import com.auxolabs.capture.dateparser.models.ProbabilityModel;
import com.auxolabs.capture.dateparser.models.TokenModel;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ProbabilityUtils {
    @Inject
    public ProbabilityUtils(){}

    public HashMap<Integer,ProbabilityModel> getPrecedenceProbabilityMap(List<TokenModel> sortedDateCandidates){
        HashMap<Integer,ProbabilityModel> indexPrecedenceMap = new HashMap<>();
        int lenOfSortedCandidates = sortedDateCandidates.size()-1;
        float probailitySplit = 100.0f/lenOfSortedCandidates;
        float relativeProbailitySplit = 0;
         for( int i = 0;i<sortedDateCandidates.size() ;++i){
            if(i ==0){
                ProbabilityModel probabilityModel = new ProbabilityModel();
                probabilityModel.setProbaForDOB(100.0f);
                probabilityModel.setProbaForDischargeDate(0.0f);
                probabilityModel.setProbaForFaxDate(0.0f);
                indexPrecedenceMap.put(0,probabilityModel);
                relativeProbailitySplit += probailitySplit;
                continue;
            }
            if(i == sortedDateCandidates.size()-1){
                ProbabilityModel probabilityModel = new ProbabilityModel();
                probabilityModel.setProbaForDOB(0.0f);
                probabilityModel.setProbaForDischargeDate(100.0f);
                probabilityModel.setProbaForFaxDate(100.0f);
                indexPrecedenceMap.put(sortedDateCandidates.size()-1,probabilityModel);
                break;
            }
             float relativeProbabilityForDOB  = 100 -relativeProbailitySplit;
             float relativeProbabilityForFaxDate = relativeProbailitySplit;
             float relativeProbabilityForDOD = relativeProbailitySplit;
             ProbabilityModel probabilityModel = new ProbabilityModel();
             probabilityModel.setProbaForDOB(relativeProbabilityForDOB);
             probabilityModel.setProbaForFaxDate(relativeProbabilityForFaxDate);
             probabilityModel.setProbaForDischargeDate(relativeProbabilityForDOD);
             indexPrecedenceMap.put(i,probabilityModel);
             relativeProbailitySplit += probailitySplit;


         }
         return indexPrecedenceMap;
    }

    public static void main(String[] args) {
        ProbabilityUtils probabilityUtils = new ProbabilityUtils();
        List<TokenModel> test = new ArrayList<>();
        TokenModel t1 = new TokenModel();
        TokenModel t2 = new TokenModel();
        TokenModel t3 = new TokenModel();
        TokenModel t4 = new TokenModel();
        t1.put("speAno","28 may 94");
        t1.put("x",150);
        t2.put("speAno","31 may 94");
        t2.put("x",80);
        t3.put("speAno","31-05-2000");
        t3.put("x",40);
        t4.put("x",20);

        test.add(t1);
        test.add(t2);
        test.add(t3);
        test.add(t4);
        HashMap<Integer,ProbabilityModel> indexPrecedenceMap = probabilityUtils.getPrecedenceProbabilityMap(test);
        System.out.println("test");
    }
}
