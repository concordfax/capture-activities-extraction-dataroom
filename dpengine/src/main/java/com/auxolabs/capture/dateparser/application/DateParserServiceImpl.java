package com.auxolabs.capture.dateparser.application;


import com.auxolabs.capture.dateparser.DPEngineService;
import com.auxolabs.capture.dateparser.constants.DateParserUtilityConstants;
import com.auxolabs.capture.dateparser.models.DateCandidateModel;
import com.auxolabs.capture.dateparser.models.DateNormXYModel;
import com.auxolabs.capture.dateparser.models.OutputDateModel;
import com.auxolabs.capture.dateparser.models.TokenModel;
import com.auxolabs.capture.dateparser.parser.DateRuleParser;
import com.auxolabs.capture.dateparser.utils.DateParserUtility;
import com.concordfax.capture.core.models.document.result.extraction.ExtractionField;
import com.concordfax.capture.core.models.document.result.extraction.SimpleExtractionField;


import javax.inject.Inject;
import java.util.*;

public class DateParserServiceImpl implements DPEngineService {

    private DateParserUtility dateParserUtility;

    @Inject
    public DateParserServiceImpl(){
        this.dateParserUtility = new DateParserUtility();
    }

    @Override
    public HashMap<String, ExtractionField> process(List<TokenModel> dateCandidates, List<TokenModel> tagCandidates,List<String> tagDoe,List<String> tagDos,List<String> tagOrderDate, List<String> tagDod,List<String> tagDob) {
        List<TokenModel> sortedDateCandidates = dateParserUtility.parseAndGetSorted(dateCandidates);
        //if id of two tag candidates are equal we need to find the longest match so we can avoid duplicate tag candidates
        preprocessInputCandidates(tagCandidates);
//        HashMap<Integer, ProbabilityModel> indexPrecedenceMap = probabilityUtils.getPrecedenceProbabilityMap(sortedDateCandidates);
        HashMap<String,List<DateNormXYModel>> finalResult = new HashMap<>();
        List<DateCandidateModel> dateParserInput = new ArrayList<>();
        HashMap<String,ExtractionField> pageLevelDateResult = new HashMap<>();
        for(int index =0; index<sortedDateCandidates.size();++index){
            DateCandidateModel dateCandidate = new DateCandidateModel();
//            dateCandidate.setProbability(indexPrecedenceMap.get(index));
            dateCandidate.setOrginalValue(sortedDateCandidates.get(index).get(DateParserUtilityConstants.Properties.SPE_ANO).toString());
            dateCandidate.setNormValue(sortedDateCandidates.get(index).get(DateParserUtilityConstants.Properties.NORM_DATE).toString());
            dateCandidate.setX((double) sortedDateCandidates.get(index).get(DateParserUtilityConstants.Properties.X));
            dateCandidate.setY((double) sortedDateCandidates.get(index).get(DateParserUtilityConstants.Properties.Y));
            dateCandidate.setW((double) sortedDateCandidates.get(index).get(DateParserUtilityConstants.Properties.W));
            dateCandidate.setH((double) sortedDateCandidates.get(index).get(DateParserUtilityConstants.Properties.H));
//              this is to check the dateCandidate is inside of any other tag segment
//            if (!dateParserUtility.checkIfSameSegmentWithTag(sortedDateCandidates.get(index), tagCandidates).isEmpty()) {
//                TokenModel tagForDate = dateParserUtility.checkIfSameSegmentWithTag(sortedDateCandidates.get(index), tagCandidates);
//                dateCandidate.setInsideASegment(true);
//
//                dateCandidate.setSameTagSegment(tagForDate);
//                dateCandidate.setTagValue(tagForDate.get(DateParserUtilityConstants.Properties.SPE_ANO).toString());
//                dateParserInput.add(dateCandidate);
//                // if the date candidate is inside of any tag segment then no need to find out proximal tag segments.
//                continue;
//            }
//            else {
                dateCandidate.setInsideASegment(false);
                List<TokenModel> sameLineTags = dateParserUtility.getAllInLineTags(sortedDateCandidates.get(index),tagCandidates);
                if(sameLineTags != null){
                    // getting the closest left tag for the date candidate
                    List<TokenModel> leftTags = dateParserUtility.getAllLeftTags(sortedDateCandidates.get(index),sameLineTags);
                    if(leftTags != null){
                        dateCandidate.setLeftTagSegment(dateParserUtility.getClosestTagFromRow(sortedDateCandidates.get(index),leftTags));
                    }
                    // getting the closest right tag for the date candidate
                    List<TokenModel> rightTags = dateParserUtility.getAllRightTags(sortedDateCandidates.get(index),sameLineTags);
                    if(rightTags != null){
                        dateCandidate.setRightTagSegment(dateParserUtility.getClosestTagFromRow(sortedDateCandidates.get(index),rightTags));
                    }
                }
                // getting the closest upper tag for the date candidate
                List<TokenModel> upperTags = dateParserUtility.getAllUpperTags(sortedDateCandidates.get(index),tagCandidates);
                if(upperTags != null){
                    if(dateParserUtility.getClosestTagFromColumn(sortedDateCandidates.get(index),upperTags) != null){
                        dateCandidate.setUpperTagSegment(dateParserUtility.getClosestTagFromColumn(sortedDateCandidates.get(index),upperTags));
                    }
                }
                // getting the closest bottom tag for the date candidate
                List<TokenModel> bottomTags = dateParserUtility.getAllLowerTags(sortedDateCandidates.get(index),tagCandidates);
                if(bottomTags !=null){
                    dateCandidate.setLowerTagSegment(dateParserUtility.getClosestTagFromColumn(sortedDateCandidates.get(index),bottomTags));
                }
//            }
            dateParserInput.add(dateCandidate);
        }

        finalResult = DateRuleParser.parseAndGetResult(dateParserInput);
        //we have to find out the least date for each fields(DOE,DOS,order date and date of discharge) for this single page
        List<OutputDateModel> dobValues = new ArrayList<>();
        List<OutputDateModel> doeValues= new ArrayList<>();
        List<OutputDateModel> dosValues= new ArrayList<>();
        List<OutputDateModel> orderDateValues= new ArrayList<>();
        List<OutputDateModel> dodValues= new ArrayList<>();
        String dobTag="";
        String doeTag="";
        String dosTag="";
        String odTag="";
        String dodTag="";
        // TODO: 3/21/20 change the normalized value into original value and make a logic to sort it based on original value in output configuration step
        for (Map.Entry<String, List<DateNormXYModel>> entry : finalResult.entrySet()) {
            String key = entry.getKey();
            List<DateNormXYModel> value = entry.getValue();
            if (tagDob.contains(key)) {
                OutputDateModel outputDateModel = new OutputDateModel();
                outputDateModel.setNormalizedDate(value.get(0).getNormValue());
                outputDateModel.setOriginalDateValue(value.get(0).getOrgValue());
                dobValues.add(outputDateModel);
                dobTag = value.get(0).getTagValue();
            } else if (tagDoe.contains(key)) {
                //checking whether the tag-value pair is residing in the header of the document. if yes taking this as output.
                for (DateNormXYModel date : value) {
                    if (date.isFirstOccurence() & date.getY() < 400.0) {
                        OutputDateModel outputDateModel = new OutputDateModel();
                        outputDateModel.setNormalizedDate(value.get(0).getNormValue());
                        outputDateModel.setOriginalDateValue(value.get(0).getOrgValue());
                        doeValues.add(outputDateModel);
                        doeTag = date.getTagValue();
                    }
                }

            } else if (tagDos.contains(key)) {
                //checking whether the tag-value pair is residing in the header of the document. if yes taking this as output.
                for (DateNormXYModel date : value) {
                    if (date.isFirstOccurence() & date.getY() < 400.0) {
                        OutputDateModel outputDateModel = new OutputDateModel();
                        outputDateModel.setNormalizedDate(value.get(0).getNormValue());
                        outputDateModel.setOriginalDateValue(value.get(0).getOrgValue());
                        dosValues.add(outputDateModel);
                        dosTag = date.getTagValue();
                    }
                }
            } else if (tagOrderDate.contains(key)) {
                OutputDateModel outputDateModel = new OutputDateModel();
                outputDateModel.setNormalizedDate(value.get(0).getNormValue());
                outputDateModel.setOriginalDateValue(value.get(0).getOrgValue());
                orderDateValues.add(outputDateModel);
                odTag = value.get(0).getTagValue();
            } else if (tagDod.contains(key)) {
                OutputDateModel outputDateModel = new OutputDateModel();
                outputDateModel.setNormalizedDate(value.get(0).getNormValue());
                outputDateModel.setOriginalDateValue(value.get(0).getOrgValue());
                dodValues.add(outputDateModel);
                dodTag = value.get(0).getTagValue();
            }
        }
        for (Map.Entry<String, List<DateNormXYModel>> entry : finalResult.entrySet()) {
            String key = entry.getKey();
            List<DateNormXYModel> value = entry.getValue();
            if (tagDoe.contains(key)) {
                //if there is no tag-value pair present in the header of the document , taking the lowest value out of all the combinations
                if (doeValues.isEmpty()) {
                    OutputDateModel outputDateModel = new OutputDateModel();
                    outputDateModel.setNormalizedDate(value.get(0).getNormValue());
                    outputDateModel.setOriginalDateValue(value.get(0).getOrgValue());
                    doeValues.add(outputDateModel);
                    doeTag = value.get(0).getTagValue();
                }
            } else if (tagDos.contains(key)) {
                //if there is no tag-value pair present in the header of the document , taking the lowest value out of all the combinations
                if (dosValues.isEmpty()) {
                    OutputDateModel outputDateModel = new OutputDateModel();
                    outputDateModel.setNormalizedDate(value.get(0).getNormValue());
                    outputDateModel.setOriginalDateValue(value.get(0).getOrgValue());
                    dosValues.add(outputDateModel);
                    dosTag = value.get(0).getTagValue();
                }
            }
        }
        if(isPresent(dobValues)){
            sortDatesAscend(dobValues);
            pageLevelDateResult.put("Patient DOB Identifier",new SimpleExtractionField("DOB", 0.75f, dobValues.get(0).getOriginalDateValue(),dobTag));
        }
        if(isPresent(doeValues)){
            sortDatesAscend(doeValues);
            pageLevelDateResult.put("Patient DOE Identifier",new SimpleExtractionField("DOE",0.75f,doeValues.get(0).getOriginalDateValue(),doeTag));
        }
        if(isPresent(dosValues)){
            sortDatesAscend(dosValues);
            pageLevelDateResult.put("Patient DOS Identifier",new SimpleExtractionField("DOS",0.75f,dosValues.get(0).getOriginalDateValue(),dosTag));
        }
        if(isPresent(orderDateValues)){
            sortDatesAscend(orderDateValues);
            pageLevelDateResult.put("Order Date Identifier",new SimpleExtractionField("Order_Date",0.75f,orderDateValues.get(0).getOriginalDateValue(),odTag));

        }
        if(isPresent(dodValues)){
            sortDatesAscend(dodValues);
            pageLevelDateResult.put("Patient DOD Identifier",new SimpleExtractionField("DOD",0.75f,dodValues.get(0).getOriginalDateValue(),dodTag));
        }
        return pageLevelDateResult;
    }

    private void preprocessInputCandidates(List<TokenModel> tagCandidates){
        List<TokenModel> duplicateEntries = new ArrayList<>();
        for(int index =0 ; index<tagCandidates.size();++index) {
            for (int i = index + 1; i < tagCandidates.size(); ++i) {
                if ((int)(tagCandidates.get(i).get(DateParserUtilityConstants.Properties.ID)) == (int)(tagCandidates.get(index).get(DateParserUtilityConstants.Properties.ID))) {

                    if (tagCandidates.get(i).get(DateParserUtilityConstants.Properties.SPE_ANO).toString().length() > tagCandidates.get(index).get(DateParserUtilityConstants.Properties.SPE_ANO).toString().length()) {
                        duplicateEntries.add(tagCandidates.get(index));
                    } else {
                        duplicateEntries.add(tagCandidates.get(i));
                    }
                }

            }
        }
        tagCandidates.removeAll(duplicateEntries);
    }

    private void sortDatesAscend(List<OutputDateModel> listOfDates){
        Comparator<OutputDateModel> byYear = Comparator.comparingInt(date->Integer.parseInt(date.getNormalizedDate().substring(6)));
        Comparator<OutputDateModel> byMonth = Comparator.comparing(date-> Integer.parseInt(date.getNormalizedDate().substring(0,2)));
        Comparator<OutputDateModel> byDay = Comparator.comparing(date -> Integer.parseInt(date.getNormalizedDate().substring(3,5)));
        listOfDates.sort(byYear.thenComparing(byMonth).thenComparing(byDay));
    }

    private boolean isPresent(Object obj){
        boolean present = false;

        if(obj instanceof List){
            if(!((List) obj).isEmpty()){
                present = true;
            }
        }
        if (obj instanceof String){
            if(((String) obj).length() !=0){
                present = true;
            }
        }
        if(obj instanceof HashMap){
            if(!((HashMap) obj).isEmpty()){
                present = true;
            }
        }

        return present;
    }


}
