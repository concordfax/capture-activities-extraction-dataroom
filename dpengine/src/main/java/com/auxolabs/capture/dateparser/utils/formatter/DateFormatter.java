package com.auxolabs.capture.dateparser.utils.formatter;

import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
public class DateFormatter {
    public static String nattyPreProcess(String date) {
        if (date == null) {
            return null;
        }
        date = changeHyphenToSlash(date);
        String dateFormat1 = "mm/dd/yyyy", dateFormat2 = "mm/dd/yy";

        if (isValidFormat(dateFormat1, date) && Integer.valueOf(date.substring(0, 2)) > 12) {
            date = date.substring(3, 5) + '/' + date.substring(0, 2) + date.substring(6);
        } else if (isValidFormat(dateFormat2, date) && Integer.valueOf(date.substring(0, 2)) > 12) {
            date = date.substring(3, 5) + '/' + date.substring(0, 2) + date.substring(5);
        }

        if (((isValidFormat(dateFormat1, date)) || (isValidFormat(dateFormat2, date))) && !checkIfProperDate(date)) {
            date = "";
        }
        return date;
    }

    private static String changeHyphenToSlash(String date) {
        if (date.contains("-")) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < date.length(); i++) {
                if (date.charAt(i) == '-')
                    sb.append('/');
                else
                    sb.append(date.charAt(i));
            }
            date = sb.toString();
        }
        return date;
    }

    private static boolean isValidFormat(String format, String value) {
        Date date = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            date = sdf.parse(value);
            if (!value.equals(sdf.format(date))) {
                date = null;
            }
        } catch (ParseException ex) {
            log.error("Date Parse exception in DateFormatter.isValidFormat()");
        }
        return date != null;
    }

    private static boolean checkIfProperDate(String date) {
        int month = Integer.valueOf(date.substring(0, 2));
        int day = Integer.valueOf(date.substring(3, 5));
        if ((month > 0 && month <= 12) && (day > 0 && day <= 31)) {
            return true;
        }
        return false;
    }
}
