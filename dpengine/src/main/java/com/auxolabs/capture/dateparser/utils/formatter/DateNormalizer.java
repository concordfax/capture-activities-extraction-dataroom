package com.auxolabs.capture.dateparser.utils.formatter;

import com.joestelmach.natty.DateGroup;
import com.joestelmach.natty.Parser;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class DateNormalizer {

    private static final String[] REPRESENTATIONS_OF_CURRENT_DATE = {"present", "any follow-ups", "date"};
    private static final String dateFormat = "(\\d{1,2}/\\d{1,2}/\\d{2,4})";

    public DateNormalizer() {
    }

    private String dateFormatter(String date, boolean hasWords) {
        try {
            if (date.contains(",")) {
                int indexOfComma = date.indexOf(',');
                if (date.charAt(indexOfComma - 1) == ' ')
                    date = date.substring(0, indexOfComma - 1) + date.substring(indexOfComma);
            }
            List<Date> dateInOption = null;
            if (!hasWords) {
                date = DateFormatter.nattyPreProcess(date);
            } else {

                // For handling the corner case like: 29 Jan 87
                String[] words = date.split("\\s+");
                if (words.length > 0) {
                    String last_date = words[words.length -1];
                    if (last_date.length() == 2 && (last_date.matches("[0-9]+"))){
                        Date twoCharYear = new SimpleDateFormat("yy").parse(last_date);
                        SimpleDateFormat fourCharYearFormat = new SimpleDateFormat("yyyy");
                        String fourCharYear = fourCharYearFormat.format(twoCharYear);
                        words[words.length -1] = fourCharYear;
                        date = String.join(" ", words);
                    }
                }
            }
            dateInOption = nattyConverter(date);
            if (dateInOption == null || dateInOption.size() == 0) {
                return date;
            }
            Date date1 = dateInOption.get(0);
            DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
            return dateFormat.format(date1);
        } catch (Exception e) {
            // ignore error
            return date;
        }
    }

    //transforms text to required date format, helper function for startDateAndEndDateNormalizer
    private List<Date> nattyConverter(String option) {
        Parser parser = new Parser();
        List<DateGroup> groups = parser.parse(option);
        DateGroup dateGroup = groups.get(0);
        return dateGroup.getDates();
    }

    private String getCurrentDate() throws ParseException {
        SimpleDateFormat reqFormat = new SimpleDateFormat("MM/dd/yyyy");
        reqFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date date = new Date();
        return reqFormat.format(date);
    }

    private String dateToString(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return dateFormat.format(date);
    }

    private boolean checkIfContainWords(String date) {
        for (int index = 0; index < date.length(); index++)
            if (Character.isLetter(date.charAt(index)))
                return true;
        return false;
    }

    public String dateFieldFormatter(String date) throws ParseException {
        if (date == null || date.isEmpty()){
            return date;
        }
        date  = date.replaceAll("\\s+/\\s+","/");
        if (date.matches("\\d{1,2}\\.\\d{1,2}\\.\\d{4}")
                ||date.matches("\\d{1,2}\\.\\d{1,2}\\.\\d{2}")
                ||date.matches("\\s*\\d{1,2}\\s*\\.\\s*\\d{1,2}\\s*\\.\\s*\\d{2}\\s*")
                ||date.matches("\\s*\\d{1,2}\\s*\\.\\s*\\d{1,2}\\s*\\.\\s*\\d{4}\\s*")) {
            date = date.replaceAll("\\.","/");
        }
        date = date.trim();
        if (date.contains("-")) {
            // check if date contains only numbers or contains alphabets as well
            if(checkIfContainWords(date))
                date = date.replaceAll("-"," ");
            else
                date = date.replaceAll("-", "/");
        }
        if (date.contains("@")){
            date = date.substring(0,date.indexOf("@"));
        }
        if (!checkIfContainWords(date) && date.split(" ")[0].matches(dateFormat)){
            date = date.split(" ")[0];
        }
        String result = "";
        if (date.matches("[0-9]+") && date.length() == 4) {
            result = date;
        } else if (Character.isDigit(date.charAt(1)) && Integer.valueOf(date.substring(0, 2)) > 12 && date.matches(dateFormat)) {
            SimpleDateFormat sdf = null;
            if (date.matches("(\\d{1,2}/\\d{1,2}/\\d{4})"))
                sdf = new SimpleDateFormat("dd/MM/yyyy");
            else if (date.matches("(\\d{1,2}/\\d{1,2}/\\d{2})"))
                sdf = new SimpleDateFormat("dd/MM/yy");
            Date date1 = sdf.parse(date);

            sdf = new SimpleDateFormat("MM/dd/yyyy");
            result = sdf.format(date1);
        } else if (Arrays.asList(REPRESENTATIONS_OF_CURRENT_DATE).contains(date.toLowerCase())) {
            result = getCurrentDate();
            //here write all the cases other than 'present' for future
            //Check if there are spaces followed by date separators. Ex. 10/ 20 / 89
        } else if ((date.trim().matches("(\\d{1,2}/\\d{1,2}/\\d{4})")) ||
                (date.trim().matches("(\\d{1}\\s*\\d{1}\\s*\\/\\s*\\d{1}\\s*\\d{1}\\s*\\/\\s*\\d{1}\\s*\\d{1}\\s*\\d{1}\\s*\\d{1})"))) {
            date = date.replaceAll("\\s", "");
            result = dateFormatter(date, false);
        } else if ((date.trim().matches("\\d{1,2}/\\d{1,2}/\\d{2}")) ||
                (date.trim().matches("(\\d{1}\\s*\\d{1}\\s*\\/\\s*\\d{1}\\s*\\d{1}\\s*\\/\\s*\\d{1}\\s*\\d{1})"))) {
            date = date.replaceAll("\\s", "");
            result = dateFormatter(date, false);
            if (!result.isEmpty() && Integer.parseInt(result.substring(6))> Calendar.getInstance().get(Calendar.YEAR)){
                int faultYear= Integer.parseInt(result.substring(6));
                String actualYear = Integer.toString(faultYear-100);
                result=result.replaceAll(result.substring(6),actualYear);
            }
        } else if (checkIfContainWords(date)) {
            date = date.toLowerCase();
            result = dateFormatter(date,true);
        } else{
            try {
                SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
                Date tempdate = sdf.parse(date);
                if (!date.equals(sdf.format(tempdate))) {
                    Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(date);
                    result = dateToString(date1);
                } else {
                    Date date1 = new SimpleDateFormat("MM/dd/yyyy").parse(date);
                    result = dateToString(date1);
                }
                return result;
            } catch (Exception e) {
                result = date;
            }
        }
        if(result.isEmpty())
            return getCurrentDate();

        return result;
    }
}
