package com.auxolabs.capture.dateparser.parser;

import com.auxolabs.capture.dateparser.constants.DateParserUtilityConstants;
import com.auxolabs.capture.dateparser.models.DateCandidateModel;
import com.auxolabs.capture.dateparser.models.DateNormXYModel;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class DateRuleParser {
    @Inject
    public DateRuleParser(){}


    public static HashMap<String,List<DateNormXYModel>> parseAndGetResult(List<DateCandidateModel> dateCandidates){
        HashMap<String,List<DateNormXYModel>> finalResult = new HashMap<>();
        for(DateCandidateModel dateCandidateModel : dateCandidates) {
            //checking the date candidate model and tag model was in a same segment then we need to take the tag and output it
            if (dateCandidateModel.isInsideASegment()) {
                addValueToFinalResult(finalResult,dateCandidateModel,"inside");
            }
            //checking the date candidate model have any left tag
            else if (dateCandidateModel.getLeftTagSegment() != null) {
                addValueToFinalResult(finalResult,dateCandidateModel,"left");
            } else if (dateCandidateModel.getUpperTagSegment() != null) {
                if (!isLeftTagOfAnotherDate(dateCandidateModel, dateCandidates)) {
                    addValueToFinalResult(finalResult,dateCandidateModel,"upper");
                }
            } else if (dateCandidateModel.getRightTagSegment() != null) {
                if (!isLeftTagOfAnotherDate(dateCandidateModel, dateCandidates)) {
                    addValueToFinalResult(finalResult,dateCandidateModel,"right");
                }
            }
        }
        return finalResult;
    }

    private static boolean isLeftTagOfAnotherDate(DateCandidateModel dateCandidateModel,List<DateCandidateModel> dateCandidates){
        boolean ok = false;
        for(DateCandidateModel dModel:dateCandidates){
            if(dModel.getLeftTagSegment() != null & dateCandidateModel.getRightTagSegment() != null){
                if(Integer.parseInt(dateCandidateModel.getRightTagSegment().get(DateParserUtilityConstants.Properties.ID).toString()) ==
                        Integer.parseInt(dModel.getLeftTagSegment().get(DateParserUtilityConstants.Properties.ID).toString())) {
                    ok = true;
                    break;
                }
            }
            else if (dModel.getLeftTagSegment() != null & dateCandidateModel.getUpperTagSegment() != null){
                if(Integer.parseInt(dateCandidateModel.getUpperTagSegment().get(DateParserUtilityConstants.Properties.ID).toString()) ==
                        Integer.parseInt(dModel.getLeftTagSegment().get(DateParserUtilityConstants.Properties.ID).toString())) {
                    ok = true;
                    break;
                }
            }
            else if (dModel.getLeftTagSegment() != null & dateCandidateModel.getLowerTagSegment() != null){
                if(Integer.parseInt(dateCandidateModel.getLowerTagSegment().get(DateParserUtilityConstants.Properties.ID).toString()) ==
                        Integer.parseInt(dModel.getLeftTagSegment().get(DateParserUtilityConstants.Properties.ID).toString())) {
                    ok = true;
                    break;
                }
            }
        }
        return ok;
    }

        private static void addValueToFinalResult(HashMap<String,List<DateNormXYModel>> finalResult,DateCandidateModel dateCandidateModel ,String flag){

        switch (flag){
            case "inside":
                if(!finalResult.containsKey(dateCandidateModel.getTagValue())){
                    List<DateNormXYModel> listOfvalues = new ArrayList<>();
                    DateNormXYModel dateNormXYModel = setDateNormXY(dateCandidateModel);
                    dateNormXYModel.setTagValue(dateCandidateModel.getTagValue());
                    listOfvalues.add(dateNormXYModel);
                    finalResult.put(dateCandidateModel.getTagValue(),listOfvalues);

                }
                else {
                    DateNormXYModel dateNormXYModel = setDateNormXY(dateCandidateModel);
                    dateNormXYModel.setTagValue(dateCandidateModel.getTagValue());
                    finalResult.get(dateCandidateModel.getTagValue()).add(dateNormXYModel);
                    sortCandidatesBasedOnY(finalResult.get(dateCandidateModel.getTagValue()));
                    sortCandidatesBasedOnValues(finalResult.get(dateCandidateModel.getTagValue()));
                }
                break;
            case "left":
                if(!finalResult.containsKey(dateCandidateModel.getLeftTagSegment().get(DateParserUtilityConstants.Properties.SPE_ANO).toString())){
                    List<DateNormXYModel> listOfvalues = new ArrayList<>();
                    DateNormXYModel dateNormXYModel = setDateNormXY(dateCandidateModel);
                    dateNormXYModel.setTagValue(dateCandidateModel.getLeftTagSegment().get(DateParserUtilityConstants.Properties.SPE_ANO).toString());
                    listOfvalues.add(dateNormXYModel);
                    finalResult.put(dateCandidateModel.getLeftTagSegment().get(DateParserUtilityConstants.Properties.SPE_ANO).toString(),listOfvalues);

                }
                else {
                    DateNormXYModel dateNormXYModel = setDateNormXY(dateCandidateModel);
                    dateNormXYModel.setTagValue(dateCandidateModel.getLeftTagSegment().get(DateParserUtilityConstants.Properties.SPE_ANO).toString());
                    finalResult.get(dateCandidateModel.getLeftTagSegment().get(DateParserUtilityConstants.Properties.SPE_ANO).toString()).add(dateNormXYModel);
                    sortCandidatesBasedOnY(finalResult.get(dateCandidateModel.getLeftTagSegment().get(DateParserUtilityConstants.Properties.SPE_ANO).toString()));
                    sortCandidatesBasedOnValues(finalResult.get(dateCandidateModel.getLeftTagSegment().get(DateParserUtilityConstants.Properties.SPE_ANO).toString()));
                }
                break;
            case "upper":
                //this is to avoid case of taking date as part of tag "Inpatient" in upper and right case. need to optimize the code
                if(!finalResult.containsKey(dateCandidateModel.getUpperTagSegment().get(DateParserUtilityConstants.Properties.SPE_ANO).toString())){
                    if(!dateCandidateModel.getUpperTagSegment().get(DateParserUtilityConstants.Properties.SPE_ANO).toString().toLowerCase().equals("inpatient")){
                        List<DateNormXYModel> listOfvalues = new ArrayList<>();
                        DateNormXYModel dateNormXYModel = setDateNormXY(dateCandidateModel);
                        dateNormXYModel.setTagValue(dateCandidateModel.getUpperTagSegment().get(DateParserUtilityConstants.Properties.SPE_ANO).toString());
                        listOfvalues.add(dateNormXYModel);
                        finalResult.put(dateCandidateModel.getUpperTagSegment().get(DateParserUtilityConstants.Properties.SPE_ANO).toString(),listOfvalues);
                    }

                }
                else {
                    DateNormXYModel dateNormXYModel = setDateNormXY(dateCandidateModel);
                    dateNormXYModel.setTagValue(dateCandidateModel.getUpperTagSegment().get(DateParserUtilityConstants.Properties.SPE_ANO).toString());
                    finalResult.get(dateCandidateModel.getUpperTagSegment().get(DateParserUtilityConstants.Properties.SPE_ANO).toString()).add(dateNormXYModel);
                    sortCandidatesBasedOnY(finalResult.get(dateCandidateModel.getUpperTagSegment().get(DateParserUtilityConstants.Properties.SPE_ANO).toString()));
                    sortCandidatesBasedOnValues(finalResult.get(dateCandidateModel.getUpperTagSegment().get(DateParserUtilityConstants.Properties.SPE_ANO).toString()));

                }
                break;
            case "right":
                if(!finalResult.containsKey(dateCandidateModel.getRightTagSegment().get(DateParserUtilityConstants.Properties.SPE_ANO).toString()) ){
                    if(!dateCandidateModel.getRightTagSegment().get(DateParserUtilityConstants.Properties.SPE_ANO).toString().toLowerCase().equals("inpatient")){
                        List<DateNormXYModel> listOfvalues = new ArrayList<>();
                        DateNormXYModel dateNormXYModel = setDateNormXY(dateCandidateModel);
                        dateNormXYModel.setTagValue(dateCandidateModel.getRightTagSegment().get(DateParserUtilityConstants.Properties.SPE_ANO).toString());
                        listOfvalues.add(dateNormXYModel);
                        finalResult.put(dateCandidateModel.getRightTagSegment().get(DateParserUtilityConstants.Properties.SPE_ANO).toString(),listOfvalues);
                    }

                }
                else {
                    DateNormXYModel dateNormXYModel = setDateNormXY(dateCandidateModel);
                    dateNormXYModel.setTagValue(dateCandidateModel.getRightTagSegment().get(DateParserUtilityConstants.Properties.SPE_ANO).toString());
                    finalResult.get(dateCandidateModel.getRightTagSegment().get(DateParserUtilityConstants.Properties.SPE_ANO).toString()).add(dateNormXYModel);
                    sortCandidatesBasedOnY(finalResult.get(dateCandidateModel.getRightTagSegment().get(DateParserUtilityConstants.Properties.SPE_ANO).toString()));
                    sortCandidatesBasedOnValues(finalResult.get(dateCandidateModel.getRightTagSegment().get(DateParserUtilityConstants.Properties.SPE_ANO).toString()));
                }
                break;
        }
        //setting first occurance for one element if it's y is less than 400
        finalResult.forEach((k,v)->{
            if(v.size() == 1 & v.get(0).getY() < 400.0){
                v.get(0).setFirstOccurence(true);
            }
        });
    }

    private static void sortCandidatesBasedOnValues(List<DateNormXYModel> listOfDates){
        Comparator<DateNormXYModel> byYear = Comparator.comparingInt(date->Integer.parseInt(date.getNormValue().substring(6)));
        Comparator<DateNormXYModel> byMonth = Comparator.comparing(date-> Integer.parseInt(date.getNormValue().substring(0,2)));
        Comparator<DateNormXYModel> byDay = Comparator.comparing(date -> Integer.parseInt(date.getNormValue().substring(3,5)));
        listOfDates.sort(byYear.thenComparing(byMonth).thenComparing(byDay));
    }

    private static void sortCandidatesBasedOnY(List<DateNormXYModel> dateNormXYModels){
        dateNormXYModels.sort(Comparator.comparingDouble(DateNormXYModel::getY));
        dateNormXYModels.get(0).setFirstOccurence(true);
    }

    private static DateNormXYModel setDateNormXY(DateCandidateModel dateCandidateModel){
        DateNormXYModel dateNormXYModel = new DateNormXYModel();
        dateNormXYModel.setOrgValue(dateCandidateModel.getOrginalValue());
        dateNormXYModel.setNormValue(dateCandidateModel.getNormValue());
        dateNormXYModel.setX(dateCandidateModel.getX());
        dateNormXYModel.setY(dateCandidateModel.getY());
        dateNormXYModel.setH(dateCandidateModel.getH());
        dateCandidateModel.setW(dateCandidateModel.getW());
        return dateNormXYModel;
    }




    }
