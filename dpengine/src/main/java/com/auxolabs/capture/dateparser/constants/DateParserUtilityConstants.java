package com.auxolabs.capture.dateparser.constants;

public class DateParserUtilityConstants {

    public static class Properties{
        public static final String X = "x";
        public static final String Y = "y";
        public static final String W = "w";
        public static final String H = "h";
        public static final String TEXT = "text";
        public static final String SPE_ANO = "speAno";
        public static final String ID = "id";
        public static final String ANNOTATION = "annotation";
        public static final String NORM_DATE = "normDate";
    }

    public static class StringConstants{
        public static final char[] delimiters = {' ','_'};
    }

    public static class TagConstants{
        public static final String DIRECT = "direct";
        public static final String TAG_DOB = "dp_dob_tags";
        public static final String TAG_DOE = "dp_doe_tags";
        public static final String TAG_DOS = "dp_dos_tags";
        public static final String TAG_ORDER_DATE = "dp_order_date_tags";
        public static final String TAG_DOD = "dp_dod_tags";
    }

}
