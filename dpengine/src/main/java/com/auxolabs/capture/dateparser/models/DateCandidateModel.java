package com.auxolabs.capture.dateparser.models;


import lombok.Data;

@Data
public class DateCandidateModel {
    private double x;
    private double y;
    private double w;
    private double h;
    private String orginalValue;
    private String normValue;
    private boolean isInsideASegment;
    private TokenModel sameTagSegment;
    private TokenModel rightTagSegment;
    private TokenModel leftTagSegment;
    private TokenModel upperTagSegment;
    private TokenModel lowerTagSegment;
    private String tagValue;
    private ProbabilityModel probability;
}
