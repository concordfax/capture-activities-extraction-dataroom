package com.auxolabs.capture.dateparser.exceptions;

public class TokenPropertyNotFoundException extends DateParserRuleException {

    public TokenPropertyNotFoundException(String property) {
        super(String.format("token property '%s' not found.", property));
    }
}
