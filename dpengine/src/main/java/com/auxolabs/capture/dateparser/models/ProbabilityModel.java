package com.auxolabs.capture.dateparser.models;

import lombok.Data;

@Data
public class ProbabilityModel {
    private float probaForDOB;
    private float probaForDischargeDate;
    private float probaForFaxDate;
}
