package com.auxolabs.capture.dateparser.exceptions;

public class DateParserInvalidFormat extends RuntimeException {
    public DateParserInvalidFormat(String message){super(message);}
}
