package com.auxolabs.capture.dateparser.exceptions;

public class DateParserRuleException extends RuntimeException {
    public DateParserRuleException(String message){super(message);}
}
