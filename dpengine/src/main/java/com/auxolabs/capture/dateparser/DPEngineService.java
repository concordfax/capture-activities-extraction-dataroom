package com.auxolabs.capture.dateparser;


import com.auxolabs.capture.dateparser.models.TokenModel;
import com.concordfax.capture.core.models.document.result.extraction.ExtractionField;

import java.util.HashMap;
import java.util.List;


public interface DPEngineService {
    HashMap<String, ExtractionField> process(List<TokenModel> dateCandidates, List<TokenModel> tagCandidates,List<String> tagDoe,List<String> tagDos,List<String> tagOrderDate, List<String> tagDod,List<String> tagDob);
}
