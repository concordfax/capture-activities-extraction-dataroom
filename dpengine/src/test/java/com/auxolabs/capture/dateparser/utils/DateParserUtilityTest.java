package com.auxolabs.capture.dateparser.utils;

import com.auxolabs.capture.dateparser.models.TokenModel;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class DateParserUtilityTest {

    private List<TokenModel> dateCandidates = new ArrayList<>();
    private TokenModel dateCandidate1 = new TokenModel();

    @Test
    public void parseAndGetSorted() {
    }

    @Test
    public void checkIfSameSegmentWithTag() {
    }

    @Test
    public void getAllInLineTags() {
    }

    @Test
    public void getAllLeftTags() {
    }

    @Test
    public void getAllRightTags() {
    }

    @Test
    public void getAllUpperTags() {
    }

    @Test
    public void getAllLowerTags() {
    }

    @Test
    public void getClosestTagFromRow() {
    }

    @Test
    public void getClosestTagFromColumn() {
    }

    @Test
    public void removeFalseDates() {
        dateCandidate1.put("normDate","12/      20");
        dateCandidates.add(dateCandidate1);
        DateParserUtility dateParserUtility = new DateParserUtility();
        assertEquals(dateCandidates,dateParserUtility.removeFalseDates(dateCandidates));

    }
}