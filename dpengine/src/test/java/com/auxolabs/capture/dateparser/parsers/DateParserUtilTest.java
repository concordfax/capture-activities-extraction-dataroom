//package com.auxolabs.capture.dateparser.parsers;
//
//import org.junit.Test;
//
//import java.io.FileNotFoundException;
//import java.util.ArrayList;
//import java.util.List;
//
//import static org.junit.Assert.*;
//
//public class DateParserUtilTest {
//
//    @Test
//    public void parseAndGetSorted() {
//        List<String> dateCandidatesInput = new ArrayList<>();
//        dateCandidatesInput.add("/05/94");
//        dateCandidatesInput.add("may 18 2000");
//        dateCandidatesInput.add("31 may 1996");
//        dateCandidatesInput.add("31/may/2019");
//        dateCandidatesInput.add("05-31-1994");
//
//        List<String> dateCandidatesOutput = new ArrayList<>();
//        dateCandidatesInput.add("05/31/1994");
//        dateCandidatesInput.add("05/31/1994");
//        dateCandidatesInput.add("05/31/1994");
//        dateCandidatesInput.add("05/31/1996");
//        dateCandidatesInput.add("05/31/2019");
//
//
//        assertEquals(dateCandidatesOutput,DateParserUtil.parseAndGetSorted(dateCandidatesInput));
//
//    }
//}