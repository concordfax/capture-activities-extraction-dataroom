package com.kaiser.eval;

import com.kaiser.exceptions.InvalidExpressionException;
import com.kaiser.models.*;
import com.kaiser.models.containsexpression.ContainsExpressionBPC;
import com.kaiser.models.containsexpression.ContainsLogicalBPC;
import com.kaiser.models.containsexpression.NotContainsBPC;
import com.kaiser.models.followsExpression.SequenceExpressionBPC;
import com.kaiser.models.relationalexpression.ExpressionUnitBPC;
import com.kaiser.models.relationalexpression.LogicalBPC;
import com.kaiser.models.relationalexpression.NegationBPC;
import com.kaiser.models.relationalexpression.RelationalExpressionBPC;

import java.util.List;

public class KaiserParserFactory {

    public static LogicalBPC getLogicalBPC(BaseParserComponent bpc1, BaseParserComponent bpc2,
                                              String logicalOperator) throws InvalidExpressionException {
        return new LogicalBPC(bpc1, bpc2, logicalOperator);
    }

    public static RelationalExpressionBPC getRelationalExpressionBPC(String property, String operator, String value) {
        return new RelationalExpressionBPC(property, operator, value);
    }

    public static ExpressionUnitBPC getExpressionUnitBPC(String tag, List<RelationalExpressionBPC> expressionCore) {
        return new ExpressionUnitBPC(tag, expressionCore);
    }

    public static NegationBPC getNegationBPC(BaseParserComponent bpc) {
        return new NegationBPC(bpc);
    }

    public static ContainsExpressionBPC getContainsExpressionBPC(List<ParserBPC> optionals) {
        return new ContainsExpressionBPC(optionals);
    }

    public static ContainsExpressionBPC getContainsExpressionBPC(ParserBPC mandatory) {
        return new ContainsExpressionBPC(mandatory);
    }

    public static ContainsExpressionBPC getContainsExpressionBPC(List<ParserBPC> optionals,
                                                                 ParserBPC mandatory) {
        return new ContainsExpressionBPC(optionals, mandatory);
    }

    public static ContainsLogicalBPC getContainsLogicalBPC(ParserBPC bpc1, ParserBPC bpc2,
                                                           String logicalOperation) {
        return new ContainsLogicalBPC(bpc1, logicalOperation, bpc2);
    }
    

    public static NotContainsBPC getNotContainsBPC(ParserBPC bpc) {
        return new NotContainsBPC(bpc);
    }

    public static BaseExpressionBPC getBaseExpressionBPC(BaseParserComponent bpc) {
        return new BaseExpressionBPC(bpc);
    }

    public static SequenceExpressionBPC getFollowsExpressionBPC(ParserBPC bpc) {
        return new SequenceExpressionBPC(bpc);
    }
}
