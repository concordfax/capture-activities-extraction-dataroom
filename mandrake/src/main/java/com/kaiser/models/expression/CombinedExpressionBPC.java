package com.kaiser.models.expression;

import com.kaiser.exceptions.InvalidExpressionException;
import com.kaiser.exceptions.UnsupportedOpException;
import com.kaiser.lex.KaiserKeyWords;
import com.kaiser.models.BaseParserComponent;
import com.kaiser.models.ParserBPC;
import com.kaiser.models.ParserValue;
import com.kaiser.models.ResultModel;
import com.kaiser.models.classification.ExpressionFamily;
import com.kaiser.models.classification.ExpressionType;
import com.kaiser.models.relationalexpression.LogicalBPC;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CombinedExpressionBPC extends ParserBPC{
    private transient BaseParserComponent bpc;
    private String combinator;
    private List<BaseParserComponent> expressions;

    public CombinedExpressionBPC(String combinator, List<BaseParserComponent> expressions) throws InvalidExpressionException {
        this.combinator = combinator;
        this.expressions = expressions;
        String logicalOperator;
        switch (combinator) {
            case KaiserKeyWords.TOKEN_COMBINED_EXPRESSION_ALL:
                logicalOperator = KaiserKeyWords.LOGICAL_AND;
                break;
            case KaiserKeyWords.TOKEN_COMBINED_EXPRESSION_ANY:
                logicalOperator = KaiserKeyWords.LOGICAL_OR;
                break;
            default:
                throw new InvalidExpressionException();
        }

        boolean firstExpression = true;
        ExpressionFamily expressionFamily;
        for(BaseParserComponent expression: expressions) {
            if(firstExpression) {
                bpc = expression;
                firstExpression = false;
                expressionFamily = bpc.getExpressionFamily();
            } else {
                if(expression.getExpressionFamily() != bpc.getExpressionFamily()) {
                    throw new InvalidExpressionException();
                }
                bpc = new LogicalBPC(bpc, expression, logicalOperator);
            }
        }
        setExpressionAttributes(ExpressionType.combinedExpression, bpc.getExpressionFamily());
    }

    @Override
    public ResultModel compute(Map<String, List<ParserValue>> parserValues) throws UnsupportedOpException {
        return bpc.compute(parserValues);
    }

    @Override
    public ResultModel compute(ParserValue parserValue) throws UnsupportedOpException {
        return bpc.compute(parserValue);
    }

    @Override
    public String generateExpression() {
        String expressionList = "" + KaiserKeyWords.TOKEN_SEPARATOR;
        for(BaseParserComponent expression: expressions) {
            expressionList = expressionList + expression.generateExpression() + KaiserKeyWords.TOKEN_SEPARATOR;
        }

        return combinator + KaiserKeyWords.TOKEN_SEPARATOR + KaiserKeyWords.SYMBOL_PARENTHESIS_OPEN +
                    expressionList + KaiserKeyWords.SYMBOL_PARENTHESIS_CLOSE;
    }

    @Override
    public boolean updateTag(String currentTag, String newTag) {
        boolean status = false;
        for(BaseParserComponent expression: expressions) {
            status = expression.updateTag(currentTag, newTag);
            if(!status) {
                return status;
            }
        }
        return status;
    }

    @Override
    public boolean updateComponent(String tag, String currentComponent, String newComponent) {
        boolean status = false;
        for(BaseParserComponent expression: expressions) {
            status = expression.updateComponent(tag, currentComponent, newComponent);
            if(!status) {
                return status;
            }
        }
        return status;
    }

    @Override
    public boolean updateValue(String tag, String component, String currentValue, String newValue) {
        boolean status = false;
        for(BaseParserComponent expression: expressions) {
            status = expression.updateValue(tag, component, currentValue, newValue);
            if(!status) {
                return status;
            }
        }
        return status;
    }

    @Override
    public boolean isInferenceReferred(long inferenceId) {
        return bpc.isInferenceReferred(inferenceId);
    }

    @Override
    public Set<String> getEntities() {
        Set<String> entities = new HashSet<>();
        if(this.getExpressionFamily() == ExpressionFamily.extractor) {
            expressions.forEach(expression -> {
                ParserBPC extractor = (ParserBPC) expression;
                entities.addAll(extractor.getEntities());
            });
        }
        return entities;
    }
}
