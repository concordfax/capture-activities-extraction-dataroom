package com.kaiser.models;

import com.kaiser.lex.KaiserKeyWords;
import com.kaiser.models.classification.ExtractorType;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by nirmal on 3/27/17.
 */
public abstract class ParserBPC extends BaseParserComponent {
    private transient ExtractorType extractorType;
    private transient Set<String> referredSequences = new HashSet<>();
    private boolean optional = false;
    private boolean oneOrMore = false;
    private boolean zeroOrMore = false;
    private boolean forbidden = false;

    public boolean isForbidden() {
        return forbidden;
    }

    public void setForbidden(boolean forbidden) {
        this.forbidden = forbidden;
        if (forbidden) this.optional = true;
    }

    public boolean isOptional() {
        return optional;
    }

    public void setOptional(boolean optional) {
        this.optional = optional;
    }

    public boolean isOneOrMore() {
        return oneOrMore;
    }

    public void setOneOrMore(boolean oneOrMore) {
        this.oneOrMore = oneOrMore;
    }

    public boolean isZeroOrMore() {
        return zeroOrMore;
    }

    public void setZeroOrMore(boolean zeroOrMore) {
        this.zeroOrMore = zeroOrMore;
    }

    public void setOccurrence(String occurrence) {
        switch (occurrence) {
            case KaiserKeyWords.TOKEN_ONE_OR_MORE:
                this.setOneOrMore(true);
                break;
            case KaiserKeyWords.TOKEN_ZERO_OR_MORE:
                this.setZeroOrMore(true);
                break;
            case KaiserKeyWords.TOKEN_ZERO_OR_ONE:
                this.setOptional(true);
                break;
            case KaiserKeyWords.TOKEN_NONE:
                this.setForbidden(true);
                break;
        }
    }

    public abstract Set<String> getEntities();

    public ExtractorType getExtractorType() {
        return extractorType;
    }

    public void setExtractorType(ExtractorType extractorType) {
        this.extractorType = extractorType;
    }

    public Set<String> getReferredSequences() {
        return referredSequences;
    }

    public void setReferredSequences(Set<String> referredSequences) {
        this.referredSequences = referredSequences;
    }
}
