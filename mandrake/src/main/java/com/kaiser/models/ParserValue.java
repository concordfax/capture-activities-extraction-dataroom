package com.kaiser.models;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class ParserValue extends HashMap<String, Object> {
    Logger logger = LoggerFactory.getLogger(ParserValue.class);
    private Set<ResultAnnotation> triggerPhrases = new HashSet<>();
    private BaseParserComponent triggeredRule;

    public String getStringValue(String property) {
        Object value = this.get(property);
        if(value == null) {
            return null;
        } else if (value instanceof String) {
            return (String) value;
        } else if (value instanceof ValueModel) {
            ValueModel valueModel = (ValueModel) value;
            return valueModel.getDisplayText();
        } else {
            logger.info("Can't get String value:: Unsupported value type");
        }

        return null;
    }

    public Object get(Object property) {
        return super.get(property);
    }

    public Object put(String property, Object value) {
        return super.put(property, value);
    }

    public Set<ResultAnnotation> getTriggerPhrases() {
        return this.triggerPhrases;
    }

    public void setTriggerPhrases(Set<ResultAnnotation> triggerPhrases) {
        this.triggerPhrases = triggerPhrases;
    }

    public void addTriggerPhrase(ResultAnnotation triggerPhrase) {
        this.triggerPhrases.add(triggerPhrase);
    }

    public BaseParserComponent getTriggeredRule() {
        return triggeredRule;
    }

    public void setTriggeredRule(BaseParserComponent triggeredRule) {
        this.triggeredRule = triggeredRule;
    }

    @Override
    public String toString() {
        String pv = "";
        for(Map.Entry<String, Object> entry: this.entrySet()) {
            Object value = entry.getValue();
            String textValue = "";
            if (value instanceof String) {
                textValue = (String) value;
            } else if (value instanceof ValueModel) {
                ValueModel valueModel = (ValueModel) value;
                textValue = valueModel.getText();
            }
            pv = pv + entry.getKey() + "-" + textValue + "\n";
        }
        return pv;
    }
}
