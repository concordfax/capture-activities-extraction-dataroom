package com.kaiser.models.classification;

/**
 * Created by nirmal on 3/27/17.
 */
public enum  ExpressionFamily {
    evaluator, // Expressions to evaluate the extracted information
    extractor, // Sequence/Contains expressions to extract information
    ;
}
