package com.kaiser.models.classification;

/**
 * Type of the extractor
 */
public enum ExtractorType {
    sequenceOnly,
    containsOnly,
    sequenceAndContains,
}
