package com.kaiser.models.classification;

/**
 * Created by nirmal on 3/27/17.
 */
public enum ExpressionType {
    relationalExpression,
    binaryLogicalExpression,
    negationExpression,
    relationalExpressionUnit,
    containsExpression,
    containsEntityCore,
    containsLogical,
    notContains,
    containsEntity,
    sequence,
    sequenceExpressionCore,
    sequenceExpressionUnit,
    extractorReference,
    evaluatorReference,
    combinedExpression,
    ;
}
