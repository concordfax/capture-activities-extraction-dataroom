package com.kaiser.models;

import lombok.Data;

import java.util.Map;

@Data
public class ResultAnnotation {
    private int sentenceIndex;
    private String text;
    private int startIndex;
    private int endIndex;
    private String tag;
    private Map<String, String> features;

    public ResultAnnotation(int sentenceIndex, String text, int startIndex, int endIndex,
                            String tag, Map<String, String> features) {
        this.sentenceIndex = sentenceIndex;
        this.text = text;
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.tag = tag;
        this.features = features;
    }
}
