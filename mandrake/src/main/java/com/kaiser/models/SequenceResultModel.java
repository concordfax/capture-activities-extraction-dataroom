package com.kaiser.models;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by nirmal on 3/26/17.
 */
public class SequenceResultModel {
    private ParserValue matchingComponent;
    private int lastMatchingOffset;
    private int firstMatchingOffset;
    private int sentenceIndex;
    private Set<ResultAnnotation> resultAnnotations = new HashSet<>();

    public SequenceResultModel(ParserValue matchingComponent, int lastMatchingOffset,
                               int firstMatchingOffset, int sentenceIndex, Set<ResultAnnotation> resultAnnotations) {
        this.matchingComponent = matchingComponent;
        this.lastMatchingOffset = lastMatchingOffset;
        this.firstMatchingOffset = firstMatchingOffset;
        this.resultAnnotations = resultAnnotations;
        this.sentenceIndex = sentenceIndex;
    }

    public ParserValue getMatchingComponent() {
        return matchingComponent;
    }

    public void setMatchingComponent(ParserValue matchingComponent) {
        this.matchingComponent = matchingComponent;
    }

    public int getLastMatchingOffset() {
        return lastMatchingOffset;
    }

    public void setLastMatchingOffset(int lastMatchingOffset) {
        this.lastMatchingOffset = lastMatchingOffset;
    }

    public int getFirstMatchingOffset() {
        return firstMatchingOffset;
    }

    public void setFirstMatchingOffset(int firstMatchingOffset) {
        this.firstMatchingOffset = firstMatchingOffset;
    }

    public Set<ResultAnnotation> getResultAnnotations() {
        return resultAnnotations;
    }

    public void setResultAnnotations(Set<ResultAnnotation> resultAnnotations) {
        this.resultAnnotations = resultAnnotations;
    }

    public int getSentenceIndex() {
        return sentenceIndex;
    }

    public void setSentenceIndex(int sentenceIndex) {
        this.sentenceIndex = sentenceIndex;
    }

    @Override
    public boolean equals(Object obj) {
        if(getClass() != obj.getClass()) {
            return false;
        } else {
            SequenceResultModel other = (SequenceResultModel) obj;
            if(this.firstMatchingOffset == other.firstMatchingOffset &&
                    this.lastMatchingOffset == other.lastMatchingOffset &&
                    this.resultAnnotations.size() == other.resultAnnotations.size()) {
                return true;
            } else {
                return false;
            }
        }
    }

    @Override
    public int hashCode() {
        return this.firstMatchingOffset + this.resultAnnotations.size() + this.lastMatchingOffset;
    }
}
