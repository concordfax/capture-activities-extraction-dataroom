package com.kaiser.models.followsExpression;

import com.kaiser.exceptions.UnsupportedOpException;
import com.kaiser.models.BaseParserComponent;
import com.kaiser.models.ParserValue;
import com.kaiser.models.ResultModel;

import java.util.List;
import java.util.Map;

/**
 * Created by nirmal on 6/2/17.
 */
public class MetaPropertyBPC extends BaseParserComponent {
    private String property;
    private String operator;
    private Object value;

    public MetaPropertyBPC(String property, String operator, Object value) {
        this.property = property;
        this.operator = operator;
        this.value = value;
    }

    @Override
    public ResultModel compute(Map<String, List<ParserValue>> parserValues) throws UnsupportedOpException {
        throw new UnsupportedOpException();
    }

    @Override
    public ResultModel compute(ParserValue parserValue) throws UnsupportedOpException {
        return null;
    }

    @Override
    public String generateExpression() {
        return null;
    }

    @Override
    public boolean updateTag(String currentTag, String newTag) {
        return false;
    }

    @Override
    public boolean updateComponent(String tag, String currentComponent, String newComponent) {
        return false;
    }

    @Override
    public boolean updateValue(String tag, String component, String currentValue, String newValue) {
        return false;
    }

    @Override
    public boolean isInferenceReferred(long inferenceId) {
        return false;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
