package com.kaiser.models.followsExpression.helpers;

import com.kaiser.models.*;

import java.util.*;

/**
 * Created by nirmal on 3/27/17.
 */
public class FollowsExpressionHelper {

    public static ResultModel getSequenceResult(ResultModel result1, ResultModel result2,
                                                boolean isOptional1, boolean isOptional2, int offsetWindowSize,
                                                boolean isParser1Ref, boolean isParser2Ref,
                                                String key1, String key2) {
        Set<SequenceResultModel> sequence1 = result1.getSequenceResults();
        Set<SequenceResultModel> sequence2 = result2.getSequenceResults();
        Set<SequenceResultModel> sequenceResults = new HashSet<>();

        for (SequenceResultModel property1 : sequence1) {
            Set<ResultAnnotation> resultAnnotations = new HashSet<>();
            Set<SequenceResultModel> tempSeqResults = new HashSet<>();
            for (SequenceResultModel property2 : sequence2) {
                if (property2.getFirstMatchingOffset() > property1.getLastMatchingOffset() &&
                        (property2.getFirstMatchingOffset() - property1.getLastMatchingOffset()) <= offsetWindowSize) {
                    ParserValue matchingComponents = new ParserValue();
                    resultAnnotations = new HashSet<>();

                    if (isParser1Ref) {
                        matchingComponents.put(key1, property1.getMatchingComponent());
                    } else {
                        matchingComponents.putAll(property1.getMatchingComponent());
                    }

                    if (isParser2Ref) {
                        matchingComponents.put(key2, property2.getMatchingComponent());
                    } else {
                        matchingComponents.putAll(property2.getMatchingComponent());
                    }

                    resultAnnotations.addAll(property1.getResultAnnotations());
                    resultAnnotations.addAll(property2.getResultAnnotations());

                    SequenceResultModel sequenceResult = new SequenceResultModel(matchingComponents,
                            property2.getLastMatchingOffset(), property1.getFirstMatchingOffset(),
                            property1.getSentenceIndex(), resultAnnotations);
                    tempSeqResults.add(sequenceResult);
                } else if (isOptional1) {
                    ParserValue matchingComponents = new ParserValue();
                    resultAnnotations = new HashSet<>();

                    if (isParser2Ref) {
                        matchingComponents.put(key2, property2.getMatchingComponent());
                    } else {
                        matchingComponents.putAll(property2.getMatchingComponent());
                    }
                    resultAnnotations.addAll(property2.getResultAnnotations());

                    SequenceResultModel sequenceResult = new SequenceResultModel(matchingComponents,
                            property2.getLastMatchingOffset(), property1.getFirstMatchingOffset(),
                            property1.getSentenceIndex(), resultAnnotations);
                    tempSeqResults.add(sequenceResult);
                }
            }
            if (tempSeqResults.size() > 0) {
                sequenceResults.addAll(tempSeqResults);
            } else if (isOptional2) {
                sequenceResults.addAll(sequence1);
            }
        }

        if (sequenceResults.size() > 0 ) {
            ResultModel resultModel = new ResultModel(BaseParserComponent.Result.YES);
            resultModel.setSequenceResults(sequenceResults);
            return resultModel;
        } else if (isOptional1 && isOptional2) {
            return new ResultModel(BaseParserComponent.Result.NA);
        } else if (isOptional1) {
            return result2;
        } else if (isOptional2) {
            return result1;
        } else {
            return new ResultModel(BaseParserComponent.Result.NO);
        }
    }

}
