package com.kaiser.models.followsExpression;

import com.kaiser.exceptions.UnsupportedOpException;
import com.kaiser.lex.KaiserKeyWords;
import com.kaiser.models.*;
import com.kaiser.models.classification.ExpressionFamily;
import com.kaiser.models.classification.ExpressionType;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by nirmal on 3/26/17.
 */
public class ContainsEntityBPC extends ParserBPC {
    private String key;
    private String visibleKey;
    private transient String emitKey;
    private List<MetaPropertyBPC> metaProperties;

    public ContainsEntityBPC(String key, String visibleKey, List<MetaPropertyBPC> metaProperties) {
        this.key = key;
        this.visibleKey = visibleKey;
        if(visibleKey == null) {
            this.emitKey = key;
        } else {
            this.emitKey = visibleKey;
        }
        this.metaProperties = metaProperties;
        setExpressionAttributes(ExpressionType.containsEntity, ExpressionFamily.extractor);
    }

    public ContainsEntityBPC(List<MetaPropertyBPC> metaProperties) {
        this.metaProperties = metaProperties;
    }

    @Override
    public ResultModel compute(Map<String, List<ParserValue>> parserValues) throws UnsupportedOpException {
        return null;
    }

    @Override
    public ResultModel compute(ParserValue parserValue) throws UnsupportedOpException {
        Set<SequenceResultModel> sequenceResults = new HashSet<>();

        if(key != null) {
            sequenceResults.addAll(evaluateKey(parserValue));
            if(sequenceResults.size() <= 0) {
                if(this.isForbidden()){
                    return new ResultModel(Result.YES);
                }
                return new ResultModel(Result.NO);
            }
        }
        if(metaProperties != null) {
            Set<SequenceResultModel> finalSequenceResults = sequenceResults;
            parserValue.forEach((k, v) -> {
                if(v instanceof  ValueModel) {
                    ValueModel value = (ValueModel)v;
                    if(evaluateMetaProperties(value)) {
                        ParserValue matchingComponents = new ParserValue();
                        matchingComponents.put(emitKey, value);
                        SequenceResultModel sequenceResult = new SequenceResultModel(matchingComponents,
                                value.getEndIndex(), value.getStartIndex(), value.getSentenceIndex(),
                                value.getTriggerPhrases());
                        finalSequenceResults.add(sequenceResult);
                    }
                }
            });
        }
        if(this.isForbidden()){
            if(sequenceResults.size() > 0) {
                return new ResultModel(Result.FORBIDDEN);
            }else {
                return new ResultModel(Result.YES);
            }

        }
        if(sequenceResults.size() > 0) {
            ResultModel resultModel = new ResultModel(Result.YES);
            resultModel.setSequenceResults(sequenceResults);
            return resultModel;
        } else {
            return new ResultModel(Result.NO);
        }
    }

    private List<SequenceResultModel> evaluateKey(ParserValue parserValue) {
        List<SequenceResultModel> sequenceResults = new ArrayList<>();

        Object value = parserValue.get(key);
        if(value != null) {
            if (value instanceof ValueModel) {
                ValueModel valueModel = (ValueModel) value;
                ParserValue matchingComponents = new ParserValue();
                matchingComponents.put(emitKey, value);

                SequenceResultModel sequenceResult = new SequenceResultModel(matchingComponents,
                        valueModel.getStartIndex(), valueModel.getStartIndex(), valueModel.getSentenceIndex(),
                        valueModel.getTriggerPhrases());
                sequenceResults.add(sequenceResult);
            } else {
                ArrayList<ValueModel> valueModels = (ArrayList<ValueModel>) value;
                valueModels.forEach(keyPhraseModel -> {
                    ParserValue matchingComponents = new ParserValue();
                    matchingComponents.put(emitKey, keyPhraseModel);
                    SequenceResultModel sequenceResult = new SequenceResultModel(matchingComponents,
                            keyPhraseModel.getEndIndex(), keyPhraseModel.getStartIndex(), keyPhraseModel.getSentenceIndex(),
                            keyPhraseModel.getTriggerPhrases());
                    sequenceResults.add(sequenceResult);
                });
            }
        }

        return sequenceResults;
    }

    private boolean evaluateMetaProperties(ValueModel value) {
        Map<String, Object> properties = value.getProperties();
        AtomicBoolean result = new AtomicBoolean(true);

        boolean failure = metaProperties.stream().anyMatch(metaProperty ->
            !properties.containsKey(metaProperty.getProperty()) ||
            !evaluate(metaProperty.getOperator(), metaProperty.getValue(), properties.get(metaProperty.getProperty()))
        );

        return !failure;
    }

    private boolean evaluate(String testOp, Object testValue, Object value) {
        switch (testOp) {
            case KaiserKeyWords.OPERATOR_EQUALS:
                return testValue.toString().equalsIgnoreCase(value.toString());
            case KaiserKeyWords.OPERATOR_NOT_EQUALS:
                return !testValue.toString().equalsIgnoreCase(value.toString());
            case KaiserKeyWords.OPERATOR_GREATER_THAN:
                return testValue.toString().compareToIgnoreCase(value.toString()) < 0;
            case KaiserKeyWords.OPERATOR_LESS_THAN:
                return testValue.toString().compareToIgnoreCase(value.toString()) > 0;
            case KaiserKeyWords.OPERATOR_GREATER_THAN_OR_EQUALS:
                return testValue.toString().compareToIgnoreCase(value.toString()) <= 0;
            case KaiserKeyWords.OPERATOR_LESS_THAN_OR_EQUALS:
                return testValue.toString().compareToIgnoreCase(value.toString()) >= 0;
            case KaiserKeyWords.OPERATOR_IS_ANY_OF:
                List<String> testValues = (ArrayList) testValue;
                return testValues.stream().anyMatch(value.toString()::equalsIgnoreCase);
            default:
                return false;
        }

    }

    @Override
    public String generateExpression() {
        String visiblePart = "";
        if(visibleKey != null) {
            visiblePart = KaiserKeyWords.TOKEN_SEPARATOR + KaiserKeyWords.TOKEN_ALIAS + KaiserKeyWords.TOKEN_SEPARATOR +
                        KaiserKeyWords.DOUBLE_QUOTE + visibleKey + KaiserKeyWords.DOUBLE_QUOTE;
        }

        return KaiserKeyWords.SYMBOL_BRACE_OPEN +
                    KaiserKeyWords.DOUBLE_QUOTE + key + KaiserKeyWords.DOUBLE_QUOTE +
                    visiblePart +
                KaiserKeyWords.SYMBOL_BRACE_CLOSE;
    }

    @Override
    public boolean updateTag(String currentTag, String newTag) {
        return false;
    }

    @Override
    public boolean updateComponent(String tag, String currentComponent, String newComponent) {
        return false;
    }

    @Override
    public boolean updateValue(String tag, String component, String currentValue, String newValue) {
        return false;
    }

    @Override
    public boolean isInferenceReferred(long inferenceId) {
        return false;
    }

    @Override
    public Set<String> getEntities() {
        Set<String> entities = new HashSet<>();
        entities.add(key);
        return entities;
    }
}
