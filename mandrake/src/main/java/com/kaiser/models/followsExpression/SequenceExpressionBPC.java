package com.kaiser.models.followsExpression;

import com.kaiser.exceptions.UnsupportedOpException;
import com.kaiser.lex.KaiserKeyWords;
import com.kaiser.models.BaseParserComponent;
import com.kaiser.models.ParserBPC;
import com.kaiser.models.ParserValue;
import com.kaiser.models.ResultModel;
import com.kaiser.models.classification.ExpressionFamily;
import com.kaiser.models.classification.ExpressionType;
import com.kaiser.models.classification.ExtractorType;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class SequenceExpressionBPC extends ParserBPC {
    private ParserBPC sequence;

    public SequenceExpressionBPC(ParserBPC sequence) {
        this.sequence = sequence;
        setExpressionAttributes(ExpressionType.sequence, ExpressionFamily.extractor);
        this.setExtractorType(ExtractorType.sequenceOnly);
        this.setReferredSequences(sequence.getReferredSequences());
    }

    @Override
    public ResultModel compute(Map<String, List<ParserValue>> parserValues) throws UnsupportedOpException {
        return sequence.compute(parserValues);
    }

    @Override
    public ResultModel compute(ParserValue parserValue) throws UnsupportedOpException {
        return sequence.compute(parserValue);
    }

    @Override
    public String generateExpression() {
        return KaiserKeyWords.TOKEN_SEQUENCE + KaiserKeyWords.TOKEN_SEPARATOR +
                    KaiserKeyWords.SYMBOL_SQUARE_BRACKET_OPEN + KaiserKeyWords.TOKEN_SEPARATOR +
                        sequence.generateExpression() + KaiserKeyWords.TOKEN_SEPARATOR +
                    KaiserKeyWords.SYMBOL_SQUARE_BRACKET_CLOSE;
    }

    @Override
    public boolean updateTag(String currentTag, String newTag) {
        return sequence.updateTag(currentTag, newTag);
    }

    @Override
    public boolean updateComponent(String tag, String currentComponent, String newComponent) {
        return sequence.updateComponent(tag, currentComponent, newComponent);
    }

    @Override
    public boolean updateValue(String tag, String component, String currentValue, String newValue) {
        return false;
    }

    @Override
    public boolean isInferenceReferred(long inferenceId) {
        return false;
    }

    @Override
    public Set<String> getEntities() {
        return sequence.getEntities();
    }
}
