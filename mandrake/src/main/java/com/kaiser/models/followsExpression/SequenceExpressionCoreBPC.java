package com.kaiser.models.followsExpression;

import com.kaiser.exceptions.UnsupportedOpException;
import com.kaiser.lex.KaiserKeyWords;
import com.kaiser.models.*;
import com.kaiser.models.classification.ExpressionFamily;
import com.kaiser.models.classification.ExpressionType;
import com.kaiser.models.classification.ExtractorType;
import com.kaiser.models.followsExpression.helpers.FollowsExpressionHelper;
import com.kaiser.models.reference.ParserReferenceBPC;

import java.util.*;

public class SequenceExpressionCoreBPC extends ParserBPC {
    private ParserBPC component1;
    private ParserBPC component2;
    private int offsetWindowSize;
    private transient boolean isparser1Ref = false;
    private transient boolean isparser2Ref = false;
    private transient String ref1Name = "";
    private transient String ref2Name = "";

    public SequenceExpressionCoreBPC(ParserBPC component1, int offsetWindowSize, ParserBPC component2) {
        this.component1 = component1;
        this.component2 = component2;
        this.offsetWindowSize = offsetWindowSize;
        setExpressionAttributes(ExpressionType.sequenceExpressionCore, ExpressionFamily.extractor);
        this.setExtractorType(ExtractorType.sequenceOnly);
        Set<String> referredSequences = new HashSet<>();
        if(component1.getExpressionType() == ExpressionType.extractorReference) {
            referredSequences.addAll(component1.getReferredSequences());
            isparser1Ref = true;
            ParserReferenceBPC bpc = (ParserReferenceBPC) component1;
            ref1Name = bpc.getVisibleKey();
        }
        if(component2.getExpressionType() == ExpressionType.extractorReference) {
            referredSequences.addAll(component2.getReferredSequences());
            isparser2Ref = true;
            ParserReferenceBPC bpc = (ParserReferenceBPC) component2;
            ref2Name = bpc.getVisibleKey();
        }
        setReferredSequences(referredSequences);
    }

    public ResultModel compute(Map<String, List<ParserValue>> parserValues) {
        return null;
    }

    public ResultModel compute(ParserValue parserValue) throws UnsupportedOpException {
        ResultModel result1 = component1.compute(parserValue);
        ResultModel result2 = component2.compute(parserValue);
        boolean isComponent1Present = true;
        boolean isComponent2Present = true;

        if(result1.getResult() != Result.YES) {
            if (component1.isOptional()) {
                isComponent1Present = false;
            } else {
                return new ResultModel(Result.NO);
            }
        }

        if(result2.getResult() != Result.YES) {
            if (component2.isOptional() && result2.getResult() != Result.FORBIDDEN) {
                isComponent2Present = false;
            } else {
                return new ResultModel(Result.NO);
            }
        }

        if(isComponent1Present) {
            if(isComponent2Present) {
                return FollowsExpressionHelper.getSequenceResult(result1, result2, component1.isOptional(),
                        component2.isOptional(), offsetWindowSize, isparser1Ref, isparser2Ref, ref1Name, ref2Name);
            } else {
                return result1;
            }
        } else if (isComponent2Present){
            return result2;
        } else {
            return new ResultModel(Result.NA);
        }
    }

    public String generateExpression() {
        String follows = KaiserKeyWords.SYMBOL_FOLLOWS;
        if(offsetWindowSize != 1) {
            follows = follows +
                    KaiserKeyWords.SYMBOL_PARENTHESIS_OPEN +
                        KaiserKeyWords.DOUBLE_QUOTE + offsetWindowSize + KaiserKeyWords.DOUBLE_QUOTE +
                    KaiserKeyWords.SYMBOL_PARENTHESIS_CLOSE;
        }

        String expression1 = component1.generateExpression();
        if(component1.isOptional()) {
            expression1 = KaiserKeyWords.TOKEN_OPTIONAL + KaiserKeyWords.TOKEN_SEPARATOR + expression1;
        } else if (component1.isOneOrMore()) {
            expression1 = expression1 + KaiserKeyWords.TOKEN_SEPARATOR + "+";
        }

        String expression2 = component2.generateExpression();
        if(component2.isOptional()) {
            expression2 = KaiserKeyWords.TOKEN_OPTIONAL + KaiserKeyWords.TOKEN_SEPARATOR + expression2;
        } else if (component2.isOneOrMore()) {
            expression2 = expression2 + KaiserKeyWords.TOKEN_SEPARATOR + "+";
        }

        return expression1 + KaiserKeyWords.TOKEN_SEPARATOR +
                    follows +
                KaiserKeyWords.TOKEN_SEPARATOR + expression2;
    }

    @Override
    public boolean updateTag(String currentTag, String newTag) {
        return false;
    }

    @Override
    public boolean updateComponent(String tag, String currentComponent, String newComponent) {
        return false;
    }

    @Override
    public boolean updateValue(String tag, String component, String currentValue, String newValue) {
        return false;
    }

    @Override
    public boolean isInferenceReferred(long inferenceId) {
        return false;
    }

    @Override
    public Set<String> getEntities() {
        Set<String> entities = component1.getEntities();
        entities.addAll(component2.getEntities());
        return entities;
    }
}
