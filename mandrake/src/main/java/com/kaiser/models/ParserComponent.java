package com.kaiser.models;

import com.kaiser.exceptions.UnsupportedOpException;

import java.util.List;
import java.util.Map;

/**
 * A base parser component that can be emitted by post processor of Lex
 */
public class ParserComponent extends BaseParserComponent {

    @Override
    public ResultModel compute(Map<String, List<ParserValue>> parserValues) throws UnsupportedOpException {
        return null;
    }

    @Override
    public ResultModel compute(ParserValue parserValue) throws UnsupportedOpException {
        return null;
    }

    @Override
    public String generateExpression() {
        return null;
    }

    @Override
    public boolean updateTag(String currentTag, String newTag) {
        return false;
    }

    @Override
    public boolean updateComponent(String tag, String currentComponent, String newComponent) {
        return false;
    }

    @Override
    public boolean updateValue(String tag, String component, String currentValue, String newValue) {
        return false;
    }

    @Override
    public boolean isInferenceReferred(long inferenceId) {
        return false;
    }
}
