package com.kaiser.models;

import java.util.*;

public class ResultModel {
    private BaseParserComponent.Result result;
    private Set<ResultAnnotation> triggerPhrases = new HashSet<>();
    private transient boolean notContains = false;
    private transient Set<SequenceResultModel> sequenceResults = new HashSet<>();
    private transient List<ParserValue> matchingComponents = new ArrayList<>();

    public ResultModel(BaseParserComponent.Result result) {
        this.result = result;
    }

    public ResultModel(BaseParserComponent.Result result, Set<ResultAnnotation> triggerPhrases) {
        this.result = result;
        this.triggerPhrases = triggerPhrases;
    }

    public ResultModel(BaseParserComponent.Result result, ParserValue matchingComponents,
                       Set<ResultAnnotation> triggerPhrases) {
        this.result = result;
        this.matchingComponents.add(0, matchingComponents);
        this.triggerPhrases = triggerPhrases;
    }

    public BaseParserComponent.Result getResult() {
        return result;
    }

    public void setResult(BaseParserComponent.Result result) {
        this.result = result;
    }

    public Set<ResultAnnotation> getTriggerPhrases() {
        return triggerPhrases;
    }

    public void setTriggerPhrases(Set<ResultAnnotation> triggerPhrases) {
        this.triggerPhrases = triggerPhrases;
    }

    public void addTriggerPhrases(Set<ResultAnnotation> triggerPhrases) {
        this.triggerPhrases.addAll(triggerPhrases);
    }

    public ParserValue getTopMatchingComponents() {
        if(matchingComponents.size() <=0 ) {
            return new ParserValue();
        }
        return matchingComponents.get(0);
    }

    public void addMatchingComponent(String component, Object value) {
        ParserValue existingMatch;
        if(matchingComponents.size() <= 0) {
            existingMatch = new ParserValue();
            existingMatch.put(component, value);
            matchingComponents.add(existingMatch);
        } else {
            existingMatch = matchingComponents.get(0);
            existingMatch.put(component, value);
            matchingComponents.set(0, existingMatch);
        }
    }

    public void addMatchingComponent(ParserValue components) {
        ParserValue existingMatch;
        if(matchingComponents.size() <= 0) {
            existingMatch = new ParserValue();
            existingMatch.putAll(components);
            matchingComponents.add(existingMatch);
        } else {
            existingMatch = matchingComponents.get(0);
            existingMatch.putAll(components);
            matchingComponents.set(0, existingMatch);

        }
    }

    public List<ParserValue> getMatchingComponents() {
        return matchingComponents;
    }

    public void setMatchingComponents(List<ParserValue> matchingComponents) {
        this.matchingComponents = matchingComponents;
    }

    public Set<SequenceResultModel> getSequenceResults() {
        return sequenceResults;
    }

    public void setSequenceResults(Set<SequenceResultModel> sequenceResults) {
        this.sequenceResults = sequenceResults;
    }

    public boolean isNotContains() {
        return notContains;
    }

    public void setNotContains(boolean notContains) {
        this.notContains = notContains;
    }

    @Override
    public String toString() {
        return "" + result ;
    }
}
