package com.kaiser.models;

import com.kaiser.exceptions.UnsupportedOpException;
import com.kaiser.models.classification.ExpressionFamily;
import com.kaiser.models.classification.ExpressionType;
import com.kaiser.models.relationalexpression.ExpressionUnitBPC;
import com.kaiser.models.relationalexpression.LogicalBPC;
import com.kaiser.models.relationalexpression.NegationBPC;
import com.kaiser.models.relationalexpression.RelationalExpressionBPC;

import java.util.*;

/**
 * Defines a parser instance that can be understood by an atomic expression on Yacc
 */
public abstract class BaseParserComponent implements Cloneable{
    private ExpressionType expressionType;

    private ExpressionFamily expressionFamily;

    private ResultModel result;

    public abstract ResultModel compute(Map<String, List<ParserValue>> parserValues) throws UnsupportedOpException;

    public abstract ResultModel compute(ParserValue parserValue) throws UnsupportedOpException;

    public abstract String generateExpression();

    public abstract boolean updateTag(String currentTag, String newTag);

    public abstract boolean updateComponent(String tag, String currentComponent, String newComponent);

    public abstract boolean updateValue(String tag, String component, String currentValue, String newValue);

    public abstract boolean isInferenceReferred(long inferenceId);

    public ResultModel getResult() {
        return result;
    }

    public void setResult(ResultModel result) {
        this.result = result;
    }

    public enum Result {
        YES,
        NO,
        NA,
        FORBIDDEN
    }

    protected Result logicalAnd(Result result1, Result result2) {
        if(result1 == result2) {
            if(result1 == Result.NA) {
                return Result.NA;
            } else if (result1 == Result.YES) {
                return Result.YES;
            } else {
                return Result.NO;
            }
        } else if (result1 == Result.NA || result2 == Result.NA) {
            return Result.NA;
        } else {
            return Result.NO;
        }
    }

    protected Result logicalOr(Result result1, Result result2) {
        if(result1 == result2) {
            if(result1 == Result.NA) {
                return Result.NA;
            } else if (result1 == Result.NO) {
                return Result.NO;
            } else {
                return Result.YES;
            }
        } else if (result1 == Result.YES || result2 == Result.YES) {
            return Result.YES;
        } else {
            return Result.NO;
        }
    }

    protected Result negate(Result result) {
        switch (result) {
            case YES:
                return Result.NO;
            case NO:
                return Result.YES;
            default:
                return Result.NA;
        }
    }

    public void setExpressionAttributes(ExpressionType type, ExpressionFamily family) {
        this.expressionType = type;
        this.expressionFamily = family;
    }

    public ExpressionType getExpressionType() {
        return expressionType;
    }

    public ExpressionFamily getExpressionFamily() {
        return expressionFamily;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
