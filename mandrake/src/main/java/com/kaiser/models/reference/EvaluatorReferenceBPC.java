package com.kaiser.models.reference;

import com.auxolabs.capture.mandrake.models.RuleParserModel;
import com.auxolabs.capture.mandrake.utils.RuleProcessorFactory;
import com.kaiser.exceptions.InvalidExpressionException;
import com.kaiser.exceptions.UnsupportedOpException;
import com.kaiser.lex.KaiserKeyWords;
import com.kaiser.models.*;
import com.kaiser.models.classification.ExpressionFamily;
import com.kaiser.models.classification.ExpressionType;

import java.util.List;
import java.util.Map;

/**
 * Created by nirmal on 3/27/17.
 */
public class EvaluatorReferenceBPC extends BaseParserComponent {
    private transient BaseParserComponent bpc;

    private String referredExpressionName;

    public EvaluatorReferenceBPC(String expressionName, RuleProcessorFactory ruleProcessorFactory) throws InvalidExpressionException {
        this.referredExpressionName = expressionName;
        if(!ruleProcessorFactory.getRules().containsKey(referredExpressionName)){
            throw new RuntimeException(String.format("rule with name %s not found", referredExpressionName));
        }
        RuleParserModel ruleModel = ruleProcessorFactory.getRules().get(referredExpressionName);
        if(ruleModel.getBpc()==null)
            ruleProcessorFactory.generateAST(ruleModel);
        this.bpc = ruleModel.getBpc();
        setExpressionAttributes(ExpressionType.evaluatorReference, ExpressionFamily.evaluator);
    }

    public String getReferredExpressionName() {
        return referredExpressionName;
    }

    public void setReferredExpressionName(String referredExpressionName) {
        this.referredExpressionName = referredExpressionName;
    }

    public BaseParserComponent getBpc() {
        return bpc;
    }

    public void setBpc(BaseParserComponent bpc) {
        this.bpc = bpc;
    }

    @Override
    public ResultModel compute(Map<String, List<ParserValue>> parserValues) throws UnsupportedOpException {
        ResultModel result = new ResultModel(Result.NA);
        if(bpc != null) {
            result = this.bpc.compute(parserValues);
        }

        this.setResult(result);
        return result;
    }

    @Override
    public ResultModel compute(ParserValue parserValue) throws UnsupportedOpException {
        ResultModel result = new ResultModel(Result.NA);
        if(bpc != null) {
            result = this.bpc.compute(parserValue);
        }

        this.setResult(result);
        return result;
    }

    @Override
    public String generateExpression() {
        return KaiserKeyWords.TOKEN_CONDITION + KaiserKeyWords.TOKEN_SEPARATOR + KaiserKeyWords.SYMBOL_DOUBLE_SQUARE_OPEN +
                KaiserKeyWords.TOKEN_SEPARATOR + this.referredExpressionName + KaiserKeyWords.TOKEN_SEPARATOR +
                KaiserKeyWords.SYMBOL_DOUBLE_SQUARE_CLOSE;
    }

    @Override
    public boolean updateTag(String currentTag, String newTag) {
        return this.bpc.updateTag(currentTag, newTag);
    }

    @Override
    public boolean updateComponent(String tag, String currentComponent, String newComponent) {
        return this.bpc.updateComponent(tag, currentComponent, newComponent);
    }

    @Override
    public boolean updateValue(String tag, String component, String currentValue, String newValue) {
        return this.bpc.updateValue(tag, component, currentValue, newValue);
    }

    @Override
    public boolean isInferenceReferred(long inferenceId) {
        return referredExpressionName.equals(inferenceId);
    }

    @Override
    public String toString() {
        return this.bpc.toString();
    }

}
