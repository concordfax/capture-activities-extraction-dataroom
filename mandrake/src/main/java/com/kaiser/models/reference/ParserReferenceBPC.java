package com.kaiser.models.reference;

import com.auxolabs.capture.mandrake.models.RuleParserModel;
import com.auxolabs.capture.mandrake.utils.RuleProcessorFactory;
import com.kaiser.exceptions.InvalidExpressionException;
import com.kaiser.exceptions.UnsupportedOpException;
import com.kaiser.lex.KaiserKeyWords;
import com.kaiser.models.ParserBPC;
import com.kaiser.models.ParserValue;
import com.kaiser.models.ResultModel;
import com.kaiser.models.classification.ExpressionFamily;
import com.kaiser.models.classification.ExpressionType;

import java.util.*;

public class ParserReferenceBPC  extends ParserBPC{
    private transient ParserBPC bpc;

    private String referredNonTerminal;
    private String visibleKey;

    public ParserReferenceBPC(String referredNonTerminal, String visibleKey, RuleProcessorFactory ruleProcessorFactory) throws InvalidExpressionException {
        this.referredNonTerminal = referredNonTerminal;
        if(visibleKey == null) {
            this.visibleKey = referredNonTerminal;
        } else {
            this.visibleKey = visibleKey;
        }
        if(!ruleProcessorFactory.getRules().containsKey(referredNonTerminal)){
            throw new RuntimeException(String.format("rule with name %s not found", referredNonTerminal));
        }
        RuleParserModel ruleModel = ruleProcessorFactory.getRules().get(referredNonTerminal);
        if(ruleModel.getBpc()==null)
            ruleProcessorFactory.generateAST(ruleModel);
        this.bpc = (ParserBPC) ruleModel.getBpc();
        setExpressionAttributes(ExpressionType.extractorReference, ExpressionFamily.extractor);
        Set<String> referredSequences = new HashSet<>();
        referredSequences.add(referredNonTerminal);
        this.setReferredSequences(referredSequences);
    }

    public String getReferredNonTerminal() {
        return referredNonTerminal;
    }

    public void setReferredNonTerminal(String referredNonTerminal) {
        this.referredNonTerminal = referredNonTerminal;
    }

    public String getVisibleKey() {
        return visibleKey;
    }

    public void setVisibleKey(String visibleKey) {
        this.visibleKey = visibleKey;
    }

    public ParserBPC getBpc() {
        return bpc;
    }

    public void setBpc(ParserBPC bpc) {
        this.bpc = bpc;
    }

    @Override
    public ResultModel compute(Map<String, List<ParserValue>> parserValues) throws UnsupportedOpException {
        ResultModel result = new ResultModel(Result.NA);
        if(bpc != null) {
            result = this.bpc.compute(parserValues);
        }

        this.setResult(result);
        return result;
    }

    @Override
    public ResultModel compute(ParserValue parserValue) throws UnsupportedOpException {
        ResultModel result = new ResultModel(Result.NA);
        if(bpc != null) {
            result = this.bpc.compute(parserValue);
        }

        this.setResult(result);
        return result;
    }

    @Override
    public String generateExpression() {
        return KaiserKeyWords.TOKEN_NON_TERMINAL + KaiserKeyWords.TOKEN_SEPARATOR + KaiserKeyWords.SYMBOL_DOUBLE_SQUARE_OPEN +
                KaiserKeyWords.TOKEN_SEPARATOR + this.referredNonTerminal + KaiserKeyWords.TOKEN_SEPARATOR +
                KaiserKeyWords.SYMBOL_DOUBLE_SQUARE_CLOSE;
    }

    @Override
    public boolean updateTag(String currentTag, String newTag) {
        return this.bpc.updateTag(currentTag, newTag);
    }

    @Override
    public boolean updateComponent(String tag, String currentComponent, String newComponent) {
        return this.bpc.updateComponent(tag, currentComponent, newComponent);
    }

    @Override
    public boolean updateValue(String tag, String component, String currentValue, String newValue) {
        return this.bpc.updateValue(tag, component, currentValue, newValue);
    }

    @Override
    public boolean isInferenceReferred(long inferenceId) {
        return referredNonTerminal.equals(inferenceId);
    }

    @Override
    public String toString() {
        return this.bpc.toString();
    }

    @Override
    public Set<String> getEntities() {
        return bpc.getEntities();
    }
}
