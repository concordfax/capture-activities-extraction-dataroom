package com.kaiser.models.relationalexpression;

import com.kaiser.exceptions.UnsupportedOpException;
import com.kaiser.lex.KaiserKeyWords;
import com.kaiser.models.BaseParserComponent;
import com.kaiser.models.ParserBPC;
import com.kaiser.models.ParserValue;
import com.kaiser.models.ResultModel;
import com.kaiser.models.classification.ExpressionFamily;
import com.kaiser.models.classification.ExpressionType;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class NegationBPC extends ParserBPC implements Cloneable{
    private BaseParserComponent negationRule;

    public NegationBPC(BaseParserComponent bpc) {
        this.negationRule = bpc;
        setExpressionAttributes(ExpressionType.negationExpression, negationRule.getExpressionFamily());
        if(getExpressionFamily() == ExpressionFamily.extractor) {
            ParserBPC parser = (ParserBPC) negationRule;
            setExtractorType(parser.getExtractorType());
            setReferredSequences(parser.getReferredSequences());
        }
    }

    public BaseParserComponent getNegationRule() {
        return negationRule;
    }

    public void setNegationRule(BaseParserComponent negationRule) {
        this.negationRule = negationRule;
    }

    public ResultModel compute(Map<String, List<ParserValue>> parserValues) throws UnsupportedOpException {
        ResultModel result = negationRule.compute(parserValues);
        Result negatedResult =  negate(result.getResult());
        ResultModel resultModel = new ResultModel(negatedResult, result.getTriggerPhrases());
        if(getExpressionFamily() == ExpressionFamily.extractor) {
            resultModel.setSequenceResults(result.getSequenceResults());
        }
        return resultModel;
    }

    public ResultModel compute(ParserValue parserValues) throws UnsupportedOpException {
        ResultModel result = negationRule.compute(parserValues);
        Result negatedResult =  negate(result.getResult());
        ResultModel resultModel = new ResultModel(negatedResult, result.getTriggerPhrases());
        if(getExpressionFamily() == ExpressionFamily.extractor) {
            resultModel.setSequenceResults(result.getSequenceResults());
        }
        resultModel.setMatchingComponents(result.getMatchingComponents());
        return resultModel;
    }

    @Override
    public String generateExpression() {
        return KaiserKeyWords.SYMBOL_PARENTHESIS_OPEN + KaiserKeyWords.TOKEN_SEPARATOR +
                    KaiserKeyWords.LOGICAL_NOT + KaiserKeyWords.TOKEN_SEPARATOR +
                        negationRule.generateExpression() + KaiserKeyWords.TOKEN_SEPARATOR +
                KaiserKeyWords.SYMBOL_PARENTHESIS_CLOSE;
    }

    @Override
    public boolean updateTag(String currentTag, String newTag) {
        return negationRule.updateTag(currentTag, newTag);
    }

    @Override
    public boolean updateComponent(String tag, String currentComponent, String newComponent) {
        return negationRule.updateComponent(tag, currentComponent, newComponent);
    }

    @Override
    public boolean updateValue(String tag, String component, String currentValue, String newValue) {
        return negationRule.updateValue(tag, component, currentValue, newValue);
    }

    @Override
    public boolean isInferenceReferred(long inferenceId) {
        return negationRule.isInferenceReferred(inferenceId);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        NegationBPC expression = (NegationBPC)super.clone();
        expression.setNegationRule((BaseParserComponent) negationRule.clone());
        return expression;
    }

    @Override
    public Set<String> getEntities() {
        if(getExpressionFamily() == ExpressionFamily.extractor) {
            return ((ParserBPC) negationRule).getEntities();
        }
        return null;
    }
}
