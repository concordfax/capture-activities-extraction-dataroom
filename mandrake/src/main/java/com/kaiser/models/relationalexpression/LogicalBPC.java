package com.kaiser.models.relationalexpression;

import com.kaiser.exceptions.InvalidExpressionException;
import com.kaiser.exceptions.UnsupportedOpException;
import com.kaiser.lex.KaiserKeyWords;
import com.kaiser.models.*;
import com.kaiser.models.classification.ExpressionFamily;
import com.kaiser.models.classification.ExpressionType;
import com.kaiser.models.classification.ExtractorType;

import java.util.*;

public class LogicalBPC extends ParserBPC implements Cloneable{
    private BaseParserComponent rule1;
    private String condition;
    private BaseParserComponent rule2;

    public LogicalBPC(BaseParserComponent bpc1, BaseParserComponent bpc2, String logicalOperator)
            throws InvalidExpressionException {
        this.rule1 = bpc1;
        this.rule2 = bpc2;
        this.condition = logicalOperator;
        if(logicalOperator.equals("&")) {
            this.condition = KaiserKeyWords.LOGICAL_AND;
        } else if (logicalOperator.equals("|")) {
            this.condition = KaiserKeyWords.LOGICAL_OR;
        }

        if(rule1.getExpressionFamily() != rule2.getExpressionFamily()) {
            throw new InvalidExpressionException();
        }
        setExpressionAttributes(ExpressionType.binaryLogicalExpression, rule1.getExpressionFamily());
        if(getExpressionFamily() == ExpressionFamily.extractor) {
            ParserBPC parser1 = (ParserBPC) rule1;
            ParserBPC parser2 = (ParserBPC) rule2;
            if(parser1.getExtractorType() == parser2.getExtractorType()) {
                setExtractorType(parser1.getExtractorType());
            } else {
                setExtractorType(ExtractorType.sequenceAndContains);
            }
            Set<String> referredSequences = parser1.getReferredSequences();
            referredSequences.addAll(parser2.getReferredSequences());
            setReferredSequences(referredSequences);
        }
    }

    public BaseParserComponent getRule1() {
        return rule1;
    }

    public void setRule1(BaseParserComponent rule1) {
        this.rule1 = rule1;
    }

    public BaseParserComponent getRule2() {
        return rule2;
    }

    public void setRule2(BaseParserComponent rule2) {
        this.rule2 = rule2;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public ResultModel compute(Map<String, List<ParserValue>> parserValues) throws UnsupportedOpException{
        ResultModel result1 = rule1.compute(parserValues);
        ResultModel result2 = rule2.compute(parserValues);
        Result result = getLogicalResult(result1, result2, condition);

        Set<ResultAnnotation> triggerPhrases = new HashSet<>();
        triggerPhrases.addAll(result1.getTriggerPhrases());
        triggerPhrases.addAll(result2.getTriggerPhrases());
        ResultModel resultModel = new ResultModel(result, triggerPhrases);
        if(getExpressionFamily() == ExpressionFamily.extractor) {
            Set<SequenceResultModel> sequenceResults = result1.getSequenceResults();
            sequenceResults.addAll(result2.getSequenceResults());
            resultModel.setSequenceResults(sequenceResults);
        }

        return resultModel;
    }

    public ResultModel compute(ParserValue parserValues) throws UnsupportedOpException{
        ResultModel result1 = rule1.compute(parserValues);
//        if(condition.equals(KaiserKeyWords.LOGICAL_AND) && result1.getResult() == Result.NO) {
//            return result1;
//        } else if (condition.equals(KaiserKeyWords.LOGICAL_OR) && result1.getResult() == Result.YES) {
//            return result1;
//        }

        ResultModel result2 = rule2.compute(parserValues);
        Result result = getLogicalResult(result1, result2, condition);
        Set<ResultAnnotation> triggerPhrases = new HashSet<>();
        triggerPhrases.addAll(result1.getTriggerPhrases());
        triggerPhrases.addAll(result2.getTriggerPhrases());
        ResultModel resultModel = new ResultModel(result, triggerPhrases);
        List<ParserValue> matchingComponents = result1.getMatchingComponents();
        matchingComponents.addAll(result2.getMatchingComponents());
        resultModel.setMatchingComponents(matchingComponents);
        if(getExpressionFamily() == ExpressionFamily.extractor) {
            Set<SequenceResultModel> sequenceResults = result1.getSequenceResults();
            sequenceResults.addAll(result2.getSequenceResults());
            resultModel.setSequenceResults(sequenceResults);
        }

        if(result == Result.NO) {
            if((condition.equals(KaiserKeyWords.LOGICAL_AND) && result2.isNotContains()) ||
                    (condition.equals(KaiserKeyWords.LOGICAL_OR) && result1.isNotContains() && result2.isNotContains())) {
                resultModel.setNotContains(true);
            }
        }

        return resultModel;
    }

    private Result getLogicalResult(ResultModel result1, ResultModel result2, String condition) {
        Result result;
        switch (condition) {
            case KaiserKeyWords.LOGICAL_AND:
                result  = logicalAnd(result1.getResult(), result2.getResult());
                break;
            case KaiserKeyWords.LOGICAL_OR:
                result =  logicalOr(result1.getResult(), result2.getResult());
                break;
            default:
                result = Result.NA;
        }

        return result;

    }

    @Override
    public String generateExpression() {
        return KaiserKeyWords.SYMBOL_PARENTHESIS_OPEN + KaiserKeyWords.TOKEN_SEPARATOR +
                    rule1.generateExpression() + KaiserKeyWords.TOKEN_SEPARATOR +
                        condition + KaiserKeyWords.TOKEN_SEPARATOR +
                    rule2.generateExpression() + KaiserKeyWords.TOKEN_SEPARATOR +
                KaiserKeyWords.SYMBOL_PARENTHESIS_CLOSE;
    }

    @Override
    public boolean updateTag(String currentTag, String newTag) {
        boolean status1 = rule1.updateTag(currentTag, newTag);
        boolean status2 = rule2.updateTag(currentTag, newTag);
        return status1 || status2;
    }

    @Override
    public boolean updateComponent(String tag, String currentComponent, String newComponent) {
        boolean status1 = rule1.updateComponent(tag, currentComponent, newComponent);
        boolean status2 = rule2.updateComponent(tag, currentComponent, newComponent);
        return status1 || status2;
    }

    @Override
    public boolean updateValue(String tag, String component, String currentValue, String newValue) {
        boolean status1 = rule1.updateValue(tag, component, currentValue, newValue);
        boolean status2 = rule2.updateValue(tag, component, currentValue, newValue);
        return status1 || status2;
    }

    @Override
    public boolean isInferenceReferred(long inferenceId) {
        return rule1.isInferenceReferred(inferenceId) || rule2.isInferenceReferred(inferenceId);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        LogicalBPC clonned = (LogicalBPC) super.clone();
        clonned.setRule1((BaseParserComponent) rule1.clone());
        clonned.setRule2((BaseParserComponent) rule2.clone());
        return clonned;
    }

    @Override
    public Set<String> getEntities() {
        if(getExpressionFamily() == ExpressionFamily.extractor) {
            Set<String> entities = ((ParserBPC) rule1).getEntities();
            entities.addAll(((ParserBPC) rule2).getEntities());
            return entities;
        }
        return null;
    }
}
