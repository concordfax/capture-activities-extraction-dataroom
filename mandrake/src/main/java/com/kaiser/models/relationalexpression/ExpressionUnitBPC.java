package com.kaiser.models.relationalexpression;

import com.kaiser.exceptions.UnsupportedOpException;
import com.kaiser.lex.KaiserKeyWords;
import com.kaiser.models.BaseParserComponent;
import com.kaiser.models.ResultAnnotation;
import com.kaiser.models.ParserValue;
import com.kaiser.models.ResultModel;
import com.kaiser.models.classification.ExpressionFamily;
import com.kaiser.models.classification.ExpressionType;

import java.util.*;

public class ExpressionUnitBPC extends BaseParserComponent implements Cloneable{
    private String tag;
    private List<RelationalExpressionBPC> relationalExpressions;
    private transient RelationalExpressionBPC pivotalExpression;
    private transient List<RelationalExpressionBPC> supportingExpressions = new ArrayList<>();

    public ExpressionUnitBPC(String tag, List<RelationalExpressionBPC> expressionCoreBPC) {
        this.tag = tag;
        this.relationalExpressions = expressionCoreBPC;
        for(RelationalExpressionBPC expression: this.relationalExpressions) {
            if(expression.isPivotal()) {
                this.pivotalExpression = expression;
            } else {
                supportingExpressions.add(expression);
            }
        }
        setExpressionAttributes(ExpressionType.relationalExpression, ExpressionFamily.evaluator);
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public List<RelationalExpressionBPC> getExpressionCore() {
        return relationalExpressions;
    }

    public void setExpressionCore(List<RelationalExpressionBPC> expressionCore) {
        this.relationalExpressions = expressionCore;
    }

    public RelationalExpressionBPC getPivotalExpression() {
        return pivotalExpression;
    }

    public void setPivotalExpression(RelationalExpressionBPC pivotalExpression) {
        this.pivotalExpression = pivotalExpression;
    }

    public List<RelationalExpressionBPC> getSupportingExpressions() {
        return supportingExpressions;
    }

    public void setSupportingExpressions(List<RelationalExpressionBPC> supportingExpressions) {
        this.supportingExpressions = supportingExpressions;
    }

    public ResultModel compute(Map<String, List<ParserValue>> parserValues) throws UnsupportedOpException{
        List<ParserValue> valueList = parserValues.get(tag);
        if (valueList == null || valueList.size() <= 0) {
            return new ResultModel(Result.NA);
        }

        for (ParserValue value: valueList) {
            boolean isValidated = true;
            Set<ResultAnnotation> triggerPhrases = new HashSet<>();
            for (RelationalExpressionBPC relationalExpression : supportingExpressions) {
                ResultModel resultModel = relationalExpression.compute(value);
                Result result = resultModel.getResult();
                if (result == Result.YES) {
                    triggerPhrases.addAll(resultModel.getTriggerPhrases());
                } else {
                    isValidated = false;
                    break;
                }
            }

            ResultModel resultModel;
            if (isValidated) {
                if(pivotalExpression != null) {
                    ResultModel result = pivotalExpression.compute(value);
                    switch (result.getResult()) {
                        case YES:
                            triggerPhrases.addAll(result.getTriggerPhrases());
                            resultModel = new ResultModel(Result.YES, triggerPhrases);
                            resultModel.addMatchingComponent(value);
                            setResult(resultModel);
                            return resultModel;
                        case NO:
                            triggerPhrases.addAll(result.getTriggerPhrases());
                            resultModel = new ResultModel(Result.NO, triggerPhrases);
                            resultModel.addMatchingComponent(value);
                            setResult(resultModel);
                            return resultModel;
                    }
                } else {
                    resultModel = new ResultModel(Result.YES, triggerPhrases);
                    setResult(resultModel);
                    return resultModel;
                }
            }
        }

        return new ResultModel(Result.NA);
    }

    @Override
    public ResultModel compute(ParserValue parserValue) throws UnsupportedOpException {
        throw new UnsupportedOpException();
    }

    @Override
    public String generateExpression() {
        String expression = KaiserKeyWords.SYMBOL_BRACE_OPEN + KaiserKeyWords.TOKEN_SEPARATOR +
                KaiserKeyWords.DOUBLE_QUOTE + tag + KaiserKeyWords.DOUBLE_QUOTE;
        for(RelationalExpressionBPC expressionBPC: relationalExpressions) {
            expression = expression + KaiserKeyWords.TOKEN_SEPARATOR + expressionBPC.generateExpression();
        }
        expression = expression + KaiserKeyWords.TOKEN_SEPARATOR + KaiserKeyWords.SYMBOL_BRACE_CLOSE;

        return expression;
    }

    @Override
    public boolean updateTag(String currentTag, String newTag) {
        if (this.tag.equals(currentTag)) {
            tag = newTag;
            return true;
        }
        return false;
    }

    @Override
    public boolean updateComponent(String tag, String currentComponent, String newComponent) {
        if (this.tag.equals(tag)) {
            boolean status = false;
            for (RelationalExpressionBPC bpc: relationalExpressions) {
                if (bpc.updateComponent(tag, currentComponent, newComponent)) {
                    status = true;
                }
            }
            return status;
        }
        return false;
    }

    @Override
    public boolean updateValue(String tag, String component, String currentValue, String newValue) {
        boolean status = false;
        if (this.tag.equals(tag)) {
            for (RelationalExpressionBPC bpc: relationalExpressions) {
                if (bpc.updateValue(tag, component, currentValue, newValue)) {
                    status = true;
                }
            }
        }
        return status;
    }

    @Override
    public boolean isInferenceReferred(long inferenceId) {
        return false;
    }

    public String toString() {
        return tag + " " + pivotalExpression.toString();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        ExpressionUnitBPC clonned = (ExpressionUnitBPC) super.clone();
        List<RelationalExpressionBPC> relationalExps = new ArrayList<>();
        List<RelationalExpressionBPC> supportingExps = new ArrayList<>();
        supportingExpressions.forEach(exp -> {
            try {
                supportingExps.add((RelationalExpressionBPC) exp.clone());
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
        });
        relationalExps.addAll(supportingExps);
        if(pivotalExpression != null) {
            RelationalExpressionBPC pivotalClone = (RelationalExpressionBPC) pivotalExpression.clone();
            relationalExps.add(pivotalClone);
            clonned.setPivotalExpression(pivotalClone);
        }

        clonned.setExpressionCore(relationalExps);
        clonned.setSupportingExpressions(supportingExps);

        
        return clonned;
    }
}
