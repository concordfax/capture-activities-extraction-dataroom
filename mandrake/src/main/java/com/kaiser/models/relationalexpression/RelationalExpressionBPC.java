package com.kaiser.models.relationalexpression;

import com.kaiser.exceptions.UnsupportedOpException;
import com.kaiser.lex.KaiserKeyWords;
import com.kaiser.models.*;
import com.kaiser.models.classification.ExpressionFamily;
import com.kaiser.models.classification.ExpressionType;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RelationalExpressionBPC extends BaseParserComponent implements Cloneable{
    private String component;
    private String operator;
    private String value;
    private boolean isPivotal = false;

    public RelationalExpressionBPC(String property, String operator, String value) {
        this.operator = operator;
        this.component = property;
        this.value = value;
        setExpressionAttributes(ExpressionType.relationalExpressionUnit, ExpressionFamily.evaluator);
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String property) {
        this.component = property;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isPivotal() {
        return isPivotal;
    }

    public void setIsPivotal(boolean isPivotal) {
        this.isPivotal = isPivotal;
    }

    public ResultModel compute(Map<String, List<ParserValue>> parserValues) throws UnsupportedOpException{
        throw new UnsupportedOpException();
    }

    public ResultModel compute(ParserValue parserValue) throws UnsupportedOpException {
        if(parserValue == null) {
            return new ResultModel(Result.NA);
        }

        String textValue;
        Set<ResultAnnotation> triggerPhrases = new HashSet<>();
        Object componentValue = parserValue.get(component);
        if(componentValue instanceof ValueModel) {
            ValueModel valueModel = (ValueModel) componentValue;
            textValue = valueModel.getDisplayText();
            triggerPhrases = valueModel.getTriggerPhrases();
        } else {
            textValue = parserValue.getStringValue(component);
        }

        Double textNumeric = 0.0;
        if (textValue == null) {
            return new ResultModel(Result.NA);
        }
        if (componentValue.equals("Any") && operator.equals("=")) {
            if (parserValue.getStringValue(component) != null) {
                return new ResultModel(Result.YES, triggerPhrases);
            } else {
                return new ResultModel(Result.NO, triggerPhrases);
            }
        }
        Double numericValue = 0.0;
        boolean numeric = false;
        try {
            numericValue = Double.parseDouble(value);
            numeric = true;
        } catch (Exception ex) {
            numeric = false;
        }

        if (numeric) {
            try {
                textNumeric = Double.parseDouble(textValue);
            } catch (Exception ex) {
                throw new UnsupportedOpException();
            }
        }
        Result result = Result.NA;
        switch (operator) {
            case ">":
                if (!numeric) {
                    throw new UnsupportedOpException();
                }
                result = greaterThan(numericValue, textNumeric);
                break;
            case "<":
                if (!numeric) {
                    throw new UnsupportedOpException();
                }
                result = lessThan(numericValue, textNumeric);
                break;
            case ">=":
                if (!numeric) {
                    throw new UnsupportedOpException();
                }
                result = greaterThanOrEquals(numericValue, textNumeric);
                break;
            case "<=":
                if (!numeric) {
                    throw new UnsupportedOpException();
                }
                result = lessThanOrEquals(numericValue, textNumeric);
                break;
            case "=":
                if (!numeric) {
                    result = equalToString(value, textValue);
                } else {
                    result = equalToNumeric(numericValue, textNumeric);
                }
                break;
        }

        if(result.equals(Result.NA)) {
            return new ResultModel(Result.NA);
        } else {
            return new ResultModel(result, triggerPhrases);
        }
    }

    private Result greaterThan(Double ruleVal, Double currentVal) {
        if(currentVal > ruleVal)
            return Result.YES;
        else
            return Result.NO;
    }

    private Result greaterThanOrEquals(Double ruleVal, Double currentVal) {
        if(currentVal >= ruleVal)
            return Result.YES;
        else
            return Result.NO;
    }

    private Result lessThan(Double ruleVal, Double currentVal) {
        if(currentVal < ruleVal)
            return Result.YES;
        else
            return Result.NO;
    }

    private Result lessThanOrEquals(Double ruleVal, Double currentVal) {
        if(currentVal <= ruleVal)
            return Result.YES;
        else
            return Result.NO;
    }

    private Result equalToNumeric(Double ruleVal, Double currentVal) {
        if(ruleVal == currentVal)
            return Result.YES;
        else
            return Result.NO;
    }

    private Result equalToString(String ruleVal, String currentVal) {
        if(ruleVal.equalsIgnoreCase(currentVal))
            return Result.YES;
        else
            return Result.NO;
    }

    @Override
    public String generateExpression() {
        String bracketOpen = KaiserKeyWords.SYMBOL_SQUARE_BRACKET_OPEN;
        String bracketClose = KaiserKeyWords.SYMBOL_SQUARE_BRACKET_CLOSE;
        if(isPivotal) {
            bracketOpen = KaiserKeyWords.SYMBOL_DOUBLE_SQUARE_OPEN;
            bracketClose = KaiserKeyWords.SYMBOL_DOUBLE_SQUARE_CLOSE;
        }

        return bracketOpen + KaiserKeyWords.TOKEN_SEPARATOR +
                    KaiserKeyWords.DOUBLE_QUOTE + component + KaiserKeyWords.DOUBLE_QUOTE +
                        KaiserKeyWords.TOKEN_SEPARATOR + operator + KaiserKeyWords.TOKEN_SEPARATOR +
                    KaiserKeyWords.DOUBLE_QUOTE + value + KaiserKeyWords.DOUBLE_QUOTE +
                KaiserKeyWords.TOKEN_SEPARATOR + bracketClose;

    }

    @Override
    public boolean updateTag(String currentTag, String newTag) {
        return false;
    }

    @Override
    public boolean updateComponent(String tag, String currentComponent, String newComponent) {
        if(this.component.equals(currentComponent)) {
            this.component = newComponent;
            return true;
        }
        return false;
    }

    @Override
    public boolean updateValue(String tag, String component, String currentValue, String newValue) {
        if(this.component.equals(component) && this.value.equals(currentValue)) {
            this.value = newValue;
            return true;
        }
        return false;
    }

    @Override
    public boolean isInferenceReferred(long inferenceId) {
        return false;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String toString() {
        return component + " " + operator + " " + value;
    }
}
