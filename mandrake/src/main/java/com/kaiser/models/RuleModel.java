package com.kaiser.models;

import com.kaiser.models.classification.ExpressionFamily;
import com.kaiser.models.classification.ExtractorType;
import lombok.Data;

import java.util.Set;

@Data
public class RuleModel {
    
    private String name;
    private String rule;
    private String id;
    private String userId;
    private String groupId;
    private String top;
    private Boolean isWrongModal;
    private ExpressionFamily expressionFamily;
    private ExtractorType extractorType;
    private Set<String> entities;
    private Set<String> referredSequences;
    private BaseParserComponent AST;
}
