package com.kaiser.models.containsexpression;

import com.kaiser.exceptions.UnsupportedOpException;
import com.kaiser.lex.KaiserKeyWords;
import com.kaiser.models.BaseParserComponent;
import com.kaiser.models.ParserBPC;
import com.kaiser.models.ParserValue;
import com.kaiser.models.ResultModel;
import com.kaiser.models.classification.ExpressionFamily;
import com.kaiser.models.classification.ExpressionType;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class NotContainsBPC extends ParserBPC{
    private ParserBPC containsBPC;

    public NotContainsBPC(ParserBPC containsBPC) {
        this.containsBPC = containsBPC;
        setExpressionAttributes(ExpressionType.notContains, ExpressionFamily.extractor);
    }

    public ParserBPC getContainsBPC() {
        return containsBPC;
    }

    public void setContainsBPC(ParserBPC containsBPC) {
        this.containsBPC = containsBPC;
    }

    @Override
    public ResultModel compute(Map<String, List<ParserValue>> parserValues) throws UnsupportedOpException {
        throw new UnsupportedOpException();
    }

    public ResultModel compute(ParserValue parserValue) throws UnsupportedOpException {
        ResultModel result = containsBPC.compute(parserValue);
        Result negatedResult =  negate(result.getResult());
        result.setResult(negatedResult);
        return result;
    }

    public String generateExpression() {
        return KaiserKeyWords.LOGICAL_NOT + KaiserKeyWords.TOKEN_SEPARATOR +
                    containsBPC.generateExpression();
    }

    @Override
    public boolean updateTag(String currentTag, String newTag) {
        return containsBPC.updateTag(currentTag, newTag);
    }

    @Override
    public boolean updateComponent(String tag, String currentComponent, String newComponent) {
        return containsBPC.updateComponent(tag, currentComponent, newComponent);
    }

    @Override
    public boolean updateValue(String tag, String component, String currentValue, String newValue) {
        return false;
    }

    @Override
    public boolean isInferenceReferred(long inferenceId) {
        return false;
    }

    @Override
    public Set<String> getEntities() {
        return containsBPC.getEntities();
    }
}
