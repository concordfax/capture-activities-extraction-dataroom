package com.kaiser.models.containsexpression;

import com.kaiser.exceptions.UnsupportedOpException;
import com.kaiser.lex.KaiserKeyWords;
import com.kaiser.models.*;
import com.kaiser.models.classification.ExpressionFamily;
import com.kaiser.models.classification.ExpressionType;
import com.kaiser.models.classification.ExtractorType;

import java.util.*;

public class ContainsExpressionBPC extends ParserBPC{
    private List<ParserBPC> optionals;
    private ParserBPC mandatory;

    public ContainsExpressionBPC(List<ParserBPC> optionals) {
        this.optionals = optionals;
        setExpressionAttributes(ExpressionType.containsExpression, ExpressionFamily.extractor);
        this.setExtractorType(ExtractorType.containsOnly);
    }

    public ContainsExpressionBPC(ParserBPC mandatory) {
        this.mandatory = mandatory;
        setExpressionAttributes(ExpressionType.containsExpression, ExpressionFamily.extractor);
        this.setExtractorType(ExtractorType.containsOnly);
    }

    public ContainsExpressionBPC(List<ParserBPC> optionals, ParserBPC mandatory) {
        this.optionals = optionals;
        this.mandatory = mandatory;
        setExpressionAttributes(ExpressionType.containsExpression, ExpressionFamily.extractor);
        this.setExtractorType(ExtractorType.containsOnly);
    }

    public List<ParserBPC> getOptionals() {
        return optionals;
    }

    public void setOptionals(List<ParserBPC> optionals) {
        this.optionals = optionals;
    }

    public ParserBPC getMandatory() {
        return mandatory;
    }

    public void setMandatory(ParserBPC mandatory) {
        this.mandatory = mandatory;
    }

    public ResultModel compute(Map<String, List<ParserValue>> parserValues) {
        return null;
    }

    public ResultModel compute(ParserValue parserValue) throws UnsupportedOpException {
        ResultModel resultModel = new ResultModel(Result.YES);
        if(mandatory != null) {
            resultModel = mandatory.compute(parserValue);
            Result mandatoryResult = resultModel.getResult();
            if(mandatoryResult == Result.NO || mandatoryResult == Result.NA) {
                return resultModel;
            }
        }

        if (optionals != null) {
            ParserValue matchingComponents = resultModel.getTopMatchingComponents();
            Set<ResultAnnotation> triggerPhrases = resultModel.getTriggerPhrases();
            for (BaseParserComponent optional : optionals) {
                ResultModel optionalResult = optional.compute(parserValue);
                matchingComponents.putAll(optionalResult.getTopMatchingComponents());
                triggerPhrases.addAll(optionalResult.getTriggerPhrases());
            }
            resultModel.addMatchingComponent(matchingComponents);
            resultModel.addTriggerPhrases(triggerPhrases);
        }

        return resultModel;
    }

    public String generateExpression() {
        String mandatoryExpression = "";
        if(mandatory != null) {
            mandatoryExpression = KaiserKeyWords.TOKEN_MANDATORY +  KaiserKeyWords.TOKEN_SEPARATOR +
                                        KaiserKeyWords.SYMBOL_BRACE_OPEN +  KaiserKeyWords.TOKEN_SEPARATOR +
                                            mandatory.generateExpression() +  KaiserKeyWords.TOKEN_SEPARATOR +
                                        KaiserKeyWords.SYMBOL_BRACE_CLOSE;
        }
        String optionalExpression = "";
        if (optionals != null && optionals.size() > 0) {
            optionalExpression = KaiserKeyWords.TOKEN_OPTIONAL + KaiserKeyWords.TOKEN_SEPARATOR +
                    KaiserKeyWords.SYMBOL_BRACE_OPEN +  KaiserKeyWords.TOKEN_SEPARATOR;
            for (BaseParserComponent optionalBPC: optionals) {
                optionalExpression = optionalExpression +
                            optionalBPC.generateExpression() + KaiserKeyWords.TOKEN_SEPARATOR;
            }
            optionalExpression = optionalExpression + KaiserKeyWords.SYMBOL_BRACE_CLOSE;
        }

        String containsExpression = KaiserKeyWords.TOKEN_CONTAINS + KaiserKeyWords.TOKEN_SEPARATOR +
                                        KaiserKeyWords.SYMBOL_SQUARE_BRACKET_OPEN + KaiserKeyWords.TOKEN_SEPARATOR +
                                            mandatoryExpression + KaiserKeyWords.TOKEN_SEPARATOR +
                                            optionalExpression + KaiserKeyWords.TOKEN_SEPARATOR +
                                        KaiserKeyWords.SYMBOL_SQUARE_BRACKET_CLOSE;

        return containsExpression;
    }

    @Override
    public boolean updateTag(String currentTag, String newTag) {
        boolean status1 = mandatory.updateTag(currentTag, newTag);
        boolean status2 = false;
        if(optionals != null) {
            for (BaseParserComponent optional : optionals) {
                if(optional.updateTag(currentTag, newTag)) {
                    status2 = true;
                }
            }
        }
        return status1 || status2;
    }

    @Override
    public boolean updateComponent(String tag, String currentComponent, String newComponent) {
        boolean status1 = mandatory.updateComponent(tag, currentComponent, newComponent);
        boolean status2 = false;
        if(optionals != null) {
            for (BaseParserComponent optional : optionals) {
                if(optional.updateComponent(tag, currentComponent, newComponent)) {
                    status2 = true;
                }
            }
        }
        return status1 || status2;
    }

    @Override
    public boolean updateValue(String tag, String component, String currentValue, String newValue) {
        return false;
    }

    @Override
    public boolean isInferenceReferred(long inferenceId) {
        return false;
    }

    @Override
    public Set<String> getEntities() {
        Set<String> entities = new HashSet<>();
        if (mandatory != null) {
            entities.addAll(mandatory.getEntities());
        }
        if (optionals != null) {
            optionals.forEach(bpc -> {
                entities.addAll(bpc.getEntities());
            });
        }
        return entities;
    }
}
