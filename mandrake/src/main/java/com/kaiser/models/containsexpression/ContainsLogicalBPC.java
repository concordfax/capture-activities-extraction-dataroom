package com.kaiser.models.containsexpression;

import com.kaiser.exceptions.UnsupportedOpException;
import com.kaiser.lex.KaiserKeyWords;
import com.kaiser.models.*;
import com.kaiser.models.classification.ExpressionFamily;
import com.kaiser.models.classification.ExpressionType;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class ContainsLogicalBPC extends ParserBPC{
    private ParserBPC rule1;
    private String condition;
    private ParserBPC rule2;

    public ContainsLogicalBPC(ParserBPC rule1, String condition, ParserBPC rule2) {
        this.rule1 = rule1;
        this.condition = condition;
        this.rule2 = rule2;
        setExpressionAttributes(ExpressionType.containsLogical, ExpressionFamily.extractor);
    }

    public ParserBPC getRule1() {
        return rule1;
    }

    public void setRule1(ParserBPC rule1) {
        this.rule1 = rule1;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public ParserBPC getRule2() {
        return rule2;
    }

    public void setRule2(ParserBPC rule2) {
        this.rule2 = rule2;
    }

    @Override
    public ResultModel compute(Map<String, List<ParserValue>> parserValues) throws UnsupportedOpException {
        throw new UnsupportedOpException();
    }

    public ResultModel compute(ParserValue parserValue) throws UnsupportedOpException {
        ResultModel result1 = rule1.compute(parserValue);
        if(condition.equals(KaiserKeyWords.LOGICAL_AND) && result1.getResult() == Result.NO) {
            return result1;
        } else if (condition.equals(KaiserKeyWords.LOGICAL_OR) && result1.getResult() == Result.YES) {
            return result1;
        }

        Result result;
        ResultModel result2 = rule2.compute(parserValue);
        switch (condition) {
            case KaiserKeyWords.LOGICAL_AND:
                result  = logicalAnd(result1.getResult(), result2.getResult());
                break;
            case KaiserKeyWords.LOGICAL_OR:
                result =  logicalOr(result1.getResult(), result2.getResult());
                break;
            default:
                result = Result.NA;
        }

        ParserValue matchingComponents = result1.getTopMatchingComponents();
        Set<ResultAnnotation> triggerPhrases = result1.getTriggerPhrases();
        matchingComponents.putAll(result2.getTopMatchingComponents());
        triggerPhrases.addAll(result2.getTriggerPhrases());

        ResultModel resultModel = new ResultModel(result, triggerPhrases);
        if(matchingComponents.size() > 0 ) {
            resultModel.addMatchingComponent(matchingComponents);
        }
        if(result == Result.NO) {
            if((condition.equals(KaiserKeyWords.LOGICAL_AND) && result2.isNotContains()) ||
                    (condition.equals(KaiserKeyWords.LOGICAL_OR) && (result1.isNotContains() && result2.isNotContains()))) {
                resultModel.setNotContains(true);
            }
        }

        return resultModel;
    }

    public String generateExpression() {
        return KaiserKeyWords.SYMBOL_PARENTHESIS_OPEN + KaiserKeyWords.TOKEN_SEPARATOR +
                    rule1.generateExpression() + KaiserKeyWords.TOKEN_SEPARATOR +
                        condition + KaiserKeyWords.TOKEN_SEPARATOR +
                    rule2.generateExpression() + KaiserKeyWords.TOKEN_SEPARATOR +
                KaiserKeyWords.SYMBOL_PARENTHESIS_CLOSE;
    }

    @Override
    public boolean updateTag(String currentTag, String newTag) {
        boolean status1 = rule1.updateTag(currentTag, newTag);
        boolean status2 = rule2.updateTag(currentTag, newTag);
        return status1 || status2;
    }

    @Override
    public boolean updateComponent(String tag, String currentComponent, String newComponent) {
        boolean status1 = rule1.updateComponent(tag, currentComponent, newComponent);
        boolean status2 = rule2.updateComponent(tag, currentComponent, newComponent);
        return status1 || status2;
    }

    @Override
    public boolean updateValue(String tag, String component, String currentValue, String newValue) {
        return false;
    }

    @Override
    public boolean isInferenceReferred(long inferenceId) {
        return false;
    }

    @Override
    public Set<String> getEntities() {
        Set<String> entities = rule1.getEntities();
        entities.addAll(rule2.getEntities());
        return entities;
    }
}
