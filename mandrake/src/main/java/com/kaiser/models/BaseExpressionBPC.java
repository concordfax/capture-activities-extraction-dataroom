package com.kaiser.models;

import com.kaiser.exceptions.UnsupportedOpException;
import com.kaiser.lex.KaiserKeyWords;

import java.util.List;
import java.util.Map;

public class BaseExpressionBPC extends BaseParserComponent implements Cloneable{
    private BaseParserComponent AST;

    public BaseExpressionBPC(BaseParserComponent AST) {
        this.AST = AST;
    }

    public BaseParserComponent getAST() {
        return AST;
    }

    public void setAST(BaseParserComponent AST) {
        this.AST = AST;
    }

    public ResultModel compute(Map<String, List<ParserValue>> parserValues) throws UnsupportedOpException {
        return AST.compute(parserValues);
    }

    public ResultModel compute(ParserValue parserValue) throws UnsupportedOpException{
        return AST.compute(parserValue);
    }

    public String generateExpression() {
        return KaiserKeyWords.TOKEN_IF + KaiserKeyWords.TOKEN_SEPARATOR +
                    AST.generateExpression();
    }

    @Override
    public boolean updateTag(String currentTag, String newTag) {
        return AST.updateTag(currentTag, newTag);
    }

    @Override
    public boolean updateComponent(String tag, String currentComponent, String newComponent) {
        return AST.updateComponent(tag, currentComponent, newComponent);
    }

    @Override
    public boolean updateValue(String tag, String component, String currentValue, String newValue) {
        return AST.updateValue(tag, component, currentValue, newValue);
    }

    @Override
    public boolean isInferenceReferred(long inferenceId) {
        return AST.isInferenceReferred(inferenceId);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        BaseExpressionBPC expression = (BaseExpressionBPC)super.clone();
        expression.setAST((BaseParserComponent) AST.clone());
        return expression;
    }
}
