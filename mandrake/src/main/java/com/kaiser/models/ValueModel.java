package com.kaiser.models;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Triggered Phrase with details for UI annotation
 */

public class ValueModel {
    private String text;
    private Map<String, Object> properties = new HashMap<>();
    private boolean isNegated = false;
    private int startIndex;
    private int endIndex;
    private int sentenceIndex;
    Set<ResultAnnotation> triggerPhrases = new HashSet<>();
    private String displayText;

    public ValueModel(String text) {
        this.text = text;
    }

    public ValueModel(String text, boolean isNegated, int startIndex) {
        this.text = text;
        this.isNegated = isNegated;
        this.startIndex = startIndex;
    }

    public ValueModel(String text, ResultAnnotation triggerPhrase) {
        this.text = text;
        this.triggerPhrases.add(triggerPhrase);
    }

    public ValueModel(String text, Set<ResultAnnotation> triggerPhrases) {
        this.text = text;
        if(triggerPhrases != null) {
            this.triggerPhrases = triggerPhrases;
        }
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isNegated() {
        return isNegated;
    }

    public void setIsNegated(boolean isNegated) {
        this.isNegated = isNegated;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public void setEndIndex(int endIndex) {
        this.endIndex = endIndex;
    }

    public Set<ResultAnnotation> getTriggerPhrases() {
        return triggerPhrases;
    }

    public void setTriggerPhrases(Set<ResultAnnotation> triggerPhrases) {
        this.triggerPhrases = triggerPhrases;
    }

    public void addTriggerPhrase(ResultAnnotation triggerPhrase) {
        this.triggerPhrases.add(triggerPhrase);
    }

    public String getDisplayText() {
        return displayText;
    }

    public void setDisplayText(String displayText) {
        this.displayText = displayText;
    }

    public Map<String, Object> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, Object> properties) {
        this.properties = properties;
    }

    @Override
    public String toString() {
        return this.text;
    }

    public int getSentenceIndex() {
        return sentenceIndex;
    }

    public void setSentenceIndex(int sentenceIndex) {
        this.sentenceIndex = sentenceIndex;
    }
}
