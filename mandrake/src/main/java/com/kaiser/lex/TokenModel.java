package com.kaiser.lex;

/**
 * Model for Tokens from Lex
 */
public class TokenModel {
    private String token;
    private short type;

    public TokenModel(String token, short type) {
        this.token = token;
        this.type = type;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public short getType() {
        return type;
    }

    public void setType(short type) {
        this.type = type;
    }
}
