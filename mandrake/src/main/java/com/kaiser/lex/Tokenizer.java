package com.kaiser.lex;

import java.util.*;

public class Tokenizer {
    private static final String EMPTY_STRING = "";
    
    private LexState root;
    private List<LexKeyWordModel> langKeyWords;
    private short variableToken = -1;

    public Tokenizer() {
        root = new LexState();
        root.setStateType(LexStateType.NON_LEAF);
        langKeyWords = new ArrayList<>();
    }

    public void build() {
        for(LexKeyWordModel languageKeyWord: langKeyWords) {
            List<String> keyWordList = languageKeyWord.getKeywords();
            for(String keyWord: keyWordList) {
                addKeyWord(keyWord, languageKeyWord.getTag());
            }
        }
    }

    public boolean addKeyWord(String token, short tag) {
        if (token == null || token.length() == 0) {
            return false;
        }

        LexState node = root;
        int tokenLength = token.length();

        for (short handle = 0; handle < tokenLength; handle++) {
            node = node.addTransitionPathFromState(token.charAt(handle), (handle == tokenLength - 1 ?
                    LexStateType.LEAF_TERMINATING : LexStateType.NON_LEAF));
        }
        addTagToNode(node, tag);
        return true;
    }

    private void addTagToNode(LexState node, short tag) {
        node.setTag(tag);
    }

    public List<TokenModel> tokenize(String text) {
        boolean stringStarted = false;
        boolean doEscape = false;
        boolean keyWordStarted = false;

        List<TokenModel> tokens = new ArrayList<>();
        String currentToken = EMPTY_STRING;
        LexState currentNode = root;
        String lastAmbiguousKeyWord = EMPTY_STRING;
        short lastAmbiguoysKeyTag = -1;
        String keyWord = EMPTY_STRING;
        int i=0;
        while (i < text.length()) {
            char ch = text.charAt(i);
            i++;
            if(stringStarted) {
                if (ch == KaiserKeyWords.ESCAPE_CHARACTER) {
                    if (doEscape) {
                        currentToken += ch;
                        doEscape = false;
                        continue;
                    } else {
                        doEscape = true;
                        continue;
                    }
                }
                if (doEscape) {
                    currentToken += ch;
                    doEscape = false;
                    continue;
                }
                if(ch == KaiserKeyWords.DOUBLE_QUOTE) {
                    tokens.add(new TokenModel(currentToken.trim(), variableToken));
                    currentToken = new String(EMPTY_STRING);
                    stringStarted = false;
                } else {
                    currentToken += ch;
                }
            } else {
                if (ch == KaiserKeyWords.DOUBLE_QUOTE) {
                    if (lastAmbiguousKeyWord.length() > 0) {
                        tokens.add(new TokenModel(lastAmbiguousKeyWord, lastAmbiguoysKeyTag));
                        lastAmbiguousKeyWord = new String(EMPTY_STRING);
                        keyWord = new String(EMPTY_STRING);
                    }

                    stringStarted = true;
                    continue;
                } else {
                    boolean isPreviousRoot = false;
                    if(currentNode.equals(root)) {
                        isPreviousRoot = true;
                    }
                    currentNode = currentNode.followTransitionPathFromState(ch);
                    if(currentNode != null) {
                        keyWordStarted = true;
                        keyWord += ch;
                        if(currentNode.getStateType().equals(LexStateType.LEAF_TERMINATING)) {
                            if(currentToken.trim().length() > 0) {
                                tokens.add(new TokenModel(currentToken.trim(), variableToken));
                            }
                            tokens.add(new TokenModel(keyWord, currentNode.getTag()));
                            currentToken = new String(EMPTY_STRING);
                            keyWord = new String(EMPTY_STRING);
                            lastAmbiguousKeyWord = new String(EMPTY_STRING);
                            keyWordStarted = false;
                            currentNode = root;
                        } else if (currentNode.getStateType().equals(LexStateType.AMBIGUOUS)) {
                            lastAmbiguousKeyWord = new String(keyWord);
                            lastAmbiguoysKeyTag = currentNode.getTag();
                        }
                    } else {
                        currentNode = root;
                        if(!isPreviousRoot) {
                            i--;
                        }
                        if(keyWordStarted) {
                            if (lastAmbiguousKeyWord.length() > 0) {
                                tokens.add(new TokenModel(lastAmbiguousKeyWord, lastAmbiguoysKeyTag));
                                if (lastAmbiguousKeyWord.length() < keyWord.length()) {
                                    int currentTokenIndex = lastAmbiguousKeyWord.length();
                                    currentToken = keyWord.substring(currentTokenIndex).trim();
                                }
                                lastAmbiguousKeyWord = new String(EMPTY_STRING);
                            }
                            keyWordStarted = false;
                            keyWord = new String(EMPTY_STRING);
                        }
                    }
                }
            }
        }

        if (lastAmbiguousKeyWord.length() > 0) {
            tokens.add(new TokenModel(lastAmbiguousKeyWord, lastAmbiguoysKeyTag));
        }

        return tokens;
    }

    public void addLangKeyWords(short tokenType, List<String> keyWords) {
        langKeyWords.add(new LexKeyWordModel(tokenType, keyWords));
    }

    public short getVariableToken() {
        return variableToken;
    }

    public void setVariableToken(short variableToken) {
        this.variableToken = variableToken;
    }

    public static void main(String[] args) {
        Tokenizer tokenizer = new Tokenizer();
        short i = 5;
        tokenizer.setVariableToken(i);
        tokenizer.addLangKeyWords(i++, Arrays.asList(KaiserKeyWords.OPERATOR_EQUALS,
                KaiserKeyWords.OPERATOR_LESS_THAN,
                KaiserKeyWords.OPERATOR_GREATER_THAN,
                KaiserKeyWords.OPERATOR_LESS_THAN_OR_EQUALS,
                KaiserKeyWords.OPERATOR_GREATER_THAN_OR_EQUALS));
        tokenizer.addLangKeyWords(i++, Arrays.asList( KaiserKeyWords.LOGICAL_AND, KaiserKeyWords.LOGICAL_OR));
        tokenizer.addLangKeyWords(i++, Arrays.asList(KaiserKeyWords.LOGICAL_NOT));
        tokenizer.addLangKeyWords(i++, Arrays.asList(KaiserKeyWords.TOKEN_MANDATORY));
        tokenizer.addLangKeyWords(i++, Arrays.asList(KaiserKeyWords.TOKEN_OPTIONAL));
        tokenizer.addLangKeyWords(i++, Arrays.asList(KaiserKeyWords.TOKEN_CONTAINS));
        tokenizer.addLangKeyWords(i++, Arrays.asList(KaiserKeyWords.TOKEN_SEQUENCE));
        tokenizer.addLangKeyWords(i++, Arrays.asList(KaiserKeyWords.TOKEN_IF));
        tokenizer.addLangKeyWords(i++, Arrays.asList(KaiserKeyWords.SYMBOL_FOLLOWS));
        tokenizer.addLangKeyWords(i++, Arrays.asList(KaiserKeyWords.SYMBOL_BRACE_OPEN));
        tokenizer.addLangKeyWords(i++, Arrays.asList(KaiserKeyWords.SYMBOL_BRACE_CLOSE));
        tokenizer.addLangKeyWords(i++, Arrays.asList(KaiserKeyWords.SYMBOL_SQUARE_BRACKET_OPEN));
        tokenizer.addLangKeyWords(i++, Arrays.asList(KaiserKeyWords.SYMBOL_SQUARE_BRACKET_CLOSE));
        tokenizer.addLangKeyWords(i++, Arrays.asList(KaiserKeyWords.TOKEN_ALIAS));
        tokenizer.addLangKeyWords(i++, Arrays.asList(KaiserKeyWords.SYMBOL_DOUBLE_SQUARE_OPEN));
        tokenizer.addLangKeyWords(i++, Arrays.asList(KaiserKeyWords.SYMBOL_DOUBLE_SQUARE_CLOSE));
        tokenizer.addLangKeyWords(i++, Arrays.asList("(", ")"));
        tokenizer.build();

        List<TokenModel> tokenModels = tokenizer.tokenize("[TIGER]|[LION]");
        System.out.println("here");
    }
}
