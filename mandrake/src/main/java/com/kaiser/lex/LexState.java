package com.kaiser.lex;

import java.util.TreeMap;

/**
 * A node on the Lex tree for Kaiser Expression Language
 */
public class LexState {
    private TreeMap<Character, LexState> transitionStates;
    private LexStateType stateType;
    private short tag;

    public LexState() {
        transitionStates = null;
        tag = -1;
    }

    public LexState addTransitionPathFromState(Character ch, LexStateType stateType) {
        LexState nextState = null;
        if (transitionStates == null) {
            transitionStates = new TreeMap<Character, LexState>();
        } else {
            nextState = transitionStates.get(ch);
        }
        if (nextState == null) {
            nextState = new LexState();
            transitionStates.put(ch, nextState);
            nextState.setStateType(stateType);
        } else if (stateType != LexStateType.LEAF_TERMINATING && nextState.getStateType() == LexStateType.LEAF_TERMINATING) {
            nextState.setStateType(LexStateType.AMBIGUOUS);
        } else if (stateType == LexStateType.LEAF_TERMINATING && nextState.getStateType() != LexStateType.LEAF_TERMINATING) {
            nextState.setStateType(LexStateType.AMBIGUOUS);
        }
        return nextState;
    }

    public LexState followTransitionPathFromState(Character ch) {
        if (transitionStates == null) {
            return null;
        } else {
            return transitionStates.get(ch);
        }
    }

    public TreeMap<Character, LexState> getTransitionStates() {
        return transitionStates;
    }

    public void setTransitionStates(TreeMap<Character, LexState> transitionStates) {
        this.transitionStates = transitionStates;
    }

    public LexStateType getStateType() {
        return stateType;
    }

    public void setStateType(LexStateType stateType) {
        this.stateType = stateType;
    }

    public short getTag() {
        return tag;
    }

    public void setTag(short tag) {
        this.tag = tag;
    }
}
