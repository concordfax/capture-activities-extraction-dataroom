package com.kaiser.lex;

import java.util.List;

/**
 * KeyWord Models for Lex of Kaiser Expression Language
 */
public class LexKeyWordModel {
    private short tag;
    private List<String> keywords;

    public LexKeyWordModel(short tag, List<String> keywords) {
        this.tag = tag;
        this.keywords = keywords;
    }

    public short getTag() {
        return tag;
    }

    public void setTag(short tag) {
        this.tag = tag;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }
}
