package com.kaiser.lex;

public class KaiserKeyWords {
    public static final String OPERATOR_EQUALS = "=";
    public static final String OPERATOR_NOT_EQUALS = "!=";
    public static final String OPERATOR_GREATER_THAN_OR_EQUALS = ">=";
    public static final String OPERATOR_LESS_THAN_OR_EQUALS = "<=";
    public static final String OPERATOR_GREATER_THAN = ">";
    public static final String OPERATOR_LESS_THAN = "<";
    public static final String OPERATOR_IS_ANY_OF = "IS ANY OF";

    public static final String LOGICAL_AND = "AND IF";
    public static final String LOGICAL_OR = "OR IF";
    public static final String LOGICAL_NOT = "NOT";
    public static final String TOKEN_COMBINED_EXPRESSION_ANY = "ANY OF";
    public static final String TOKEN_COMBINED_EXPRESSION_ALL = "ALL OF";

    public static final String TOKEN_MANDATORY = "MANDATORY";
    public static final String TOKEN_NON_TERMINAL = "NT";
    public static final String TOKEN_CONDITION = "RULE";
    public static final String TOKEN_OPTIONAL = "OPTIONAL";
    public static final String TOKEN_ZERO_OR_ONE = "ZERO OR ONE";
    public static final String TOKEN_ONE_OR_MORE = "ONE OR MORE";
    public static final String TOKEN_ZERO_OR_MORE = "ZERO OR MORE";
    public static final String TOKEN_NONE = "NONE";
    public static final String TOKEN_CONTAINS = "CONTAINS";
    public static final String TOKEN_SEQUENCE = "SEQUENCE";
    public static final String TOKEN_IF = "IF";
    public static final String TOKEN_ALIAS = "AS";
    public static final String TOKEN_META = "META";

    public static final String SYMBOL_FOLLOWS = "->";
    public static final String SYMBOL_BRACE_OPEN = "{";
    public static final String SYMBOL_BRACE_CLOSE = "}";
    public static final String SYMBOL_SQUARE_BRACKET_OPEN = "[";
    public static final String SYMBOL_SQUARE_BRACKET_CLOSE = "]";
    public static final String SYMBOL_PARENTHESIS_OPEN = "(";
    public static final String SYMBOL_PARENTHESIS_CLOSE = ")";
    public static final String SYMBOL_DOUBLE_SQUARE_OPEN = "[[";
    public static final String SYMBOL_DOUBLE_SQUARE_CLOSE = "]]";

    public static final char DOUBLE_QUOTE = '"';
    public static final char ESCAPE_CHARACTER = '\\';
    public static final char TOKEN_SEPARATOR = ' ';
}
