package com.kaiser.lex;

/**
 * Node Type of the Lex Tree.
 */
public enum LexStateType {
    NON_LEAF,               //Not a leaf node
    LEAF_TERMINATING,       //Leaf node and not con
    AMBIGUOUS,               //CONTINUED AND TERMINATING
    ;
}
