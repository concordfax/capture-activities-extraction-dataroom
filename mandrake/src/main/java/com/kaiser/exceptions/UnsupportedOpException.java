package com.kaiser.exceptions;

public class UnsupportedOpException extends Exception {
    public UnsupportedOpException() {
        super("Unsupported operation");
    }
}
