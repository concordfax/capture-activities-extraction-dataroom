package com.kaiser.exceptions;

public class InvalidExpressionException extends Exception {
    public InvalidExpressionException() {
        super("Expression is invalid");
    }
}
