package com.kaiser.dao;

import com.kaiser.models.BaseParserComponent;

/**
 * Created by nirmal on 3/26/17.
 */
public abstract class ExpressionVariablesDao {
    public abstract BaseParserComponent getReferredExpressionBPC(String expression);
}
