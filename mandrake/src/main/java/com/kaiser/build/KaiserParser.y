%{
import java.io.*;
	import java.util.*;
	import java.util.ArrayList;
	import java.util.List;

    import com.google.gson.Gson;
    import com.kaiser.dao.ExpressionVariablesDao;
	import com.kaiser.lex.KaiserKeyWords;
    import com.kaiser.lex.Tokenizer;
    import com.kaiser.lex.TokenModel;

    import com.kaiser.exceptions.InvalidExpressionException;
	import com.kaiser.eval.KaiserParserFactory;
	import com.kaiser.models.expression.CombinedExpressionBPC;
    import com.kaiser.models.followsExpression.ContainsEntityBPC;
    import com.kaiser.models.followsExpression.SequenceExpressionBPC;
    import com.kaiser.models.followsExpression.SequenceExpressionCoreBPC;
    import com.kaiser.models.followsExpression.MetaPropertyBPC;
    import com.kaiser.models.relationalexpression.RelationalExpressionBPC;
	import com.kaiser.models.reference.*;
	import com.kaiser.models.*;
%}

/* YACC Declarations */
%token variable operator list_operator brace_open brace_close square_bracket_open square_bracket_close
mandatory occurrence optional one_or_more zero_or_more follows contains sequence if_token parentheses
alias double_square_open double_square_close nonTerminal condition expression_combinator meta
%left logical_op
%right negation_op

/* Grammar follows */
%%

INPUT: /* empty string */
 | INPUT LINE
 ;

LINE: '\n'
 | COMPLETE_EXPRESSION '\n' {
				inferenceRule = (BaseParserComponent) $1.obj;
				printDebugMessage(" Rule Matched ");
			}
 ;

COMPLETE_EXPRESSION: if_token EXPR {
				$$ = new KaiserParserVal(KaiserParserFactory.getBaseExpressionBPC((BaseParserComponent) $2.obj));
				}
 ;

EXPR: BASE_EXPR {
				printDebugMessage("Base Expression Rule Matched");
				}
 | COMBINED_EXPR {
                printDebugMessage("Combined Expressions Rule Matched");
                }
 | EXPR logical_op EXPR {
 				$$ = new KaiserParserVal(KaiserParserFactory.getLogicalBPC((BaseParserComponent)$1.obj, (BaseParserComponent) $3.obj, $2.sval));
 				printDebugMessage("EXPR & EXPR");
 				}
 | negation_op EXPR     {
				$$ = new KaiserParserVal(KaiserParserFactory.getNegationBPC((BaseParserComponent)$2.obj));
                printDebugMessage("! EXPR");
 				}
 | '(' EXPR ')' {
 				$$ = new KaiserParserVal((BaseParserComponent)$2.obj);
 				printDebugMessage("EXPR");
 				}
 ;


COMBINED_EXPR: expression_combinator '(' BASE_EXPR_LIST ')' {
                $$ = new KaiserParserVal(new CombinedExpressionBPC($1.sval, (ArrayList) $3.obj));
                printDebugMessage("COMBINED_EXPR");
                }


BASE_EXPR_LIST: BASE_EXPR {
                    List<BaseParserComponent> expressions = new ArrayList();
                    expressions.add((BaseParserComponent) $1.obj);
                    $$ = new KaiserParserVal(expressions);
                }
| BASE_EXPR_LIST BASE_EXPR {
                    List<BaseParserComponent> expressions = (ArrayList)$1.obj;
                    expressions.add((BaseParserComponent) $2.obj);
                    $$ = new KaiserParserVal(expressions);
                }
;

BASE_EXPR: REL_EXPR {
                printDebugMessage("Relation Expression Rule Matched");
                }
| RULE_REFERENCE {
                printDebugMessage("Evaluator Reference Matched");
               }
| CONTAINS_EXPR {
                printDebugMessage("Contains Rule Matched");
                }
| SEQUENCE_EXPR {
                printDebugMessage(" Sequence Rule Matched ");
                }
;


REL_EXPR: brace_open variable REL_EXPR_LIST brace_close {
				$$ = new KaiserParserVal(KaiserParserFactory.getExpressionUnitBPC($2.sval,(ArrayList)$3.obj));
				}
 ;

REL_EXPR_LIST: REL_EXPR_CORE {
				RelationalExpressionBPC sequence = (RelationalExpressionBPC) $1.obj;
				List<RelationalExpressionBPC> expressions = new ArrayList<>();
				expressions.add(sequence);
				$$ = new KaiserParserVal(expressions);
				}
 | REL_EXPR_LIST REL_EXPR_CORE {
 				RelationalExpressionBPC sequence = (RelationalExpressionBPC) $2.obj;
 				List<RelationalExpressionBPC> expressions = (ArrayList) $1.obj;
 				expressions.add(sequence);
 				$$ = new KaiserParserVal(expressions);
 				}
 ;

REL_EXPR_CORE: square_bracket_open variable operator variable square_bracket_close {
 				$$ = new KaiserParserVal(KaiserParserFactory.getRelationalExpressionBPC($2.sval, $3.sval, $4.sval));
                printDebugMessage("New Expression unit: Tag-" + $2.sval + ", op-" + $3.sval +", val-" + $4.dval);
				}
 | double_square_open variable operator variable double_square_close {
 				RelationalExpressionBPC sequence = KaiserParserFactory.getRelationalExpressionBPC($2.sval, $3.sval, $4.sval);
 				sequence.setIsPivotal(true);
 				$$ = new KaiserParserVal(sequence);
 				printDebugMessage("New Pivotal Expression unit: Tag-" + $2.sval + ", op-" + $3.sval +", val-" + $4.dval);
 				}
 ;


//--------------------Contains Expression Rules -------------------------------------//

CONTAINS_EXPR: contains square_bracket_open CONTAINS_EXPR_MANDATORY square_bracket_close {
				ParserBPC mandatory = (ParserBPC) $3.obj;
				$$ = new KaiserParserVal(KaiserParserFactory.getContainsExpressionBPC(mandatory));
				printDebugMessage(" CONTAINS_EXPR - CONTAINS_EXPR_MANDATORY ");
				}
| contains square_bracket_open CONTAINS_EXPR_OPTIONAL square_bracket_close {
				List<ParserBPC> expressions = (ArrayList) $3.obj;
				$$ = new KaiserParserVal(KaiserParserFactory.getContainsExpressionBPC(expressions));
				printDebugMessage(" CONTAINS_EXPR - CONTAINS_EXPR_OPTIONAL ");
				}
| contains square_bracket_open CONTAINS_EXPR_MANDATORY CONTAINS_EXPR_OPTIONAL square_bracket_close {
				ParserBPC mandatory = (ParserBPC) $3.obj;
				List<ParserBPC> expressions = (ArrayList) $4.obj;
				$$ = new KaiserParserVal(KaiserParserFactory.getContainsExpressionBPC(expressions, mandatory));
				printDebugMessage(" CONTAINS_EXPR - CONTAINS_EXPR_OPTIONAL + CONTAINS_EXPR_MANDATORY ");
				}
;

CONTAINS_EXPR_MANDATORY: mandatory brace_open CONTAINS_EXPR_LOGICAL_UNIT brace_close {
				$$ = new KaiserParserVal($3.obj);
				printDebugMessage(" CONTAINS_EXPR_MANDATORY ");
				}
 ;

CONTAINS_EXPR_LOGICAL_UNIT: CONTAINS_PROPERTY {
				printDebugMessage(" CONTAINS_EXPR_LOGICAL_UNIT ");
				}
 | CONTAINS_EXPR_LOGICAL_UNIT logical_op CONTAINS_EXPR_LOGICAL_UNIT {
 				$$ = new KaiserParserVal(KaiserParserFactory.getContainsLogicalBPC((ParserBPC)$1.obj, (ParserBPC) $3.obj, $2.sval));
				printDebugMessage(" CONTAINS_EXPR_LOGICAL_UNIT &  ");
 				}
 | '(' CONTAINS_EXPR_LOGICAL_UNIT ')' {
 				$$ = new KaiserParserVal((ParserBPC)$2.obj);
				printDebugMessage(" (CONTAINS_EXPR_LOGICAL_UNIT) ");
 				}
 | negation_op CONTAINS_EXPR_LOGICAL_UNIT {
 				$$ = new KaiserParserVal(KaiserParserFactory.getNotContainsBPC((ParserBPC)$2.obj));
				printDebugMessage(" !CONTAINS_EXPR_LOGICAL_UNIT ");
 				}
 ;

CONTAINS_EXPR_OPTIONAL: optional brace_open CONTAINS_EXPR_LIST brace_close {
				$$ = new KaiserParserVal($3.obj);
				printDebugMessage(" CONTAINS_EXPR_OPTIONAL ");
				}
;

CONTAINS_EXPR_LIST: CONTAINS_PROPERTY {
				ParserBPC sequence = (ParserBPC) $1.obj;
				List<ParserBPC> expressions = new ArrayList<>();
				expressions.add(sequence);
				$$ = new KaiserParserVal(expressions);
				printDebugMessage(" CONTAINS_EXPR_LIST  - CONTAINS_PROPERTY ");
 				}
 | CONTAINS_EXPR_LIST CONTAINS_PROPERTY {
 				ParserBPC sequence = (ParserBPC) $2.obj;
 				List<ParserBPC> expressions = (ArrayList) $1.obj;
 				expressions.add(sequence);
 				$$ = new KaiserParserVal(expressions);
 				printDebugMessage(" CONTAINS_EXPR_LIST  ");
 				}
 ;

CONTAINS_PROPERTY: CONTAINS_ENTITY {
				printDebugMessage(" CONTAINS_PROPERTY ");
 				}
 | SEQUENCE_EXPR {
 				printDebugMessage("SEQUENCE EXPRESSION AS CONTAINS_PROPERTY");
 				}
 ;


//---------------------------- Sequence -----------------------------------//

SEQUENCE_EXPR: sequence square_bracket_open SEQUENCE_EXPR_CORE square_bracket_close {
					$$ = new KaiserParserVal(new SequenceExpressionBPC((ParserBPC) $3.obj));
					printDebugMessage(" SEQUENCE_EXPR - SEQUENCE[SEQUENCE_EXPR_CORE]");
				}
 | sequence square_bracket_open CONTAINS_ENTITY_WITH_PROPS square_bracket_close {
                    $$ = new KaiserParserVal(new SequenceExpressionBPC((ParserBPC) $3.obj));
                    printDebugMessage(" SEQUENCE_EXPR - SEQUENCE[CONTAINS_ENTITY_WITH_PROPS]");
                }
 ;


SEQUENCE_EXPR_CORE: CONTAINS_ENTITY_WITH_PROPS FOLLOWS_EXPR CONTAINS_ENTITY_WITH_PROPS {
                $$ = new KaiserParserVal(new SequenceExpressionCoreBPC((ParserBPC) $1.obj, $2.ival, (ParserBPC)$3.obj));
                printDebugMessage(" SEQUENCE_EXPR_CORE :: CONTAINS_ENTITY_WITH_PROPS -> CONTAINS_ENTITY_WITH_PROPS ");
                }
 | SEQUENCE_EXPR_CORE FOLLOWS_EXPR CONTAINS_ENTITY_WITH_PROPS {
                $$ = new KaiserParserVal(new SequenceExpressionCoreBPC((ParserBPC) $1.obj, $2.ival, (ParserBPC)$3.obj));
                printDebugMessage(" SEQUENCE_EXPR_CORE  :: SEQUENCE_EXPR_CORE -> CONTAINS_ENTITY_WITH_PROPS");
                }
;

FOLLOWS_EXPR: follows {
				$$ = new KaiserParserVal(1);
				printDebugMessage(" follows - 1 ");
				}
| follows '(' variable ')' {
				int offsetWindowSize = Integer.parseInt($3.sval);
				$$ = new KaiserParserVal(offsetWindowSize);
				printDebugMessage(" follows - number ");

				}
;


CONTAINS_ENTITY_WITH_PROPS: occurrence CONTAINS_ENTITY {
                ParserBPC containsEntity = (ParserBPC) $2.obj;
                containsEntity.setOccurrence($1.sval);
                $$ = new KaiserParserVal(containsEntity);
                printDebugMessage(" CONTAINS_ENTITY_WITH_PROPS - optional ");
                }
| CONTAINS_ENTITY {
                printDebugMessage(" CONTAINS_ENTITY_WITH_PROPS");
                }
;

CONTAINS_ENTITY: brace_open variable brace_close {
				$$ = new KaiserParserVal(new ContainsEntityBPC($2.sval, null, null));
				printDebugMessage(" CONTAINS_ENTITY " + $2.sval);
				}
| brace_open variable meta META_PROPERTY_LIST brace_close {
                List<MetaPropertyBPC> metaProperties = (ArrayList) $4.obj;
                $$ = new KaiserParserVal(new ContainsEntityBPC($2.sval, null, metaProperties));
                printDebugMessage(" CONTAINS_ENTITY " + $2.sval + " with Meta Properties");
                }
| brace_open variable alias variable brace_close {
  				$$ = new KaiserParserVal(new ContainsEntityBPC($2.sval, $4.sval, null));
  				printDebugMessage(" CONTAINS_ENTITY - with visible name " + $2.sval + "-" + $4.sval);
  				}
| brace_open variable alias variable meta META_PROPERTY_LIST brace_close {
                List<MetaPropertyBPC> metaProperties = (ArrayList) $6.obj;
                $$ = new KaiserParserVal(new ContainsEntityBPC($2.sval, $4.sval, metaProperties));
                printDebugMessage(" CONTAINS_ENTITY " + $2.sval + " with Meta Properties");
                }
| brace_open meta META_PROPERTY_LIST brace_close {
                List<MetaPropertyBPC> metaProperties = (ArrayList) $3.obj;
                $$ = new KaiserParserVal(new ContainsEntityBPC(metaProperties));
                printDebugMessage(" CONTAINS_ENTITY with only Meta Properties");
                }
| PARSER_REFERENCE {
                printDebugMessage(" CONTAINS_ENTITY - parser reference");
                }
;

META_PROPERTY_LIST: META_PROPERTY {
                List<MetaPropertyBPC> metaProperties = new ArrayList();
                metaProperties.add((MetaPropertyBPC) $1.obj);
                $$ = new KaiserParserVal(metaProperties);
                }
| META_PROPERTY_LIST META_PROPERTY {
                List<MetaPropertyBPC> metaProperties = (ArrayList) $1.obj;
                metaProperties.add((MetaPropertyBPC) $2.obj);
                $$ = new KaiserParserVal(metaProperties);
                }
;

META_PROPERTY: brace_open variable operator variable brace_close {
                $$ = new KaiserParserVal(new MetaPropertyBPC($2.sval, $3.sval, $4.sval));
                printDebugMessage("META_PROPERTY - " + $2.sval + " " + $3.sval + " " + $4.sval);
                }
| brace_open variable list_operator '(' VARIABLE_LIST ')' brace_close {
                  $$ = new KaiserParserVal(new MetaPropertyBPC($2.sval, $3.sval, $5.obj));
                  printDebugMessage("META_PROPERTY - " + $2.sval + " " + $3.sval + " " + $4.sval);
                  }
;

VARIABLE_LIST: variable {
                List<String> variables = new ArrayList();
                variables.add($1.sval);
                $$ = new KaiserParserVal(variables);
                printDebugMessage("VARIABLE - " + $1.sval);
                }
| VARIABLE_LIST variable {
                List<String> variables = (ArrayList) $1.obj;
                variables.add($2.sval);
                $$ = new KaiserParserVal(variables);
                printDebugMessage("VARIABLE LIST - " + variables.toString());
}
;

// ----------------------------------- References ----------------------------------------//


PARSER_REFERENCE: nonTerminal brace_open variable brace_close {
				$$ = new KaiserParserVal(new ParserReferenceBPC($3.sval, null, dao));
				printDebugMessage("PARSER_REFERENCE - " + $3.sval);
				}
| nonTerminal brace_open variable alias variable brace_close {
  				$$ = new KaiserParserVal(new ParserReferenceBPC($3.sval, $5.sval, dao));
  				printDebugMessage("PARSER_REFERENCE - " + $3.sval);
  				}


RULE_REFERENCE: condition brace_open variable brace_close {
                $$ = new KaiserParserVal(new EvaluatorReferenceBPC($3.sval, dao));
                printDebugMessage("External Rule Referred-" + $3.sval);
                }

// ----------------------------------- Productions Finishes here ----------------------------------------//



%%


private String ins;
private List<TokenModel> textTokens;
private int tokenIndex = 0;
private BaseParserComponent inferenceRule;
private boolean debugMode = false;
private ExpressionVariablesDao dao = null;

void yyerror(String s) throws InvalidExpressionException{
	System.out.println("par:"+s);
	throw new InvalidExpressionException();
}

boolean newline;
int yylex() {
	String token;
	int tok=-1;

 	if (tokenIndex >= textTokens.size()) {
 		if (!newline) {
 			newline=true;
 			return '\n'; //So we look like classic YACC example
 		}
 		else {
 			return 0;
 		}
 	}

	TokenModel tokenModel = textTokens.get(tokenIndex);
 	token = tokenModel.getToken();
 	tok = tokenModel.getType();
 	tokenIndex++;
 	yylval = new KaiserParserVal(token);
 	if(tok == parentheses) {
 		tok = token.charAt(0);
 	}

	return tok;
 }

private Tokenizer getTokenizer() {
	Tokenizer tokenizer = new Tokenizer();
	tokenizer.setVariableToken(variable);
	tokenizer.addLangKeyWords(operator, Arrays.asList(KaiserKeyWords.OPERATOR_EQUALS,
                                KaiserKeyWords.OPERATOR_LESS_THAN,
                                KaiserKeyWords.OPERATOR_GREATER_THAN,
                                KaiserKeyWords.OPERATOR_LESS_THAN_OR_EQUALS,
                                KaiserKeyWords.OPERATOR_IS_ANY_OF,
                                KaiserKeyWords.OPERATOR_GREATER_THAN_OR_EQUALS));
    tokenizer.addLangKeyWords(list_operator, Arrays.asList(
                                    KaiserKeyWords.OPERATOR_IS_ANY_OF));
	tokenizer.addLangKeyWords(logical_op, Arrays.asList( KaiserKeyWords.LOGICAL_AND, KaiserKeyWords.LOGICAL_OR));
	tokenizer.addLangKeyWords(negation_op, Arrays.asList(KaiserKeyWords.LOGICAL_NOT));
	tokenizer.addLangKeyWords(mandatory, Arrays.asList(KaiserKeyWords.TOKEN_MANDATORY));
	tokenizer.addLangKeyWords(nonTerminal, Arrays.asList(KaiserKeyWords.TOKEN_NON_TERMINAL));
	tokenizer.addLangKeyWords(condition, Arrays.asList(KaiserKeyWords.TOKEN_CONDITION));
	tokenizer.addLangKeyWords(occurrence, Arrays.asList(KaiserKeyWords.TOKEN_ONE_OR_MORE,
	                                                    KaiserKeyWords.TOKEN_ZERO_OR_MORE,
	                                                    KaiserKeyWords.TOKEN_NONE,
	                                                    KaiserKeyWords.TOKEN_ZERO_OR_ONE));
	tokenizer.addLangKeyWords(optional, Arrays.asList(KaiserKeyWords.TOKEN_OPTIONAL));
    //tokenizer.addLangKeyWords(one_or_more, Arrays.asList(KaiserKeyWords.TOKEN_ONE_OR_MORE));
    //tokenizer.addLangKeyWords(zero_or_more, Arrays.asList(KaiserKeyWords.TOKEN_ZERO_OR_MORE));
	tokenizer.addLangKeyWords(contains, Arrays.asList(KaiserKeyWords.TOKEN_CONTAINS));
	tokenizer.addLangKeyWords(sequence, Arrays.asList(KaiserKeyWords.TOKEN_SEQUENCE));
	tokenizer.addLangKeyWords(if_token, Arrays.asList(KaiserKeyWords.TOKEN_IF));
	tokenizer.addLangKeyWords(meta, Arrays.asList(KaiserKeyWords.TOKEN_META));
    tokenizer.addLangKeyWords(follows, Arrays.asList(KaiserKeyWords.SYMBOL_FOLLOWS));
	tokenizer.addLangKeyWords(brace_open, Arrays.asList(KaiserKeyWords.SYMBOL_BRACE_OPEN));
	tokenizer.addLangKeyWords(brace_close, Arrays.asList(KaiserKeyWords.SYMBOL_BRACE_CLOSE));
	tokenizer.addLangKeyWords(square_bracket_open, Arrays.asList(KaiserKeyWords.SYMBOL_SQUARE_BRACKET_OPEN));
	tokenizer.addLangKeyWords(square_bracket_close, Arrays.asList(KaiserKeyWords.SYMBOL_SQUARE_BRACKET_CLOSE));
	tokenizer.addLangKeyWords(alias, Arrays.asList(KaiserKeyWords.TOKEN_ALIAS));
	tokenizer.addLangKeyWords(double_square_open, Arrays.asList(KaiserKeyWords.SYMBOL_DOUBLE_SQUARE_OPEN));
	tokenizer.addLangKeyWords(double_square_close, Arrays.asList(KaiserKeyWords.SYMBOL_DOUBLE_SQUARE_CLOSE));
	tokenizer.addLangKeyWords(expression_combinator, Arrays.asList(KaiserKeyWords.TOKEN_COMBINED_EXPRESSION_ANY,
                                    KaiserKeyWords.TOKEN_COMBINED_EXPRESSION_ALL));
	tokenizer.addLangKeyWords(parentheses, Arrays.asList("(", ")"));

	tokenizer.build();
	return tokenizer;
}

private void doTest() throws InvalidExpressionException{
	Tokenizer tokenizer = getTokenizer();

	BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	while (true) {
		try {
			ins = in.readLine();
		} catch (Exception e) {}
		textTokens = tokenizer.tokenize(ins);
		tokenIndex = 0;
		newline=false;
		yyparse();

        System.out.println(new Gson().toJson(inferenceRule));
		System.out.println(inferenceRule.generateExpression());
	}
}

private void printDebugMessage(String text) {
	if(debugMode) {
		System.out.println(text);
	}
}

public BaseParserComponent parseRule(String rule) throws InvalidExpressionException{
	Tokenizer tokenizer = getTokenizer();
	textTokens = tokenizer.tokenize(rule);
	tokenIndex = 0;
    newline=false;
    yyparse();
    return inferenceRule;
}



public static void main(String args[]) {
	KaiserParser parser = new KaiserParser();
	parser.setDebugModeTrue();
	try {
 		parser.doTest();
 	} catch (Exception ex) {
 		ex.printStackTrace();
 	}
}

public KaiserParser(ExpressionVariablesDao dao) {
    this.dao = dao;
    this.setDebugModeTrue();
}

public void setDebugModeTrue() {
	this.debugMode = true;
}
