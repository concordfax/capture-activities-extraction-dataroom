//### This file created by BYACC 1.8(/Java extension  1.15)
//### Java capabilities added 7 Jan 97, Bob Jamison
//### Updated : 27 Nov 97  -- Bob Jamison, Joe Nieten
//###           01 Jan 98  -- Bob Jamison -- fixed generic semantic constructor
//###           01 Jun 99  -- Bob Jamison -- added Runnable support
//###           06 Aug 00  -- Bob Jamison -- made state variables class-global
//###           03 Jan 01  -- Bob Jamison -- improved flags, tracing
//###           16 May 01  -- Bob Jamison -- added custom stack sizing
//###           04 Mar 02  -- Yuval Oren  -- improved java performance, added options
//###           14 Mar 02  -- Tomas Hurka -- -d support, static initializer workaround
//### Please send bug reports to tom@hukatronic.cz
//### static char yysccsid[] = "@(#)yaccpar	1.8 (Berkeley) 01/20/90";



package com.kaiser.build;



//#line 2 "KaiserParser.y"
import java.io.*;
	import java.util.*;
	import java.util.ArrayList;
	import java.util.List;

import com.auxolabs.capture.mandrake.utils.RuleProcessorFactory;
import com.google.gson.Gson;
import com.kaiser.lex.KaiserKeyWords;
    import com.kaiser.lex.Tokenizer;
    import com.kaiser.lex.TokenModel;

    import com.kaiser.exceptions.InvalidExpressionException;
	import com.kaiser.eval.KaiserParserFactory;
	import com.kaiser.models.expression.CombinedExpressionBPC;
    import com.kaiser.models.followsExpression.ContainsEntityBPC;
    import com.kaiser.models.followsExpression.SequenceExpressionBPC;
    import com.kaiser.models.followsExpression.SequenceExpressionCoreBPC;
    import com.kaiser.models.followsExpression.MetaPropertyBPC;
    import com.kaiser.models.relationalexpression.RelationalExpressionBPC;
	import com.kaiser.models.reference.*;
	import com.kaiser.models.*;
//#line 39 "KaiserParser.java"




public class KaiserParser
{

boolean yydebug;        //do I want debug output?
int yynerrs;            //number of errors so far
int yyerrflag;          //was there an error?
int yychar;             //the current working character

//########## MESSAGES ##########
//###############################################################
// method: debug
//###############################################################
void debug(String msg)
{
  if (yydebug)
    System.out.println(msg);
}

//########## STATE STACK ##########
final static int YYSTACKSIZE = 500;  //maximum stack size
int statestk[] = new int[YYSTACKSIZE]; //state stack
int stateptr;
int stateptrmax;                     //highest index of stackptr
int statemax;                        //state when highest index reached
//###############################################################
// methods: state stack push,pop,drop,peek
//###############################################################
final void state_push(int state)
{
  try {
		stateptr++;
		statestk[stateptr]=state;
	 }
	 catch (ArrayIndexOutOfBoundsException e) {
     int oldsize = statestk.length;
     int newsize = oldsize * 2;
     int[] newstack = new int[newsize];
     System.arraycopy(statestk,0,newstack,0,oldsize);
     statestk = newstack;
     statestk[stateptr]=state;
  }
}
final int state_pop()
{
  return statestk[stateptr--];
}
final void state_drop(int cnt)
{
  stateptr -= cnt; 
}
final int state_peek(int relative)
{
  return statestk[stateptr-relative];
}
//###############################################################
// method: init_stacks : allocate and prepare stacks
//###############################################################
final boolean init_stacks()
{
  stateptr = -1;
  val_init();
  return true;
}
//###############################################################
// method: dump_stacks : show n levels of the stacks
//###############################################################
void dump_stacks(int count)
{
int i;
  System.out.println("=index==state====value=     s:"+stateptr+"  v:"+valptr);
  for (i=0;i<count;i++)
    System.out.println(" "+i+"    "+statestk[i]+"      "+valstk[i]);
  System.out.println("======================");
}


//########## SEMANTIC VALUES ##########
//public class KaiserParserVal is defined in KaiserParserVal.java


String   yytext;//user variable to return contextual strings
KaiserParserVal yyval; //used to return semantic vals from action routines
KaiserParserVal yylval;//the 'lval' (result) I got from yylex()
KaiserParserVal valstk[];
int valptr;
//###############################################################
// methods: value stack push,pop,drop,peek.
//###############################################################
void val_init()
{
  valstk=new KaiserParserVal[YYSTACKSIZE];
  yyval=new KaiserParserVal();
  yylval=new KaiserParserVal();
  valptr=-1;
}
void val_push(KaiserParserVal val)
{
  if (valptr>=YYSTACKSIZE)
    return;
  valstk[++valptr]=val;
}
KaiserParserVal val_pop()
{
  if (valptr<0)
    return new KaiserParserVal();
  return valstk[valptr--];
}
void val_drop(int cnt)
{
int ptr;
  ptr=valptr-cnt;
  if (ptr<0)
    return;
  valptr = ptr;
}
KaiserParserVal val_peek(int relative)
{
int ptr;
  ptr=valptr-relative;
  if (ptr<0)
    return new KaiserParserVal();
  return valstk[ptr];
}
final KaiserParserVal dup_yyval(KaiserParserVal val)
{
  KaiserParserVal dup = new KaiserParserVal();
  dup.ival = val.ival;
  dup.dval = val.dval;
  dup.sval = val.sval;
  dup.obj = val.obj;
  return dup;
}
//#### end semantic value section ####
public final static short variable=257;
public final static short operator=258;
public final static short list_operator=259;
public final static short brace_open=260;
public final static short brace_close=261;
public final static short square_bracket_open=262;
public final static short square_bracket_close=263;
public final static short mandatory=264;
public final static short occurrence=265;
public final static short optional=266;
public final static short one_or_more=267;
public final static short zero_or_more=268;
public final static short follows=269;
public final static short contains=270;
public final static short sequence=271;
public final static short if_token=272;
public final static short parentheses=273;
public final static short alias=274;
public final static short double_square_open=275;
public final static short double_square_close=276;
public final static short nonTerminal=277;
public final static short condition=278;
public final static short expression_combinator=279;
public final static short meta=280;
public final static short logical_op=281;
public final static short negation_op=282;
public final static short YYERRCODE=256;
final static short yylhs[] = {                           -1,
    0,    0,    1,    1,    2,    3,    3,    3,    3,    3,
    5,    6,    6,    4,    4,    4,    4,    7,   11,   11,
   12,   12,    9,    9,    9,   13,   15,   15,   15,   15,
   14,   17,   17,   16,   16,   10,   10,   19,   19,   21,
   21,   20,   20,   18,   18,   18,   18,   18,   18,   22,
   22,   24,   24,   25,   25,   23,   23,    8,
};
final static short yylen[] = {                            2,
    0,    2,    1,    2,    2,    1,    1,    3,    2,    3,
    4,    1,    2,    1,    1,    1,    1,    4,    1,    2,
    5,    5,    4,    4,    5,    4,    1,    3,    3,    2,
    4,    1,    2,    1,    1,    4,    4,    3,    3,    1,
    4,    2,    1,    3,    5,    5,    7,    4,    1,    1,
    2,    5,    7,    1,    2,    4,    6,    4,
};
final static short yydefred[] = {                         1,
    0,    0,    3,    2,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    6,    7,   14,   15,   16,   17,    4,
    0,    0,    0,    0,    0,    9,    0,    0,    0,    0,
    0,   19,    0,    0,    0,    0,    0,    0,    0,   43,
    0,    0,   49,    0,   12,    0,   10,    8,    0,    0,
   18,   20,    0,    0,   23,    0,   24,    0,    0,   42,
    0,   36,    0,    0,   37,    0,   58,   11,   13,    0,
    0,    0,    0,   35,    0,   27,   34,   32,    0,   25,
   44,    0,    0,    0,    0,   50,    0,    0,   39,   38,
    0,    0,   30,    0,   26,    0,   31,   33,    0,    0,
    0,   48,   51,   56,    0,    0,   21,   22,   29,   28,
   46,    0,   45,    0,    0,    0,   41,    0,    0,    0,
   57,   47,   52,   54,    0,   55,    0,   53,
};
final static short yydgoto[] = {                          1,
    4,    5,   13,   14,   15,   46,   16,   17,   18,   19,
   31,   32,   35,   36,   75,   76,   79,   77,   41,   42,
   64,   85,   43,   86,  125,
};
final static short yysindex[] = {                         0,
  -10,  -39,    0,    0,    8, -205, -235, -201, -191,   38,
  -39,  -39, -226,    0,    0,    0,    0,    0,    0,    0,
 -242, -199, -207, -165, -214,    0,  -34,  -39, -164, -163,
 -187,    0, -162, -161, -186, -168, -243, -230, -160,    0,
 -241, -231,    0, -159,    0,  -26,    0,    0, -157, -155,
    0,    0,  -36, -209,    0, -167,    0, -245, -156,    0,
 -152,    0,   57, -207,    0, -207,    0,    0,    0, -151,
 -150,  -36,  -36,    0, -240,    0,    0,    0, -211,    0,
    0, -149, -156, -148, -178,    0, -198, -147,    0,    0,
 -146, -158,    0,  -33,    0,  -36,    0,    0, -238, -176,
 -172,    0,    0,    0, -145,   70,    0,    0,    0,    0,
    0, -156,    0, -144,   74, -142,    0, -170, -141, -136,
    0,    0,    0,    0,  -38,    0, -139,    0,
};
final static short yyrindex[] = {                         0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,  105,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0, -206,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,
};
final static short yygindex[] = {                         0,
    0,    0,   61,   -6,    0,    0,    0,    0,    0,  -48,
    0,   85,    0,   88,  -60,  -45,    0,  -21,    0,   15,
   82,  -73,    0,  -74,    0,
};
final static int YYTABLESIZE=262;
static short yytable[];
static { yytable();}
static void yytable(){
yytable = new short[]{                          3,
   12,   40,  127,   73,   74,   74,   47,  109,   78,  100,
  103,   93,   94,   58,   68,   81,   60,   20,   45,   29,
   95,   62,  111,   74,   74,  103,   22,   63,   82,   37,
   74,   65,   30,   98,   83,  110,   59,   63,  118,   69,
   96,  112,   40,  103,   40,    6,   39,   74,   37,   97,
   37,   21,   37,   40,   28,    7,    8,   38,   40,    8,
   23,    8,  104,    9,   33,   39,   34,   39,   24,   39,
   40,   26,   27,   51,   29,  105,   55,   25,   89,   34,
   90,   84,  102,   84,  113,  114,  115,   30,   48,   84,
  122,   44,   49,   50,   57,   80,   88,   53,   54,   61,
   70,   67,   71,   84,   87,   91,   92,   99,  101,  106,
  117,  116,  119,  120,    5,   52,  107,  108,  121,  123,
  124,  128,   56,   66,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,  126,    0,
    6,    0,    0,   37,    0,    0,    0,    0,    0,    0,
    7,    8,    0,    6,    8,    0,    0,    0,    9,   10,
   39,    0,   11,    7,    8,   72,   28,   96,    0,    0,
    0,    9,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    2,
};
}
static short yycheck[];
static { yycheck(); }
static void yycheck() {
yycheck = new short[] {                         10,
   40,   23,   41,   40,   53,   54,   41,   41,   54,   83,
   85,   72,   73,  257,   41,  261,   38,   10,   25,  262,
  261,  263,  261,   72,   73,  100,  262,  269,  274,  260,
   79,  263,  275,   79,  280,   96,  280,  269,  112,   46,
  281,  280,   64,  118,   66,  260,  277,   96,  260,  261,
  260,  257,  260,  260,  281,  270,  271,  265,  265,  271,
  262,  271,  261,  278,  264,  277,  266,  277,  260,  277,
  277,   11,   12,  261,  262,  274,  263,   40,   64,  266,
   66,  260,  261,  260,  261,  258,  259,  275,   28,  260,
  261,  257,  257,  257,  263,  263,   40,  260,  260,  260,
  258,  261,  258,  260,  257,  257,  257,  257,  257,  257,
   41,  257,  257,   40,   10,   31,  263,  276,  261,  261,
  257,  261,   35,   42,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,  257,   -1,
  260,   -1,   -1,  260,   -1,   -1,   -1,   -1,   -1,   -1,
  270,  271,   -1,  260,  271,   -1,   -1,   -1,  278,  279,
  277,   -1,  282,  270,  271,  282,  281,  281,   -1,   -1,
   -1,  278,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,  272,
};
}
final static short YYFINAL=1;
final static short YYMAXTOKEN=282;
final static String yyname[] = {
"end-of-file",null,null,null,null,null,null,null,null,null,"'\\n'",null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,"'('","')'",null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,"variable","operator","list_operator","brace_open",
"brace_close","square_bracket_open","square_bracket_close","mandatory",
"occurrence","optional","one_or_more","zero_or_more","follows","contains",
"sequence","if_token","parentheses","alias","double_square_open",
"double_square_close","nonTerminal","condition","expression_combinator","meta",
"logical_op","negation_op",
};
final static String yyrule[] = {
"$accept : INPUT",
"INPUT :",
"INPUT : INPUT LINE",
"LINE : '\\n'",
"LINE : COMPLETE_EXPRESSION '\\n'",
"COMPLETE_EXPRESSION : if_token EXPR",
"EXPR : BASE_EXPR",
"EXPR : COMBINED_EXPR",
"EXPR : EXPR logical_op EXPR",
"EXPR : negation_op EXPR",
"EXPR : '(' EXPR ')'",
"COMBINED_EXPR : expression_combinator '(' BASE_EXPR_LIST ')'",
"BASE_EXPR_LIST : BASE_EXPR",
"BASE_EXPR_LIST : BASE_EXPR_LIST BASE_EXPR",
"BASE_EXPR : REL_EXPR",
"BASE_EXPR : RULE_REFERENCE",
"BASE_EXPR : CONTAINS_EXPR",
"BASE_EXPR : SEQUENCE_EXPR",
"REL_EXPR : brace_open variable REL_EXPR_LIST brace_close",
"REL_EXPR_LIST : REL_EXPR_CORE",
"REL_EXPR_LIST : REL_EXPR_LIST REL_EXPR_CORE",
"REL_EXPR_CORE : square_bracket_open variable operator variable square_bracket_close",
"REL_EXPR_CORE : double_square_open variable operator variable double_square_close",
"CONTAINS_EXPR : contains square_bracket_open CONTAINS_EXPR_MANDATORY square_bracket_close",
"CONTAINS_EXPR : contains square_bracket_open CONTAINS_EXPR_OPTIONAL square_bracket_close",
"CONTAINS_EXPR : contains square_bracket_open CONTAINS_EXPR_MANDATORY CONTAINS_EXPR_OPTIONAL square_bracket_close",
"CONTAINS_EXPR_MANDATORY : mandatory brace_open CONTAINS_EXPR_LOGICAL_UNIT brace_close",
"CONTAINS_EXPR_LOGICAL_UNIT : CONTAINS_PROPERTY",
"CONTAINS_EXPR_LOGICAL_UNIT : CONTAINS_EXPR_LOGICAL_UNIT logical_op CONTAINS_EXPR_LOGICAL_UNIT",
"CONTAINS_EXPR_LOGICAL_UNIT : '(' CONTAINS_EXPR_LOGICAL_UNIT ')'",
"CONTAINS_EXPR_LOGICAL_UNIT : negation_op CONTAINS_EXPR_LOGICAL_UNIT",
"CONTAINS_EXPR_OPTIONAL : optional brace_open CONTAINS_EXPR_LIST brace_close",
"CONTAINS_EXPR_LIST : CONTAINS_PROPERTY",
"CONTAINS_EXPR_LIST : CONTAINS_EXPR_LIST CONTAINS_PROPERTY",
"CONTAINS_PROPERTY : CONTAINS_ENTITY",
"CONTAINS_PROPERTY : SEQUENCE_EXPR",
"SEQUENCE_EXPR : sequence square_bracket_open SEQUENCE_EXPR_CORE square_bracket_close",
"SEQUENCE_EXPR : sequence square_bracket_open CONTAINS_ENTITY_WITH_PROPS square_bracket_close",
"SEQUENCE_EXPR_CORE : CONTAINS_ENTITY_WITH_PROPS FOLLOWS_EXPR CONTAINS_ENTITY_WITH_PROPS",
"SEQUENCE_EXPR_CORE : SEQUENCE_EXPR_CORE FOLLOWS_EXPR CONTAINS_ENTITY_WITH_PROPS",
"FOLLOWS_EXPR : follows",
"FOLLOWS_EXPR : follows '(' variable ')'",
"CONTAINS_ENTITY_WITH_PROPS : occurrence CONTAINS_ENTITY",
"CONTAINS_ENTITY_WITH_PROPS : CONTAINS_ENTITY",
"CONTAINS_ENTITY : brace_open variable brace_close",
"CONTAINS_ENTITY : brace_open variable meta META_PROPERTY_LIST brace_close",
"CONTAINS_ENTITY : brace_open variable alias variable brace_close",
"CONTAINS_ENTITY : brace_open variable alias variable meta META_PROPERTY_LIST brace_close",
"CONTAINS_ENTITY : brace_open meta META_PROPERTY_LIST brace_close",
"CONTAINS_ENTITY : PARSER_REFERENCE",
"META_PROPERTY_LIST : META_PROPERTY",
"META_PROPERTY_LIST : META_PROPERTY_LIST META_PROPERTY",
"META_PROPERTY : brace_open variable operator variable brace_close",
"META_PROPERTY : brace_open variable list_operator '(' VARIABLE_LIST ')' brace_close",
"VARIABLE_LIST : variable",
"VARIABLE_LIST : VARIABLE_LIST variable",
"PARSER_REFERENCE : nonTerminal brace_open variable brace_close",
"PARSER_REFERENCE : nonTerminal brace_open variable alias variable brace_close",
"RULE_REFERENCE : condition brace_open variable brace_close",
};

//#line 345 "KaiserParser.y"


private String ins;
private List<TokenModel> textTokens;
private int tokenIndex = 0;
private BaseParserComponent inferenceRule;
private boolean debugMode = false;
private RuleProcessorFactory rulesRef = null;

void yyerror(String s) throws InvalidExpressionException{
	System.out.println("par:"+s);
	throw new InvalidExpressionException();
}

boolean newline;
int yylex() {
	String token;
	int tok=-1;

 	if (tokenIndex >= textTokens.size()) {
 		if (!newline) {
 			newline=true;
 			return '\n'; //So we look like classic YACC example
 		}
 		else {
 			return 0;
 		}
 	}

	TokenModel tokenModel = textTokens.get(tokenIndex);
 	token = tokenModel.getToken();
 	tok = tokenModel.getType();
 	tokenIndex++;
 	yylval = new KaiserParserVal(token);
 	if(tok == parentheses) {
 		tok = token.charAt(0);
 	}

	return tok;
 }

private Tokenizer getTokenizer() {
	Tokenizer tokenizer = new Tokenizer();
	tokenizer.setVariableToken(variable);
	tokenizer.addLangKeyWords(operator, Arrays.asList(KaiserKeyWords.OPERATOR_EQUALS,
                                KaiserKeyWords.OPERATOR_LESS_THAN,
                                KaiserKeyWords.OPERATOR_GREATER_THAN,
                                KaiserKeyWords.OPERATOR_LESS_THAN_OR_EQUALS,
                                KaiserKeyWords.OPERATOR_IS_ANY_OF,
                                KaiserKeyWords.OPERATOR_GREATER_THAN_OR_EQUALS));
    tokenizer.addLangKeyWords(list_operator, Arrays.asList(
                                    KaiserKeyWords.OPERATOR_IS_ANY_OF));
	tokenizer.addLangKeyWords(logical_op, Arrays.asList( KaiserKeyWords.LOGICAL_AND, KaiserKeyWords.LOGICAL_OR));
	tokenizer.addLangKeyWords(negation_op, Arrays.asList(KaiserKeyWords.LOGICAL_NOT));
	tokenizer.addLangKeyWords(mandatory, Arrays.asList(KaiserKeyWords.TOKEN_MANDATORY));
	tokenizer.addLangKeyWords(nonTerminal, Arrays.asList(KaiserKeyWords.TOKEN_NON_TERMINAL));
	tokenizer.addLangKeyWords(condition, Arrays.asList(KaiserKeyWords.TOKEN_CONDITION));
	tokenizer.addLangKeyWords(occurrence, Arrays.asList(KaiserKeyWords.TOKEN_ONE_OR_MORE,
	                                                    KaiserKeyWords.TOKEN_ZERO_OR_MORE,
	                                                    KaiserKeyWords.TOKEN_NONE,
	                                                    KaiserKeyWords.TOKEN_ZERO_OR_ONE));
	tokenizer.addLangKeyWords(optional, Arrays.asList(KaiserKeyWords.TOKEN_OPTIONAL));
    //tokenizer.addLangKeyWords(one_or_more, Arrays.asList(KaiserKeyWords.TOKEN_ONE_OR_MORE));
    //tokenizer.addLangKeyWords(zero_or_more, Arrays.asList(KaiserKeyWords.TOKEN_ZERO_OR_MORE));
	tokenizer.addLangKeyWords(contains, Arrays.asList(KaiserKeyWords.TOKEN_CONTAINS));
	tokenizer.addLangKeyWords(sequence, Arrays.asList(KaiserKeyWords.TOKEN_SEQUENCE));
	tokenizer.addLangKeyWords(if_token, Arrays.asList(KaiserKeyWords.TOKEN_IF));
	tokenizer.addLangKeyWords(meta, Arrays.asList(KaiserKeyWords.TOKEN_META));
    tokenizer.addLangKeyWords(follows, Arrays.asList(KaiserKeyWords.SYMBOL_FOLLOWS));
	tokenizer.addLangKeyWords(brace_open, Arrays.asList(KaiserKeyWords.SYMBOL_BRACE_OPEN));
	tokenizer.addLangKeyWords(brace_close, Arrays.asList(KaiserKeyWords.SYMBOL_BRACE_CLOSE));
	tokenizer.addLangKeyWords(square_bracket_open, Arrays.asList(KaiserKeyWords.SYMBOL_SQUARE_BRACKET_OPEN));
	tokenizer.addLangKeyWords(square_bracket_close, Arrays.asList(KaiserKeyWords.SYMBOL_SQUARE_BRACKET_CLOSE));
	tokenizer.addLangKeyWords(alias, Arrays.asList(KaiserKeyWords.TOKEN_ALIAS));
	tokenizer.addLangKeyWords(double_square_open, Arrays.asList(KaiserKeyWords.SYMBOL_DOUBLE_SQUARE_OPEN));
	tokenizer.addLangKeyWords(double_square_close, Arrays.asList(KaiserKeyWords.SYMBOL_DOUBLE_SQUARE_CLOSE));
	tokenizer.addLangKeyWords(expression_combinator, Arrays.asList(KaiserKeyWords.TOKEN_COMBINED_EXPRESSION_ANY,
                                    KaiserKeyWords.TOKEN_COMBINED_EXPRESSION_ALL));
	tokenizer.addLangKeyWords(parentheses, Arrays.asList("(", ")"));

	tokenizer.build();
	return tokenizer;
}

private void doTest() throws InvalidExpressionException{
	Tokenizer tokenizer = getTokenizer();

	BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	while (true) {
		try {
			ins = in.readLine();
		} catch (Exception e) {}
		textTokens = tokenizer.tokenize(ins);
		tokenIndex = 0;
		newline=false;
		yyparse();

        System.out.println(new Gson().toJson(inferenceRule));
		System.out.println(inferenceRule.generateExpression());
	}
}

private void printDebugMessage(String text) {
	if(debugMode) {
		System.out.println(text);
	}
}

public BaseParserComponent parseRule(String rule) throws InvalidExpressionException{
	Tokenizer tokenizer = getTokenizer();
	textTokens = tokenizer.tokenize(rule);
	tokenIndex = 0;
    newline=false;
    yyparse();
    return inferenceRule;
}



public static void main(String args[]) {
	KaiserParser parser = new KaiserParser();
	parser.setDebugModeTrue();
	try {
 		parser.doTest();
 	} catch (Exception ex) {
 		ex.printStackTrace();
 	}
}

public KaiserParser(RuleProcessorFactory ruleProcessorFactory) {
    this.rulesRef = ruleProcessorFactory;
//    this.setDebugModeTrue();
}

public void setDebugModeTrue() {
	this.debugMode = true;
}
//#line 499 "KaiserParser.java"
//###############################################################
// method: yylexdebug : check lexer state
//###############################################################
void yylexdebug(int state,int ch)
{
String s=null;
  if (ch < 0) ch=0;
  if (ch <= YYMAXTOKEN) //check index bounds
     s = yyname[ch];    //now get it
  if (s==null)
    s = "illegal-symbol";
  debug("state "+state+", reading "+ch+" ("+s+")");
}





//The following are now global, to aid in error reporting
int yyn;       //next next thing to do
int yym;       //
int yystate;   //current parsing state from state table
String yys;    //current token string


//###############################################################
// method: yyparse : parse input and execute indicated items
//###############################################################
int yyparse() throws InvalidExpressionException
{
boolean doaction;
  init_stacks();
  yynerrs = 0;
  yyerrflag = 0;
  yychar = -1;          //impossible char forces a read
  yystate=0;            //initial state
  state_push(yystate);  //save it
  val_push(yylval);     //save empty value
  while (true) //until parsing is done, either correctly, or w/error
    {
    doaction=true;
    if (yydebug) debug("loop"); 
    //#### NEXT ACTION (from reduction table)
    for (yyn=yydefred[yystate];yyn==0;yyn=yydefred[yystate])
      {
      if (yydebug) debug("yyn:"+yyn+"  state:"+yystate+"  yychar:"+yychar);
      if (yychar < 0)      //we want a char?
        {
        yychar = yylex();  //get next token
        if (yydebug) debug(" next yychar:"+yychar);
        //#### ERROR CHECK ####
        if (yychar < 0)    //it it didn't work/error
          {
          yychar = 0;      //change it to default string (no -1!)
          if (yydebug)
            yylexdebug(yystate,yychar);
          }
        }//yychar<0
      yyn = yysindex[yystate];  //get amount to shift by (shift index)
      if ((yyn != 0) && (yyn += yychar) >= 0 &&
          yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
        {
        if (yydebug)
          debug("state "+yystate+", shifting to state "+yytable[yyn]);
        //#### NEXT STATE ####
        yystate = yytable[yyn];//we are in a new state
        state_push(yystate);   //save it
        val_push(yylval);      //push our lval as the input for next rule
        yychar = -1;           //since we have 'eaten' a token, say we need another
        if (yyerrflag > 0)     //have we recovered an error?
           --yyerrflag;        //give ourselves credit
        doaction=false;        //but don't process yet
        break;   //quit the yyn=0 loop
        }

    yyn = yyrindex[yystate];  //reduce
    if ((yyn !=0 ) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
      {   //we reduced!
      if (yydebug) debug("reduce");
      yyn = yytable[yyn];
      doaction=true; //get ready to execute
      break;         //drop down to actions
      }
    else //ERROR RECOVERY
      {
      if (yyerrflag==0)
        {
        yyerror("syntax error");
        yynerrs++;
        }
      if (yyerrflag < 3) //low error count?
        {
        yyerrflag = 3;
        while (true)   //do until break
          {
          if (stateptr<0)   //check for under & overflow here
            {
            yyerror("stack underflow. aborting...");  //note lower case 's'
            return 1;
            }
          yyn = yysindex[state_peek(0)];
          if ((yyn != 0) && (yyn += YYERRCODE) >= 0 &&
                    yyn <= YYTABLESIZE && yycheck[yyn] == YYERRCODE)
            {
            if (yydebug)
              debug("state "+state_peek(0)+", error recovery shifting to state "+yytable[yyn]+" ");
            yystate = yytable[yyn];
            state_push(yystate);
            val_push(yylval);
            doaction=false;
            break;
            }
          else
            {
            if (yydebug)
              debug("error recovery discarding state "+state_peek(0)+" ");
            if (stateptr<0)   //check for under & overflow here
              {
              yyerror("Stack underflow. aborting...");  //capital 'S'
              return 1;
              }
            state_pop();
            val_pop();
            }
          }
        }
      else            //discard this token
        {
        if (yychar == 0)
          return 1; //yyabort
        if (yydebug)
          {
          yys = null;
          if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
          if (yys == null) yys = "illegal-symbol";
          debug("state "+yystate+", error recovery discards token "+yychar+" ("+yys+")");
          }
        yychar = -1;  //read another
        }
      }//end error recovery
    }//yyn=0 loop
    if (!doaction)   //any reason not to proceed?
      continue;      //skip action
    yym = yylen[yyn];          //get count of terminals on rhs
    if (yydebug)
      debug("state "+yystate+", reducing "+yym+" by rule "+yyn+" ("+yyrule[yyn]+")");
    if (yym>0)                 //if count of rhs not 'nil'
      yyval = val_peek(yym-1); //get current semantic value
    yyval = dup_yyval(yyval); //duplicate yyval if ParserVal is used as semantic value
    switch(yyn)
      {
//########## USER-SUPPLIED ACTIONS ##########
case 4:
//#line 40 "KaiserParser.y"
{
				inferenceRule = (BaseParserComponent) val_peek(1).obj;
				printDebugMessage(" Rule Matched ");
			}
break;
case 5:
//#line 46 "KaiserParser.y"
{
				yyval = new KaiserParserVal(KaiserParserFactory.getBaseExpressionBPC((BaseParserComponent) val_peek(0).obj));
				}
break;
case 6:
//#line 51 "KaiserParser.y"
{
				printDebugMessage("Base Expression Rule Matched");
				}
break;
case 7:
//#line 54 "KaiserParser.y"
{
                printDebugMessage("Combined Expressions Rule Matched");
                }
break;
case 8:
//#line 57 "KaiserParser.y"
{
 				yyval = new KaiserParserVal(KaiserParserFactory.getLogicalBPC((BaseParserComponent)val_peek(2).obj, (BaseParserComponent) val_peek(0).obj, val_peek(1).sval));
 				printDebugMessage("EXPR & EXPR");
 				}
break;
case 9:
//#line 61 "KaiserParser.y"
{
				yyval = new KaiserParserVal(KaiserParserFactory.getNegationBPC((BaseParserComponent)val_peek(0).obj));
                printDebugMessage("! EXPR");
 				}
break;
case 10:
//#line 65 "KaiserParser.y"
{
 				yyval = new KaiserParserVal((BaseParserComponent)val_peek(1).obj);
 				printDebugMessage("EXPR");
 				}
break;
case 11:
//#line 72 "KaiserParser.y"
{
                yyval = new KaiserParserVal(new CombinedExpressionBPC(val_peek(3).sval, (ArrayList) val_peek(1).obj));
                printDebugMessage("COMBINED_EXPR");
                }
break;
case 12:
//#line 78 "KaiserParser.y"
{
                    List<BaseParserComponent> expressions = new ArrayList();
                    expressions.add((BaseParserComponent) val_peek(0).obj);
                    yyval = new KaiserParserVal(expressions);
                }
break;
case 13:
//#line 83 "KaiserParser.y"
{
                    List<BaseParserComponent> expressions = (ArrayList)val_peek(1).obj;
                    expressions.add((BaseParserComponent) val_peek(0).obj);
                    yyval = new KaiserParserVal(expressions);
                }
break;
case 14:
//#line 90 "KaiserParser.y"
{
                printDebugMessage("Relation Expression Rule Matched");
                }
break;
case 15:
//#line 93 "KaiserParser.y"
{
                printDebugMessage("Evaluator Reference Matched");
               }
break;
case 16:
//#line 96 "KaiserParser.y"
{
                printDebugMessage("Contains Rule Matched");
                }
break;
case 17:
//#line 99 "KaiserParser.y"
{
                printDebugMessage(" Sequence Rule Matched ");
                }
break;
case 18:
//#line 105 "KaiserParser.y"
{
				yyval = new KaiserParserVal(KaiserParserFactory.getExpressionUnitBPC(val_peek(2).sval,(ArrayList)val_peek(1).obj));
				}
break;
case 19:
//#line 110 "KaiserParser.y"
{
				RelationalExpressionBPC sequence = (RelationalExpressionBPC) val_peek(0).obj;
				List<RelationalExpressionBPC> expressions = new ArrayList<>();
				expressions.add(sequence);
				yyval = new KaiserParserVal(expressions);
				}
break;
case 20:
//#line 116 "KaiserParser.y"
{
 				RelationalExpressionBPC sequence = (RelationalExpressionBPC) val_peek(0).obj;
 				List<RelationalExpressionBPC> expressions = (ArrayList) val_peek(1).obj;
 				expressions.add(sequence);
 				yyval = new KaiserParserVal(expressions);
 				}
break;
case 21:
//#line 124 "KaiserParser.y"
{
 				yyval = new KaiserParserVal(KaiserParserFactory.getRelationalExpressionBPC(val_peek(3).sval, val_peek(2).sval, val_peek(1).sval));
                printDebugMessage("New Expression unit: Tag-" + val_peek(3).sval + ", op-" + val_peek(2).sval +", val-" + val_peek(1).dval);
				}
break;
case 22:
//#line 128 "KaiserParser.y"
{
 				RelationalExpressionBPC sequence = KaiserParserFactory.getRelationalExpressionBPC(val_peek(3).sval, val_peek(2).sval, val_peek(1).sval);
 				sequence.setIsPivotal(true);
 				yyval = new KaiserParserVal(sequence);
 				printDebugMessage("New Pivotal Expression unit: Tag-" + val_peek(3).sval + ", op-" + val_peek(2).sval +", val-" + val_peek(1).dval);
 				}
break;
case 23:
//#line 139 "KaiserParser.y"
{
				ParserBPC mandatory = (ParserBPC) val_peek(1).obj;
				yyval = new KaiserParserVal(KaiserParserFactory.getContainsExpressionBPC(mandatory));
				printDebugMessage(" CONTAINS_EXPR - CONTAINS_EXPR_MANDATORY ");
				}
break;
case 24:
//#line 144 "KaiserParser.y"
{
				List<ParserBPC> expressions = (ArrayList) val_peek(1).obj;
				yyval = new KaiserParserVal(KaiserParserFactory.getContainsExpressionBPC(expressions));
				printDebugMessage(" CONTAINS_EXPR - CONTAINS_EXPR_OPTIONAL ");
				}
break;
case 25:
//#line 149 "KaiserParser.y"
{
				ParserBPC mandatory = (ParserBPC) val_peek(2).obj;
				List<ParserBPC> expressions = (ArrayList) val_peek(1).obj;
				yyval = new KaiserParserVal(KaiserParserFactory.getContainsExpressionBPC(expressions, mandatory));
				printDebugMessage(" CONTAINS_EXPR - CONTAINS_EXPR_OPTIONAL + CONTAINS_EXPR_MANDATORY ");
				}
break;
case 26:
//#line 157 "KaiserParser.y"
{
				yyval = new KaiserParserVal(val_peek(1).obj);
				printDebugMessage(" CONTAINS_EXPR_MANDATORY ");
				}
break;
case 27:
//#line 163 "KaiserParser.y"
{
				printDebugMessage(" CONTAINS_EXPR_LOGICAL_UNIT ");
				}
break;
case 28:
//#line 166 "KaiserParser.y"
{
 				yyval = new KaiserParserVal(KaiserParserFactory.getContainsLogicalBPC((ParserBPC)val_peek(2).obj, (ParserBPC) val_peek(0).obj, val_peek(1).sval));
				printDebugMessage(" CONTAINS_EXPR_LOGICAL_UNIT &  ");
 				}
break;
case 29:
//#line 170 "KaiserParser.y"
{
 				yyval = new KaiserParserVal((ParserBPC)val_peek(1).obj);
				printDebugMessage(" (CONTAINS_EXPR_LOGICAL_UNIT) ");
 				}
break;
case 30:
//#line 174 "KaiserParser.y"
{
 				yyval = new KaiserParserVal(KaiserParserFactory.getNotContainsBPC((ParserBPC)val_peek(0).obj));
				printDebugMessage(" !CONTAINS_EXPR_LOGICAL_UNIT ");
 				}
break;
case 31:
//#line 180 "KaiserParser.y"
{
				yyval = new KaiserParserVal(val_peek(1).obj);
				printDebugMessage(" CONTAINS_EXPR_OPTIONAL ");
				}
break;
case 32:
//#line 186 "KaiserParser.y"
{
				ParserBPC sequence = (ParserBPC) val_peek(0).obj;
				List<ParserBPC> expressions = new ArrayList<>();
				expressions.add(sequence);
				yyval = new KaiserParserVal(expressions);
				printDebugMessage(" CONTAINS_EXPR_LIST  - CONTAINS_PROPERTY ");
 				}
break;
case 33:
//#line 193 "KaiserParser.y"
{
 				ParserBPC sequence = (ParserBPC) val_peek(0).obj;
 				List<ParserBPC> expressions = (ArrayList) val_peek(1).obj;
 				expressions.add(sequence);
 				yyval = new KaiserParserVal(expressions);
 				printDebugMessage(" CONTAINS_EXPR_LIST  ");
 				}
break;
case 34:
//#line 202 "KaiserParser.y"
{
				printDebugMessage(" CONTAINS_PROPERTY ");
 				}
break;
case 35:
//#line 205 "KaiserParser.y"
{
 				printDebugMessage("SEQUENCE EXPRESSION AS CONTAINS_PROPERTY");
 				}
break;
case 36:
//#line 213 "KaiserParser.y"
{
					yyval = new KaiserParserVal(new SequenceExpressionBPC((ParserBPC) val_peek(1).obj));
					printDebugMessage(" SEQUENCE_EXPR - SEQUENCE[SEQUENCE_EXPR_CORE]");
				}
break;
case 37:
//#line 217 "KaiserParser.y"
{
                    yyval = new KaiserParserVal(new SequenceExpressionBPC((ParserBPC) val_peek(1).obj));
                    printDebugMessage(" SEQUENCE_EXPR - SEQUENCE[CONTAINS_ENTITY_WITH_PROPS]");
                }
break;
case 38:
//#line 224 "KaiserParser.y"
{
                yyval = new KaiserParserVal(new SequenceExpressionCoreBPC((ParserBPC) val_peek(2).obj, val_peek(1).ival, (ParserBPC)val_peek(0).obj));
                printDebugMessage(" SEQUENCE_EXPR_CORE :: CONTAINS_ENTITY_WITH_PROPS -> CONTAINS_ENTITY_WITH_PROPS ");
                }
break;
case 39:
//#line 228 "KaiserParser.y"
{
                yyval = new KaiserParserVal(new SequenceExpressionCoreBPC((ParserBPC) val_peek(2).obj, val_peek(1).ival, (ParserBPC)val_peek(0).obj));
                printDebugMessage(" SEQUENCE_EXPR_CORE  :: SEQUENCE_EXPR_CORE -> CONTAINS_ENTITY_WITH_PROPS");
                }
break;
case 40:
//#line 234 "KaiserParser.y"
{
				yyval = new KaiserParserVal(1);
				printDebugMessage(" follows - 1 ");
				}
break;
case 41:
//#line 238 "KaiserParser.y"
{
				int offsetWindowSize = Integer.parseInt(val_peek(1).sval);
				yyval = new KaiserParserVal(offsetWindowSize);
				printDebugMessage(" follows - number ");

				}
break;
case 42:
//#line 247 "KaiserParser.y"
{
                ParserBPC containsEntity = (ParserBPC) val_peek(0).obj;
                containsEntity.setOccurrence(val_peek(1).sval);
                yyval = new KaiserParserVal(containsEntity);
                printDebugMessage(" CONTAINS_ENTITY_WITH_PROPS - optional ");
                }
break;
case 43:
//#line 253 "KaiserParser.y"
{
                printDebugMessage(" CONTAINS_ENTITY_WITH_PROPS");
                }
break;
case 44:
//#line 258 "KaiserParser.y"
{
				yyval = new KaiserParserVal(new ContainsEntityBPC(val_peek(1).sval, null, null));
				printDebugMessage(" CONTAINS_ENTITY " + val_peek(1).sval);
				}
break;
case 45:
//#line 262 "KaiserParser.y"
{
                List<MetaPropertyBPC> metaProperties = (ArrayList) val_peek(1).obj;
                yyval = new KaiserParserVal(new ContainsEntityBPC(val_peek(3).sval, null, metaProperties));
                printDebugMessage(" CONTAINS_ENTITY " + val_peek(3).sval + " with Meta Properties");
                }
break;
case 46:
//#line 267 "KaiserParser.y"
{
  				yyval = new KaiserParserVal(new ContainsEntityBPC(val_peek(3).sval, val_peek(1).sval, null));
  				printDebugMessage(" CONTAINS_ENTITY - with visible name " + val_peek(3).sval + "-" + val_peek(1).sval);
  				}
break;
case 47:
//#line 271 "KaiserParser.y"
{
                List<MetaPropertyBPC> metaProperties = (ArrayList) val_peek(1).obj;
                yyval = new KaiserParserVal(new ContainsEntityBPC(val_peek(5).sval, val_peek(3).sval, metaProperties));
                printDebugMessage(" CONTAINS_ENTITY " + val_peek(5).sval + " with Meta Properties");
                }
break;
case 48:
//#line 276 "KaiserParser.y"
{
                List<MetaPropertyBPC> metaProperties = (ArrayList) val_peek(1).obj;
                yyval = new KaiserParserVal(new ContainsEntityBPC(metaProperties));
                printDebugMessage(" CONTAINS_ENTITY with only Meta Properties");
                }
break;
case 49:
//#line 281 "KaiserParser.y"
{
                printDebugMessage(" CONTAINS_ENTITY - parser reference");
                }
break;
case 50:
//#line 286 "KaiserParser.y"
{
                List<MetaPropertyBPC> metaProperties = new ArrayList();
                metaProperties.add((MetaPropertyBPC) val_peek(0).obj);
                yyval = new KaiserParserVal(metaProperties);
                }
break;
case 51:
//#line 291 "KaiserParser.y"
{
                List<MetaPropertyBPC> metaProperties = (ArrayList) val_peek(1).obj;
                metaProperties.add((MetaPropertyBPC) val_peek(0).obj);
                yyval = new KaiserParserVal(metaProperties);
                }
break;
case 52:
//#line 298 "KaiserParser.y"
{
                yyval = new KaiserParserVal(new MetaPropertyBPC(val_peek(3).sval, val_peek(2).sval, val_peek(1).sval));
                printDebugMessage("META_PROPERTY - " + val_peek(3).sval + " " + val_peek(2).sval + " " + val_peek(1).sval);
                }
break;
case 53:
//#line 302 "KaiserParser.y"
{
                  yyval = new KaiserParserVal(new MetaPropertyBPC(val_peek(5).sval, val_peek(4).sval, val_peek(2).obj));
                  printDebugMessage("META_PROPERTY - " + val_peek(5).sval + " " + val_peek(4).sval + " " + val_peek(3).sval);
                  }
break;
case 54:
//#line 308 "KaiserParser.y"
{
                List<String> variables = new ArrayList();
                variables.add(val_peek(0).sval);
                yyval = new KaiserParserVal(variables);
                printDebugMessage("VARIABLE - " + val_peek(0).sval);
                }
break;
case 55:
//#line 314 "KaiserParser.y"
{
                List<String> variables = (ArrayList) val_peek(1).obj;
                variables.add(val_peek(0).sval);
                yyval = new KaiserParserVal(variables);
                printDebugMessage("VARIABLE LIST - " + variables.toString());
}
break;
case 56:
//#line 325 "KaiserParser.y"
{
				yyval = new KaiserParserVal(new ParserReferenceBPC(val_peek(1).sval, null, rulesRef));
				printDebugMessage("PARSER_REFERENCE - " + val_peek(1).sval);
				}
break;
case 57:
//#line 329 "KaiserParser.y"
{
  				yyval = new KaiserParserVal(new ParserReferenceBPC(val_peek(3).sval, val_peek(1).sval, rulesRef));
  				printDebugMessage("PARSER_REFERENCE - " + val_peek(3).sval);
  				}
break;
case 58:
//#line 335 "KaiserParser.y"
{
                yyval = new KaiserParserVal(new EvaluatorReferenceBPC(val_peek(1).sval, rulesRef));
                printDebugMessage("External Rule Referred-" + val_peek(1).sval);
                }
break;
//#line 1051 "KaiserParser.java"
//########## END OF USER-SUPPLIED ACTIONS ##########
    }//switch
    //#### Now let's reduce... ####
    if (yydebug) debug("reduce");
    state_drop(yym);             //we just reduced yylen states
    yystate = state_peek(0);     //get new state
    val_drop(yym);               //corresponding value drop
    yym = yylhs[yyn];            //select next TERMINAL(on lhs)
    if (yystate == 0 && yym == 0)//done? 'rest' state and at first TERMINAL
      {
      if (yydebug) debug("After reduction, shifting from state 0 to state "+YYFINAL+"");
      yystate = YYFINAL;         //explicitly say we're done
      state_push(YYFINAL);       //and save it
      val_push(yyval);           //also save the semantic value of parsing
      if (yychar < 0)            //we want another character?
        {
        yychar = yylex();        //get next character
        if (yychar<0) yychar=0;  //clean, if necessary
        if (yydebug)
          yylexdebug(yystate,yychar);
        }
      if (yychar == 0)          //Good exit (if lex returns 0 ;-)
         break;                 //quit the loop--all DONE
      }//if yystate
    else                        //else not done yet
      {                         //get next state and push, for next yydefred[]
      yyn = yygindex[yym];      //find out where to go
      if ((yyn != 0) && (yyn += yystate) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yystate)
        yystate = yytable[yyn]; //get new state
      else
        yystate = yydgoto[yym]; //else go to new defred
      if (yydebug) debug("after reduction, shifting from state "+state_peek(0)+" to state "+yystate+"");
      state_push(yystate);     //going again, so push state & val...
      val_push(yyval);         //for next action
      }
    }//main loop
  return 0;//yyaccept!!
}
//## end of method parse() ######################################



//## run() --- for Thread #######################################
/**
 * A default run method, used for operating this parser
 * object in the background.  It is intended for extending Thread
 * or implementing Runnable.  Turn off with -Jnorun .
 */
public void run() throws InvalidExpressionException
{
  yyparse();
}
//## end of method run() ########################################



//## Constructors ###############################################
/**
 * Default constructor.  Turn off with -Jnoconstruct .

 */
public KaiserParser()
{
  //nothing to do
}


/**
 * Create a parser, setting the debug to true or false.
 * @param debugMe true for debugging, false for no debug.
 */
public KaiserParser(boolean debugMe)
{
  yydebug=debugMe;
}
//###############################################################



}
//################### END OF CLASS ##############################
