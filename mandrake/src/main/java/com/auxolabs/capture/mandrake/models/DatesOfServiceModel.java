package com.auxolabs.capture.mandrake.models;

import lombok.Data;

@Data
public class DatesOfServiceModel {
    private String startDate;
    private String endDate;
    private String option;

    private Boolean isAllDates;

    public DatesOfServiceModel() {
    }

    public DatesOfServiceModel(String startDate, String endDate, String option) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.option = option;
    }

    public DatesOfServiceModel(String startDate, String endDate, Boolean isAllDates) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.isAllDates = isAllDates;
    }
}
