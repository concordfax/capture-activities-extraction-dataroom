package com.auxolabs.capture.mandrake.models;

import lombok.Data;

import java.io.Serializable;

@Data
public class KeyPhraseModel implements Serializable {
    private String phraseId;
    private String lexiconId;
    private String phrase;
    private String userId;

    public KeyPhraseModel() {
    }

    public KeyPhraseModel(String phraseId) {
        this.phraseId = phraseId;
    }

    public KeyPhraseModel(String lexiconId, String phrase, String userId) {
        this.lexiconId = lexiconId;
        this.phrase = phrase;
        this.userId = userId;
    }
}
