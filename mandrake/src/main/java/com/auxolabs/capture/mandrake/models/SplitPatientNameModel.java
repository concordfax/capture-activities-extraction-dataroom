package com.auxolabs.capture.mandrake.models;

import lombok.Data;

@Data
public class SplitPatientNameModel {
    private String firstName;
    private String secondName;
    private String middleName;
}
