package com.auxolabs.capture.mandrake.models;

import lombok.Data;

import java.io.Serializable;

@Data
public class ResponseModel<T> implements Serializable {

    private String status;
    private String message = "";
    private T payload;

    public ResponseModel() {
    }
}
