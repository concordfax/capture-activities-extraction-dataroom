package com.auxolabs.capture.mandrake.models;

import lombok.Data;

@Data
public class CredentialsModel {
    private String emailId;
    private String password;
}
