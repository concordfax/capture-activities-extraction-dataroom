package com.auxolabs.capture.mandrake.models;

import com.kaiser.models.BaseParserComponent;
import com.kaiser.models.ResultAnnotation;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

/**
 * Result of each Morphology
 */
public class RuleResultModel {
    private String ruleName;
    @Setter
    @Getter
    private int tagStartIndex = -1;
    @Setter
    @Getter
    private int tagEndIndex = -1;
    @Setter
    @Getter
    private int valueStartIndex = -1;
    @Setter
    @Getter
    private int valueEndIndex = -1;
    @Getter
    @Setter
    private int sentIndex = -1;
    private BaseParserComponent.Result result;
    private Set<ResultAnnotation> annotations;

    private String uniqueName;

    public RuleResultModel(Set<ResultAnnotation> annotations, BaseParserComponent.Result result, String ruleName, String uniqueName) {
        this.annotations = annotations;
        this.result = result;
        this.ruleName = ruleName;
        this.uniqueName = uniqueName;
    }

    public RuleResultModel(Set<ResultAnnotation> annotations, BaseParserComponent.Result result, String ruleName) {
        this.annotations = annotations;
        this.result = result;
        this.ruleName = ruleName;
    }

    public Set<ResultAnnotation> getAnnotations() {
        return annotations;
    }

    public void setAnnotations(Set<ResultAnnotation> annotations) {
        this.annotations = annotations;
    }

    public BaseParserComponent.Result getResult() {
        return result;
    }

    public void setResult(BaseParserComponent.Result result) {
        this.result = result;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }
}
