package com.auxolabs.capture.mandrake.constants;

import lombok.Data;

@Data
public class MandrakeConstants {
    public static class Rules {
        public static final String patientNameRule = "Patient Name Identifier";
        public static final String patientDOBRule = "Patient DOB Identifier";
        public static final String ssnRule = "SSN Identifier";
        public static final String addressRule = "Address Identifier";
        public static final String requestorAddressRule = "Temp Address Rule";
        public static final String datesOfServiceRule = "Temp Date of Service";
        //        public static final String reservedDatesOfServiceRule = "DOS parser";
        public static final String patientEmailRule = "Patient Email Identifier";
        public static final String patientPhoneNumberRule = "Patient Phone Identifier";
        public static final String patientDaytimePhoneNumberRule = "Patient Daytime Phone Identifier";
        public static final String patientPHNRule = "Patient PHN Identifier";
        public static final String splitPatientNameRule = "Patient name v3";
        public static final String mrnIdentifierRule = "MRN Identifier";
        public static final String dateOfEncounterRule = "Encounter Date Rule";
        public static final String extendedPatientNameRule = "Ex Patient Name Identifier";
        public static final String groupNumberRule = "Group Identifier";
        public static final String memberNumberRule = "Member Number Identifier";
        public static final String physicianIndetifierRule = "Physician Identifier";
    }

    public static class ApiEndpoints {
        public static final String LOGIN_URL = "/user/signin";
        public static final String PROCESS_URL_PREFIX = "/result/";
    }

    /**
     * If you create Lexicon as Regex in Mandrake it will come as the original name of the Lexicon
     */
    public static class Lexicons {

        public static final String tagPatientName = "TAG_NAME";
        public static final String tagPatientDOB = "TAG_DOB";
        public static final String patientName = "PersonName";
        public static final String negationPOS = "Negation POS";
        public static final String dob = "DOB";

        public static class PhysicianInformation {
            public static final String Tag_Physician_Information = "Tag Physician Information";
        }

        public static class Mrn {
            public static final String Mrn_Tags = "MRN Tags";
            public static final String Mrn_Values = "MRN Values";
        }

        public static class SplitPatientName {
            public static final String Patient_First_Name = "Patient_First_Name";
            public static final String Patient_Second_Name = "Patient_Second_name";
            public static final String Patient_Middle_Name = "Patient_Middle_Name";
            public static final String Patient_v2_Tag = "Patient_v2_Tag";
        }

        public static class DateOfBirth {
            public static final String day = "Day";
            public static final String month = "Month";
            public static final String year = "Year";
            public static final String directDate = "Direct";
        }

        public static class SSN {
            public static final String FourLetterSSN = "Four Letter SSN";
            public static final String NineLetterSSN = "Nine Letter SSN";
            public static final String ssn = "SSN";
            public static final String tagSsn = "TAG_SSN";
        }

        public static class PhoneNumber {
            public static final String Number = "Phone Number";
            public static final String TagDaytimePhone = "Tag Daytime Phone";
            public static final String TagPatientCellPhone = "Tag Patient Phone";
        }

        public static class PHN {
            public static final String PatientPHN = "Patient PHN";
            public static final String TagPatientPHN = "Tag Patient PHN";
        }

        public static class Email {
            public static final String Email = "Email";
            public static final String TagPatientEmail = "TAG_PATIENT_EMAIL";
        }

        public static class Address {
            public static final String locations = "Locations";
            public static final String stateAbbr = "US State Abbreviations";
            public static final String zipCode = "Zip Code";
            public static final String poBox = "Post Box";
            public static final String tagAddress = "TAG_ADDRESS";
        }

        public static class DatesOfService {
            public static final String tagDatesOfService = "TAG_DATES_OF_SERVICE";
            public static final String specialDates = "SPECIAL_DATES";
            public static final String specialStartDates = "SPECIAL_START_DATE";
            public static final String dateSeparator = "DATE_SEPARATOR";
            public static final String date = "DATE";
            public static final String dateRegex = "Date_regex";
            public static final String dateText = "DATE_TEXT";
            public static final String day = "Day";
            public static final String month = "Month";
            public static final String year = "Year";
        }

        public static class POS {
            public static final String NOUN = "POS TAG::NN";
            public static final String NOUN_PLURAL = "POS TAG::NNP";
            public static final String NOUN_SINGULAR = "POS TAG::NNS";
        }

        public static class DateOfEncounter {
            public static final String encounterDate = "Encounter Date";
            public static final String encounterTag = "Encounter Tag";
            public static final String encounterSpecialDateTag = "Encounter Special Date Tag";
            public static final String time = "Time";
            public static final String timeSeperator = "Time Seperator";
            public static final String physiotherapyTitle = "Physiotherapy Title";
        }

        public static class GroupNumber {
            public static final String groupTag = "Group Tag";
            public static final String groupNumber = "Group Value";
        }

        public static class MemberNumber {
            public static final String memberTag = "Member tag";
            public static final String memberNumber = "Member Value";
        }

    }
}
