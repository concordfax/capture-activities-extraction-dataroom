package com.auxolabs.capture.mandrake.config;

import com.auxolabs.capture.mandrake.models.CredentialsModel;
import lombok.Data;

@Data
public class MandrakeConfigModel {
    private String endPoint;
    private String userId;
    private CredentialsModel login;
    private String healthCheckEndPoint;
}
