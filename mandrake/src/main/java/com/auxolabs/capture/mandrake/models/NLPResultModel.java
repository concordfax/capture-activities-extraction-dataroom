package com.auxolabs.capture.mandrake.models;

import com.kaiser.models.ResultAnnotation;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
public class NLPResultModel {
    private List<String> sentences;
    private Set<ResultAnnotation> annotations;
    private List<RuleResultModel> ruleResults;

    public NLPResultModel() {
        sentences = new ArrayList<>();
        annotations = new HashSet<>();
        ruleResults = new ArrayList<>();
    }

    public void addSentence(String sentence) {
        this.sentences.add(sentence);
    }

    public void addAnnotations(Set<ResultAnnotation> annotations) {
        this.annotations.addAll(annotations);
    }

    public void addRuleResult(RuleResultModel ruleResultModel) {
        this.ruleResults.add(ruleResultModel);
    }

    public void addRuleResults(List<RuleResultModel> ruleResultModel) {
        this.ruleResults.addAll(ruleResultModel);
    }
}

