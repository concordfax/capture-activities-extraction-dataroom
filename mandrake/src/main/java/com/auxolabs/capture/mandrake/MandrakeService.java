package com.auxolabs.capture.mandrake;

import com.auxolabs.capture.mandrake.models.RuleParserModel;
import com.concordfax.capture.core.models.extraction.mandrake.trigger.MandrakeTriggerConfiguration;
import com.kaiser.models.ParserValue;

import java.util.List;

public interface MandrakeService {

    // TODO: 2018-12-13 remove ParserValue Object to match positional
    List<RuleParserModel> process(MandrakeTriggerConfiguration triggerConfiguration, ParserValue parserValue);
}
