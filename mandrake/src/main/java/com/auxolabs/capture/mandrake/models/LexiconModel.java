package com.auxolabs.capture.mandrake.models;

import lombok.Data;

import java.io.Serializable;

@Data
public class LexiconModel implements Serializable {
    private String lexiconId;
    private String name;
    private String parentId;
    private String userId;
    private String path;

    public LexiconModel() {
    }

    public LexiconModel(String lexiconId) {
        this.lexiconId = lexiconId;
    }

    public LexiconModel(String name, String parentId, String userId) {
        this.name = name;
        this.parentId = parentId;
        this.userId = userId;
    }
}
