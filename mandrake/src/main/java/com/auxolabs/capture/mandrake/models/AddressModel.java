package com.auxolabs.capture.mandrake.models;

import lombok.Data;

@Data
public class AddressModel {
    private String poBox;
    private String zipCode;
    private String locations;
    private String stateAbbr;
}
