package com.auxolabs.capture.mandrake.application;

import com.auxolabs.capture.mandrake.MandrakeService;
import com.auxolabs.capture.mandrake.models.RuleParserModel;
import com.auxolabs.capture.mandrake.utils.RuleProcessorFactory;
import com.concordfax.capture.core.models.extraction.mandrake.trigger.MandrakeTriggerConfiguration;
import com.kaiser.models.ParserValue;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class MandrakeServiceImpl implements MandrakeService {

    @Inject
    public MandrakeServiceImpl() {
    }

    @Override
    public List<RuleParserModel> process(MandrakeTriggerConfiguration triggerConfiguration, ParserValue parserValue) {
        List<RuleParserModel> ruleParserModels = new ArrayList<>();
        triggerConfiguration.getRules().forEach(ruleModel -> {
            RuleParserModel ruleParserModel = new RuleParserModel();
            ruleParserModel.setRuleModel(ruleModel);
            ruleParserModels.add(ruleParserModel);
        });

        RuleProcessorFactory ruleProcessorFactory = new RuleProcessorFactory(ruleParserModels);

        // TODO: 2018-12-14 optimise to not generate AST for each sentence
        ruleParserModels.forEach(ruleParserModel -> {
            ruleProcessorFactory.generateAST(ruleParserModel);
            ruleParserModel.processRule(parserValue);
        });

        return ruleParserModels;
    }


}
