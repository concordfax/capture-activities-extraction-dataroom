package com.auxolabs.capture.mandrake.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class ProcessRequestModel {
    private String text;
    @JsonIgnore
    private transient String userId;

    public ProcessRequestModel() {
    }

    public ProcessRequestModel(String text) {
        this.text = text;
    }
}
