package com.auxolabs.capture.mandrake.models;

import com.concordfax.capture.core.models.extraction.mandrake.trigger.RuleModel;
import com.kaiser.exceptions.UnsupportedOpException;
import com.kaiser.models.*;
import lombok.Data;

import java.util.*;

@Data
public class RuleParserModel {
    private RuleModel ruleModel;
    private BaseParserComponent bpc;
    private List<RuleResultModel> results;

    private static String delim =":";

    public RuleParserModel(){
        this.results = new ArrayList<>();
    }

    public void processRule(ParserValue pv){
        try {
            ResultModel resultModel = bpc.compute(pv);
            if(resultModel.getResult() == BaseParserComponent.Result.YES) {
                if(resultModel.getSequenceResults().size() <= 0) {
                    ParserValue matchingComponent = new ParserValue();
                    matchingComponent.put("Status", new ValueModel("Validation",
                            getTriggerPhrasesFromPV(matchingComponent)));
                    RuleResultModel ruleResult = new RuleResultModel(new HashSet<>(),
                            resultModel.getResult(), ruleModel.getRuleName(), ruleModel.getRuleName());
                    results.add(ruleResult);
                } else {
                    for (SequenceResultModel sequenceResult: resultModel.getSequenceResults()) {
                        ParserValue matchingComponent = sequenceResult.getMatchingComponent();
                        matchingComponent.put("Status", new ValueModel("Validation",
                                sequenceResult.getResultAnnotations()));
                        String uniqueName = ruleModel.getRuleName() +  delim +resultModel.getResult().name() + delim +
                                sequenceResult.getFirstMatchingOffset() + delim +sequenceResult.getLastMatchingOffset();
                        RuleResultModel ruleResult = new RuleResultModel(sequenceResult.getResultAnnotations(),
                                resultModel.getResult(), ruleModel.getRuleName(), uniqueName);
                        results.add(ruleResult);
                    }
                }
            }
        } catch (UnsupportedOpException e) {
            throw new RuntimeException(e);
        }
    }

    private Set<ResultAnnotation> getTriggerPhrasesFromPV(ParserValue pv) {
        Set<ResultAnnotation> triggerPhrases = new HashSet<>();

        for(Map.Entry<String, Object> entry: pv.entrySet()) {
            Object value = entry.getValue();
            if(value instanceof ValueModel) {
                triggerPhrases.addAll(((ValueModel) value).getTriggerPhrases());
            }
        }

        return triggerPhrases;
    }
}
