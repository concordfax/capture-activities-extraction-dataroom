package com.auxolabs.capture.mandrake.models;

import lombok.Data;

import java.util.List;

@Data
public class MandrakeResultModel {
    private String preprocessedText;

    private String name;
    private String dob;
    private String ssn;
    private DatesOfServiceModel datesOfService;
    private String patientMRN;
    private String dateOfEncounter;
    private String groupNumber;
    private String memberNumber;

    private String extendedPatientName;
    private String extendedPatientDOB;

    private SplitPatientNameModel splitPatientName;

    private List<AddressModel> address;
    private AddressModel requestorAddress;
    private DatesOfServiceModel reservedDOS;
    private String patientEmail;
    private String patientPhoneNumber;
    private String patientDaytimePhoneNumber;
    private String patientPHN;
    private String providerName;

    private String physicianInformation;
}
