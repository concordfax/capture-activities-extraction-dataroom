package com.auxolabs.capture.mandrake.utils;

import com.auxolabs.capture.mandrake.models.RuleParserModel;
import com.kaiser.build.KaiserParser;
import com.kaiser.exceptions.InvalidExpressionException;
import com.kaiser.models.BaseExpressionBPC;
import lombok.Getter;

import java.util.HashMap;
import java.util.List;

/**
 * Creates a Rule Processor from the expression
 */
public class RuleProcessorFactory {

    @Getter
    private HashMap<String, RuleParserModel> rules;

    public RuleProcessorFactory(List<RuleParserModel> rules){
        this.rules = new HashMap<>();
        rules.forEach(ruleModel -> {
            this.rules.put(ruleModel.getRuleModel().getRuleName(), ruleModel);
        });
    }

    public void generateAST(RuleParserModel ruleParserModel) {
        KaiserParser parser = new KaiserParser(this);
        BaseExpressionBPC bpc = null;
        try {
            bpc = (BaseExpressionBPC) parser.parseRule(ruleParserModel.getRuleModel().getRule());
        } catch (InvalidExpressionException e) {
            throw new RuntimeException(e);
        }
        if(bpc == null) {
            throw new RuntimeException("invalid rule");
        }
        ruleParserModel.setBpc(bpc.getAST());
    }
}
