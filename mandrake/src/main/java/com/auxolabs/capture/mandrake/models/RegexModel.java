package com.auxolabs.capture.mandrake.models;

public class RegexModel {
    private String regex;
    private String regexId;
    private String userId;
    private String lexiconId;

    public RegexModel() {
    }

    public RegexModel(String regexId) {
        this.regexId = regexId;
    }

    public String getRegex() {
        return regex;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }

    public String getRegexId() {
        return regexId;
    }

    public void setRegexId(String regexId) {
        this.regexId = regexId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLexiconId() {
        return lexiconId;
    }

    public void setLexiconId(String lexiconId) {
        this.lexiconId = lexiconId;
    }
}
