package com.auxolabs.capture.mandrake.helpers;

import com.auxolabs.capture.mandrake.models.KeyPhraseModel;
import com.auxolabs.capture.mandrake.models.LexiconModel;
import com.auxolabs.capture.mandrake.models.NLPResultModel;
import com.auxolabs.capture.mandrake.models.ResponseModel;
import com.google.common.reflect.TypeToken;

import java.lang.reflect.Type;

public class GsonTypeModels {
    public static final Type lexRespModel = new TypeToken<ResponseModel<LexiconModel>>() {
    }.getType();
    public static final Type keyPhraseRespModel = new TypeToken<ResponseModel<KeyPhraseModel>>() {
    }.getType();
    public static final Type nlpResultRespModel = new TypeToken<ResponseModel<NLPResultModel>>() {
    }.getType();
}
