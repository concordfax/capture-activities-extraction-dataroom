package com.auxolabs.capture.annotator.models.request;

import com.concordfax.capture.core.models.extraction.annotators.Filter;
import com.concordfax.capture.core.models.extraction.annotators.Query;
import com.concordfax.capture.core.models.extraction.annotators.Sort;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AnnotatorRequestModel {
    private Filter filter;
    private Query query;
    private Sort sort;

    public AnnotatorRequestModel(Filter filter, Query query, Sort sort) {
        this.filter = filter;
        this.query = query;
        this.sort = sort;
    }
}
