package com.auxolabs.capture.annotator.models.response;

import com.auxolabs.capture.annotator.models.Annotation;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SentenceModel {
    private String queryString;
    private List<Annotation> annotations;

    public SentenceModel() {
    }

    public SentenceModel(String queryString, List<Annotation> annotations) {
        this.queryString = queryString;
        this.annotations = annotations;
    }
}
