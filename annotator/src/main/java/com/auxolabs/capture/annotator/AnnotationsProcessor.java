package com.auxolabs.capture.annotator;

import com.auxolabs.capture.annotator.client.AnnotatorServicesClient;
import com.auxolabs.capture.annotator.models.request.AnnotatorRequestModel;
import com.auxolabs.capture.annotator.models.response.SentenceModel;
import com.concordfax.capture.core.models.extraction.annotators.AnnotatorType;
import com.concordfax.capture.core.models.extraction.annotators.ExtractionAnnotatorConfiguration;
import com.concordfax.capture.core.models.extraction.annotators.Query;

import javax.inject.Inject;
import java.util.*;

public class AnnotationsProcessor implements AnnotatorService{
    private AnnotatorServicesClient annotatorServicesClient;

    @Inject
    public AnnotationsProcessor() {
        annotatorServicesClient = new AnnotatorServicesClient();
    }

    public Map<String,List<SentenceModel>> getAnnotatorToAnnotations(List<String> textList,ExtractionAnnotatorConfiguration annotatorConfiguration){
        Map<String,List<SentenceModel>> annotatorNameToAnnotations = new HashMap<>();
        List<AnnotatorType> annotatorsList = annotatorConfiguration.getAnnotatorTypes();
        if (annotatorsList!=null) {
            List<String> strings = new ArrayList<>();
            textList.forEach(s -> {
                strings.add(s.replaceAll("[^\\x00-\\x7f]", " "));
            });
            for (AnnotatorType annotatorType : annotatorsList) {
                AnnotatorRequestModel annotatorRequestModel = new AnnotatorRequestModel(annotatorType.getFilter(),
                        new Query(strings), null);
                List<SentenceModel> sentenceModels = annotatorServicesClient.getAnnotations(annotatorType.getUri(), annotatorRequestModel);
                annotatorNameToAnnotations.put(annotatorType.getAnnotatorName(), sentenceModels);
            }
        }
        List<SentenceModel> lexerSentenceModel = annotatorNameToAnnotations.get("lexer");
        List<SentenceModel> regexSentenceModel = annotatorNameToAnnotations.get("regex");
        for (int i =0; i<regexSentenceModel.size();i++){
            lexerSentenceModel.get(i).getAnnotations().addAll(regexSentenceModel.get(i).getAnnotations());
        }
        annotatorNameToAnnotations.remove("regex");
        return annotatorNameToAnnotations;
    }
}
