package com.auxolabs.capture.annotator.models;

import lombok.Data;

@Data
public class Annotation {
    private String word;
    private String answer;
    private TokenPosition tokenPosition;
    private Integer startTokenIndex;
    private Integer endTokenIndex;
    private String matchedPhrase;
}
