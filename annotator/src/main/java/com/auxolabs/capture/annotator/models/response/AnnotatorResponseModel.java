package com.auxolabs.capture.annotator.models.response;

import lombok.Data;

import java.util.List;

@Data
public class AnnotatorResponseModel {
    private List<SentenceModel> result;

    public AnnotatorResponseModel() {
    }

    public AnnotatorResponseModel(List<SentenceModel> result) {
        this.result = result;
    }
}
