package com.auxolabs.capture.annotator.models;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TokenPosition {
    private Integer startPosition;
    private Integer endPosition;

    public TokenPosition(Integer startPosition, Integer endPosition) {
        this.startPosition = startPosition;
        this.endPosition = endPosition;
    }

    @Override
    public String toString() {
        return "(" + startPosition + ", " + endPosition + ")";
    }
}
