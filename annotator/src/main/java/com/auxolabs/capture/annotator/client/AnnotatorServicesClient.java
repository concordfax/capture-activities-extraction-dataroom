package com.auxolabs.capture.annotator.client;

import com.auxolabs.capture.annotator.models.response.AnnotatorResponseModel;
import com.auxolabs.capture.annotator.models.request.AnnotatorRequestModel;
import com.auxolabs.capture.annotator.models.response.SentenceModel;
import com.google.gson.Gson;
import com.microsoft.applicationinsights.core.dependencies.http.HttpEntity;
import com.microsoft.applicationinsights.core.dependencies.http.client.methods.CloseableHttpResponse;
import com.microsoft.applicationinsights.core.dependencies.http.client.methods.HttpPost;
import com.microsoft.applicationinsights.core.dependencies.http.entity.StringEntity;
import com.microsoft.applicationinsights.core.dependencies.http.impl.client.CloseableHttpClient;
import com.microsoft.applicationinsights.core.dependencies.http.impl.client.HttpClients;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class AnnotatorServicesClient {
    private CloseableHttpClient httpClient;
    private Gson gson;

    public AnnotatorServicesClient() {
        this.gson = new Gson();
        this.httpClient = HttpClients.createDefault();
    }

    public List<SentenceModel> getAnnotations(String endpoint, AnnotatorRequestModel requestModel){
        HttpPost httpPost = null;
        CloseableHttpResponse response = null;
        try {
            httpPost = new HttpPost(endpoint);
            httpPost.setEntity(new StringEntity(this.gson.toJson(requestModel)));
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            response = this.httpClient.execute(httpPost);
            HttpEntity responseEntity = response.getEntity();
            AnnotatorResponseModel responseModel = this.gson.fromJson(new InputStreamReader(responseEntity.getContent()), AnnotatorResponseModel.class);
            return responseModel.getResult();
        }catch (Exception ex) {
            throw new RuntimeException(ex);
        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (httpPost != null) {
                httpPost.releaseConnection();
            }
        }
    }
}
