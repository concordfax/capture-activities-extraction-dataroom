package com.auxolabs.capture.annotator;

import com.auxolabs.capture.annotator.models.response.SentenceModel;
import com.concordfax.capture.core.models.extraction.annotators.ExtractionAnnotatorConfiguration;

import java.util.List;
import java.util.Map;

public interface AnnotatorService {
    Map<String, List<SentenceModel>> getAnnotatorToAnnotations(List<String> textList, ExtractionAnnotatorConfiguration annotatorConfiguration);
}
