FROM openjdk:8u212-jdk as build

ARG AZURE_STORAGE_ACCOUNT
ARG AZURE_STORAGE_ACCESS_KEY

ENV PROJECT_NAME "extraction"
ENV PROJECT_VER "1.0-SNAPSHOT"

ENV PROJECT_COMPILED_DIR "/usr/local/$PROJECT_NAME"

RUN apt-get update && \
    apt-get install -y lsb-release && \
    AZ_REPO=$(lsb_release -cs) && \
    echo "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ $AZ_REPO main" | \
    tee /etc/apt/sources.list.d/azure-cli.list && \
    curl -L https://packages.microsoft.com/keys/microsoft.asc | apt-key add - && \
    apt-get install -y apt-transport-https && \
    apt-get update && \
    apt-get install -y azure-cli;

RUN mkdir -p "/root/.m2"
RUN az storage blob download --container-name releases --name "extraction/settings.xml" --file "settings.xml" --output table --account-name $AZURE_STORAGE_ACCOUNT --account-key $AZURE_STORAGE_ACCESS_KEY
RUN cp settings.xml "/root/.m2/";

# building the core extraction app
WORKDIR "/root"
RUN az storage blob download --container-name releases --name "extraction/apache-maven-3.5.4-bin.zip" --file "apache-maven-3.5.4-bin.zip" --output table --account-name $AZURE_STORAGE_ACCOUNT --account-key $AZURE_STORAGE_ACCESS_KEY && \
    unzip "apache-maven-3.5.4-bin.zip";

COPY "./" "/root/extraction"

WORKDIR "/root/extraction"
RUN "/root/apache-maven-3.5.4/bin/mvn" package;
RUN  mkdir -p $PROJECT_COMPILED_DIR && \
    cp "app/target/app-$PROJECT_VER.jar" "$PROJECT_COMPILED_DIR/$PROJECT_NAME.jar" && \
    cp "dev.config.yaml" "$PROJECT_COMPILED_DIR/config.yaml";

WORKDIR "/home"
RUN az storage blob download --container-name releases --name "extraction/jre-8u202-linux-x64.tar.gz" --file "jre-8u202-linux-x64.tar.gz" --output table --account-name $AZURE_STORAGE_ACCOUNT --account-key $AZURE_STORAGE_ACCESS_KEY;

FROM phusion/baseimage:0.11 as runtime

ENV PROJECT_NAME "extraction"
ENV PROJECT_COMPILED_DIR "/usr/local/$PROJECT_NAME"

EXPOSE 80
CMD ["/sbin/my_init"]

ENV JAVA_HOME "/usr/java/jre1.8.0_202"

WORKDIR /usr/java
COPY --from=build "/home/jre-8u202-linux-x64.tar.gz" ./
RUN tar xvzf "jre-8u202-linux-x64.tar.gz" && \
    rm -rf "jre-8u202-linux-x64.tar.gz";

ENV PATH "$JAVA_HOME/bin:$PATH"

RUN mkdir -p logs

COPY --from=build $PROJECT_COMPILED_DIR $PROJECT_COMPILED_DIR

RUN mkdir /etc/service/extraction
ADD extraction_service.sh /etc/service/extraction/run
RUN chmod +x /etc/service/extraction/run

RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*;
RUN sed -i 's/\r//g' /etc/service/extraction/run
